#!/bin/bash

CUR_DIR=$( pwd )
cd "$( dirname "${BASH_SOURCE[0]}" )"

rm -rf build && mkdir build
cd build
cmake ..
make
cd "$CUR_DIR"

