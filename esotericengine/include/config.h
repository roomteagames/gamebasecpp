#ifndef ESO_ENGINE_CONFIG_STARTUP_H_
#define ESO_ENGINE_CONFIG_STARTUP_H_

#include <functional>

#include <SFML/Window.hpp>

#include "constants.h"
#include "util/showcase.tpp"

namespace eso {

//IMPORTANT: Everything in the game config is constant once the game is instantiated!
//This is because these config objects are used to create the game and may not necessarily be tied to how to game works after startup.
//These convenient props are for the user to define startup properties if desired.
//If something can change during gameplay, like 

//TODO: put general game startup properties here
struct GameProps {
    std::string Title = "My App";
    unsigned short UpdateFramerate = 100;
    unsigned short InputFramerate = 100;
    unsigned short RenderFramerate = 60;
    unsigned short NumStates = 4;
    int argc;
    char** argv;
};

//TODO: put sfml graphics/window-related props here
struct GraphicsProps {
    unsigned short WidthPixels = 1280;
    unsigned short HeightPixels = 720;
    sf::Uint32 Style = sf::Style::Default;
};

//TODO: put resource-related properties here
struct AssetProps {
    std::string ResourceDirectory = "resources/";
    std::string ImageDirectory = ResourceDirectory + "images/";
    std::string SoundDirectory = ResourceDirectory + "sounds/";
    std::string MusicDirectory = ResourceDirectory + "music/";
    std::string WorldDirectory = ResourceDirectory + "worlds/";
    std::string SpritesheetDirectory = ResourceDirectory + "spritesheets/";
    
    std::string ImageExtension = ".png";
    std::string SoundExtension = ".ogg";
    std::string MusicExtension = ".ogg";
    std::string WorldExtension = ".world";
    std::string SpritesheetExtension = ".sheet";

    std::string ShapeFilename = ResourceDirectory + "shapes";
    std::string MaterialFilename = ResourceDirectory + "materials";

    unsigned short TextureCapacity = 100;
    unsigned short SoundCapacity = 100;
    unsigned short MusicCapacity = 30;
};

//TODO: put world save/load settings, and entity measurements here
struct WorldProps {
    
    //TODO: using the EsoLoaderWriter.py, this may be deprecated
    unsigned short DataItemLength;    //TODO: this may not be necessary, as the Entity child is queried directly upon reading IDs
    unsigned short NumUniqueIdBits;
    unsigned short NumEntityTypeBits;
    unsigned short NumLocationBits;
    /* END DEPRECATED */

    unsigned short UnitsPerBlock = engine::BlockSize;  // 32x32 game units = 1 grid block
    unsigned short BlocksPerSector = engine::SectorSize; // 16x16 grid blocks = 1 grid sector
    unsigned short SectorsPerSquare = 2;  // 2x2 grid sectors = 1 map square

    //NOTE: value of -1 means feature is disabled
    short VisibleSectorRadius = 2; // num sectors AROUND focus' sector (radially) that are visible (-1 means ALWAYS rendered regardless of distance)
    short InvisibleSectorRadius = 2; // num sectors AROUND outer visible sector (radially) that are active but invisible (-1 means ALWAYS active regardless of distance)
    short InactiveSectorRadius = 2; // num sectors AROUND outer invisible sector (radially) that are inactive (-1 means ALWAYS always loaded (inc. req. assets) regardless of distance)
};

//TODO: put lifecycle hooks here
struct Game; /* forward delclaration to avoid circular dependencies */
struct LifecycleHooks {
    std::function<void(Game&)> BeforeOpen = nullptr;
    std::function<void(Game&)> AfterOpen = nullptr;
    std::function<void(Game&)> BeforeStateChange = nullptr;
    std::function<void(Game&)> AfterStateChange = nullptr;
    std::function<void(Game&)> BeforeClose = nullptr;
    std::function<void(Game&)> AfterClose = nullptr;
};

struct PropsCollection {
    GameProps Game;
    GraphicsProps Graphics;
    AssetProps Asset;
    WorldProps World;
    LifecycleHooks Hooks;
};

}

#endif /* ESO_ENGINE_CONFIG_STARTUP_H_ */