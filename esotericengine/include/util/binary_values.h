#ifndef ESO_ENGINE_UTIL_BIN_VALUES_H_
#define ESO_ENGINE_UTIL_BIN_VALUES_H_

namespace eso {

namespace bin {

//Quick binary values representing a row of something
constexpr unsigned long b1 =  1L;
constexpr unsigned long b2 =  1L << 1;
constexpr unsigned long b3 =  1L << 2;
constexpr unsigned long b4 =  1L << 3;
constexpr unsigned long b5 =  1L << 4;
constexpr unsigned long b6 =  1L << 5;
constexpr unsigned long b7 =  1L << 6;
constexpr unsigned long b8 =  1L << 7;
constexpr unsigned long b9 =  1L << 8;
constexpr unsigned long b10 = 1L << 9;
constexpr unsigned long b11 = 1L << 10;
constexpr unsigned long b12 = 1L << 11;
constexpr unsigned long b13 = 1L << 12;
constexpr unsigned long b14 = 1L << 13;
constexpr unsigned long b15 = 1L << 14;
constexpr unsigned long b16 = 1L << 15;
constexpr unsigned long b17 = 1L << 16;
constexpr unsigned long b18 = 1L << 17;
constexpr unsigned long b19 = 1L << 18;
constexpr unsigned long b20 = 1L << 19;
constexpr unsigned long b21 = 1L << 20;
constexpr unsigned long b22 = 1L << 21;
constexpr unsigned long b23 = 1L << 22;
constexpr unsigned long b24 = 1L << 23;
constexpr unsigned long b25 = 1L << 24;
constexpr unsigned long b26 = 1L << 25;
constexpr unsigned long b27 = 1L << 26;
constexpr unsigned long b28 = 1L << 27;
constexpr unsigned long b29 = 1L << 28;
constexpr unsigned long b30 = 1L << 29;
constexpr unsigned long b31 = 1L << 30;
constexpr unsigned long b32 = 1L << 31;
constexpr unsigned long b33 = 1L << 32;
constexpr unsigned long b34 = 1L << 33;
constexpr unsigned long b35 = 1L << 34;
constexpr unsigned long b36 = 1L << 35;
constexpr unsigned long b37 = 1L << 36;
constexpr unsigned long b38 = 1L << 37;
constexpr unsigned long b39 = 1L << 38;
constexpr unsigned long b40 = 1L << 39;
constexpr unsigned long b41 = 1L << 40;
constexpr unsigned long b42 = 1L << 41;
constexpr unsigned long b43 = 1L << 42;
constexpr unsigned long b44 = 1L << 43;
constexpr unsigned long b45 = 1L << 44;
constexpr unsigned long b46 = 1L << 45;
constexpr unsigned long b47 = 1L << 46;
constexpr unsigned long b48 = 1L << 47;
constexpr unsigned long b49 = 1L << 48;
constexpr unsigned long b50 = 1L << 49;
constexpr unsigned long b51 = 1L << 50;
constexpr unsigned long b52 = 1L << 51;
constexpr unsigned long b53 = 1L << 52;
constexpr unsigned long b54 = 1L << 53;
constexpr unsigned long b55 = 1L << 54;
constexpr unsigned long b56 = 1L << 55;
constexpr unsigned long b57 = 1L << 56;
constexpr unsigned long b58 = 1L << 57;
constexpr unsigned long b59 = 1L << 58;
constexpr unsigned long b60 = 1L << 59;
constexpr unsigned long b61 = 1L << 60;
constexpr unsigned long b62 = 1L << 61;
constexpr unsigned long b63 = 1L << 62;
constexpr unsigned long b64 = 1L << 63;

//Quick specific shortcuts
constexpr unsigned long bNone = 0;
constexpr unsigned long bAll32 = 0xffffffff;
constexpr unsigned long bAll64 = 0xffffffffffffffffL;
constexpr unsigned long bFirstTen = b1|b2|b3|b4|b5|b6|b7|b8|b9|b10;



}

}

#endif /* ESO_ENGINE_UTIL_BIN_VALUES_H_ */