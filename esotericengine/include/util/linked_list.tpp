#ifndef ESO_ENGINE_UTIL_LINKEDLIST_H_
#define ESO_ENGINE_UTIL_LINKEDLIST_H_

#include <iostream>
#include <string>
#include <memory>

namespace eso {

// A linked list implementation that moves unique pointers around to manage data. Pushing an element returns the unique_ptr to that element
// Useful for implementing esoteric store or any type of lifecycle management utility
// Repush allows re-insertion of Node ref to refresh its life to brand new, hence allowing unused items to be easily removed and recently used items to persist

template <typename T> class EsotericList;


/* Definition of EsotericListNode */

/* All member functions serve as getters, so they are all O(1) */
/* List stores a unique_ptr object directly in next_, pointing to the next Node. List stores a raw non-owning pointer to previous Node in previous_ */
/* Nodes are read-only in all contexts other than within the list. This prevents tampering with list data from external sources */
/* Nodes store the id of the list they belong to. Passing a Node into a Repush of the wrong list will NOT add the node to the list */

template <typename T>
class EsotericListNode {
friend EsotericList<T>;
private:
    unsigned short list_id_;
    T value_;
    EsotericListNode<T>* previous_; //raw, non-owning pointer
    std::unique_ptr<EsotericListNode<T>> next_;

public:
    template <typename... TArgs> EsotericListNode(unsigned short, EsotericListNode<T>* previous, std::unique_ptr<EsotericListNode<T>>&& next, TArgs&&...);
    template <typename... TArgs> EsotericListNode(unsigned short, std::unique_ptr<EsotericListNode<T>>&& next, TArgs&&...);
    template <typename... TArgs> EsotericListNode(unsigned short, TArgs&&...);
    EsotericListNode(unsigned short, T&&, EsotericListNode<T>* previous, std::unique_ptr<EsotericListNode<T>>&& next);
    EsotericListNode(unsigned short, T&&, std::unique_ptr<EsotericListNode<T>>&& next);
    EsotericListNode(unsigned short, T&&);
    
    T& Value() { return value_; }
    const T& Value() const { return value_; }
    bool HasPrevious() { return previous_ != nullptr; }
    bool HasNext() { return next_ != nullptr; }
    const EsotericListNode<T>* Next() const { return next_.get(); }
    const EsotericListNode<T>* Previous() const { return previous_; }

    std::string Key = "";   //used to store a key associated with a node
};

//Previous has to be an lvalue reference because it is a ref of pre-existing head_ or another next_ (as these are unique_ptrs)
//A) Next has to be an rvalue reference because it is a newly constructed or moved unique_ptr to another list node
template <typename T>
template <typename... TArgs>
EsotericListNode<T>::EsotericListNode(unsigned short list_id, EsotericListNode<T>* previous, std::unique_ptr<EsotericListNode<T>>&& next, TArgs&&... args):
        list_id_(list_id),
        previous_(previous),
        next_(std::move(next)),
        value_(std::forward<TArgs>(args)...) {}

//B) Same as A, but previous is intialized to point to null
template <typename T>
template <typename... TArgs>
EsotericListNode<T>::EsotericListNode(unsigned short list_id, std::unique_ptr<EsotericListNode<T>>&& next, TArgs&&... args):
        list_id_(list_id),
        previous_(nullptr),
        next_(std::move(next)),
        value_(std::forward<TArgs>(args)...) {}

//C) Same as A, but both previous and next is initialized to point to null
template <typename T>
template <typename... TArgs>
EsotericListNode<T>::EsotericListNode(unsigned short list_id, TArgs&&... args):
        list_id_(list_id),
        previous_(nullptr),
        next_(nullptr),
        value_(std::forward<TArgs>(args)...) {}

//D) Same as A, but allows for rvalue passing of the value itself without internal construction
template <typename T>
EsotericListNode<T>::EsotericListNode(unsigned short list_id, T&& value, EsotericListNode<T>* previous, std::unique_ptr<EsotericListNode<T>>&& next):
        list_id_(list_id),
        value_(std::forward<T>(value)),
        previous_(previous),
        next_(std::move(next)) {}

//E) Same as D, but previous is intialized to point to null
template <typename T>
EsotericListNode<T>::EsotericListNode(unsigned short list_id, T&& value, std::unique_ptr<EsotericListNode<T>>&& next):
        list_id_(list_id),
        value_(std::forward<T>(value)),
        previous_(nullptr),
        next_(std::move(next)) {}

//F) Same as D, but previous and next is initialized to point to null
template <typename T>
EsotericListNode<T>::EsotericListNode(unsigned short list_id, T&& value):
        list_id_(list_id),
        value_(std::forward<T>(value)),
        previous_(nullptr),
        next_(nullptr) {}


/* Definition of EsotericList */
/* All member functions have complexity O(1), except for Pop(unsigned int count) with O(n), where n = count. If count >= size_ complexity is O(1) */

template <typename T>
class EsotericList {
private:
    std::unique_ptr<EsotericListNode<T>> head_;
    EsotericListNode<T>* tail_ = nullptr; //raw, non-owning pointer
    unsigned int size_ = 0;
    unsigned short id_;
    
public:
    EsotericList(unsigned short id): id_(id) {}

    //modifiers
    EsotericListNode<T>& Push(T&&);
    template <typename... TArgs> EsotericListNode<T>& Emplace(TArgs&&...);
    bool Repush(EsotericListNode<T>&); //Moves pre-existing node to head_; returns true if a valid move was performed
    void Pop();
    void Pop(unsigned int);
    bool Remove(EsotericListNode<T>&); //Removes the given node from list; returns true if a valid removal was performed
    void Empty();
    
    //getters
    const T* Front();
    const T* Back();
    EsotericListNode<T>* Begin() { return head_.get(); }
    EsotericListNode<T>* End();
    
    //size
    bool IsEmpty() { return (head_ == nullptr); }
    unsigned int Size() { return size_; }
};

template <typename T>
const T* EsotericList<T>::Front() {
    if (Begin() != nullptr) {
        return &(Begin()->Value());
    }
    return nullptr;
}

template <typename T>
const T* EsotericList<T>::Back() {
    if (End() != nullptr) {
        return &(End()->Value());
    }
    return nullptr;
}

template <typename T>
EsotericListNode<T>* EsotericList<T>::End() {
    if (tail_ == nullptr) {
        return head_.get();   
    }
    //tail_ is a raw pointer, so need to get previous's next_ for std::unique_ptr&
    return tail_->previous_->next_.get();
}

template <typename T>
EsotericListNode<T>& EsotericList<T>::Push(T&& element) {
    if (size_ == 0) {
        head_ = std::make_unique<EsotericListNode<T>>(id_, std::forward<T>(element));
    } else {
        head_ = std::make_unique<EsotericListNode<T>>(id_, std::forward<T>(element), std::move(head_)); /* move old head to next of new head */
        Begin()->next_->previous_ = head_.get(); /* set the second element's previous to ptr to new head */
        if (size_ == 1) {
            tail_ = Begin()->next_.get();
        }
    }
    size_++;
    
    return *head_;
}

template <typename T>
template <typename... TArgs>
EsotericListNode<T>& EsotericList<T>::Emplace(TArgs&&... args) {
    if (size_ == 0) {
        head_ = std::make_unique<EsotericListNode<T>>(id_, std::forward<TArgs>(args)...);
    } else {
        head_ = std::make_unique<EsotericListNode<T>>(id_, std::move(head_), std::forward<TArgs>(args)...);
        Begin()->next_->previous_ = head_.get();
        if (size_ == 1) {
            tail_ = Begin()->next_.get();
        }
    }
    size_++;

    return *head_;
}

template <typename T>
bool EsotericList<T>::Repush(EsotericListNode<T>& node) {
    if (node.list_id_ == id_ && node.HasPrevious()) {   //if node IS head_, then no action is taken, as this function call is unnecessary
        std::unique_ptr<EsotericListNode<T>> node_ptr_temp = std::move(node.previous_->next_);  //used to allow node's next to overwrite previous's next
        if (node.HasNext()) {
            node.next_->previous_ = node.previous_;
            node.previous_->next_ = std::move(node.next_);
        } else {
            node.previous_->next_ = nullptr;
        }
        head_->previous_ = node_ptr_temp.get();
        node.next_ = std::move(head_);
        node.previous_ = nullptr;
        head_ = std::move(node_ptr_temp);

        return true;
    }
    return false;
}

template <typename T>
bool EsotericList<T>::Remove(EsotericListNode<T>& node) {
    if (node.list_id_ == id_) {
        if (node.HasPrevious()) {
            if (node.HasNext()) {
                node.next_->previous_ = node.previous_;
                node.previous_->next_ = std::move(node.next_); //Destroys node
            } else {
                node.previous_->next_ = nullptr; //Destroys node
            }
        } else {
            if (node.HasNext()) {
                node.next_->previous_ = nullptr;
                head_ = std::move(node.next_); //Destroys node
            } else {
                head_ = nullptr; //Destroys node
            }
        }
        return true;
    }
    return false;
}

template <typename T>
void EsotericList<T>::Pop() {
    if (size_ > 1) {
        EsotericListNode<T>* previous_tail = End()->previous_;
        if (previous_tail != nullptr) {
            tail_ = previous_tail;
            tail_->next_ = nullptr; //this SHOULD call dtor of former tail to be called
        }
        size_--;   
    } else if (size_ == 1) {
        head_ = nullptr;
    }
}

template <typename T>
void EsotericList<T>::Pop(unsigned int count) {
    if (count >= size_) {
        Empty();
    } else if (count > 0) {
        for (unsigned int i = 0; i < count; i++) {
            Pop();
        }
    }
}

template <typename T>
void EsotericList<T>::Empty() {
    head_ = nullptr;
    tail_ = nullptr;
    size_ = 0;
}

}

#endif /* ESO_ENGINE_LINKEDLIST_H_ */