#ifndef ESO_ENGINE_UTIL_CSTRINGUTILS_H_
#define ESO_ENGINE_UTIL_CSTRINGUTILS_H_

namespace eso {

namespace utils {

//Converts a substring to unsigned long without copying substrings -- references original char*
unsigned long partial_strtoul(const char*, int start_index, int length, unsigned short base = 10);

}

}

#endif /* ESO_ENGINE_UTIL_CSTRINGUTILS_H_ */