#ifndef ESO_ENGINE_UTIL_BIN_READER_H_
#define ESO_ENGINE_UTIL_BIN_READER_H_

#include <vector>
#include <cstring>

#include <SFML/System.hpp>

//TODO: temp
#include <iostream>

namespace eso {

class BinaryReader {
private:
    std::string file_name_;
    std::vector<char> data_;
    unsigned int read_index_;
    bool is_done_ = false;
    bool is_error_ = false;

    template <typename T, std::size_t SIZE> T next();
    template <typename T, std::size_t SIZE> sf::Vector2<T> nextVec();
    

public:
    BinaryReader(const std::string& file_name);

    //Initializes reader by reading the binary file
    void Init();

    //Moves read index back to 0
    void Reset();

    //Type-specific methods for interpreting portions of the buffer
    short NextShort() { return next<short, sizeof(short)>(); }
    unsigned short NextUShort() { return next<unsigned short, sizeof(unsigned short)>(); }
    
    int NextInt() { return next<int, sizeof(int)>(); }
    unsigned int NextUInt() { return next<unsigned int, sizeof(unsigned int)>(); }

    long NextLong() { return next<long, sizeof(long)>(); }
    unsigned long NextULong() { return next<unsigned long, sizeof(unsigned long)>(); }
    
    float NextFloat() { return next<float, sizeof(float)>(); }
    double NextDouble() { return next<double, sizeof(double)>(); }

    sf::Vector2f NextVec2f() { return nextVec<float, sizeof(float)>(); }
    sf::Vector2<double> NextVec2d() { return nextVec<double, sizeof(double)>(); }
    sf::Vector2u NextVec2u() { return nextVec<unsigned int, sizeof(unsigned int)>(); }
    
    //Parse string of variable length
    std::string NextVarString();

    //TODO: don't support fixed-length string for the time being
    //std::string NextFixedString();

    //Read-only informative methods
    unsigned int GetReadIndex() const { return read_index_; }
    unsigned int GetLength() const { return data_.size(); }
    bool IsDone() const { return is_done_; }
    bool IsError() const { return is_error_; }
};

template <typename T, std::size_t SIZE>
T BinaryReader::next() {
    T value;
    unsigned int length = GetLength();

    std::cout << "read_index " << read_index_ << ", SIZE: " << SIZE << ", length: " << length << std::endl;

    //Ensure there are enough bytes remaining to read value
    if (read_index_ + SIZE <= length) {
        std::memcpy(&value, &data_[read_index_], SIZE);
        read_index_ += SIZE;
        
        //Check whether this was the last datum to read
        if (read_index_ + SIZE == length) {
            is_done_ = true;
        }

    } else {
        value = '\x00';
        is_done_ = is_error_ = true;
        //NOTE: read_index_ is NOT incremented in the error case
    }
    
    std::cout << "value: " << value << std::endl;
    return value;
}

template <typename T, std::size_t SIZE>
sf::Vector2<T> BinaryReader::nextVec() {
    T value_i, value_j;
    unsigned int length = GetLength();
    
    //Ensure there are enough bytes remaining to read value
    if (read_index_ + SIZE * 2 <= length) {
        std::memcpy(&value_i, &data_[read_index_], SIZE);
        std::memcpy(&value_j, &data_[read_index_ + 1], SIZE);
        read_index_ += SIZE * 2;

        //Check whether this was the last datum to read
        if (read_index_ + SIZE * 2 == length) {
            is_done_ = true;
        }

    } else {
        value_i = value_j = '\x00';
        is_done_ = is_error_ = true;
        //NOTE: read_index_ is NOT incremented in the error case
    }
    return sf::Vector2<T>(value_i, value_j);
}

}

#endif /* ESO_ENGINE_UTIL_BIN_READER_H_ */