#ifndef ESO_ENGINE_UTIL_CAPPEDQUEUE_H_
#define ESO_ENGINE_UTIL_CAPPEDQUEUE_H_

namespace eso {

template <typename T, unsigned int N>
class CappedQueue {
private:
    T data_[N];
    const unsigned int capacity_ = N;
    unsigned int size_ = 0;
    unsigned int pop_index_ = 0;

public:
    bool Push(T&&);
    bool PushCopy(const T&);
    T Pop();    //DON'T USE IF EMPTY, UNDEFINED BEHAVIOR!
    T* Peek();
    const T* Peek() const;
    void Reset();

    unsigned int Size() const { return size_; }
    bool IsEmpty() const { return size_ == 0; }
    bool IsFull() const { return size_ == capacity_; }
};

template <typename T, unsigned int N>
bool CappedQueue<T, N>::Push(T&& item) {
    if (size_ < capacity_) {
        unsigned int index = (pop_index_ + size_) % capacity_;
        data_[index] = std::forward<T>(item);
        size_++;
        return true;
    }
    return false;
}

template <typename T, unsigned int N>
bool CappedQueue<T, N>::PushCopy(const T& item) {
    if (size_ < capacity_) {
        unsigned int index = (pop_index_ + size_) % capacity_;
        data_[index] = item;
        size_++;
        return true;
    }
    return false;
}

template <typename T, unsigned int N>
T CappedQueue<T, N>::Pop() {
    unsigned int pop_at = pop_index_;
    if (pop_at == capacity_ - 1) {
        pop_index_ = 0;
    } else {
        pop_index_++;
    }
    size_--;
    return std::move(data_[pop_at]);
}

template <typename T, unsigned int N>
T* CappedQueue<T, N>::Peek() {
    return &data_[pop_index_];
}

template <typename T, unsigned int N>
const T* CappedQueue<T, N>::Peek() const {
    return &data_[pop_index_];
}

template <typename T, unsigned int N>
void CappedQueue<T, N>::Reset() {
    size_ = 0;
    pop_index_ = 0;
}

}

#endif /* ESO_ENGINE_UTIL_CAPPEDQUEUE_H_ */