#ifndef ESO_ENGINE_UTIL_ALGORITHMS_H_
#define ESO_ENGINE_UTIL_ALGORITHMS_H_

#include <vector>

namespace eso {

namespace utils {

// Create a sequence of numbers that adds to create the given sum
// sum:             total sum that all summands will add to
// num_summands:    number to summands to calculate. These will be returned in a vector of that size (N)
// lower_threshold: farthest allowed percent below the even_factor (even_factor * N = sum) for a particular summand
// upper_threshold: farthest allowed percent above the even_factor for a particular summand
std::vector<double> CalcRandomSummands(double sum, unsigned short num_summands, double lower_threshold, double upper_threshold);


// Create a sequence of numbers that adds to create the given sum
// sum:                 total sum that all summands will add to
// num_summands:        number to summands to calculate. These will be returned in a vector of that size (N)
// smallest_threshold:  farther integer below even_factor (even_factor * N = sum) for a particular summand, even_factor - smallest_threshold = smallest_possible_summand
// largest_threshold:   farther integer above even_factor for a particular summand, even_factor + largest_threshold = largest_possible_summand
std::vector<unsigned int> CalcRandomSummands(unsigned int sum, unsigned short num_summands, unsigned int smallest_threshold, unsigned int largest_threshold);

}

}

#endif /* ESO_ENGINE_UTIL_ALGORITHMS_H_ */