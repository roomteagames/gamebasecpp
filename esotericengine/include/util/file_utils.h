#ifndef ESO_ENGINE_UTIL_FILE_H_
#define ESO_ENGINE_UTIL_FILE_H_

#include <string>

namespace eso {

namespace file {

std::string GetExecutableDirectory();
    
}

}

#endif /* ESO_ENGINE_UTIL_FILE_H_ */