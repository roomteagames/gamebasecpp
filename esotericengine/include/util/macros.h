#ifndef ESO_ENGINE_UTIL_MACROS_H_
#define ESO_ENGINE_UTIL_MACROS_H_

//#include <boost/preprocessor.hpp>


/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

//helper macros -- used by other macros
//#define __ESO_MACRO_SET_ARRAY_ELEMENT(r, arr, i, element) arr[i] = element;

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/


///////////////////////////////////////////////////////////////////////////////////////
/** Client-facing usable macros for convenience */

/* populates array, which was already declared -- requires arr pointer, followed by each element to populate in array */
//#define ESO_MACRO_POPULATE_ARRAY(arr, ...) BOOST_PP_SEQ_FOR_EACH_I(__ESO_MACRO_SET_ARRAY_ELEMENT, arr, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

///////////////////////////////////////////////////////////////////////////////////////

#endif /* ESO_ENGINE_UTIL_MACROS_H_ */