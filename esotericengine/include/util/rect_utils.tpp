#ifndef ESO_ENGINE_UTIL_RECT_H_
#define ESO_ENGINE_UTIL_RECT_H_

#include <SFML/System.hpp>

namespace eso {

namespace rect {

template <typename T>
void Scale(sf::Rect<T>&, T);

template <typename T>
void Scale(sf::Rect<T>&, T, T);

template <typename T>
void Scale(sf::Rect<T>&, const sf::Vector2<T>&);

template <typename T>
sf::Rect<T> GetScaled(const sf::Rect<T>&, T);

template <typename T>
sf::IntRect GetScaled(const sf::Rect<T>&, T, T);

template <typename T>
sf::IntRect GetScaled(const sf::Rect<T>&, const sf::Vector2<T>&);


template <typename T>
void Scale(sf::Rect<T>& rect, T scalar) {
    rect.width *= scalar;
    rect.height *= scalar;
}

template <typename T>
void Scale(sf::Rect<T>& rect, T scalar_x, T scalar_y) {
    rect.width *= scalar_x;
    rect.height *= scalar_y;
}

template <typename T>
void Scale(sf::Rect<T>& rect, const sf::Vector2<T>& multiplier) {
    rect.width *= multiplier.x;
    rect.height *= multiplier.y;
}

template <typename T>
sf::Rect<T> GetScaled(const sf::Rect<T>& rect, T scalar) {
    return sf::Rect<T>(rect.left, rect.top, rect.width * scalar, rect.height * scalar);
}

template <typename T>
sf::IntRect GetScaled(const sf::Rect<T>& rect, T scalar_x, T scalar_y) {
    return sf::Rect<T>(rect.left, rect.top, rect.width * scalar_x, rect.height * scalar_y);
}

template <typename T>
sf::IntRect GetScaled(const sf::Rect<T>& rect, const sf::Vector2<T>& multiplier) {
    return sf::Rect<T>(rect.left, rect.top, rect.width * multiplier.x, rect.height * multiplier.y);
}

}

}

#endif /* ESO_ENGINE_UTIL_RECT_H_ */