#ifndef ESO_ENGINE_UTIL_COUNTEDARRAY_H_
#define ESO_ENGINE_UTIL_COUNTEDARRAY_H_

#include "util/capped_queue.tpp"
#include "util/iterator.tpp"

namespace eso {

template <typename T, unsigned int N>
class CountedArray {
private:
    T data_[N];
    const unsigned int capacity_ = N;
    unsigned int size_ = 0;

public:
    bool Push(const T&);
    bool Push(T&&);
    const T& operator[](unsigned int) const;
    T& operator[](unsigned int);
    void Reset();

    unsigned int GetCapacity() const { return capacity_; }
    unsigned int GetSize() const { return size_; }
    bool IsFull() const { return size_ >= capacity_; }
    bool IsEmpty() const { return size_ == 0; }

    /* Iterators, allowing a foreach loop - from data_[0] to data_[size_ - 1], inclusive */
    iterator<T> begin() { return &data_[0]; }
    const_iterator<T> begin() const { return const_cast<T*>(&data_[0]); }
    iterator<T> end() { return &data_[size_]; }
    const_iterator<T> end() const { return const_cast<T*>(&data_[size_]); }
};

template <typename T, unsigned int N>
bool CountedArray<T, N>::Push(T&& item) {
    if (size_ < capacity_ - 1) {
        data_[size_] = std::forward<T>(item);
        size_++;
        return true;
    }
    return false;
}

template <typename T, unsigned int N>
bool CountedArray<T, N>::Push(const T& item) {
    if (size_ < capacity_ - 1) {
        data_[size_] = item;
        size_++;
        return true;
    }
    return false;
}

template <typename T, unsigned int N>
const T& CountedArray<T, N>::operator[](unsigned int index) const {
    return data_[index];
}

template <typename T, unsigned int N>
T& CountedArray<T, N>::operator[](unsigned int index) {
    return data_[index];
}

template <typename T, unsigned int N>
void CountedArray<T, N>::Reset() {
    size_ = 0;
}



template <typename T, unsigned int N>
class CountedArray_Replace {
private:
    T data_[N];
    CappedQueue<unsigned int, N> replace_indexes_;
    unsigned int capacity_ = N;
    unsigned int size_ = 0;

public:
    bool PushOrReplace(T&&);
    bool PushOrReplace(const T&);
    bool PushReplaceIndex(unsigned int index);
    const T& operator[](unsigned int) const;    //DOES NOT RESPECT REPLACE_INDEXES
    T& operator[](unsigned int);                //DOES NOT RESPECT REPLACE_INDEXES
    void Reset();

    unsigned int GetCapacity() const { return capacity_; }
    unsigned int GetSize() const { return size_; }
    unsigned int GetNextIndex() const;
    bool IsFull() const { return size_ >= capacity_; }
    bool IsEmpty() const { return size_ == 0; }
};

template <typename T, unsigned int N>
bool CountedArray_Replace<T, N>::PushOrReplace(T&& item) {
    if (!replace_indexes_.IsEmpty()) {
        unsigned int replace_index = replace_indexes_.Pop();
        data_[replace_index] = std::forward<T>(item);
        return true;
    }
    if (size_ < capacity_ - 1) {
        data_[size_] = std::forward<T>(item);
        size_++;
        return true;
    }
    return false;
}

template <typename T, unsigned int N>
bool CountedArray_Replace<T, N>::PushOrReplace(const T& item) {
    if (!replace_indexes_.IsEmpty()) {
        unsigned int replace_index = replace_indexes_.Pop();
        data_[replace_index] = item;
        return true;
    }
    if (size_ < capacity_ - 1) {
        data_[size_] = item;
        size_++;
        return true;
    }
    return false;
}

template <typename T, unsigned int N>
bool CountedArray_Replace<T, N>::PushReplaceIndex(unsigned int index) {
    size_ -= 1; //because some is becoming replaceable, size is decremented
    return replace_indexes_.PushCopy(index);
}

template <typename T, unsigned int N>
const T& CountedArray_Replace<T, N>::operator[](unsigned int index) const {
    return data_[index];
}

template <typename T, unsigned int N>
T& CountedArray_Replace<T, N>::operator[](unsigned int index) {
    return data_[index];
}

template <typename T, unsigned int N>
unsigned int CountedArray_Replace<T, N>::GetNextIndex() const {
    if (replace_indexes_.IsEmpty()) {
        return size_;
    } else {
        return *(replace_indexes_.Peek());
    }
}

template <typename T, unsigned int N>
void CountedArray_Replace<T, N>::Reset() {
    size_ = 0;

    replace_indexes_.Reset();
}

}

#endif /* ESO_ENGINE_UTIL_COUNTEDARRAY_H_ */