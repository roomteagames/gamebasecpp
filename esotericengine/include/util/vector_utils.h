#ifndef ESO_ENGINE_UTIL_VECTOR_H_
#define ESO_ENGINE_UTIL_VECTOR_H_

#include <string>

#include <SFML/System.hpp>

namespace eso {

namespace vector {

template <typename T>
std::string ToString(sf::Vector2<T>);

template <typename T>
sf::Vector2i Multiply(T, const sf::Vector2i&);

template <typename T>
sf::Vector2<double> Multiply(T, const sf::Vector2<double>&);

template <typename T>
sf::Vector2i Divide(const sf::Vector2i&, T);

template <typename T>
sf::Vector2<double> Divide(const sf::Vector2<double>&, T);

int Dot(const sf::Vector2i&, const sf::Vector2i&);
double Dot(const sf::Vector2<double>&, const sf::Vector2<double>&);

int CrossZ(const sf::Vector2i&, const sf::Vector2i&);
double CrossZ(const sf::Vector2<double>&, const sf::Vector2<double>&);

sf::Vector2i MultiplyVectors(const sf::Vector2i&, const sf::Vector2i&);
sf::Vector2<double> MultiplyVectors(const sf::Vector2<double>&, const sf::Vector2<double>&);

double Magnitude(const sf::Vector2i&);
double Magnitude(const sf::Vector2<double>&);
double MagnitudeDiff(const sf::Vector2i&, const sf::Vector2i&);  //Calculates magnitude of the difference between vector1 and vector2
double MagnitudeDiff(const sf::Vector2<double>&, const sf::Vector2<double>&);  //Calculates magnitude of the difference between vector1 and vector2

int MagSquared(const sf::Vector2i&);
double MagSquared(const sf::Vector2<double>&);
int MagSquaredDiff(const sf::Vector2i&, const sf::Vector2i&);
double MagSquaredDiff(const sf::Vector2<double>&, const sf::Vector2<double>&);

sf::Vector2<double> Round(const sf::Vector2<double>&, unsigned short num_places);

sf::Vector2i GetUnitVector(const sf::Vector2i&);
sf::Vector2<double> GetUnitVector(const sf::Vector2<double>&);

sf::Vector2<double> AddIntFloat(const sf::Vector2i&, const sf::Vector2<double>&);

template <typename T>
sf::Vector2i Multiply(T scalar, const sf::Vector2i& v) {
    return { (int) (scalar * v.x), (int) (scalar * v.y) };
}

template <typename T>
sf::Vector2<double> Multiply(T scalar, const sf::Vector2<double>& v) {
    return { (double) (scalar * v.x), (double) (scalar * v.y) };
}

template <typename T>
sf::Vector2i Divide(const sf::Vector2i& v, T scalar) {
    return { (int) (v.x / scalar), (int) (v.y / scalar) };
}

template <typename T>
sf::Vector2<double> Divide(const sf::Vector2<double>& v, T scalar) {
    return { (double) (v.x / scalar), (double) (v.y / scalar) };
}

template <typename T>
std::string ToString(sf::Vector2<T> v) {
    return "[" + std::to_string(v.x) + "," + std::to_string(v.y) + "]";
}

}

}

#endif /* ESO_ENGINE_UTIL_VECTOR_H_ */