#ifndef ESO_ENGINE_UTIL_SHOWCASE_H_
#define ESO_ENGINE_UTIL_SHOWCASE_H_

#include <sstream>
#include <functional>
#include <memory>
#include <vector>

#include "util/iterator.tpp"

//TODO: TEMP
#include <iostream>
//END TEMP

// #include <EASTL/vector.h>

// Define 3 types of Showcases
// Showcase - a showcase that stores objects. Use Emplace to push objects by constructor params
// TODO REMOVE: ShowcaseWithDeleteHandler - a showcase that stores objects. When an object is about to be deleted, a delete handler is called, which takes a param of type U. Allows cleanup of references to the object before being deleted
// ShowcaseRef - a showcase that stores a reference to an object. Use Push to insert object references

namespace eso {


template <typename T> class Showcase;
template <typename T> std::ostream& operator<< (std::ostream&, const Showcase<T>&);

/* TODO: move to util/wrapper.tpp */
template <typename T>
struct Wrapper {
    T Value;
    Wrapper(const T& value): Value(value) {}
    Wrapper(T&& value): Value(std::move(value)) {}
};

//Showcase for actual objects
//Showcase must hold wrappers of objects, so if you move the object around in the vector, any reference of the object doesn't get destroyed
template <typename T>
class Showcase {
private:
    unsigned short capacity_;
    std::vector<Wrapper<T>> collection_;
    std::function<bool(const T&, const T&)> equality_fn_;
    std::function<void(const T&)> before_delete_;

    /* Private helper methods */
    short index_of(const T&) const;
    void move_to_front(unsigned short);

public:
    /* Special note about O(n): n always == N, the size of ShowCase given at compile time, so technically this is a CONSTANT order O(1) */

    /* O(n) */
    Showcase(unsigned short capacity);
    Showcase(unsigned short capacity, std::function<bool(const T&, const T&)>);
    Showcase(unsigned short capacity, std::function<bool(const T&, const T&)>, std::function<void(const T&)>);

    /* O(1) */
    const T& GetTop() const { return collection_[collection_.size() - 1].Value; }
    T& GetTop() { return collection_[collection_.size() - 1.].Value; }
    const T& GetBottom() const { return collection_[0].Value; }
    T& GetBottom() { return collection_[0].Value; }

    /* Iterators, allowing a foreach loop - from most highest priority, to lowest */
    iterator<Wrapper<T>> begin() { return &collection_[0]; }
    const_iterator<Wrapper<T>> begin() const { return &collection_[0]; }
    iterator<Wrapper<T>> end() { return &collection_[collection_.size()]; }
    const_iterator<Wrapper<T>> end() const { return &collection_[collection_.size()]; }

    int GetSize() const { return collection_.size(); }
    
    /* O(n) */
    //returns true if element is already in showcase -- utilizes equality_fn_
    bool Contains(const T&) const;

    /* O(n) */
    //pushes element if not already present
    //pushed element is at top
    //pushing removes last element that exceeds capacity and moves all other elements a position farther
    void Push(T&&);

    /* O(1) */
    //removes element at given index
    void DeleteAt(unsigned short index);

    /* O(1) */
    //removes last element
    void DeleteLast();

    /* O(n) */
    // Operator overload must be defined as friend, to work with a template
    friend std::ostream& operator<< <> (std::ostream&, const Showcase&);
};


template <typename T>
Showcase<T>::Showcase(unsigned short capacity): capacity_(capacity) {
    collection_.reserve(capacity);
    equality_fn_ = [](const T& item, const T& other) {
        const T* item_ptr = &item;
        const T* other_ptr = &other;
        return item_ptr == other_ptr;   //both items are in same memory address
    };
}

template <typename T>
Showcase<T>::Showcase(unsigned short capacity, std::function<bool(const T&, const T&)> equality_fn):
        capacity_(capacity),
        equality_fn_(equality_fn)
{
    collection_.reserve(capacity);
}

template <typename T>
Showcase<T>::Showcase(unsigned short capacity, std::function<bool(const T&, const T&)> equality_fn, std::function<void(const T&)> before_delete):
        capacity_(capacity),
        equality_fn_(equality_fn),
        before_delete_(before_delete)
{
    collection_.reserve(capacity);
}

template <typename T>
short Showcase<T>::index_of(const T& item) const {
    for (int i = 0; i < collection_.size(); i++) {
        const T& current = collection_[i].Value;
        if (equality_fn_(current, item)) {   //requires custom function -- on Showcase constructor -- to check equality
            return i;
        }
    }
    return -1;
}

//NOTE: Wrapper is NEEDED because of the existance of this function
template <typename T>
void Showcase<T>::move_to_front(unsigned short index) {
    collection_.insert(collection_.begin(), std::move(collection_[index]));
    collection_.erase(collection_.begin() + index + 1);
}

template <typename T>
bool Showcase<T>::Contains(const T& item) const {
    return (index_of(item) > -1);
}

template <typename T>
void Showcase<T>::Push(T&& item)  {
    short index = index_of(item);
    if (index == -1) {
        collection_.insert(collection_.begin(), Wrapper<T>(std::move(item)));
        if (collection_.size() > capacity_) {
            DeleteLast();
        }
    } else {
        move_to_front(index);
    }
}

template <typename T>
void Showcase<T>::DeleteLast() {
    if (before_delete_ != nullptr) {
        before_delete_(collection_.back().Value);
    }
    collection_.pop_back();
}

template <typename T>
void Showcase<T>::DeleteAt(unsigned short index) {
    if (before_delete_ != nullptr) {
        before_delete_(collection_[index].Value);
    }
    collection_.erase(collection_.begin() + index);
}


template <typename T>
std::ostream& operator<< (std::ostream& stream, const Showcase<T>& showcase) {
    unsigned short size = showcase.collection_.size();
    for (int i = 0; i < size; i++) {
        if (i > 0) {
            stream << "; ";
        }
        stream << showcase.collection_[i];
    }
    return stream;
}



// Operator overload cannot be a class template member. Instead, must be declared before class is defined
template <typename T, typename... ElementArgs> class ShowcaseEmp;
template <typename T, typename... ElementArgs> std::ostream& operator<< (std::ostream&, const ShowcaseEmp<T, ElementArgs...>&);

template <typename T, typename... ElementArgs>
class ShowcaseEmp {
private:
    unsigned short capacity_;
    std::vector<T> collection_;

    //used to determine whether pre-constructed element is ALREADY contained within Showcase
    const std::function<bool(const T&, ElementArgs...)>& equality_fn_;

    int index_of(ElementArgs...) const;

public:

    /* Special note about O(n): n always == N, the size of ShowCase given at compile time, so technically this is a CONSTANT order O(1) */

    /* O(n) */
    ShowcaseEmp(unsigned short capacity, const std::function<bool(const T&, ElementArgs...)>&);

    /* O(1) */
    const T& GetTop() const { return collection_[collection_.size() - 1]; }
    T& GetTop() { return collection_[collection_.size() - 1]; }

    /* Iterators, allowing a foreach loop - from most highest priority, to lowest */
    iterator<Wrapper<T>> begin() { return &collection_[0]; }
    const_iterator<Wrapper<T>> begin() const { return &collection_[0]; }
    iterator<Wrapper<T>> end() { return &collection_[collection_.size()]; }
    const_iterator<Wrapper<T>> end() const { return &collection_[collection_.size()]; }

    int GetSize() const { return collection_.size(); }

    /* O(n) */
    //returns true if element (identified by constructor params) is already in showcase
    bool Contains(ElementArgs...) const;
    
    /* O(n) */
    //pushes element (in-place construction) if not already present
    //pushed element is at top
    //pushing removes last element that exceeds capacity and moves all other elements a position farther
    void Emplace(ElementArgs...);
    void Push(T&&);

    /* O(1) */
    //removes element at given index
    void DeleteAt(unsigned short index);

    /* O(1) */
    //removes last element
    void DeleteLast();

    /* O(n) */
    // Operator overload must be defined as friend, to work with a template
    friend std::ostream& operator<< <> (std::ostream&, const ShowcaseEmp&);
};

template <typename T, typename... ElementArgs>
ShowcaseEmp<T, ElementArgs...>::ShowcaseEmp(unsigned short capacity, const std::function<bool(const T&, ElementArgs...)>& equality_fn): capacity_(capacity), equality_fn_(equality_fn) {
    collection_.reserve(capacity);
}

template <typename T, typename... ElementArgs>
int ShowcaseEmp<T, ElementArgs...>::index_of(ElementArgs... args) const {
    for (int i = 0; i < collection_.size(); i++) {
        const T& current = collection_[i];
        if (equality_fn_(current, args...)) {   //requires custom function -- on Showcase constructor -- to check equality
            return i;
        }
    }
    return -1;
}

template <typename T, typename... ElementArgs>
bool ShowcaseEmp<T, ElementArgs...>::Contains(ElementArgs... args) const {
    return (index_of(args...) > -1);
}

//TODO: create BlindEmplace that doesn't check if item already exists... just inserts
template <typename T, typename... ElementArgs>
void ShowcaseEmp<T, ElementArgs...>::Emplace(ElementArgs... args)  {
    int index = index_of(args...);
    if (index == -1) {
        collection_.emplace(collection_.begin(), args...);
        if (collection_.size() > capacity_) {
            DeleteLast();
        }
    } else {
        collection_.insert(collection_.begin(), std::move(collection_[index]));
        collection_.erase(collection_.begin() + index + 1);
    }
}

template <typename T, typename... ElementArgs>
void ShowcaseEmp<T, ElementArgs...>::DeleteLast() {
    collection_.pop_back();
}

template <typename T, typename... ElementArgs>
std::ostream& operator<< (std::ostream& stream, const ShowcaseEmp<T, ElementArgs...>& showcase) {
    unsigned short size = showcase.collection_.size();
    for (int i = 0; i < size; i++) {
        if (i > 0) {
            stream << "; ";
        }
        stream << showcase.collection_[i];
    }
    return stream;
}


template <typename T> class ShowcaseRef;
template <typename T> std::ostream& operator<< (std::ostream&, const ShowcaseRef<T>&);

//Showcase for object references, rather than pre-constructed objects
//Some function signatures differ, so pay attention!
template <typename T>
class ShowcaseRef {
private:
    unsigned short capacity_;
    std::vector<std::reference_wrapper<T>> collection_;
    std::function<bool(const T&, const T&)> equality_fn_;
    int index_of(const T&) const;

public:
    ShowcaseRef(unsigned short capacity);
    ShowcaseRef(unsigned short capacity, std::function<bool(const T&, const T&)>);

    const T& GetTop() const { return collection_[collection_.size() - 1]; }
    T& GetTop() { return collection_[collection_.size() - 1]; }

    /* Iterators, allowing a foreach loop - from most highest priority, to lowest */
    iterator<Wrapper<T>> begin() { return &collection_[0]; }
    const_iterator<Wrapper<T>> begin() const { return &collection_[0]; }
    iterator<Wrapper<T>> end() { return &collection_[collection_.size()]; }
    const_iterator<Wrapper<T>> end() const { return &collection_[collection_.size()]; }
    
    bool Contains(const T&) const;

    //returns removed element -- returns NULL if no elements removed
    T* Push(T&);

    friend std::ostream& operator<< <> (std::ostream&, const ShowcaseRef&);
};


template <typename T>
ShowcaseRef<T>::ShowcaseRef(unsigned short capacity): capacity_(capacity) {
    collection_.reserve(capacity);
    equality_fn_ = [](const T& item, const T& other) {
        const T* item_ptr = &item;
        const T* other_ptr = &other;
        return item_ptr == other_ptr;
    };
}

template <typename T>
ShowcaseRef<T>::ShowcaseRef(unsigned short capacity, std::function<bool(const T&, const T&)> equality_fn): capacity_(capacity), equality_fn_(equality_fn) {
    collection_.reserve(capacity);
}

template <typename T>
int ShowcaseRef<T>::index_of(const T& item) const {
    for (int i = 0; i < collection_.size(); i++) {
        const T& current = collection_[i];
        if (equality_fn_(current, item)) {   //requires custom function -- on Showcase constructor -- to check equality
            return i;
        }
    }
    return -1;
}

template <typename T>
bool ShowcaseRef<T>::Contains(const T& item) const {
    return (index_of(item) > -1);
}

//return removed item as a ptr -- so we know which was removed -- if null, nothing was removed
template <typename T>
T* ShowcaseRef<T>::Push(T& item)  {
    T* removed_element = nullptr;
    int index = index_of(item);
    if (index == -1) {
        collection_.insert(collection_.begin(), item);
        if (collection_.size() > capacity_) {
            T& temp = collection_.back();
            removed_element = &temp;
            collection_.pop_back();
        }
    } else {
        collection_.insert(collection_.begin(), collection_[index]);
        collection_.erase(collection_.begin() + index + 1);
    }
    return removed_element;
}

template <typename T>
std::ostream& operator<< (std::ostream& stream, const ShowcaseRef<T>& showcase) {
    unsigned short size = showcase.collection_.size();
    for (int i = 0; i < size; i++) {
        if (i > 0) {
            stream << "; ";
        }
        stream << showcase.collection_[i];
    }
    return stream;
}

}

#endif /* ESO_ENGINE_UTIL_SHOWCASE_H_ */
