#ifndef ESO_ENGINE_STORE_TEXTURE_H_
#define ESO_ENGINE_STORE_TEXTURE_H_

#include <SFML/Graphics.hpp>

#include "util/showcase.tpp"

namespace eso {

struct TextureStore {
private:
    static bool is_created = false;
    Showcase<sf::Texture> loaded_textures;
    TextureStore();

public:
    static bool IsCreated() { return is_created; }
    static void GenerateTextureStore(unsigned short capacity);
};

namespace store {

TextureStore texture_store;

}

}

#endif /* ESO_ENGINE_STORE_TEXTURE_H_ */