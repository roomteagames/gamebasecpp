#ifndef ESO_ENGINE_MEDIA_STORE_H_
#define ESO_ENGINE_MEDIA_STORE_H_

#include <memory>
#include <unordered_map>
#include <vector>

#include <SFML/Graphics.hpp>

#include "util/linked_list.tpp" 

namespace eso {

template <typename T>
struct Store {
private:
    EsotericList<T> object_list;
    std::unordered_map<std::string, EsotericListNode<T>*> node_map;
    int capacity; //Optional: set to -1 for no capacity
    std::string* last_removal = nullptr;


public:
    Store(unsigned int _id, int _capacity = -1): object_list(_id), capacity(_capacity) {}

    // Insertion methods
    void Add(const std::string& search_key, T&& element);

    // Access methods
    T* operator[](const std::string& search_key);
    bool ContainsKey(const std::string& search_key) const { return node_map.count(search_key) > 0; }

    // Remove methods
    bool RemoveLast();
    bool Remove(const std::string& search_key);
};

template <typename T>
void Store<T>::Add(const std::string& search_key, T&& element) {
    // If already in store, repush. Else, load from file
    if (node_map.count(search_key) > 0) {
        EsotericListNode<T>& node = *node_map[search_key];
        object_list.Repush(node);
    } else {
        //element is moved or copied into list
        EsotericListNode<T>& element_node = object_list.Push(std::forward<T>(element));
        element_node.Key = search_key;  //used to find element in map if removing last element
        node_map[search_key] = &element_node;

        //if too many keys are stored, remove element at key
        if (capacity > -1 && object_list.Size() > capacity) {
            RemoveLast();
        }
    }
}

template <typename T>
T* Store<T>::operator[](const std::string& search_key) {
    if (node_map.count(search_key) > 0) {
        EsotericListNode<T>& node = *node_map[search_key];
        object_list.Repush(node);
        return &node.Value();
    }
    return nullptr;
}

template <typename T>
bool Store<T>::RemoveLast() {
    if (object_list.Size() > 0) {
        const std::string& last_key = object_list.End()->Key;
        node_map.erase(last_key);
        object_list.Pop();
        return true;
    }
    return false;
}

template <typename T>
bool Store<T>::Remove(const std::string& search_key) {
    EsotericListNode<T>& removed_node = *node_map[search_key];
    node_map.erase(search_key);
    return object_list.Remove(removed_node);
}


}

#endif /* ESO_ENGINE_MEDIA_STORE_H_ */