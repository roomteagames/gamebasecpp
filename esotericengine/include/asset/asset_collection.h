#ifndef ESO_ENGINE_MEDIA_ASSETS_H_
#define ESO_ENGINE_MEDIA_ASSETS_H_

#include <vector>
#include <unordered_map>

#include <SFML/Graphics.hpp>

#include "config.h"
#include "asset/store.tpp"
#include "model/game/shape/shape.h"
#include "model/game/shape/material.h"
#include "view/spritesheet.h"
#include "constants.h"

namespace eso {

class VisualCollection {
private:
    const AssetProps& props_;
    Store<Texture_Spritesheets> texture_store_;
    std::unordered_map<std::string, sf::Font> fonts_;

public:
    VisualCollection(const AssetProps& props);

    // Given name, if within store, returns pointer; else loads file to store and returns pointer. If given name is file, returns nullptr
    Texture_Spritesheets* FetchOrCreateTextureSpritesheet(const std::string&);
    const Texture_Spritesheets* FetchTextureSpritesheet(const std::string&);
    const sf::Texture* FetchTexture(const std::string&);
    const Spritesheet* FetchSpritesheet(const std::string&, unsigned short);
    bool HasTexture(const std::string& texture_name) const { return texture_store_.ContainsKey(texture_name); }
    
    //Font Operations
    sf::Font* FetchOrCreateFont(const std::string& font_name = system::DefaultFontName);
    const sf::Font* FetchFont(const std::string& font_name = system::DefaultFontName);

    template <typename... TArgs> short AddSpritesheet(const std::string&, TArgs&&...);
    template <typename... TArgs> short AddSpritesheet(Texture_Spritesheets&, TArgs&&...);
};

class AudioCollection {
private:
    const AssetProps& props_;
    // Store<?Sound?> sound_store_;
    // Store<?Music?> music_store_;

public:
    AudioCollection(const AssetProps& props);
    /* sf::Sound* FetchOrCreateSound(const std::string&); */
    /* sf::Music* FetchOrCreateMusic(const std::string&); */
    /* const sf::Sound* FetchSound(const std::string&); */
    /* const sf::Music* FetchMusic(const std::string&); */
};

class PhysicsCollection {
private:
    const AssetProps& props_;   //TODO: maybe we can remove assetprops from PhysicsCollection entirely
    Shape shapes_[ESO_VAR_SHAPE_COUNT_MAX];
    Material materials_[ESO_VAR_MATERIAL_COUNT_MAX];

    /*EdgeFunction edge_functions_[ESO_VAR_EDGE_FUNCTION_COUNT_MAX];*/

    unsigned short num_shapes_ = 0;
    unsigned short num_materials_ = 0;
    unsigned short num_edge_functions_ = 0;

public:
    PhysicsCollection(const AssetProps& props);

    //Fetch methods return non-const ptr to item in collection
    Shape* FetchShape(unsigned short index) { return &shapes_[index]; }
    Material* FetchMaterial(unsigned short index) { return &materials_[index]; }

    //Get methods return const ptr to item in collection
    const Shape* GetShape(unsigned short index) const { return &shapes_[index]; }
    const Material* GetMaterial(unsigned short index) const { return &materials_[index]; }

    unsigned short GetNumShapes() const { return num_shapes_; }
    unsigned short GetNumMaterials() const { return num_materials_; }
    unsigned short GetNumEdgeFunctions() const { return num_edge_functions_; }

    //move input into array at next available index -- return index that item was added to
    unsigned short AddShape(Shape&&);
    unsigned short AddMaterial(Material&&);
    //unsigned short AddEdgeFunction(EdgeFunction&&);
};

struct AssetCollection {
    VisualCollection Visuals;
    AudioCollection Audio;
    PhysicsCollection Physics;

    AssetCollection(const AssetProps& props);
};

template <typename... TArgs>
short VisualCollection::AddSpritesheet(const std::string& texture_name, TArgs&&... spritesheet_args) {
    auto* texture_spritesheet = FetchOrCreateTextureSpritesheet(texture_name);
    short spritesheet_index = -1;
    
    if (texture_spritesheet != nullptr) {
        texture_spritesheet->RefSpritesheets().emplace_back(std::forward<TArgs>(spritesheet_args)...);
        //spritesheet_index = ;
    }
    return spritesheet_index;
}

template <typename... TArgs>
short VisualCollection::AddSpritesheet(Texture_Spritesheets& obj, TArgs&&... spritesheet_args) {
    //TODO: this probably needs an actual implementation...
    return 0;
}

}

#endif /* ESO_ENGINE_MEDIA_ASSETS_H_ */