#ifndef ESO_ENGINE_VIEW_MODEL_H_
#define ESO_ENGINE_VIEW_MODEL_H_

#include <functional>
#include <memory>

#include <SFML/Graphics.hpp>

#include "asset/asset_collection.h"
#include "model/entity/entity.h"

namespace eso {

class GameState;
class ViewModel;

//AOP Functions that can be passed into view regardless of subclass
struct ViewFunctions {
    //Only has access to model and game state
    //Called before view position is set
    //If returns false, view is NOT rendered
    bool (*EvaluateModel)(const GameState&, const std::shared_ptr<Entity>&) = nullptr;
    
    //Allows for custom logic to occur BEFORE this view render logic
    //Allows for cross cutting renders regardless of view type
    void (*PreDraw)(const GameState&, const std::shared_ptr<Entity>&, ViewModel*, VisualCollection&, sf::RenderWindow*) = nullptr;

    //Allows for custom logic to occur AFTER this view render logic
    //Allows for cross cutting renders regardless of view type
    void (*PostDraw)(const GameState&, const std::shared_ptr<Entity>&, ViewModel*, VisualCollection&, sf::RenderWindow*) = nullptr;

    ViewFunctions() {}

    ViewFunctions(bool (*eval_model)(const GameState&, const std::shared_ptr<Entity>&),
                  void (*pre_draw)(const GameState&, const std::shared_ptr<Entity>&, ViewModel*, VisualCollection&, sf::RenderWindow*) = nullptr,
                  void (*post_draw)(const GameState&, const std::shared_ptr<Entity>&, ViewModel*, VisualCollection&, sf::RenderWindow*) = nullptr);
};

class ViewModel {
protected:
    const std::weak_ptr<Entity> model_;
    ViewFunctions view_functions_;

    unsigned int view_class_id_;    /* TODO: do we actually need this? */
    sf::Vector2<double> position_;
    sf::Vector2<double> dimensions_;
    sf::Vector2<double> scale_ = sf::Vector2<double>(1.0, 1.0);

    double orientation_ = 0;    //in degrees -- absolute rotation

    /* Returns true if view should be rendered
     * This logic should be based off of the specific properties of the associated model */
    virtual bool will_draw_view(const std::shared_ptr<Entity>&) const { return true; }
    
    /* Sets position where view should be rendered
     * Can be overridden, but built in logic is often desirable */
    virtual void set_view_position(const std::shared_ptr<Entity>&);

    /* Contains main render logic where graphics are drawn to window */
    virtual void draw_view(const GameState&, const std::shared_ptr<Entity>& model, VisualCollection& assets, sf::RenderWindow*) = 0;

    /* Contains logic to update view properties after render */
    virtual void update_view(const GameState&, const std::shared_ptr<Entity>& model) {}

public:
    ViewModel(const std::shared_ptr<Entity>& model, ViewFunctions&& view_functions = ViewFunctions());
    ViewModel(const std::shared_ptr<Entity>& model, sf::Vector2<double> scale, ViewFunctions&& view_functions = ViewFunctions());
    
    virtual ~ViewModel() {}

    virtual void InitDependencies(VisualCollection&) {}

    void Render(const GameState&, VisualCollection&, sf::RenderWindow*);

    const std::shared_ptr<Entity> GetModel() const { return model_.lock(); }
    const unsigned int GetViewClassId() const { return view_class_id_; }

    bool IsDestroyed() const;

    void SetPosition(int x, int y) { position_ = sf::Vector2<double>(x, y); }
    void SetPosition(const sf::Vector2<double>& position) { position_ = position; }
    void SetPosition(sf::Vector2<double>&& position) { position_ = std::move(position); }

    //TODO: perhaps this can be deprecated...
    void SetDimensions(int width, int height) { dimensions_ = sf::Vector2<double>(width, height); }
    void SetDimensions(const sf::Vector2<double>& dimensions) { dimensions_ = dimensions; }

    void SetScale(double scale) { scale_ = sf::Vector2<double>(scale, scale); }
    void SetScale(double scale_x, double scale_y) { scale_ = sf::Vector2<double>(scale_x, scale_y); }

    void SetOrientation(double degrees) { orientation_ = degrees; }

};

}

#endif /* ESO_ENGINE_VIEW_MODEL_H_ */