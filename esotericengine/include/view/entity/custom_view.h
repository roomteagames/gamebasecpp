#ifndef ESO_ENGINE_VIEW_CUSTOM_H_
#define ESO_ENGINE_VIEW_CUSTOM_H_

#include <functional>

#include "view/entity/view_model.h"

namespace eso {

class CustomView : public ViewModel {
private:
    const std::function<void(VisualCollection&, sf::RenderWindow*)>& render_fn_;

    virtual void draw_view(const GameState&, const std::shared_ptr<Entity>& model, VisualCollection& assets, sf::RenderWindow*) override;

public:
    CustomView(const std::shared_ptr<Entity>& model, const std::function<void(VisualCollection&, sf::RenderWindow*)>& render_fn);
};

}

#endif /* ESO_ENGINE_VIEW_CUSTOM_H_ */