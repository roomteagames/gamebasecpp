#ifndef ESO_ENGINE_VIEW_STATEFUL_H_
#define ESO_ENGINE_VIEW_STATEFUL_H_

#include "view/entity/view_model.h"
#include "util/binary_values.h"

namespace eso {

static constexpr unsigned short kDefaultNumStates = 5;

class StatefulView : public ViewModel {
protected:
    unsigned short frame_num_;
    unsigned short state_;
    unsigned short previous_frame_ = frame_num_;
    unsigned short previous_state_ = state_;
    
    bool auto_advance_frames_;
    unsigned int frame_length_;
    unsigned int frame_progress_;

    unsigned long looping_states_;
    unsigned long locked_states_;

    virtual void update_view(const GameState&, const std::shared_ptr<Entity>& model) override;

public:
    StatefulView(const std::shared_ptr<Entity>& model, unsigned short starting_state = 0, unsigned short starting_frame = 0, unsigned long locked_states = bin::bNone,
                bool auto_advance_frames = true, unsigned int frame_length = 5, unsigned long looping_states = bin::bAll64);

    bool IsFrameUpdated() const { return (frame_num_ != previous_frame_); }
    bool IsStateUpdated() const;

    bool IsLoopingState(unsigned long states) const { return (looping_states_ & states) == states; }
    bool IsLockedState(unsigned long states) const { return (locked_states_ & states) == states; }

    /* Override if there is special logic in subclass determining how many frames are in a state */
    virtual unsigned short GetNumStateFrames() const { return kDefaultNumStates; }

    //setters
    void SetAutoAdvanceFrames(bool a) { auto_advance_frames_ = a; }
    void UpdateFrame(unsigned short frame);
    void UpdateState(unsigned short state);

    //Updaters
    void AddLoopingStates(unsigned long states) { looping_states_ |= states; }
    void RemoveLoopingStates(unsigned long states) { looping_states_ &= ~states; }
    void AddLockedStates(unsigned long states) { locked_states_ |= states; }
    void RemoveLockedStates(unsigned long states) { locked_states_ &= ~states; }
};

}

#endif /* ESO_ENGINE_VIEW_STATEFUL_H_ */
