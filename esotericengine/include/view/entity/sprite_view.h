#ifndef ESO_ENGINE_VIEW_SPRITE_H_
#define ESO_ENGINE_VIEW_SPRITE_H_

#include <tuple>

#include "view/entity/stateful_view.h"
#include "view/spritesheet.h"
#include "sample/sample_sprite_rects.h"

namespace eso {

struct SpritesheetConfig {
    const std::string Name;
    unsigned short SheetIndex = 0;

    SpritesheetConfig() {}
    SpritesheetConfig(const std::string& name, unsigned short sheet_index): Name(name), SheetIndex(sheet_index) {}
};

class SpriteView : public StatefulView {
protected:
    sf::Sprite sprite_;
    SpritesheetConfig spritesheet_config_;
    const Spritesheet* spritesheet_ = nullptr;

    //THESE FIELDS UTILIZED BY ROOM EDITOR:
    //FUTURE: If true, room editor will determine which sprite to assign to view based on worldly position (proximity to other views with same view_class_id_)
    bool tiled_ = false;
    //FUTURE: World theme (of which world?? -- there can be multiple) can be swapped out; determining which view classes to use with a particular object based on current theme (0 is default/global)
    unsigned short theme_ = 0;

    virtual sf::IntRect build_sprite_rect();
    virtual void update_sprite_structure();
    
    //Can be overriden by subclass, but practical default logic is provided
    virtual void draw_view(const GameState&, const std::shared_ptr<Entity>& model, VisualCollection& assets, sf::RenderWindow*) override;
    virtual void draw_sprite(VisualCollection&, sf::RenderWindow*);
    virtual void prepare_sprite(const std::shared_ptr<Entity>& model);

public:
    SpriteView(const std::shared_ptr<Entity>& model, const std::string& texture_name, unsigned short spritesheet_index, unsigned short starting_state = 0, unsigned short starting_frame = 0, unsigned long locked_states = bin::bNone,
                bool auto_advance_frames = true, unsigned int frame_length = 5, unsigned long looping_states = bin::bAll64);

    // NOTE: this ctor should be used when writing custom/manual loading code
    SpriteView(const std::shared_ptr<Entity>& model, const sf::Texture& texture, const Spritesheet* spritesheet, unsigned short starting_state = 0, unsigned short starting_frame = 0, unsigned long locked_states = bin::bNone,
                bool auto_advance_frames = true, unsigned int frame_length = 5, unsigned long looping_states = bin::bAll64);

    virtual void InitDependencies(VisualCollection&) override;

    //getters
    bool Tiled() const { return tiled_; }
    unsigned short GetTheme() const { return theme_; }
    virtual unsigned short GetNumStateFrames() const override { return spritesheet_->GetNumStateFrames(state_); }

    //setters
    void SetSpritesheet(const Spritesheet* sheet) { spritesheet_ = sheet; }

    //Updaters


};

}

#endif /* ESO_ENGINE_VIEW_SPRITE_H_ */