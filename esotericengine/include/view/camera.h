#ifndef ESO_ENGINE_VIEW_VIEWPORT_H_
#define ESO_ENGINE_VIEW_VIEWPORT_H_

#include <vector>
#include <memory>

#include <SFML/Graphics.hpp>

#include "config.h"
#include "model/entity/entity.h"

namespace eso {

struct ViewportIndex {
    const sf::View& Viewport;
    int ViewTypeIndex;
    ViewportIndex(const sf::View& _viewport, int _view_type_index): Viewport(_viewport), ViewTypeIndex(_view_type_index) {}
};

class Camera : public Entity {
private:
    sf::View viewport_;
    const std::weak_ptr<Entity> focus_;
    unsigned short view_type_index_;

public:
    // Camera(sf::View&&, unsigned short);
    Camera(unsigned int, unsigned short, const std::shared_ptr<Entity>&, sf::Vector2<double>&&, double zoom = 1.0f, unsigned int = 0x0); 

    const sf::View& GetViewport() const { return viewport_; }
    unsigned short GetViewTypeIndex() const { return view_type_index_; }
    const std::shared_ptr<Entity> GetFocus() const { return focus_.lock(); }

    ViewportIndex ConstructViewportIndex() const { return ViewportIndex(viewport_, view_type_index_); }


    virtual void Update(Game&, World&) override;
};

}

#endif /* ESO_ENGINE_VIEW_VIEWPORT_H_ */