#ifndef ESO_ENGINE_VIEW_SPRITE_CONFIG_H_
#define ESO_ENGINE_VIEW_SPRITE_CONFIG_H_

#include <array>
#include <vector>

#include <SFML/Graphics.hpp>

namespace eso {

constexpr unsigned short kMaxSpritesheetStates = 64;

class Spritesheet {
private:
    sf::Vector2u origin_;
    sf::Vector2u frame_size_;
    std::array<unsigned short, kMaxSpritesheetStates> frames_per_state_;

public:
    Spritesheet(const sf::Vector2u& origin, const sf::Vector2u& frame_size, const std::array<unsigned short, kMaxSpritesheetStates>& frames_per_state);
    Spritesheet(sf::Vector2u&& origin = {0, 0}, sf::Vector2u&& frame_size = {32, 32}, std::array<unsigned short, kMaxSpritesheetStates>&& frames_per_state = {5});
    
    //Getters
    const sf::Vector2u& GetOrigin() const { return origin_; }
    const sf::Vector2u& GetFrameSize() const { return frame_size_; }
    unsigned short GetNumStates() const { return frames_per_state_.size(); }
    unsigned short GetNumStateFrames(unsigned short state_index) const { return frames_per_state_[state_index]; }
};

class Texture_Spritesheets {
private:
    const std::string name_;
    sf::Texture texture_;
    std::vector<Spritesheet> spritesheets_;
    bool texture_loaded_ = false;

public:
    Texture_Spritesheets(const std::string& name, unsigned short num_spritesheets = 3);

    const std::string& GetName() const { return name_; }
    const sf::Texture& GetTexture() const { return texture_; }
    const Spritesheet& GetSpritesheet(unsigned short index) const { return spritesheets_[index]; }
    bool IsTextureLoaded() const { return texture_loaded_; }
    bool IsSpritesheetsCreated() const { return spritesheets_.size() > 0; }
    unsigned short GetNumSpritesheets() const { return spritesheets_.size(); }

    sf::Texture& RefTexture() { return texture_; }
    std::vector<Spritesheet>& RefSpritesheets() { return spritesheets_; }

    //Updaters
    bool LoadTexture(const std::string& file_name);
    unsigned short AddSpritesheet(const sf::Vector2u&, const sf::Vector2u&, const std::array<unsigned short, kMaxSpritesheetStates>&);
    unsigned short AddSpritesheet(sf::Vector2u&&, sf::Vector2u&&, std::array<unsigned short, kMaxSpritesheetStates>&&);
    unsigned short AddSpritesheet(Spritesheet&&);

};

}

#endif /* ESO_ENGINE_VIEW_SPRITE_CONFIG_H_ */