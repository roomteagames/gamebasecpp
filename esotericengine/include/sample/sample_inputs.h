#ifndef ESO_ENGINE_SAMPLE_INPUTS_H_
#define ESO_ENGINE_SAMPLE_INPUTS_H_

#include "model/game/input_model.h"

namespace eso {

namespace sample {

void PopulateKeyboardInputs1(InputConfig&);
void PopulateKeyboardInputs2(InputConfig&);
void PopulateKeyboardInputs3(InputConfig&);

}

}

#endif /* ESO_ENGINE_SAMPLE_INPUTS_H_ */