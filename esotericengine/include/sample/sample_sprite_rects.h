#ifndef ESO_ENGINE_SAMPLE_SPRITE_RECTS_H_
#define ESO_ENGINE_SAMPLE_SPRITE_RECTS_H_

#include <array>

#include <SFML/Graphics.hpp>

namespace eso {

namespace sample {

struct {
    sf::IntRect Quad16[4] = {
        {0, 0, 16, 16},
        {16, 0, 16, 16},
        {32, 0, 16, 16},
        {48, 0, 16, 16}
    };
    sf::IntRect Quad32[4] = {
        {0, 0, 32, 32},
        {32, 0, 32, 32},
        {64, 0, 32, 32},
        {96, 0, 32, 32}
    };
    sf::IntRect Quad64[4] = {
        {0, 0, 64, 64},
        {64, 0, 64, 64},
        {128, 0, 64, 64},
        {192, 0, 64, 64}
    };
} const SpriteRects;

}

}


#endif /* ESO_ENGINE_SAMPLE_SPRITE_RECTS_H_ */