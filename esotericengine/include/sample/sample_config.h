#ifndef ESO_ENGINE_SAMPLE_CONFIG_H_
#define ESO_ENGINE_SAMPLE_CONFIG_H_

#include "config.h"
#include "model/game/game_state.h"
#include "sample/sample_inputs.h"

namespace eso {

namespace sample {

PropsCollection GenerateSampleProps();

// void RunLoops(GameState&);

}

}


#endif /* ESO_ENGINE_SAMPLE_CONFIG_H_ */