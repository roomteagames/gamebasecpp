#ifndef ESO_ENGINE_CONTROLLER_LOOPS_H_
#define ESO_ENGINE_CONTROLLER_LOOPS_H_

//TODO: replace with EASTL
#include <vector>

#include "model/game/game.h"

namespace eso {

namespace loops {

//TODO: should this be a private loop, maintained by a loop controller that auto-runs the appropriate loops in a thread
//TODO: are there other SDL events we want to respond too, or should this just be for inputs?
void InputLoop(Game&);

void UpdateLoop(Game&);

void RenderLoop(Game&);

}

}

#endif /* ESO_ENGINE_CONTROLLER_LOOPS_H_ */