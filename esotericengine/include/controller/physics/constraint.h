#ifndef ESO_ENGINE_CONTROLLER_PHYSICS_MOMENTUM_H_
#define ESO_ENGINE_CONTROLLER_PHYSICS_MOMENTUM_H_

#include <map>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <tuple>

#include "model/entity/entity.h"

//Special hashing implementation to work for std::tuple
namespace std{
    namespace
    {
        // Code from boost
        // Reciprocal of the golden ratio helps spread entropy
        //     and handles duplicates.
        // See Mike Seymour in magic-numbers-in-boosthash-combine:
        //     https://stackoverflow.com/questions/4948780

        template <class T>
        inline void hash_combine(std::size_t& seed, T const& v)
        {
            seed ^= hash<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        // Recursive template code derived from Matthieu M.
        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
            HashValueImpl<Tuple, Index-1>::apply(seed, tuple);
            hash_combine(seed, get<Index>(tuple));
          }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple,0>
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
            hash_combine(seed, get<0>(tuple));
          }
        };
    }

    template <typename ... TT>
    struct hash<std::tuple<TT...>> 
    {
        size_t
        operator()(std::tuple<TT...> const& tt) const
        {                                              
            size_t seed = 0;                             
            HashValueImpl<std::tuple<TT...> >::apply(seed, tt);    
            return seed;                                 
        }                                              
    };
}

namespace eso {

//Reusible wrapper to keep track of which constraint system entity should exist (ignoring fixed entities)
struct Entity_ConstraintWrapper {
    Entity* _Entity = nullptr;
    short OuterIndex = -1;      //index determining which collision system this entity will be a part of
    short InnerIndex = -1;      //index determining location of entity WITHIN its CollisionSystem

    Entity_ConstraintWrapper() {}
    Entity_ConstraintWrapper(Entity* entity): _Entity(entity) {}
    
    Entity* operator->() { return _Entity; }
    Entity* get() { return _Entity; }
    bool IsValid() const { return _Entity != nullptr; }
};

struct ConstraintMember {
    Entity_ConstraintWrapper& MemberEntity;
    sf::Vector2<double> LinearJacobian;
    double AngularJacobian;

    ConstraintMember(Entity_ConstraintWrapper&, const sf::Vector2<double>&, double);
    ConstraintMember(Entity_ConstraintWrapper&, sf::Vector2<double>&&, double);
};

struct Constraint {
    ConstraintMember First;
    ConstraintMember Second;

    short GetIdsIndex() const;  //retrieves index of ids array -- contains outer index 

    //The following methods assume that a constraint can contain no more than 1 fixed entity
    bool HasFixed() const { return First.MemberEntity->IsFixed() || Second.MemberEntity->IsFixed(); }
    ConstraintMember* FetchFixed();     //return ptr to the only fixed membered; OR returns nullptr if no fixed present
    ConstraintMember* FetchNonFixed() { return (!First.MemberEntity->IsFixed()? &First : &Second ); }   //returns ptr to the FIRST non-fixed element found. If BOTH non-fixed, returns First member
};

/* There are some eccentric algorithms using sets and tuples -- will need to read comments below to to understand */
class ConstraintSystem {
private:
    std::vector<Constraint*> constraints_;
    std::unordered_map<unsigned int, Entity_ConstraintWrapper*> entity_map_;

    //used for flagging duplicate/redundant constraints
    //tuple contains: entity instanceId, entity jacobian-x, jacobian-y, jacobian-alpha, is used 2 or more times?, related fixed already accepted? (rest will be excluded)
    std::unordered_set<std::tuple<unsigned int,double,double,double,bool,bool>> non_fixed_tracking_set_;

    //not all fixed entities will be accepted into constraint system, so applicants are stored here
    //tuple contains: entity instanceId, application submitted two or more times?
    std::unordered_set<std::tuple<unsigned int,bool>> fixed_applicant_set_;

    //used for flagging duplicate pairs of fixed/non-fixed entities
    //tuple contains: entity instanceId, other entity instanceId
    //TODO: this may not be needed, but leaving here just in case...
    std::unordered_set<std::tuple<unsigned int,unsigned int>> entity_pairing_set_;

public:
    void OpenAppeal(Constraint*);     //submit constraint for system -- system will track relevant data which will help it decide whether it should be accepted
    bool ConcludeAppeal(Constraint*); //compel acceptance decision to be made about constraint based on on current data -- returns true if accepted into system
    void AddConstraint(Constraint*);  //action of adding constraint to system -- called by ConcludeAppeal if deemed acceptable

    std::vector<Constraint*>& RefConstraints() { return constraints_; }
    std::unordered_map<unsigned int, Entity_ConstraintWrapper*>& RefEntityMap() { return entity_map_; }

    unsigned short GetNumEntities() const { return entity_map_.size(); }
    unsigned short GetNumContraints() const { return constraints_.size(); }
    const std::vector<Constraint*>& GetConstraints() const { return constraints_; }
    const std::unordered_map<unsigned int, Entity_ConstraintWrapper*>& GetEntityMap() const { return entity_map_; }
    std::unordered_set<std::tuple<unsigned int,double,double,double,bool,bool>>& GetNonFixedTrackingSet() { return non_fixed_tracking_set_; }
    std::unordered_set<std::tuple<unsigned int,bool>>& GetFixedApplicantSet() { return fixed_applicant_set_; }
};

namespace constraint {

//Get reusible constraint wrapper in order to create new constraint
Entity_ConstraintWrapper& GetConstraintWrapper(std::shared_ptr<Entity>&, std::unordered_map<unsigned int, Entity_ConstraintWrapper>&);

//appends miscellaneous entity constraints to world list (excluding collisions)
void Generate(std::vector<std::shared_ptr<Entity>>&, std::vector<Constraint>&);

//solves list of constraints adding corrective forces to each involved entity
void Solve(std::vector<Constraint>&);

//Creates groupings of constraints that must be solved simultaneously based on entity pairings within constraints
void group_constraints(std::vector<Constraint>&, ConstraintSystem*, unsigned short& max_index);
void solve_system(ConstraintSystem&);   //generate system matrices using Armadillo, and solve

//Helper methods
void add_colliding_force(Collision&);
void calculate_impulse_force(Collision&); //TODO: look into how we need to implement this

}
    
}

#endif /* ESO_ENGINE_CONTROLLER_PHYSICS_MOMENTUM_H_ */