#ifndef ESO_ENGINE_CONTROLLER_PHYSICS_COLLISION_H_
#define ESO_ENGINE_CONTROLLER_PHYSICS_COLLISION_H_

#include <memory>
#include <unordered_map>

#include <SFML/System.hpp>

#include "model/entity/entity.h"
#include "model/game/shape/edge.h"
#include "controller/physics/constraint.h"

namespace eso {

enum class CollisionType {
    EdgeEdge,
    VertexEdge,
    VertexVertex
};

struct CollisionMember {
    std::shared_ptr<Entity>& MemberEntity;
    
    Edge const* MinEdge = nullptr;
    Edge const* MaxEdge = nullptr;
    
    Edge const* EdgeAlpha = nullptr;
    Edge const* EdgeBeta = nullptr;
    
    sf::Vector2<double> Min;  //In units, with respect to the shape center (entity position)
    sf::Vector2<double> Max;

    sf::Vector2<double> MinVelocity;
    sf::Vector2<double> MaxVelocity;

    sf::Vector2<double> ProspectPosition;

    bool IsVertex = false;
    
    CollisionMember(std::shared_ptr<Entity>&);
    CollisionMember(std::shared_ptr<Entity>&, Edge const*, const sf::Vector2<double>&, const sf::Vector2<double>&);
    CollisionMember(std::shared_ptr<Entity>&, Edge const*, Edge const*, const sf::Vector2<double>&, const sf::Vector2<double>&);

    CollisionMember(const CollisionMember&);
    CollisionMember(CollisionMember&&);
    CollisionMember& operator=(const CollisionMember&);
    CollisionMember& operator=(CollisionMember&&);


};

struct Collision {
    CollisionMember First;
    CollisionMember Second;
    CollisionType Type = CollisionType::EdgeEdge;
    
    sf::Vector2<double> UnitNormal;
    double Restitution;
    
    double Sweep;
    bool IsSolid;
    bool IsDirect;
    bool IsValid = true;

    bool PrimaryFirst = true;
    bool VertexFirst = true;

    Collision(CollisionMember&&, CollisionMember&&);
    Collision(CollisionMember&&, CollisionMember&&, double, bool);

    CollisionMember* FetchEdgeMember(); //returns A is IsVertex=false; else returns B
    CollisionMember* FetchVertexMember(); //returns A is IsVertex=true; else returns B 

    CollisionMember* FetchEdgeMemberOrNull(); //returns A is IsVertex=false; else return B if IsVertex=false; else returns nullptr
    CollisionMember* FetchVertexMemberOrNull(); //returns A is IsVertex=true; else return B if IsVertex=true; else returns nullptr

    const CollisionMember* FetchEdgeMember() const; //returns A is IsVertex=false; else returns B
    const CollisionMember* FetchVertexMember() const; //returns A is IsVertex=true; else returns B 

    const CollisionMember* FetchEdgeMemberOrNull() const; //returns A is IsVertex=false; else return B if IsVertex=false; else returns nullptr
    const CollisionMember* FetchVertexMemberOrNull() const; //returns A is IsVertex=true; else return B if IsVertex=true; else returns nullptr
};

//TODO: use this to store information about collisions that should probably not be directly in entity class...
struct Entity_CollisionWrapper {

};

namespace collision {

//Main collision detection workflow
void Execute(std::vector<std::shared_ptr<Entity>>&, std::vector<Collision>&);    //for entity vector
void Execute(std::vector<std::shared_ptr<Entity>>&, unsigned short, unsigned short, unsigned short);    //for entity grid
void ExecuteAdvanced(std::vector<std::shared_ptr<Entity>>&);     //for entity vector, with complex collisions
void ExecuteAdvanced(std::vector<std::shared_ptr<Entity>>&, unsigned short, unsigned short, unsigned short); //for entity grid, with complex collisions
void SetCollisionProperties(Collision*);
void AppendConstraint(Collision&, std::vector<Constraint>&, std::unordered_map<unsigned int, Entity_ConstraintWrapper>&);    //appends constraint related to collision onto vector, assuming collision generates a constrain (moving toward)

//Collision refinement workflow -- passes through vector of generated collisions
void Refine(std::vector<Collision>&);

//Reusible methods also used in collision detection process
bool IsOverlappingDomain(const Edge&, const sf::Vector2<double>&, const Edge&, const sf::Vector2<double>&); //Returns true if a_ and b_ of current and other overlap at some point
bool IsOverlappingDomain(double, double, double, double); //Returns two if x-values of endpoints (for two edges) overlap
bool IsMovingToward(const Collision&);  //Returns true if the two entities involved are moving toward each other
bool IsValidByIndex(const std::vector<Collision>&, unsigned int);    //returns true if collision is valid (exists because validity must be checked during forward declaration)
sf::Vector2<double> GetLinearIntersection(const LinearEdge&, const LinearEdge&);

//Non-reusible helper methods for collision process
void update_border_flags(std::shared_ptr<Entity>&, unsigned short, unsigned short, unsigned short);     //update flags used to mark collision checking between two entities in multiple sector -- avoids duplicate checks

//AABB helper methods
bool direct_rest_aabb(std::shared_ptr<Entity>&, std::shared_ptr<Entity>&, Collision*); //Both entities are already touching at edges; set min-max data without sweeping
bool direct_sweep_aabb(std::shared_ptr<Entity>&, std::shared_ptr<Entity>&, Collision*); //Sets prospect positions of both entities, adds edge, min, max data to collision, returns true if valid sweep or miss
bool indirect_sweep_aabb(std::shared_ptr<Entity>&, std::shared_ptr<Entity>&, Collision*); //Sets prospect position for primary entity, adds, edge, min, max data to collision, returns true if valid indirect sweep 

void calculate_collision_min_max_ee(Collision*);
void calculate_collision_min_max_ve(Collision*);
void calculate_collision_min_max_vv(Collision*);
void set_edge_alpha_beta_ve(CollisionMember&, Edge const*, Edge const*, Edge const*);
/* TODO: maybe give this a more descriptive name */void process_collision(std::shared_ptr<Entity>&, std::vector<Collision>&, unsigned int); //modify and assign collision to entity's prim/second/neither based on it's properties

}

}

#endif /* ESO_ENGINE_CONTROLLER_PHYSICS_COLLISION_H_ */