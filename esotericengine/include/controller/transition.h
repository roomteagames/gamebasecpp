#ifndef ESO_ENGINE_CONTROLLER_TRANSITION_H_
#define ESO_ENGINE_CONTROLLER_TRANSITION_H_

#include <tuple>
#include <thread>

#include "model/game/game.h"

namespace eso {

namespace transition {

void EnterNextState(Game&);

}

}

#endif /* ESO_ENGINE_CONTROLLER_TRANSITION_H_ */