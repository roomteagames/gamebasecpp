#ifndef ESO_ENGINE_CONTROLLER_LOADING_H_
#define ESO_ENGINE_CONTROLLER_LOADING_H_

#include <functional>
#include <thread>
#include <tuple>
#include <unordered_set>
#include <vector>

#include "model/game/world.h"
#include "view/entity/sprite_view.h"
#include "asset/asset_collection.h"
#include "util/counted_array.tpp"
#include "view/entity/sprite_view.h"
#include "constants.h"

namespace eso {

namespace loading {

using SpritesheetLoader = std::function<void(const std::string&, VisualCollection&, const AssetProps&)>;
using WorldLoader = std::function<void(const std::string&, World&, AssetCollection&, const AssetProps&)>;

/* Texture loading */
extern std::unordered_set<std::string> requested_textures;

void RequestTexture(const std::string& texture_name);
void LoadTextures(VisualCollection&, const AssetProps&);

// loads spritesheet from binary file stored in directory: AssetProps.SpritesheetDirectory
// default loader, but can be overriden by game dev by passing in a different loader fn
void esoteric_load_spritesheets(const std::string& texture_name, VisualCollection&, const AssetProps&);
void LoadSpritesheets(VisualCollection&, const AssetProps&, SpritesheetLoader = esoteric_load_spritesheets);


/* World loading */

extern CountedArray_Replace<std::thread, ESO_VAR_WORLD_LOADING_THREAD_MAX> running_world_loads;
extern std::vector<std::function<void(unsigned int)>> pending_world_loads;

// loads world from binary file stored in directory: AssetProps.WorldDirectory
// default loader, but can be overriden by game dev by passing in a different loader fn
void esoteric_load_world(const std::string& world_name, World&, AssetCollection&, const AssetProps&);

// loads world and all required assets -- world and spritesheets synchronously; and textures, music and sounds possibly asynchronously
void LoadWorldAssets(const std::string& world_name, World&, AssetCollection&, const AssetProps&, WorldLoader = esoteric_load_world, SpritesheetLoader = esoteric_load_spritesheets);

// queues world asset loading so it can occur on a separate thread
void QueueLoadWorldAssets(const std::string& world_name, World&, AssetCollection&, const AssetProps&, WorldLoader = esoteric_load_world, SpritesheetLoader = esoteric_load_spritesheets);

//Update loop should call this every frame or couple of frames to swap running for pending threads
void UpdateLoadingThreads();

}

}

#endif /* ESO_ENGINE_CONTROLLER_LOADING_H_ */