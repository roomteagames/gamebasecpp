#ifndef ESO_ENGINE_CONTROLLER_CONSTRUCTION_H_
#define ESO_ENGINE_CONTROLLER_CONSTRUCTION_H_

#include <memory>

#include "asset/asset_collection.h"
#include "model/entity/entity.h"
#include "model/game/shape/shape.h"
#include "model/game/world.h"

namespace eso {

namespace construction {

void BuildWorld(World&, AssetCollection&, const AssetProps&);

template <typename T, typename... TArgs>
void MakeEntity(World&, AssetCollection&, TArgs&&...);


//Template implementations

template <typename T, typename... TArgs>
void MakeEntity(World& world, AssetCollection& assets, TArgs&&... args) {
    world.GetEntities().push_back(std::make_shared<T>(std::forward<TArgs>(args)...));
    std::shared_ptr<Entity>& entity = world.GetEntities().back();

    const Shape& shape = *assets.Physics.GetShape(entity->GetShapeIndex());
    if (entity->IsUniqueShape()) {
        world.GetUniqueShapes().push_back(shape);   //copies shape into unique_shapes -- copying shape auto makes it mutable
    }

    for (unsigned short i = 0; i < shape.GetEdges().size(); i++) {
        entity->Shape().EdgeMaterials.emplace_back(
            assets.Physics.GetMaterial(entity->GetOptions().MaterialIndices[i])
        );
    }
}

}
    
}

#endif /* ESO_ENGINE_CONTROLLER_CONSTRUCTION_H_ */