#ifndef ESO_ENGINE_CONTROLLER_TIMING_H_
#define ESO_ENGINE_CONTROLLER_TIMING_H_

namespace eso {

namespace timing {

unsigned long GetSystemTime();
unsigned long GetSystemTimeNanos();

}

class LoopTimer {
protected:
    unsigned short frame_length_;
    unsigned long start_time_;
    unsigned long iteration_count_ = 0;

public:
    LoopTimer(unsigned short frame_rate);

    unsigned short GetFrameLength() const { return frame_length_; }
    unsigned long GetStartTime() const { return start_time_; }
    unsigned long GetIterationCount() const { return iteration_count_; }
    
    void ResetFramerate(unsigned short);    // Resets start_time and iteration count, and recalculates framelength
    virtual void operator()() = 0;
};

//Use as a functor to to enforce looping that always starts frames at the frame begin time. Skips frames if need be
class LazyLoopTimer : public LoopTimer {
public:
    LazyLoopTimer(unsigned short);
    
    void operator()() override;
};

//Use as a functor to enforce frames that attempt to never skip any frames. If count is behind time, will not sleep and try to catch up
class ActiveLoopTimer : public LoopTimer {
public:
    ActiveLoopTimer(unsigned short);
    
    void operator()() override;
};

class MilliTicker {
private:
    unsigned long start_time_;

public:
    MilliTicker();
    void Reset();
    unsigned long operator()();
};

class NanoTicker {
private:
    unsigned long start_time_;

public:
    NanoTicker();
    void Reset();
    unsigned long operator()();
};

}

#endif /* ESO_ENGINE_CONTROLLER_TIMING_H_ */