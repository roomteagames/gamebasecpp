#ifndef ESO_ENGINE_CONTROLLER_STARTUP_H_
#define ESO_ENGINE_CONTROLLER_STARTUP_H_

#include "config.h"
#include "model/game/game.h"
#include "model/game/input_model.h"
#include "asset/asset_collection.h"



namespace eso {

namespace startup {

//defined by the game developer
void Init(PropsCollection&, InputConfig&, AssetCollection&, std::vector<GameState>&, short& starting_state);

void Execute(Game&);

}

}

#endif /* ESO_ENGINE_CONTROLLER_STARTUP_H_ */