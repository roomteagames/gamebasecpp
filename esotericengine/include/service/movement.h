#ifndef ESO_ENGINE_SERVICE_MOVEMENT_H_
#define ESO_ENGINE_SERVICE_MOVEMENT_H_

#include "model/entity/entity.h"

namespace eso {

struct WalkScaleOptions {
    unsigned short FeetIndex = 2;       // which edge of entity's shape is considered the "feet"
    unsigned short WalkFrequency = 5;   // how many frames in between each step (account for leg mvmt)
    double ForceMultiplier = 1;          // how much parallel force is exerted by feet on "floor" -- this is a scalar multiplying entity weight (>= 1)
    double LegLengthRatio = 0.3;         // what percentage of distance from midpoint of feed to COM are the legs? 1.0 means legs go all the way to COM. 0.5 means legs go halfway to COM
    double LegExtention = 0.5f;          // how large are the steps being taken wrt leg -- 1.0 = splits; 0.1 = very small steps; 0.5 = 45 degree steps
    //TODO: add something that accounts for sliding threshold: after how much "loss of control" can you no longer take steps
};

//TODO: how do we gather force from this???
struct WalkSpeedOptions {
    unsigned short FeetIndex = 2;
    unsigned short WalkFrequency = 5;
    unsigned short TargetWalkSpeed = 5; // overall speed that entity should travel when walking
}

namespace move {

// Returns true if there is a collision between feet and another edge with enough parallel surface to be able to walk
bool CanWalk(const std::shared_ptr<Entity>&, unsigned short feet_index);

// Performs walk by updating entity's NetForce, if walk is possible, and updates walk_state frame counter
// Walk state is a required variable on the entity subclass
void Walk(const WalkScaleOptions&, std::shared_ptr<Entity>&, unsigned short& walk_state);

void Walk(const WalkSpeedOptions&, std::shared_ptr<Entity>&, unsigned short& walk_state);


//bool CanJump(const std::shared_ptr<>)

}

#endif /* ESO_ENGINE_SERVICE_MOVEMENT_H_ */