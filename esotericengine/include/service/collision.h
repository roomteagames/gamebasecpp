#ifndef ESO_ENGINE_SERVICE_COLLISION_H_
#define ESO_ENGINE_SERVICE_COLLISION_H_

#include <functional>
#include <memory>

#include "controller/physics/collision.h"
#include "model/game/game.h"
#include "model/entity/entity.h"
#include "model/game/shape/edge.h"



/** All files in the service folder contain reusable logic that can be used by the client developers. These methods are not utilized by the engine */

namespace eso {

namespace collision {

bool IsCollidingWithInstance(const Entity*, const std::vector<Collision>&, unsigned int);
bool IsCollidingWithType(const Entity*, const std::vector<Collision>&, unsigned int);

//TODO: need to have variations for first of ANY vs first of ALL -- also need perhaps method to run for collisions with ANYTHING AT ALL, not any specific ids

void ExecuteForFirstCollisionWithTypes(Entity*, const std::vector<Collision>&, const std::initializer_list<unsigned int>&, std::function<void(Entity*, Entity*, const Collision&)>);
void ExecuteForAllCollisionsWithTypes(Entity*, const std::vector<Collision>&, const std::initializer_list<unsigned int>&, std::function<void(Entity*, Entity*, const Collision&)>);
    
void ExecuteForFirstCollisionWithInstances(Entity*, const std::vector<Collision>&, const std::initializer_list<unsigned int>&, std::function<void(Entity*, Entity*, const Collision&)>);
void ExecuteForAllCollisionsWithInstances(Entity*, const std::vector<Collision>&, const std::initializer_list<unsigned int>&, std::function<void(Entity*, Entity*, const Collision&)>);

//Allow access to certain fields with Collision and CollisionMember from within an entity subclass context where Collision ONLY forward declared
const Edge& GetCollidingEdge(Entity*, const Collision&);
const sf::Vector2<double>& GetCollidingEdgeMin(Entity*, const Collision&);
const sf::Vector2<double>& GetCollidingEdgeMax(Entity*, const Collision&);

}

}

#endif /* ESO_ENGINE_SERVICE_COLLISION_H_ */