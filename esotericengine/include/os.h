#ifndef ESO_ENGINE_OS_H_
#define ESO_ENGINE_OS_H_

//Set compile-time constants based on OS
#if defined __APPLE__ && defined __MACH__
    #define OS_IS_MAC
#elif defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    #define OS_IS_WINDOWS
#else
    #define OS_IS_LINUX
#endif

//Define Mac-specific constants / imports
#ifdef OS_IS_MAC    
    //Block for getting absolute file path on Mac, since relative file path loading of resources is not supported
    #include <unistd.h>
    #include <mach-o/dyld.h>
    #include <errno.h>

    //Define font location paths
    #define OS_FONTS_DIRECTORY "/System/Library/Fonts"
    #define OS_DEFAULT_FONT "Keyboard"

#endif /* IS_OS_X */

//Define Windows-specific constants / imports
#ifdef OS_IS_WINDOWS
    //Define font location paths
    //TODO: lookup best to represent paths on Windows
#endif /* IS_WINDOWS */

//Define Linux-specific constants / imports
#ifdef OS_IS_LINUX
    #include <unistd.h>
    #include <errno.h>

    //Define font location paths
    #define OS_FONTS_DIRECTORY "/usr/share/fonts/truetype"
    #define OS_DEFAULT_FONT "liberation/LiberationSans-Regular"
#endif /* IS_LINUX */

#endif /* ESO_ENGINE_OS_H_ */