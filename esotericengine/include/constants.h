#ifndef ESO_ENGINE_CONSTANTS_H_
#define ESO_ENGINE_CONSTANTS_H_

#include <string>

#include <SFML/System.hpp>

#include "os.h"
#include "util/file_utils.h"


/////////////////////////////////COMPILE-TIME VARIABLES//////////////////////////////////////////
/** Default values for preprocessor variables -- usually related to array sizes */
/** Can be overridden by adding definition in CMakeLists.txt*/


/***** CAPS FOR STATIC MEMORY USAGE *****/

/* Max number of shape presets that can be stored in memory */
#ifndef ESO_VAR_SHAPE_COUNT_MAX
	#define ESO_VAR_SHAPE_COUNT_MAX 50
#endif /* ESO_VAR_SHAPE_COUNT_MAX */

/* Max number of material presets that can be stored in memory */
#ifndef ESO_VAR_MATERIAL_COUNT_MAX
	#define ESO_VAR_MATERIAL_COUNT_MAX 10
#endif /* ESO_VAR_MATERIAL_COUNT_MAX */

/* Max number of edge functions presets that can be stored in memory */
#ifndef ESO_VAR_EDGE_FUNCTION_COUNT_MAX
	#define ESO_VAR_EDGE_FUNCTION_COUNT_MAX 5
#endif /* ESO_VAR_EDGE_FUNCTION_COUNT_MAX */

/* Max number of textures that can be stored in memory before being purged */
#ifndef ESO_VAR_TEXTURE_QUEUE_SIZE
	#define ESO_VAR_TEXTURE_QUEUE_SIZE 200
#endif /* ESO_VAR_TEXTURE_QUEUE_SIZE */

/* Max number of textures that can be stored in memory before being purged */
#ifndef ESO_VAR_SPRITESHEET_QUEUE_SIZE
	#define ESO_VAR_SPRITESHEET_QUEUE_SIZE 2000
#endif /* ESO_VAR_SPRITESHEET_QUEUE_SIZE */

/* Max number of loading threads that can exist simultaneously */
#ifndef ESO_VAR_WORLD_LOADING_THREAD_MAX
	#define ESO_VAR_WORLD_LOADING_THREAD_MAX 5
#endif /* ESO_VAR_WORLD_LOADING_THREAD_MAX */

/* Max number of materials/edges that can be associated with a particular entity's shape */
#ifndef ESO_VAR_SHAPE_EDGE_COUNT_MAX
	#define ESO_VAR_SHAPE_EDGE_COUNT_MAX 6
#endif /* ESO_VAR_SHAPE_EDGE_COUNT_MAX */

/* Max number of primary collisions that can be stored for a particular entity */
#ifndef ESO_VAR_PRIMARY_COLLISION_COUNT_MAX
	#define ESO_VAR_PRIMARY_COLLISION_COUNT_MAX 5
#endif /* ESO_VAR_PRIMARY_COLLISION_COUNT_MAX */

/* Max number of secondary collisions that can be stored for a particular entity */
#ifndef ESO_VAR_SECONDARY_COLLISION_COUNT_MAX
	#define ESO_VAR_SECONDARY_COLLISION_COUNT_MAX 5
#endif /* ESO_VAR_SECONDARY_COLLISION_COUNT_MAX */

/* Max number of separate constraint systems that can be stored in memory */
#ifndef ESO_VAR_CONSTRAINT_SYSTEM_COUNT_MAX
	#define ESO_VAR_CONSTRAINT_SYSTEM_COUNT_MAX 20
#endif /* ESO_VAR_CONSTRAINT_SYSTEM_COUNT_MAX */

/* Max number of params to support when deserializing an entity */
#ifndef ESO_VAR_DESERIALIZATION_PARAM_COUNT_MAX
	#define ESO_VAR_DESERIALIZATION_PARAM_COUNT_MAX 10
#endif /* ESO_VAR_DESERIALIZATION_PARAM_COUNT_MAX */


/* Max number of outward forces that can be exerted at once for a particular entity */
#ifndef ESO_VAR_OUTWARD_FORCE_MAX
	#define ESO_VAR_OUTWARD_FORCE_MAX 2
#endif /* ESO_VAR_OUTWARD_FORCE_MAX */

/* Method of determining lambda for constraint resolution */
/*
	0 -- pure matrix multiplication 
	1 -- linear equation solver
*/
#ifndef ESO_VAR_CONSTRAINT_SOLVER_TYPE
	#define ESO_VAR_CONSTRAINT_SOLVER_TYPE 0
#endif


///////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////NAMESPACE GLOBAL VARIABLES///////////////////////////////

namespace eso {

namespace system {

//Executable Location
extern std::string ExecutableDirectory;

//Asset Locations - May vary by OS
extern std::string FontsDirectory;
extern std::string FontsExtension;
extern std::string DefaultFontName;
//TODO: add other OS-specific paths...

}

namespace engine {

//World sizing
extern unsigned short BlockSize;
extern unsigned short SectorSize;

//Physics
extern sf::Vector2<double> Gravity;
extern sf::Vector2<double> GetGravity();   //used to return copy of Gravity (as r-value)

//Debug
extern unsigned int DebugCollisionTrackedInstanceID;
extern unsigned int EdgeFunctionID;

}

}

///////////////////////////////////////////////////////////////////////////////////////




#endif /* ESO_ENGINE_CONSTANTS_H_ */