#ifndef ESO_ENGINE_MODEL_ENTITY_TIMELINE_H_
#define ESO_ENGINE_MODEL_ENTITY_TIMELINE_H_

#include <iostream>

#include "model/entity/entity.h"
#include "constants.h"

namespace eso {

template <unsigned short N>
class Timeline : public Entity {
private:
    unsigned short current_state_ = 0;
    unsigned short state_progress_ = 0;
    bool is_finished_ = false;

protected:
    unsigned long delays_[N];
    virtual void serialize_into(std::ostream&) const override;
    virtual void on_state_change() = 0;

public:
    Timeline<N>(unsigned int instance_id, unsigned int class_id);
    // ~Timeline();
    // Timeline(const Timeline&) = delete;
    // Timeline(Timeline&&);
    // Timeline& operator =(const Timeline&) = delete;
    // Timeline& operator =(Timeline&&);

    void Update(Game&) override;
    unsigned short GetState() const { return current_state_; }
    unsigned short GetStateProgress() const { return state_progress_; }
    bool IsFinished() const { return is_finished_; }
    bool ChangedState() const { return state_progress_ == 0; }
};

template <unsigned short N>
Timeline<N>::Timeline(unsigned int instance_id, unsigned int class_id):
        Entity(instance_id, class_id, sf::Vector2<double>(0,0), Shape()) {}

// Timeline::~Timeline() {
//     if (delays_) {
//         delete[] delays_;
//     }
// }

// Timeline::Timeline(Timeline&& other): delays_(other.delays_) {
//     other.delays_ = nullptr;
// }

// Timeline& Timeline::operator =(Timeline&& other) {
//     delete[] delays_;
//     delays_ = other.delays_;
//     other.delays_ = nullptr;
//     return *this;
// }

template <unsigned short N>
void Timeline<N>::serialize_into(std::ostream& os) const {
    Entity::serialize_into(os);
    os << ", Timeline<" << N << ">::current_state_ = " << current_state_ <<
            ", Timeline<" << N << ">::state_progress_ = " << state_progress_ <<
            ", Timeline<" << N << ">::is_finished_ = " << is_finished_;
}

template <unsigned short N>
void Timeline<N>::Update(Game& app) {
    if (!IsFinished()) {
        if (state_progress_ == delays_[current_state_]) {
            current_state_++;
            state_progress_ = 0;
            is_finished_ = (current_state_ >= N);
            on_state_change();
        } else {
            state_progress_++;
        }
    }
    //Subclass for custom behavior based on state
}



}

#endif /* ESO_ENGINE_MODEL_ENTITY_TIMELINE_H_ */