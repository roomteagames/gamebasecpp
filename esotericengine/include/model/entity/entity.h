#ifndef ESO_ENGINE_MODEL_ENTITY_H_
#define ESO_ENGINE_MODEL_ENTITY_H_

#include <iostream>
#include <vector>
#include <array>

#include <SFML/Graphics.hpp>

#include "model/game/shape/shape.h"
#include "model/game/physics.h"
#include "util/counted_array.tpp"
#include "util/iterator.tpp"
#include "constants.h"

//TEMP
#include <iostream>

namespace eso {

// Forward declaration to avoid circular dependencies
struct Game;
class World;
struct Collision;

namespace collision {
bool IsValidByIndex(const std::vector<Collision>&, unsigned int);
}

enum class CollisionMode {
    None,
    Solid,
    Ghost
};

// Wrapper allowing variadic number of provided material indices
class MaterialIndices_Wrapper {
private:
    std::array<int, ESO_VAR_SHAPE_EDGE_COUNT_MAX> indices_;

public:
    template <typename... IntVals>
    MaterialIndices_Wrapper(IntVals&&...);

    const iterator<const int> begin() const { return &indices_[0]; }
    const iterator<const int> end() const { return &indices_[ESO_VAR_SHAPE_EDGE_COUNT_MAX + 1]; }
    int operator[](unsigned int index) const { return indices_[index]; }
};

// Wrapper allowing variadic number of provided serialization types
class SerializationTypes_Wrapper {
private:
    std::array<const char, ESO_VAR_DESERIALIZATION_PARAM_COUNT_MAX> types_;

public:
    template <typename... CharVals>
    SerializationTypes_Wrapper(CharVals&&...);

    const iterator<const char> begin() const { return &types_[0]; }
    const iterator<const char> end() const { return &types_[ESO_VAR_DESERIALIZATION_PARAM_COUNT_MAX + 1]; }
};

//misc entity flags that must come before material indices parameter pack
struct EntityOptions {
    //Physics Options
    CollisionMode Mode = CollisionMode::Solid;      // whether collision detection should consider entity (Solid), or is collisions should be checked but no physics (ghost), or nothing at all (none)
    bool Mobile = false;                             // whether or not entity is affected by external linear forces
    bool HasRotation = false;                       // whether or not rotation can occur; if this is false, can reference Shape without making a copy

    //Shape Options
    const unsigned short ShapeIndex = 0;  // index for Shape to be referenced when entity is constructed
    MaterialIndices_Wrapper MaterialIndices = {0}; // indices for Materials to be reference by edge when entity is constructed
    //NOTE: material_indices_ has to be int[]; perfect forwarding will NOT work with unsigned short[]
    const bool UniqueShape = false;       // whether Shape must be copied per instance


    //Walk Options
    //TODO: decide whether this should go in movement service file, or here!
    unsigned short FootIndex = 2;       // index of side (of entity's shape) that represents foot
    unsigned short WalkCadence = 10;    // number of frames between next step (TODO: figure out how to modify or store multiple cadences)

    EntityOptions(CollisionMode mode=CollisionMode::Solid, bool mobile=true, unsigned short shape_index=0, MaterialIndices_Wrapper&& material_indices={0}, bool unique_shape=false, bool has_rotation=false);
};

class Entity {
public:
    enum Status {
        UNDEFINED,
        ACTIVE_VISIBLE,
        ACTIVE_INVISIBLE,
        INACTIVE,
        DEAD,
        INACTIVE_DEAD
    };

protected:
    const unsigned int instance_id_, class_id_;
    bool destroyed_ = false;
    
    sf::Vector2<double> position_; //center of bounding box
    sf::Vector2<double> initial_position_; //initial center of bounding box
    sf::Vector2<double> prospect_position_; //running/calculated position where entity will go due to collision sweeps
    sf::Vector2<double> toggle_velocity_;  //velocity to be added to current frame and removed at end of frame -- TODO: depcrecate this

    double orientation_ = 0; //in degrees, counterclockwise
    
    //z-axis fields - TODO: in a future release, merge this concept into the position vector (must be converted to 3D)
    int z_min_ = 0;
    int z_depth_ = 1;

    //Definition of shape and physics
    EntityShape shape_;                     // wrapper for entity's shape and material
    Physics physics_;                       // current status of entity velocity and acceleration
    Physics initial_physics_;               // physics to be applied to entity on creation
    BoundingBox movement_system_;           // AABB encasing entity around both its current position and projected position based on velocity
    
    //Const Shape/Material properties passed in to ctor
    EntityOptions options_;     //contain constant options pass into ctor

    //Used for memory/render management
    Status status_ = Status::ACTIVE_VISIBLE;    //TODO: urgent: default to UNDEFINED -- to be managed based on location from camera
    bool sector_border_right_ = false;
    bool sector_border_down_ = false;
    const char deserialize_types_[ESO_VAR_DESERIALIZATION_PARAM_COUNT_MAX] = {'z', 'z', 'z', 'z', 'z'}; // field types for deserialization

    //Collision reporting
    int primary_collision_indexes_[ESO_VAR_PRIMARY_COLLISION_COUNT_MAX];
    int secondary_collision_indexes_[ESO_VAR_SECONDARY_COLLISION_COUNT_MAX];
    unsigned short primary_collision_count_ = 0;
    unsigned short secondary_collision_count_ = 0;
    double primary_sweep_ = 1;

    virtual void serialize_into(std::ostream&) const;
    
public:
    template <typename T, typename... IntVals>
    Entity(unsigned int instance_id, unsigned int class_id, T&& position, EntityOptions options);
    
    template <typename T, typename U, typename... IntVals>
    Entity(unsigned int instance_id, unsigned int class_id, T&& position, U&& physics, EntityOptions options);
    
    virtual ~Entity() {}

    unsigned int GetInstanceId() const { return instance_id_; }
    unsigned int GetClassId() const { return class_id_; }

    //Refs (non-const)
    double& X() { return position_.x; }
    double& Y() { return position_.y; }
    int& ZMin() { return z_min_; }
    int& ZDepth() { return z_depth_; }
    double& Orientation() { return orientation_; }
    
    //Refs, allowing field to be modified
    CollisionMode& OptionsMode() { return options_.Mode; }
    bool& Mobile() { return options_.Mobile; }
    bool& SectorBorderRight() { return sector_border_right_; }
    bool& SectorBorderDown() { return sector_border_down_; }
    sf::Vector2<double>& Position() { return position_; }
    EntityShape& Shape() { return shape_; }
    Physics& RefPhysics() { return physics_; }
    Status& RefStatus() { return status_; }

    //Const getters
    int GetX() const { return position_.x; }
    int GetY() const { return position_.y; }
    int GetZMin() const { return z_min_; }
    int GetZDepth() const { return z_depth_; }
    unsigned int GetWidth() const { return shape_.ShapeDefinition->GetBoundingBox().Width; }
    unsigned int GetHeight() const { return shape_.ShapeDefinition->GetBoundingBox().Height; }
    double GetOrientation() const { return orientation_; }
    double GetOrientationDegrees() const { return orientation_ * 180 / M_PI; }
    unsigned short GetShapeIndex() const { return options_.ShapeIndex; }
    bool IsUniqueShape() const { return options_.UniqueShape; }
    const EntityOptions& GetOptions() const { return options_; }
    unsigned short GetNumEdges() const { return shape_.ShapeDefinition->GetEdges().size(); }    //this will break if ShapeDefinition is null
    const int* GetPrimaryCollisionIndexes() const { return primary_collision_indexes_; }
    const int* GetSecondaryCollisionIndexes() const { return secondary_collision_indexes_; }
    unsigned short GetPrimaryCollisionCount() const { return primary_collision_count_; }
    unsigned short GetSecondaryCollisionCount() const { return secondary_collision_count_; }
    const sf::Vector2<double>& GetPosition() const { return position_; }
    BoundingBox GetBoundingBox() const;
    const sf::Vector2<double>& GetProspectPosition() const { return prospect_position_; }
    double GetPrimarySweep() const { return primary_sweep_; }
    sf::Vector2<double> GetBoundingMin() const { return shape_.ShapeDefinition->GetBoundingBox().Min + position_; }
    sf::Vector2<double> GetBoundingSecond() const { return shape_.ShapeDefinition->GetBoundingBox().Second + position_; }
    sf::Vector2<double> GetBoundingMax() const { return shape_.ShapeDefinition->GetBoundingBox().Max + position_; }
    sf::Vector2<double> GetBoundingFourth() const { return shape_.ShapeDefinition->GetBoundingBox().Fourth + position_; }
    const EntityShape& GetShape() const { return shape_; }
    const Physics& GetPhysics() const { return physics_; }
    const BoundingBox& GetMovementSystem() const { return movement_system_; }
    bool IsSolid() const { return options_.Mode == CollisionMode::Solid; }
    bool IsGhostCollision() const { return options_.Mode == CollisionMode::Ghost; }
    bool IsNoneCollision() const { return options_.Mode == CollisionMode::None; }
    CollisionMode GetOptionsMode() const { return options_.Mode; }
    bool IsMobile() const { return options_.Mobile; }
    bool HasRotation() const { return options_.HasRotation; }
    bool IsFixed() const { return !options_.Mobile && !options_.HasRotation; }
    bool IsSectorBorderRight() const { return sector_border_right_; }
    bool IsSectorBorderDown() const { return sector_border_down_; }
    bool IsDestroyed() const { return destroyed_; }
    Status GetStatus() const { return status_; }
    /* debug purposes */ const sf::Vector2<double>& GetToggleVelocity() const { return toggle_velocity_; }

    //Same logic as IsOverlap in BoundingBox, but factors in entity position -- TODO: remove the following: and allows for possible position override -- needed for SweepAlgorithm
    //TODO: change Entity& param to const!
    bool IsOverlapBoundingBox(const Entity&, sf::Vector2<double>* = nullptr, sf::Vector2<double>* = nullptr) const;
    bool IsOverlapBoundingBoxProspect(const Entity&) const;

    //Setters
    void SetPosition(const sf::Vector2<double> v) { position_ = v; }
    void SetProspectPosition(const sf::Vector2<double> v) { prospect_position_ = v; }
    void SetVelocity(const sf::Vector2<double> v);
    void SetPrimarySweep(double s) { primary_sweep_ = s; }
    void ApplyForce(const sf::Vector2<double> f);
    void ApplyTorque(double t);
    void SetToggleVelocity(const sf::Vector2<double> tv) { toggle_velocity_ = tv; }

    //Updaters -- public access, but should only be called by engine (update loop) -- TODO: should we add a double underscore in front of name?
    void __InitLocation();
    void UpdateMovementSystem();
    void UpdatePosition();
    void ReplacePositionWithProspect() { position_ = prospect_position_; }
    void UpdateAcceleration();
    void UpdateVelocity();
    
    void UpdateToggleVelocity();
    void RevertToggleVelocity();
    
    void ClearAcceleration() { physics_.Acceleration = {0,0}; physics_.RotationalAcceleration = 0; } //TODO: should this also clear rotational acceleration??
    void ClearForces() { physics_.NetForce = {0,0}; physics_.NetTorque = 0; }
    
    bool PushPrimaryCollisionIndex(unsigned int);
    bool PushPrimaryCollisionIndexIfNotPresent(unsigned int);
    bool PushSecondaryCollisionIndexIfNotPresent(unsigned int, const std::vector<Collision>&);
    void ClearPrimaryCollisionIndexes();
    void ClearSecondaryCollisionIndexes();

    virtual void Update(Game&, World&) {}

    virtual void Reset(const Game&);   //resets to initial position and physics

    virtual bool OnCollision(Entity* other, Game&); //not receiving Collision object because it's not fully initialized at this point

    friend std::ostream& operator<<(std::ostream&, const Entity&);
};

template <typename T, typename... IntVals>
Entity::Entity(unsigned int instance_id, unsigned int class_id, T&& position, EntityOptions options):
        instance_id_(instance_id),
        class_id_(class_id),
        initial_position_(position),    //copies position vector
        prospect_position_(position),   //copies position vector
        position_(std::forward<T>(position)),
        options_(std::move(options))
{}

template <typename T, typename U, typename... IntVals>
Entity::Entity(unsigned int instance_id, unsigned int class_id, T&& position, U&& physics, EntityOptions options):
        instance_id_(instance_id),
        class_id_(class_id),
        initial_position_(position),    //copies position vector
        prospect_position_(position),   //copies position vector
        position_(std::forward<T>(position)),
        initial_physics_(physics),      //copies physics
        physics_(std::forward<U>(physics)),
        options_(std::move(options))
{}

template<typename... IntVals>
MaterialIndices_Wrapper::MaterialIndices_Wrapper(IntVals&&... indices):
        indices_{std::forward<IntVals>(indices)...}
{}

template<typename... CharVals>
SerializationTypes_Wrapper::SerializationTypes_Wrapper(CharVals&&... types):
        types_{std::forward<CharVals>(types)...}
{}

}

#endif /* ESO_ENGINE_MODEL_ENTITY_H_ */