#ifndef ESO_ENGINE_MODEL_GAME_STATE_H_
#define ESO_ENGINE_MODEL_GAME_STATE_H_

#include <vector>

#include <SFML/Window.hpp>

#include "config.h"
#include "asset/asset_collection.h"
#include "model/game/world.h"
#include "util/showcase.tpp"

namespace eso {

const unsigned short kDefaultVolatileWorldsSize = 2;

struct Game;    //avoid circular dependencies

class GameState {
private:
    bool is_initialized;

    // List of worlds that always remain in memory
    std::vector<std::unique_ptr<World>> permanent_worlds_;

    // Showcase of worlds where only so many can exist in memory at a time
    Showcase<std::unique_ptr<World>> volatile_worlds_;

    // List of current worlds (references) that are being displayed/handled -- includes permanent and volatile worlds
    std::vector<std::reference_wrapper<World>> current_worlds_;

    //TODO: add list of preloaded permanent worlds that will be loaded through the same means -- that way adding any world goes through the same process
    // std::vector<unsigned int> preload_init_permanent_world_ids_;
    //TODO: do we need preload_deferred_permanent_world_ids_;
    
    // List of volatile world names that are to be loaded, added to volatile worlds, and added to current worlds when state is freshly entered
    std::vector<unsigned int> preload_init_volatile_world_ids_;

    // List of volatile world names that are to be loaded and added to volatile worlds (but NOT added to current worlds) when state is freshly entered
    std::vector<unsigned int> preload_deferred_volatile_world_ids_;

public:
    GameState();
    GameState(unsigned short num_volatile_worlds);

//Used after preload worlds have ALREADY been loaded
    void Init(const AssetProps&, const WorldProps&, AssetCollection&);
    
    //getters
    std::vector<std::reference_wrapper<World>>& GetCurrentWorlds() { return current_worlds_; }
    const std::vector<std::reference_wrapper<World>>& GetCurrentWorlds() const { return current_worlds_; }

    std::vector<std::unique_ptr<World>>& GetPermanentWorlds() { return permanent_worlds_; }
    const std::vector<std::unique_ptr<World>>& GetPermanentWorlds() const { return permanent_worlds_; }

    Showcase<std::unique_ptr<World>>& GetVolatileWorlds() { return volatile_worlds_; }
    const Showcase<std::unique_ptr<World>>& GetVolatileWorlds() const { return volatile_worlds_; }

    //setters -- used for initialization
    // void SetCurrentWorlds(std::vector<World>) /* TODO: do we want this?? */

    template <typename... IntVals>
    void SetPreloadInitVolatileWorldIds(IntVals&&...);

    template <typename... IntVals>
    void SetPreloadDeferredVolatileWorldIds(IntVals&&...);

    //Adds permanent world to current worlds
    World* ActivatePermanentWorldById(unsigned int world_id);
    World* ActivatePermanentWorldByIndex(unsigned short world_index);

    //Loads volatile world from file into volatile_worlds_
    World* LoadVolatileWorld(unsigned int world_id);
    World* LoadVolatileWorld(unsigned int world_id, std::function<void(unsigned int, World&)> load_fn);

    //Loads volatile world (if doesn't exist) and adds it as a current world
    World* ActivateVolatileWorldById(unsigned int world_id);
    World* ActivateVolatileWorldById(unsigned int world_id, std::function<void(unsigned int, World&)> load_fn);
    World* ActivateVolatileWorldByIndex(unsigned short world_index);

    //Removes world from current worlds, if exists -- returns true if world was successfully deactivated
    bool DeactivateWorld(unsigned int world_id);

    //Deletes world -- only applies to volatile worlds
    bool DeleteVolatileWorldById(unsigned int world_id);
    bool DeleteVolatileWorldByIndex(unsigned short world_index);
};

    template <typename... IntVals>
    void GameState::SetPreloadInitVolatileWorldIds(IntVals&&... ids) {
        preload_init_volatile_world_ids_ = {std::forward<IntVals>(ids)...};
    }

    template <typename... IntVals>
    void GameState::SetPreloadDeferredVolatileWorldIds(IntVals&&... ids) {
        preload_deferred_volatile_world_ids_ = {std::forward<IntVals>(ids)...};
    }

}

#endif /* ESO_ENGINE_MODEL_GAME_STATE_H_ */