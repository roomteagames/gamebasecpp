#ifndef ESO_ENGINE_MODEL_GAME_H_
#define ESO_ENGINE_MODEL_GAME_H_

#include <SFML/Window.hpp>

#include "config.h"
#include "model/game/game_state.h"
#include "model/game/input_model.h"
#include "asset/asset_collection.h"


namespace eso {

struct Game {
    static const short kNoStateChange = -1;
    
    std::vector<GameState>& States;
    short ChangeStateIndex = kNoStateChange;
    PropsCollection& StartupProps;
    AssetCollection& Assets;
    InputModel& Inputs;

    GameState* CurrentState;
    sf::RenderWindow* Window = nullptr;
    
    bool Closing = false;
    bool FreezeUpdate = false;

    //Storing current framerates
    unsigned short UpdateFramerate = StartupProps.Game.UpdateFramerate;
    unsigned short RenderFramerate = StartupProps.Game.RenderFramerate;
    unsigned short InputFramerate = StartupProps.Game.InputFramerate;

    //temp
    bool other_flag = false;

};

}

#endif /* ESO_ENGINE_MODEL_GAME_H_ */