#ifndef ESO_ENGINE_MODEL_GAME_INPUT_H_
#define ESO_ENGINE_MODEL_GAME_INPUT_H_

#include "config.h"

#define INPUT_MAP_SIZE 256

namespace eso {

namespace input {

    const unsigned int kFaceUp =         0b10000000000000000000000000000000;
    const unsigned int kFaceRight =      0b01000000000000000000000000000000;
    const unsigned int kFaceDown =       0b00100000000000000000000000000000;
    const unsigned int kFaceLeft =       0b00010000000000000000000000000000;
    const unsigned int kDirectionUp =    0b00001000000000000000000000000000;
    const unsigned int kDirectionRight = 0b00000100000000000000000000000000;
    const unsigned int kDirectionDown =  0b00000010000000000000000000000000;
    const unsigned int kDirectionLeft =  0b00000001000000000000000000000000;
    const unsigned int kShoulderRight =  0b00000000100000000000000000000000;
    const unsigned int kShoulderLeft =   0b00000000010000000000000000000000;
    const unsigned int kTriggerRight =   0b00000000001000000000000000000000;
    const unsigned int kTriggerLeft =    0b00000000000100000000000000000000;
    const unsigned int kThrustRight =    0b00000000000010000000000000000000;
    const unsigned int kThrustLeft =     0b00000000000001000000000000000000;
    const unsigned int kFrontRight =     0b00000000000000100000000000000000;
    const unsigned int kFrontLeft =      0b00000000000000010000000000000000;
    const unsigned int kAux1 =           0b00000000000000001000000000000000;
    const unsigned int kAux2 =           0b00000000000000000100000000000000;
    const unsigned int kAux3 =           0b00000000000000000010000000000000;
    const unsigned int kAux4 =           0b00000000000000000001000000000000;
    const unsigned int kAux5 =           0b00000000000000000000100000000000;
    const unsigned int kAux6 =           0b00000000000000000000010000000000;
    const unsigned int kAux7 =           0b00000000000000000000001000000000;
    const unsigned int kAux8 =           0b00000000000000000000000100000000;
}

/**
 * UniqueSeed is a modulo constant allowing a spread-out of key code numeric values within a small domain
 * use SFML keycodes as values -- these will be passed into the input model array as indexes -- I don't mention SFML in this code so it can be loosely coupled
 * only the .cc files will explicitly refer to SFML
 * 
 */
struct InputConfig {
    unsigned short UniqueSeed = 219;

    unsigned int FaceUp;
    unsigned int FaceRight;
    unsigned int FaceDown;
    unsigned int FaceLeft;

    unsigned int DirectionUp;
    unsigned int DirectionRight;
    unsigned int DirectionDown;
    unsigned int DirectionLeft;
    
    unsigned int ShoulderRight;
    unsigned int ShoulderLeft;
    
    unsigned int TriggerRight;
    unsigned int TriggerLeft; 
    
    unsigned int ThrustRight; 
    unsigned int ThrustLeft;  
    
    unsigned int FrontRight;
    unsigned int FrontLeft;
    
    unsigned int Aux1;
    unsigned int Aux2;
    unsigned int Aux3;
    unsigned int Aux4;
    unsigned int Aux5;
    unsigned int Aux6;
    unsigned int Aux7;
    unsigned int Aux8;
};


class InputModel {

private:
    unsigned short index_seed_;

    unsigned int state_;
    unsigned int previous_state_;
    unsigned int state_buffer_;
    unsigned int keycode_map_[INPUT_MAP_SIZE];

    InputConfig config_;

    void init_keycode_map(const InputConfig&);
    
    //mod keycode by unique seed to get mapping index
    inline unsigned int calc_virtual_keycode(unsigned int keycode) const { return keycode_map_[keycode % index_seed_]; }

public:
    InputModel(const InputConfig&);
    InputModel(InputConfig&&);
    InputModel(const InputModel&) = delete;

    // modify the state buffer
    void AddInputPress(unsigned int);
    void RemoveInputPress(unsigned int);

    // set state and previous state based on buffer
    void Update();

    // reset model to utilize a NEW input config/mapping
    void Reset(InputConfig&);

    // getters
    unsigned int GetState() const { return state_; }
    const InputConfig& GetInputConfig() const { return config_; }
    
    //Polling operations -- look at state and previous state(s), but NOT buffer
    //You can check multiple inputs by ORing them together "|"
    bool IsInputPressed(unsigned int) const;                                //Check if input is currently down, whether initially, or is being held
    bool IsInputBegun(unsigned int) const;                                  //Check if input has just started being pressed -- this will only return true ONCE, until released and pressed again
    bool IsInputReleased(unsigned int) const;                               //Check if input has just been released -- in the previous frame, input was being pressed; now it is not

    /** TODO: this may not be possible to implement; relies on counting how many frames input was held */
    bool IsInputHeld(unsigned int, unsigned int num_frames = 5) const;      //Checks if input has been pressed/held for >= num_frames
    bool IsInputTapped(unsigned int, unsigned int num_frames = 5) const;    //Similar to released, but checks if input was formerly pressed for <= num_frames before being released
};

}

#endif /* ESO_ENGINE_MODEL_GAME_INPUT_H_ */