#ifndef ESO_ENGINE_MODEL_GAME_WORLD_H_
#define ESO_ENGINE_MODEL_GAME_WORLD_H_

#include <vector>

#include <SFML/Graphics.hpp>

#include "config.h"
#include "controller/physics/collision.h"
#include "controller/physics/constraint.h"
#include "model/entity/entity.h"
#include "model/game/entity_grid.h"
#include "constants.h"
#include "view/camera.h"
#include "view/entity/view_model.h"

namespace eso {

// TODO: figure out an efficient way to use this to allow world overrides in the future
// If UseGrid, -1 means copy from Props.WorldProps
struct WorldGridProps {
    bool UseGrid = false;
        
    short UnitsPerBlock = -1;
    short BlocksPerSector = -1;
    short SectorsPerSquare = -1;
    
    short VisibleSectorRadius = -1;
    short InvisibleSectorRadius = -1;
    short InactiveSectorRadius = -1;
};

enum class LoadStatus {
    Starting = 0,
    WorldLoaded = 1,
    GraphicsLoaded = 2,
    AudioLoaded = 3,
    Done = 4
};

class World {
private:
    unsigned int id_;
    std::string name_;
    bool use_grid_;
    bool use_advanced_collisions_;
    unsigned short num_collision_iterations_;
    sf::Vector2<double> gravity_;
    
    std::vector<std::shared_ptr<Entity>> entities_; // CAN be reallocated -- do not stored references to elements
    std::vector<EntityGrid> entity_grids_;          // list of grids, by focus
    std::vector<std::weak_ptr<Entity>> focuses_;    // world loading and viewing focuses on these entities (single player usually only has one)
    std::vector<Shape> unique_shapes_;              // shapes copied from the Assets that are mutable and reserved for indiv instances
    
    //Physics collections
    std::vector<Collision> current_collisions_;     // list of all possible collisions for the current frame
    std::vector<Constraint> current_constraints_;    // list of all existing equality constraints for the current frame

    /*TODO: make this a static array of vectors, one day -- array size is static for the whole app, set with a macro? */
    std::vector<std::vector<std::unique_ptr<ViewModel>>> views_;    //inner vector CAN be reallocated
    std::vector<ViewportIndex> viewports_;

    //Required for multi-threaded loading
    LoadStatus load_status_ = LoadStatus::Starting;
    LoadStatus load_acknowledgement_ = LoadStatus::Starting;

public:
    World(int id = 0, std::string&& name = "", sf::Vector2<double>&& gravity = engine::GetGravity(), bool use_complex_collisions = false, bool use_grid = false, LoadStatus load_status_ = LoadStatus::Starting, unsigned short num_collision_iterations = 1);
    World(const World&) = delete;
    bool operator==(const World& other) { return id_ == other.id_; }

    //TODO: Maybe the same could be done for shapes??? -- perhaps in OnWorldLoad
    //TODO: move logic to assign shape to entity here -- then maybe we can dissolve the construction module
    void OnWorldLoad(AssetCollection&);
    void OnGraphicsLoad(VisualCollection&);
    void OnAudioLoad(AudioCollection&);

    void InitEntityGrids(const WorldProps&);
    unsigned short CreateViewType(unsigned int = 5);

    //non-const refs
    /* TODO: update -- these "getters" are non-const which breaks my conventions -- change to Ref instead */
    std::vector<std::shared_ptr<Entity>>& GetEntities() { return entities_; }
    std::vector<EntityGrid>& GetEntityGrids() { return entity_grids_; }
    std::vector<std::weak_ptr<Entity>>& GetFocuses() { return focuses_; }
    std::vector<std::unique_ptr<ViewModel>>& GetViewsByTypeIndex(unsigned short type_index);
    std::vector<ViewportIndex>& GetViewports() { return viewports_; }   //TODO: change the lingo of Camera because this conflicts with sf::View::setViewport
    std::vector<Shape>& GetUniqueShapes() { return unique_shapes_; }
    std::vector<Collision>& CurrentCollisions() { return current_collisions_; } //This method IS following my conventions
    std::vector<Constraint>& CurrentConstraints() { return current_constraints_; }
    LoadStatus& RefLoadStatus() { return load_status_; }
    
    //getters
    unsigned int GetId() const { return id_; }
    unsigned short GetNumCollisionIterations() const { return num_collision_iterations_; }
    const std::string& GetName() const { return name_; }
    const sf::Vector2<double>& GetGravity() const { return gravity_; }
    bool UseGrid() const { return use_grid_; }
    bool UseAdvancedCollisions() const { return use_advanced_collisions_; }
    LoadStatus GetLoadStatus() const { return load_status_; }
    LoadStatus GetLoadAcknowledgement() const { return load_acknowledgement_; }
    bool IsFullyLoaded() const { return load_acknowledgement_ == LoadStatus::Done; }

    sf::Vector2<double>& Gravity() { return gravity_; }

    //Mutators/Updaters
    void ResetEntities(const Game& g);
};

}

#endif /* ESO_ENGINE_MODEL_GAME_WORLD_H_ */