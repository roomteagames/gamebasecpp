#ifndef ESO_ENGINE_MODEL_GAME_SHAPE_EDGE_H_
#define ESO_ENGINE_MODEL_GAME_SHAPE_EDGE_H_

#include <memory>
#include <string>

#include <SFML/Graphics.hpp>

#include "model/game/shape/edge_function.h"
#include "model/game/shape/material.h"
#include "util/vector_utils.h"

namespace eso {

class Edge {
private:
    unsigned short index_;
    sf::Vector2<double> a_;     
    sf::Vector2<double> b_;     
    EdgeFunction* fn_; //f(x) defined as if a_ were (0,0) -- pointer allows for polymorphism
    double incline_angle_;   //auto-calculated in ctor -- TODO: this logic may need to be moved to LinearEdgeFunction class

    //ctor is private, must use factory fn
    Edge(unsigned short, sf::Vector2<double>, sf::Vector2<double>, EdgeFunction*);

    //helper functions
    void update_incline_angle();    //called by ctor AND should be called internally whenever edge rotation is updated

public:
    Edge() {}   //this empty constructor is needed in order to be able to declare an Edge or empty vector of Edges -- TODO: maybe this isn't necessary??
    ~Edge();

    //NOTE: in these copy/move ctors, MaterialInstance is NOT copied. Copying an edge will reset all material lifecycle properties
    Edge(const Edge&);
    Edge(Edge&&);   //needs to be defined so EdgeFunction can be properly moved
    Edge& operator=(const Edge&);
    Edge& operator=(Edge&&);

    //TODO: come up with a constructor that both allows entry of a and b, but also allows creation of polynomial
    /**
     * ctor1: Edge(sf::Vector2<double>, sf::Vector2<double>);
     * ctor2: Edge(...allow for MORE coefficients. How are they refresented?)
     */
    static Edge CreateLinearEdge(unsigned short, sf::Vector2<double>, sf::Vector2<double>);
    static Edge CreateParabolicEdge(unsigned short, sf::Vector2<double>, sf::Vector2<double>);
    static Edge CreateCubicEdge(unsigned short, sf::Vector2<double>, sf::Vector2<double>);
    static Edge Create4thDegreeEdge(unsigned short, sf::Vector2<double>, sf::Vector2<double>);
    static Edge Create5thDegreeEdge(unsigned short, sf::Vector2<double>, sf::Vector2<double>);
    //TODO: create others, like for 

    unsigned short GetIndex() const { return index_; }
    const sf::Vector2<double>& GetA() const { return a_; }
    const sf::Vector2<double>& GetB() const { return b_; }
    const EdgeFunction* GetFn() const { return fn_; }
    double GetInclineAngle() const { return incline_angle_; }
    bool IsVerticalLine() const { return (incline_angle_ == 90 || incline_angle_ == 270); }
    std::string ToString() const { return "{A=" + vector::ToString(GetA()) + ",B=" + vector::ToString(GetB()) + "InclineAngle=" + std::to_string(GetInclineAngle()) + ",IsVerticalLine=" + std::to_string(IsVerticalLine()) + "}"; }

    //Calculations -- TODO: store output of this in the future?
    sf::Vector2<double> GetNormalVector(double=0) const;
    sf::Vector2<double> GetUnitNormalVector(double=0) const;
    sf::Vector2<double> GetSecantVector() const;       //vector connecting A and B
    sf::Vector2<double> GetUnitSecantVector() const;   //unit vector connecting A and B

    EdgeFunction* Fn() { return fn_; }
    void SetA(int x, int y) { a_ = sf::Vector2<double>(x, y); }
    void SetB(int x, int y) { b_ = sf::Vector2<double>(x, y); }
};

//Creates a LinearEdge with an EdgeFunction on the stack, rather than the heap -- NOT compatible with Edge*
//DOES NOT store material properties -- only exists for the sake of convenient intersection detection
class LinearEdge {
private:
    sf::Vector2<double> a_;
    sf::Vector2<double> b_;
    LinearEdgeFunction fn_; //f(x) defined as if a_ were (0,0)

    //ctor is private, must use factory fn
    LinearEdge(sf::Vector2<double>, sf::Vector2<double>, LinearEdgeFunction&&);

public:
    //LinearEdge() {}   //this empty constructor is needed in order to be able to declare an Edge or empty vector of Edges -- TODO: maybe this isn't necessary??

    static LinearEdge CreateLinearEdge(sf::Vector2<double>, sf::Vector2<double>);

    const sf::Vector2<double>& GetA() const { return a_; }
    const sf::Vector2<double>& GetB() const { return b_; }
    const LinearEdgeFunction& GetFn() const { return fn_; }
    bool IsVerticalLine() const { return fn_.GetSlope() == mathconstants::NO_SLOPE; }
    std::string ToString() const { return "{A=" + vector::ToString(GetA()) + ",B=" + vector::ToString(GetB()) + ",IsVerticalLine=" + std::to_string(IsVerticalLine()) + "}"; }
};
    
    
}

#endif /* ESO_ENGINE_MODEL_GAME_SHAPE_EDGE_H_ */