#ifndef ESO_ENGINE_MODEL_GAME_SHAPE_H_
#define ESO_ENGINE_MODEL_GAME_SHAPE_H_

#include <vector>

#include <SFML/Graphics.hpp>

#include "model/game/shape/edge.h"
#include "model/game/shape/bounding_box.h"
#include "util/iterator.tpp"

//temp
#include <iostream>

namespace eso {

class PhysicsCollection;

//Shape has no position -- all points are described as offsets from the center
class Shape {
private:
    std::string name_ = "empty";
    BoundingBox bounding_box_;
    sf::Vector2<double> center_;
    std::vector<Edge> edges_;
    
    bool mutable_ = false;
    bool empty_ = true;     //if this is true, entity creation will print a warning -- this will cause aabb collision detection to crash
    const Shape* source_ = nullptr;

    void update_bounding_box();

    friend class PhysicsCollection;

public:
    Shape() {}
    Shape(const std::string&, const sf::Vector2<double>&, const sf::Vector2<double>&);  //TODO: we need this just for the ability to manually make a shape??
    Shape(std::string&&, sf::Vector2<double>&&, sf::Vector2<double>&&);  //TODO: we need this just for the ability to manually make a shape??

    Shape(const Shape&);
    Shape(Shape&&);
    Shape& operator=(const Shape&);
    Shape& operator=(Shape&&);
    
    //Static shape creation methods
    static Shape CreateRectangle(std::string&&, unsigned int width, unsigned int height);
    static Shape CreateRectangle(std::string&&, sf::Vector2<double> min, sf::Vector2<double> max);
    //TODO: need more preset ways to create shape

    //Getters and Refs
    const std::string& GetName() const { return name_; }
    const BoundingBox& GetBoundingBox() const { return bounding_box_; }
    std::vector<Edge>& Edges() { return edges_; }
    const std::vector<Edge>& GetEdges() const { return edges_; }
    const sf::Vector2<double> GetCenter() const { return center_; }
    bool IsMutable() const { return mutable_; }
    bool IsEmpty() const { return empty_; }
    /*TODO: implement:
    const sf::Vector2<double> TranslateCenter(const sf::Vector2<double>& translation) const { return center_ + translation; }*/

    //Modifiers -- perform no action is not mutable
    bool ScaleFromCenter(double);
    bool Rotate(double);
    bool SetRotation(double);

};

struct EntityShape {
public:
    Shape* ShapeDefinition = nullptr;
    std::vector<EdgeMaterial> EdgeMaterials;
    // static CreateEntityShape(/*TODO: figure out params*/);

    EntityShape() {}
    EntityShape(Shape* _shape): ShapeDefinition(_shape) {}

    bool Exists() const { return ShapeDefinition != nullptr; }
    bool Rotate(double radians) { if (ShapeDefinition != nullptr) return ShapeDefinition->Rotate(radians); return false; }
    bool SetRotation(double orientation) { if (ShapeDefinition != nullptr) return ShapeDefinition->SetRotation(orientation); return false; }
    
    EdgeMaterial& RefEdgeMaterial(Edge const*);

    const EdgeMaterial& GetEdgeMaterial(Edge const*) const;
    double GetEdgeFriction(Edge const*) const;
    double GetEdgeRestitution(Edge const*) const;
};

//TODO: create a factory method that creates entity shape based on whether shape comes from assets or is new. Where is shape stored if new? Prob need to pass assets into this

}

#endif /* ESO_ENGINE_MODEL_GAME_SHAPE_H_ */