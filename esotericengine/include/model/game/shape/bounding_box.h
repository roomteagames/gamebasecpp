#ifndef ESO_ENGINE_MODEL_GAME_SHAPE_BOUNDINGBOX_H_
#define ESO_ENGINE_MODEL_GAME_SHAPE_BOUNDINGBOX_H_

#include <string>
#include <cmath>

#include <SFML/System.hpp>

#include "util/iterator.tpp"

namespace eso {

struct BoundingBox {
    sf::Vector2<double> Min;
    sf::Vector2<double> Second;
    sf::Vector2<double> Max;
    sf::Vector2<double> Fourth;

    unsigned int Width;
    unsigned int Height;

    BoundingBox();
    BoundingBox(sf::Vector2<double> _min, sf::Vector2<double> _second, sf::Vector2<double> _max, sf::Vector2<double> _fourth);
    BoundingBox(sf::Vector2<double> _center, double _width, double _height);

    //Iterators, allowing for a loop through all 4 bounding endpoints
    iterator<sf::Vector2<double>> begin() { return &Min; }
    const_iterator<sf::Vector2<double>> begin() const { return const_cast<sf::Vector2<double>*>(&Min); }
    iterator<sf::Vector2<double>> end() { return &Fourth + 1; }
    const_iterator<sf::Vector2<double>> end() const { return const_cast<sf::Vector2<double>*>(&Fourth + 1); }

    bool IsOverlap(const BoundingBox&) const;
    double GetHorizontalOverlap(const BoundingBox&) const;
    double GetVerticalOverlap(const BoundingBox&) const;

    bool IsTouchingAboveExact(const BoundingBox& other) const { return this->Max.y == other.Min.y && GetHorizontalOverlap(other) > GetVerticalOverlap(other); }
    bool IsTouchingBelowExact(const BoundingBox& other) const { return this->Min.y == other.Max.y && GetHorizontalOverlap(other) > GetVerticalOverlap(other); }
    bool IsTouchingLeftExact(const BoundingBox& other) const { return this->Max.x == other.Min.x && GetVerticalOverlap(other) > GetHorizontalOverlap(other); }
    bool IsTouchingRightExact(const BoundingBox& other) const { return this->Min.x == other.Max.x && GetVerticalOverlap(other) > GetHorizontalOverlap(other); }

    bool IsTouchingAbove(const BoundingBox& other) const { return std::abs(this->Max.y - other.Min.y) < 1 && GetHorizontalOverlap(other) > GetVerticalOverlap(other); }
    bool IsTouchingBelow(const BoundingBox& other) const { return std::abs(this->Min.y - other.Max.y) < 1 && GetHorizontalOverlap(other) > GetVerticalOverlap(other); }
    bool IsTouchingLeft(const BoundingBox& other) const { return std::abs(this->Max.x - other.Min.x) < 1 && GetVerticalOverlap(other) > GetHorizontalOverlap(other); }
    bool IsTouchingRight(const BoundingBox& other) const { return std::abs(this->Min.x - other.Max.x) < 1 && GetVerticalOverlap(other) > GetHorizontalOverlap(other); }

    int GetWidth() const { return Max.x - Min.x; }
    int GetHeight() const { return Max.y - Min.y; }
    BoundingBox GetTranslation(const sf::Vector2<double>&) const;

    std::string ToString() const;
};

namespace boundingbox {

bool IsOverlap(const sf::Vector2<double>& center1, unsigned int width1, unsigned int height1, const sf::Vector2<double>& center2, unsigned int width2, unsigned int height2);

}

}

#endif /* ESO_ENGINE_MODEL_GAME_SHAPE_BOUNDINGBOX_H_ */