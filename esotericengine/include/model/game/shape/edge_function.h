#ifndef ESO_ENGINE_MODEL_GAME_SHAPE_EDGEFUNCTION_H_
#define ESO_ENGINE_MODEL_GAME_SHAPE_EDGEFUNCTION_H_

#include <limits>

#include <SFML/Graphics.hpp>

//temp
#include <iostream>
#include "constants.h"

namespace eso {

namespace mathconstants {

//Math/Graph special case values
const int __max_int                         = std::numeric_limits<int>::max();
const int __min_int                         = std::numeric_limits<int>::min();
const double __min_float                     = std::numeric_limits<double>::min();

const int NO_SOLUTION                       = __min_int;
const int INFINITE_SOLUTIONS                = __max_int;
const double NO_SLOPE                        = __min_float;
const sf::Vector2<double> NO_SOLUTION_VEC          = {__min_int, __min_int};
const sf::Vector2<double> INFINITE_SOLUTIONS_VEC   = {__max_int, __max_int};
const sf::Vector2<double> ORIGIN                   = {0.0, 0.0};

}

class EdgeFunction {
public:
    enum FunctionType { /* maybe we only really need ARC and LINEAR types -- the other two type will be tough to deal with rotation */
        ARC,
        ROOT,
        POLYNOMIAL,
        LINEAR
    };

protected:
    FunctionType type_;

public:
    EdgeFunction(FunctionType type): type_(type), ID(engine::EdgeFunctionID++) { /* std::cout << "cted edge with ID=" << ID << std::endl; */ }
    virtual ~EdgeFunction() = 0;
    
    // EdgeFunction(const EdgeFunction&);
    // EdgeFunction(EdgeFunction&&);
    // EdgeFunction& operator=(const EdgeFunction&);
    // EdgeFunction& operator=(EdgeFunction&&);

    //Allows deep copying of function when calling copy ctor of Edge
    virtual EdgeFunction* Clone() const = 0;

    //pure virtual functions that MUST be defined
    virtual double Evaluate(double) const = 0;    //find y, given x
    virtual double GetSlope(double x=0) const = 0;    //find y', given x

    virtual void SetSlope(double m) {}   //TODO: this really only makes sense for linear fns at this point
    
    FunctionType GetType() const { return type_; }
    sf::Vector2<double> GetNormalVector(double=0) const;
    sf::Vector2<double> GetUnitNormalVector(double=0) const;

    std::string ToString(double=0) const;

    int ID;
};

//TODO: split into separate functions by degree -- how will this handle rotation???
class PolynomialEdgeFunction : public EdgeFunction {
private:
    double coefficient_;
    unsigned int degree_;

public:
    PolynomialEdgeFunction(double, int);

    // PolynomialEdgeFunction(const PolynomialEdgeFunction&);
    // PolynomialEdgeFunction(PolynomialEdgeFunction&&);
    // PolynomialEdgeFunction& operator=(const PolynomialEdgeFunction&);
    // PolynomialEdgeFunction& operator=(PolynomialEdgeFunction&&);

    virtual PolynomialEdgeFunction* Clone() const override { return new PolynomialEdgeFunction(*this); }
    
    virtual double Evaluate(double) const override;
    virtual double GetSlope(double=0) const override;

    double GetCoefficient() const { return coefficient_; }
    unsigned int GetDegree() const { return degree_; }

    bool IsLinear() const { return degree_ == 1; }
    bool IsQuadratic() const { return degree_ == 2; }
    bool IsCubic() const { return degree_ == 3; }
};

//Main way to represent a line (vertical or otherwise)
class LinearEdgeFunction : public EdgeFunction {
private:
    double slope_ = 0;

public:
    LinearEdgeFunction(double);

    // LinearEdgeFunction(const LinearEdgeFunction&);
    // LinearEdgeFunction(LinearEdgeFunction&&);
    // LinearEdgeFunction& operator=(const LinearEdgeFunction&);
    // LinearEdgeFunction& operator=(LinearEdgeFunction&&);

    virtual LinearEdgeFunction* Clone() const override { return new LinearEdgeFunction(*this); }
    virtual double Evaluate(double) const override;
    virtual double GetSlope(double=0) const override { return slope_; }
    virtual void SetSlope(double m) override { slope_ = m; }

    bool IsVertical() const { return slope_ == mathconstants::NO_SLOPE; }
};


}

#endif /* ESO_ENGINE_MODEL_GAME_SHAPE_EDGEFUNCTION_H_ */