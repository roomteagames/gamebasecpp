#ifndef ESO_ENGINE_MODEL_GAME_SHAPE_MATERIAL_H_
#define ESO_ENGINE_MODEL_GAME_SHAPE_MATERIAL_H_

#include <SFML/System.hpp>

namespace eso {

class Material {
private:
    //Physical properties:
    double friction_ = 0;
    double restitution_ = 1;

    /** Lots of other fun potential material properties -- subject to change
     * A material should be referenced from a MaterialStore. Each Edge has its own instance of MaterialStatus
     * 
     * This should ALSO include the status of matter... but this engine currently doesn't support water/liquid
     
    //Mechanical properties:
    unsigned double impact_resistance_;
    unsigned double strength_;

    //Thermal properties:
    unsigned double heat_resistance_;

    //Chemical properties:
    double burn_point_;
    unsigned double fire_resistance_;
    double freeze_point_;
    unsigned double rust_resistance_;
    bool rusted_ = false;
    //other stuff involving chemicals

    //Electric properties:
    unsigned double electrical_resistance_;
    double current_amount_;

    //Magnetic properties:
    sf::Vector2<double> magnetic_force_;

    */

public:
    Material() {}
    Material(double, double);
    //TODO: extend this ctor with optional params for the other properties, OR make other ctors

    //Const getters
    double GetFriction() const { return friction_; }
    double GetRestitution() const { return restitution_; }

    //void UpdateInstances(MaterialInstance&, MaterialInstance&);  //maybe make this static/ns-global in the future-- should this be able to handle MULTIPLE colliding materials?

};


struct EdgeMaterial {
    //Neutral statuses
    double Temperature;
    
    //Progress statuses
    double Damage;
    double FireStatus; //amount of frames in contact
    double RustStatus; //amount of frames in contact
    double WetAmount; //??? not sure about this yet

    //Boolean statuses
    /*TODO: impl later*/bool OnFire() const { return false; }   //high-temperature reversible threshold status, given temperature > burn_point AND given fire_status > fire_resistance
    /*TODO: impl later*/bool Frozen() const { return false; }    //low-temperature reversible threshold status, given temperature < freeze_point
    /*TODO: impl later*/bool Rusted() const { return false; }    //chemical non-reversible threshold status, given wet_amount > wet_threshold? AND rust_status > rust_resistance

    const Material* MaterialDefinition = nullptr;   //ptr to the actual material linked to this edge

    EdgeMaterial() {}
    EdgeMaterial(const Material* _material): MaterialDefinition(_material) {}
};

namespace material {


}

}

#endif /* ESO_ENGINE_MODEL_GAME_SHAPE_MATERIAL_H_ */