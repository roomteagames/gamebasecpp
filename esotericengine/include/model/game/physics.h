#ifndef ESO_ENGINE_MODEL_GAME_PHYSICS_H_
#define ESO_ENGINE_MODEL_GAME_PHYSICS_H_

#include <cmath>
#include <tuple>

#include <SFML/System.hpp>

#include "constants.h"

namespace eso {

//Stores dynamics information about a particular entity
struct Physics {
    
    //linear components of dynamics
    double Mass = 1;  //if mass = -1, mass is infinite -- TODO: allow mass to be calculated using material density x area
    sf::Vector2<double> Velocity;
    sf::Vector2<double> Acceleration;
    sf::Vector2<double> NetForce; //total linear force ACTING ON the owning entity

    std::tuple<unsigned short, sf::Vector2<double>> OutwardForces[ESO_VAR_OUTWARD_FORCE_MAX]; 
    enum OutwardForceIndex {
        EdgeIndex,
        Force
    };

    //angular components of dynamics
    sf::Vector2<double> CenterOfRotation;  //offset from entity's Position (COR - Position)
    double MomentOfInertia = 1;      //TODO: calculate this SOMEHOW -- for now, it will just remain 1
    double RotationalAcceleration = 0;   //in radians/frame^2
    double RotationalVelocity = 0;       //in radians/frame
    double NetTorque = 0;                //sum of all torques acting on owning entity

    sf::Vector2<double> PreviousVelocity;  //TODO: deprecate!
    double TotalCrushMagnitude;      //TODO: deprecate?

    Physics(int _mass=1, sf::Vector2<double>&& _acceleration=sf::Vector2<double>(), double _rotational_acc=0, sf::Vector2<double>&& _center=sf::Vector2<double>());
    Physics(int _mass, const sf::Vector2<double>& _acceleration, double _rotational_acc, const sf::Vector2<double>& _center);
    
    const sf::Vector2i GetVelocityInt() const { return (sf::Vector2i) Velocity; }
    const sf::Vector2i GetAccelerationInt() const { return (sf::Vector2i) Acceleration; }
    const sf::Vector2i GetNetForceInt() const { return (sf::Vector2i) NetForce; }

    double GetRotationalVelocityDegrees() const { return RotationalVelocity * 180 / M_PI; }
    void SetRotationalVelocityDegrees(double value) { RotationalVelocity = value * M_PI / 180; }

    friend std::ostream& operator<<(std::ostream&, const Physics&);
};

}

#endif /* ESO_ENGINE_MODEL_GAME_PHYSICS_H_ */