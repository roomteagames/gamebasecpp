#ifndef ESO_ENGINE_MODEL_GAME_ENTITYGRID_H_
#define ESO_ENGINE_MODEL_GAME_ENTITYGRID_H_

#include <memory>

#include "config.h"
#include "model/entity/entity.h"

namespace eso {

//Avoid circular dependencies
struct Game;

struct EntityGrid {
	const std::weak_ptr<Entity> focus;	//TODO: should this be const?
	std::vector<std::vector<std::shared_ptr<Entity>>> data;
    
	unsigned short inner_tier_count;
	unsigned short middle_tier_count;
	unsigned short outer_tier_count;
	
	unsigned short units_per_sector;
	unsigned short grid_dimension;
	unsigned short grid_size;

	unsigned short focus_sector_index;
	unsigned short first_active_index;
	unsigned short last_active_index;

	//returns true if entity was initially added to grid or moved to different grid sector
	inline bool assign_entity(std::shared_ptr<Entity>&, short current_grid_index = -1);

	EntityGrid(std::vector<std::shared_ptr<Entity>>&, const std::weak_ptr<Entity>, const WorldProps&);

	//utility methods to optimize engine
	//returns true if entities in index are active -- if not, you might as well skip over that sector
	bool IsSectorActive(short) const;

	//moves entities based on updated position, and sets status based on grid position
	void Update(Game&);
};

}

#endif /* ESO_ENGINE_MODEL_GAME_ENTITYGRID_H_ */