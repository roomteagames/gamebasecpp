#include "constants.h"

#include "util/file_utils.h"

namespace eso {

namespace system {

//Executable Location
std::string ExecutableDirectory = file::GetExecutableDirectory();

//Asset Locations
std::string FontsDirectory = OS_FONTS_DIRECTORY;
std::string FontsExtension = ".ttf";
std::string DefaultFontName = OS_DEFAULT_FONT;

}

namespace engine {

//World sizing
unsigned short BlockSize = 32;
unsigned short SectorSize = 16;

//Physics
sf::Vector2<double> Gravity(0, 0.5f);
sf::Vector2<double> GetGravity() { return Gravity; }

//Debug
unsigned int DebugCollisionTrackedInstanceID = 0x00;
unsigned int EdgeFunctionID = 0;

}


}