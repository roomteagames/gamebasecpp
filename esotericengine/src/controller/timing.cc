#include "controller/timing.h"

#include <chrono>
#include <thread>

#include <SFML/System.hpp>

namespace eso {

namespace timing {

unsigned long GetSystemTime() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now().time_since_epoch()
    ).count();
}

unsigned long GetSystemTimeNanos() {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(
        std::chrono::system_clock::now().time_since_epoch()
    ).count();
}

}

LoopTimer::LoopTimer(unsigned short frame_rate) {
    ResetFramerate(frame_rate);
}

void LoopTimer::ResetFramerate(unsigned short frame_rate) {
    frame_length_ = 1000 / frame_rate;
    start_time_ = timing::GetSystemTime();
    iteration_count_ = 0;
}

LazyLoopTimer::LazyLoopTimer(unsigned short frame_rate):
        LoopTimer(frame_rate) {}

void LazyLoopTimer::operator()() {
    short sleep_time;
    unsigned short iteration_increment = 1;

    unsigned long current_time = timing::GetSystemTime();
    unsigned long projected_time = start_time_ + (iteration_count_ * frame_length_);
    int raw_sleep_time = projected_time - current_time;

    if (raw_sleep_time >= 0) {  //current time is EARLIER than projected time; this IS sleep time
        sleep_time = raw_sleep_time;
    } else {    //current time is LATER than projected time; we need to figure out how many frames we were delayed, so we can sleep till the next frame beginning
        unsigned int millis_behind = current_time - projected_time;
        unsigned int num_frames_behind = (millis_behind / frame_length_) + 1;
        sleep_time = frame_length_ - (millis_behind % frame_length_);
        iteration_increment += num_frames_behind;
    }
    sf::sleep(sf::milliseconds(sleep_time));
    iteration_count_ += iteration_increment;
}

ActiveLoopTimer::ActiveLoopTimer(unsigned short frame_rate):
        LoopTimer(frame_rate) {}

void ActiveLoopTimer::operator()() {
    unsigned long current_time = timing::GetSystemTime();
    unsigned long projected_time = start_time_ + (iteration_count_ * frame_length_);
    int raw_sleep_time = projected_time - current_time;

    if (raw_sleep_time > 0) {   // only sleep if we aren't behind
        sf::sleep(sf::milliseconds(raw_sleep_time));
    }   // otherwise immediately go to the next frame
    iteration_count_++;
}

MilliTicker::MilliTicker() {
    start_time_ = timing::GetSystemTime();
}

void MilliTicker::Reset() {
    start_time_ = timing::GetSystemTime();
}

unsigned long MilliTicker::operator()() {
    unsigned long diff_time = timing::GetSystemTime() - start_time_;
    Reset();
    return diff_time;
}

NanoTicker::NanoTicker() {
    start_time_ = timing::GetSystemTimeNanos();
}

void NanoTicker::Reset() {
    start_time_ = timing::GetSystemTimeNanos();
}

unsigned long NanoTicker::operator()() {
    unsigned long diff_time = timing::GetSystemTimeNanos() - start_time_;
    Reset();
    return diff_time;
}

}