#include "controller/loading.h"

#include "constants.h"
#include "util/binary_reader.h"
#include "asset/generated/entity_routing.h"
#include "asset/generated/view_model_routing.h"
#include "view/spritesheet.h"

//TODO: temp
#include <iostream>

namespace eso {

namespace loading {

std::unordered_set<std::string> requested_textures;

void RequestTexture(const std::string& texture_name) {
	requested_textures.insert(texture_name);
}


//TODO: reimplement this to be part of the heavy-tier asset suite -- use green code below
void LoadTextures(VisualCollection& media, const AssetProps& props) {
	std::string load_dir = system::ExecutableDirectory;
	load_dir += props.ImageDirectory;

	for (const auto& texture_name : requested_textures) {
		std::string load_file = load_dir + texture_name;
		load_file += props.ImageExtension;

		auto* texture_spritesheets = media.FetchOrCreateTextureSpritesheet(texture_name);
		if (!texture_spritesheets->IsTextureLoaded()) {
			if (!texture_spritesheets->LoadTexture(load_file)) {
				//TODO: convert to error logger
				std::cout << "ERROR: Texture load failed for " << load_file << std::endl;
			}
		}
	}
}

void esoteric_load_spritesheets(const std::string& texture_name, VisualCollection& media, const AssetProps& props) {
	std::string load_dir = file::GetExecutableDirectory();
	std::string file_name = load_dir + props.SpritesheetDirectory + texture_name + props.SpritesheetExtension;
	BinaryReader reader(file_name);

	//Open and read the entire binary file
	reader.Init();
	if (reader.IsError()) {
		std::cout << "ESO_LOAD_ERROR: The file " << file_name << " may not exist!" << std::endl;
	
	} else {
		//Obtain Texture_Spritesheet to populate
		auto* ts = media.FetchOrCreateTextureSpritesheet(texture_name);
		//TODO: do we need a null check here? -- we are already checking null in the parent fn

		//Determine how many spritesheets are contained within file
		unsigned int num_spritesheets = reader.NextUInt();
		ts->RefSpritesheets().reserve(num_spritesheets);

		for (unsigned int i = 0; i < num_spritesheets; i++) {
			
			//Initialize parameters for creating spritesheet
			auto origin = reader.NextVec2u();
			auto frame_size = reader.NextVec2u();
			auto num_states = reader.NextUShort();

			std::array<unsigned short, kMaxSpritesheetStates> frames_per_state;
			for (unsigned short j = 0; j < num_states; j++) {
				frames_per_state[j] = reader.NextUShort();
			}

			if (reader.IsError()) {
				std::cout << "ESO_LOAD_ERROR: Spritesheet for texture \"" << texture_name << "\" at index " << i << " cannot be created from file..." << std::endl;
				return;
			}

			//Create new spritesheet "at index"
			ts->RefSpritesheets().emplace_back(std::move(origin), std::move(frame_size), std::move(frames_per_state));
		}
	}
}

void LoadSpritesheets(VisualCollection& media, const AssetProps& props, SpritesheetLoader loader_fn) {
	for (const auto& texture_name : requested_textures) {
		//Only load if spritesheets not yet loaded
		if (!media.FetchTextureSpritesheet(texture_name)->IsSpritesheetsCreated()) {
			loader_fn(texture_name, media, props);
		}
	}
}



CountedArray_Replace<std::thread, ESO_VAR_WORLD_LOADING_THREAD_MAX> running_world_loads;
std::vector<std::function<void(unsigned int)>> pending_world_loads;

void esoteric_load_world(const std::string& world_name, World& world, AssetCollection& assets, const AssetProps& props) {
	std::string load_dir = file::GetExecutableDirectory();
	std::string file_name = load_dir + props.WorldDirectory + world_name + props.WorldExtension;
	BinaryReader reader(file_name);

	//Open and read the entire binary file
	reader.Init();
	if (reader.IsError()) {
		std::cout << "ESO_LOAD_ERROR: The file " << file_name << " may not exist!" << std::endl;
	
	} else {
		//Determine how many entities present in world
		unsigned int num_entities = reader.NextUInt();

		//Construct each entity based on auto-generated entity_routing function
		for (unsigned int i = 0; i < num_entities; i++) {
			unsigned int class_id = reader.NextUInt();
			assets::LoadEntity(reader, class_id, world, assets);

			if (reader.IsError()) {
				std::cout << "ESO_LOAD_ERROR: There was an error with loading entity in the binary reader!" << std::endl;
				return;
			}
		}

		//Determine how many view types in world
		unsigned short num_view_types = reader.NextUShort();

		//Perform for each view type
		for (unsigned short view_idx = 0; view_idx < num_view_types; view_idx++) {

			//Determine how many view models present within type
			unsigned int num_view_models = reader.NextUInt();

			for (unsigned int i = 0; i < num_view_models; i++) {
				unsigned int vm_class_id = reader.NextUInt();
				assets::LoadViewModel(reader, view_idx, vm_class_id, world, assets);	//TODO: implement -- throwing errors -- not yet defined

				if (reader.IsError()) {
					std::cout << "ESO_LOAD_ERROR: There was an error with loading view model in the binary reader!" << std::endl;
					return;
				} 
			}
		}

	}
}

//Loads medium-tier assets (world-level): world (entities, views), spritesheets (as needed by views), and heavy assets (possibly asynchronously)
//Incrementally increases load status of world after certain types of assets are loaded; allowing dev access to this info for tiered loading
void LoadWorldAssets(const std::string& world_name, World& world, AssetCollection& assets, const AssetProps& props, WorldLoader world_loader, SpritesheetLoader sheet_loader) {
	
	// Loading medium-tier assets (synchronously)
	world_loader(world_name, world, assets, props);

	// Create Texture_Spritesheet instance for each requested texture
	//TODO: this is a little bit hacky ... not sure if this should be moved...
	for (const auto& texture_name : requested_textures) {
		assets.Visuals.FetchTextureSpritesheet(texture_name);
	}

	// Loading heavy-tier assets (asynchronously)
	std::thread spritesheet_loading(LoadSpritesheets, std::ref(assets.Visuals), std::ref(props), sheet_loader);
	std::thread texture_loading(LoadTextures, std::ref(assets.Visuals), std::ref(props));
	//TODO: perform the same for sounds and music loading

	//TODO: this `should` be doable BEFORE images load because entities shouldn't need to know about images or spritesheets (only views)
	world.RefLoadStatus() = LoadStatus::WorldLoaded;

	spritesheet_loading.join();
	texture_loading.join();

	world.RefLoadStatus() = LoadStatus::GraphicsLoaded;

	// sound_loading.join();
	// music_loading.join();
	// world.RefLoadStatus() = LoadStatus::AudioLoaded;
	world.RefLoadStatus() = LoadStatus::Done;
}

void QueueLoadWorldAssets(const std::string& world_name, World& world, AssetCollection& assets, const AssetProps& props, WorldLoader world_loader, SpritesheetLoader sheet_loader) {
	auto load_process = [&](unsigned short index) mutable {
		LoadWorldAssets(world_name, world, assets, props, world_loader, sheet_loader);

		//marks that this thread is done and can be replaced
		running_world_loads.PushReplaceIndex(index);
	};
	if (!running_world_loads.IsFull()) {
		running_world_loads.PushOrReplace(std::thread(load_process, running_world_loads.GetNextIndex()));
	} else {
		pending_world_loads.push_back(load_process);
	}
}

void UpdateLoadingThreads() {
	if (pending_world_loads.size() > 0 && running_world_loads.GetSize() < running_world_loads.GetCapacity()) {
		// Move function from pending to running, as a thread with param next running index
		running_world_loads.PushOrReplace(
			std::thread(std::move(pending_world_loads[0]), running_world_loads.GetNextIndex())
		);
		pending_world_loads.erase(pending_world_loads.begin());
	}
}

}

}