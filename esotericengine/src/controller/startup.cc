#include "controller/startup.h"

#include "controller/transition.h"

//TODO: temp
#include <iostream>

int main(int argc, char** argv) {

    eso::PropsCollection props;
    props.Game.argc = argc;
    props.Game.argv = argv;

    eso::InputConfig input_config;
    std::vector<eso::GameState> states;
    short starting_state = 0;

    eso::AssetCollection assets(props.Asset);
    eso::startup::Init(props, input_config, assets, states, starting_state);
    eso::InputModel input_model(std::move(input_config));

    eso::Game game = {
        states,
        starting_state,
        props,
        assets,
        input_model
    };

    std::cout << "main in ImageDirectory: " << game.StartupProps.Asset.ImageDirectory <<std::endl;

    eso::startup::Execute(game);

    return 0;
}

namespace eso {

namespace startup {

void Execute(Game& app) {

    /* Contains the entire startup process from pre-window, to window creation, to entrance of loop threads and   */

    // TODO: Load default volatile worlds for default state here -- this should perhaps be done in the state loading process, wherever that would happen??
    // GameState* starting_state = app.CurrentState;
    // for (auto it = starting_state->) {

    // }

    // TODO: when do we LoadQueuedTextures? Is this done by the Update loop when changing state or worlds??

    // BeforeOpen Hook function called here
    auto before_open = app.StartupProps.Hooks.BeforeOpen;
    if (before_open != nullptr) {
        before_open(app);
    }

    std::cout << "after before open" <<std::endl;

    // Creation of sf::Window based on GraphicsProps
    auto& game_props = app.StartupProps.Game;
    auto& graphics_props = app.StartupProps.Graphics;

    sf::RenderWindow window(
            sf::VideoMode(graphics_props.WidthPixels, graphics_props.HeightPixels),
            game_props.Title,
            graphics_props.Style
    );
    app.Window = &window;

    std::cout << "after window" <<std::endl;

    // AfterOpen Hook function called here
    auto after_open = app.StartupProps.Hooks.AfterOpen;
    if (after_open != nullptr) {
        after_open(app);
    }

    std::cout << "image directory from execute: " << app.StartupProps.Asset.ImageDirectory << std::endl;

    // Enter game states until app is ready to close
    while (!app.Closing) {
        transition::EnterNextState(app);
    }

    // BeforeClose Hook function called here
    auto before_close = app.StartupProps.Hooks.BeforeClose;
    if (before_close != nullptr) {
        before_close(app);
    }

    window.close();

    // AfterClose Hook function called here
    auto after_close = app.StartupProps.Hooks.AfterClose;
    if (after_close != nullptr) {
        after_close(app);
    }

    //TODO: Destroy or free any other resources here...
}

}

}