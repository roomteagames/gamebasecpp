#include "controller/physics/collision.h"

#include "controller/physics/constraint.h"
#include "util/counted_array.tpp"
#include "constants.h"
#include "util/vector_utils.h"

#include <cmath>

//TODO: temp
#include <iostream>
#include "controller/timing.h"

namespace eso {

Collision::Collision(CollisionMember&& _first, CollisionMember&& _second):
        First(std::move(_first)),
        Second(std::move(_second)),
        Sweep(-1),
        IsDirect(false),
        IsSolid(_first.MemberEntity->IsSolid() && _second.MemberEntity->IsSolid()) {}

Collision::Collision(CollisionMember&& _first, CollisionMember&& _second, double _sweep, bool _is_direct):
        First(std::move(_first)),
        Second(std::move(_second)),
        Sweep(_sweep),
        IsDirect(_is_direct),
        IsSolid(_first.MemberEntity->IsSolid() && _second.MemberEntity->IsSolid()) {}

CollisionMember* Collision::FetchEdgeMember() {
    if (!First.IsVertex) {
        return &First;
    } else {
        return &Second;
    }
}

CollisionMember* Collision::FetchVertexMember() {
    if (First.IsVertex) {
        return &First;
    } else {
        return &Second;
    }
}

CollisionMember* Collision::FetchEdgeMemberOrNull() {
    if (!First.IsVertex) {
        return &First;
    } else if (!Second.IsVertex) {
        return &Second;
    }
    return nullptr;
}

CollisionMember* Collision::FetchVertexMemberOrNull() {
    if (First.IsVertex) {
        return &First;
    } else if (Second.IsVertex) {
        return &Second;
    }
    return nullptr;
}

const CollisionMember* Collision::FetchEdgeMember() const {
    if (!First.IsVertex) {
        return &First;
    } else {
        return &Second;
    }
}

const CollisionMember* Collision::FetchVertexMember() const {
    if (First.IsVertex) {
        return &First;
    } else {
        return &Second;
    }
}

const CollisionMember* Collision::FetchEdgeMemberOrNull() const {
    if (!First.IsVertex) {
        return &First;
    } else if (!Second.IsVertex) {
        return &Second;
    }
    return nullptr;
}

const CollisionMember* Collision::FetchVertexMemberOrNull() const {
    if (First.IsVertex) {
        return &First;
    } else if (Second.IsVertex) {
        return &Second;
    }
    return nullptr;
}

CollisionMember::CollisionMember(std::shared_ptr<Entity>& _entity):
        MemberEntity(_entity) {}

CollisionMember::CollisionMember(std::shared_ptr<Entity>& _entity, Edge const* _edge, const sf::Vector2<double>& _min, const sf::Vector2<double>& _max):
        MemberEntity(_entity),
        EdgeAlpha(_edge),
        Min(_min),
        Max(_max) {}

CollisionMember::CollisionMember(std::shared_ptr<Entity>& _entity, Edge const* _edge_alpha, Edge const* _edge_beta, const sf::Vector2<double>& _min, const sf::Vector2<double>& _max):
        MemberEntity(_entity),
        EdgeAlpha(_edge_alpha),
        EdgeBeta(_edge_beta),
        Min(_min),
        Max(_max) {}

CollisionMember::CollisionMember(const CollisionMember& other):
        MemberEntity(other.MemberEntity),
        EdgeAlpha(other.EdgeAlpha),   //shallow copy on edge --> still pointing to same edge
        EdgeBeta(other.EdgeBeta),
        Min(other.Min),
        Max(other.Max),
        MinVelocity(other.MinVelocity),
        MaxVelocity(other.MaxVelocity) {}

CollisionMember::CollisionMember(CollisionMember&& other):
        MemberEntity(other.MemberEntity),
        EdgeAlpha(other.EdgeAlpha),
        EdgeBeta(other.EdgeBeta),
        Min(other.Min),
        Max(other.Max),
        MinVelocity(other.MinVelocity),
        MaxVelocity(other.MaxVelocity)
{
    other.EdgeAlpha = nullptr;
    other.EdgeBeta = nullptr;
}

CollisionMember& CollisionMember::operator=(const CollisionMember& other) {
    MemberEntity = other.MemberEntity;
    EdgeAlpha = other.EdgeAlpha;
    EdgeBeta = other.EdgeBeta;
    Min = other.Min;
    Max = other.Max;
    MinVelocity = other.MinVelocity;
    MaxVelocity = other.MaxVelocity;

    return *this;
}

CollisionMember& CollisionMember::operator=(CollisionMember&& other) {
    MemberEntity = other.MemberEntity;
    EdgeAlpha = other.EdgeAlpha;
    EdgeBeta = other.EdgeBeta;
    Min = other.Min;
    Max = other.Max;
    MinVelocity = other.MinVelocity;
    MaxVelocity = other.MaxVelocity;

    other.EdgeAlpha = nullptr;
    other.EdgeBeta = nullptr;

    return *this;
}

namespace collision {

void Execute(std::vector<std::shared_ptr<Entity>>& entities, std::vector<Collision>& collisions) {
    unsigned short index = 1;
    for (auto it1 = entities.begin(); it1 != entities.end() - 1; it1++) {
        auto& entity = *it1;
        if (!entity->IsNoneCollision()) {
            for (auto it2 = entities.begin() + index; it2 != entities.end(); it2++) {
                auto& other = *it2;
                if (other->IsNoneCollision() || (entity->IsFixed() && other->IsFixed())) continue;
                
                //TODO: --STILL NEEDED? Figure out why IsOverlap returning false for already resting entities on top of each other
                if (entity != other && entity->GetMovementSystem().IsOverlap(other->GetMovementSystem())) {
                    Collision collision({entity}, {other});

                    /*Add collision to list if one of the following is true:
                        a. Collision involves a non-solid entity and bounding boxes are currently overlapping (TODO: enhance for non-AABB in advanced method)
                        b. Collision involves two solids and they are already touching edges at their current position (direct rest)
                        c. Collision involves two solids, is not already colliding, and WILL touch edges before the next frame, must be swept together (direct sweep) (TODO: enhance for non-AABB in advanced method)
                    */
                   if (!collision.IsSolid && entity->IsOverlapBoundingBox(*other.get())) { //a
                       collisions.push_back(std::move(collision));  //IMPORTANT: this move invalidates all pointers to collision, so entities only rely on indexes within collisions vector

                   } else if (direct_rest_aabb(entity, other, &collision) || (!entity->IsOverlapBoundingBox(*other.get()) && direct_sweep_aabb(entity, other, &collision))) {    //b or c
                        collisions.push_back(std::move(collision));  //IMPORTANT: this move invalidates all pointers to collision, so entities only rely on indexes within collisions vector

                        process_collision(entity, collisions, collisions.size() - 1);
                        process_collision(other, collisions, collisions.size() - 1);
                    }
                }
            }
        }
        index++;
    }
}

//TODO: redo based on first Execute above
void Execute(std::vector<std::shared_ptr<Entity>>& entities, unsigned short sector_index, unsigned short grid_dimension, unsigned short units_per_sector) {
    unsigned short index = 0;
    for (auto it1 = entities.begin(); it1 != entities.end() - 1; it1++) {
        auto& entity = *it1;
        for (auto it2 = entities.begin() + index; it2 != entities.end(); it2++) {
            auto& other = *it2;
            if (!(entity->IsSectorBorderRight() && other->IsSectorBorderRight()) && !(entity->IsSectorBorderDown() && other->IsSectorBorderDown()) && entity->GetMovementSystem().IsOverlap(other->GetMovementSystem())) {
                if (entity->IsOverlapBoundingBox(*other) || (direct_sweep_aabb(entity, other, nullptr) && entity->IsOverlapBoundingBox(*other))) {
                    /*if (entity->OnCollision(other) && other->OnCollision(entity)) {
                        correct_collision_positions(*entity, *other);
                        momentum::Execute(*entity, *other);
                    }*/
                }
            }
        }
        update_border_flags(entity, sector_index, grid_dimension, units_per_sector);
        index++;
    }
}

//TODO: redo based on first Execute above
void ExecuteAdvanced(std::vector<std::shared_ptr<Entity>>& entities) {
    unsigned short index = 0;
    for (auto it1 = entities.begin(); it1 != entities.end() - 1; it1++) {
        auto& entity = *it1;
        for (auto it2 = entities.begin() + index; it2 != entities.end(); it2++) {
            auto& other = *it2;
            if (entity->GetMovementSystem().IsOverlap(other->GetMovementSystem())) {
                if (entity->IsOverlapBoundingBox(*other) || (direct_sweep_aabb(entity, other, nullptr) && entity->IsOverlapBoundingBoxProspect(*other))) {
                    /* test_advanced_collision(*entity, *other);*/
                }
            }
        }
        index++;
    }
}

//TODO: redo based on first Execute above
void ExecuteAdvanced(std::vector<std::shared_ptr<Entity>>& entities, unsigned short sector_index, unsigned short grid_dimension, unsigned short units_per_sector) {
    unsigned short index = 0;
    for (auto& entity : entities) {
        for (auto it = entities.begin() + index; it != entities.end(); it++) {
            auto& other = *it;
            if (!(entity->IsSectorBorderRight() && other->IsSectorBorderRight()) && !(entity->IsSectorBorderDown() && other->IsSectorBorderDown()) && entity->GetMovementSystem().IsOverlap(other->GetMovementSystem())) {
                if (entity->IsOverlapBoundingBox(*other) || (direct_sweep_aabb(entity, other, nullptr) && entity->IsOverlapBoundingBox(*other))) {
                    /* test_advanced_collision(*entity, *other);*/
                }
            }
        }
        update_border_flags(entity, sector_index, grid_dimension, units_per_sector);
        index++;
    }
}

void Refine(std::vector<Collision>& collisions) {
    unsigned int index = 0;
    for (Collision& collision : collisions) {
        if (collision.IsSolid && !collision.IsDirect) {
            std::shared_ptr<Entity>& primary = collision.PrimaryFirst ? collision.First.MemberEntity : collision.Second.MemberEntity;
            std::shared_ptr<Entity>& non_primary = collision.PrimaryFirst ? collision.Second.MemberEntity : collision.First.MemberEntity;

            bool is_valid_sweep = indirect_sweep_aabb(primary, non_primary, &collision); // Only returns true if indirect sweep results in primary touching non-primary (misses not allowed)
            process_collision(primary, collisions, index);
            if (is_valid_sweep) {
                non_primary->PushSecondaryCollisionIndexIfNotPresent(index, collisions);
            }
        }
        index++;
    }
}

void update_border_flags(std::shared_ptr<Entity>& entity, unsigned short sector_index, unsigned short grid_dimension, unsigned short units_per_sector) {
    if (!entity->IsSectorBorderRight() || !entity->IsSectorBorderDown()) {
        sf::Vector2<double> sector_max = {
            (double) (sector_index % grid_dimension + 1) * units_per_sector,
            (double) (sector_index / grid_dimension + 1) * units_per_sector
        };
        const sf::Vector2<double>& bounding_max = entity->GetShape().ShapeDefinition->GetBoundingBox().Max;
        if (bounding_max.x > sector_max.x) {
            entity->SectorBorderRight() = true;
        }
        if (bounding_max.y > sector_max.y) {
            entity->SectorBorderDown() = true;
        }
    }
}

//TODO: normal, rest, and min/max should not be set till AFTER sweep is done and prospect is set
void SetCollisionProperties(Collision* c) {
    auto& entity_a = c->First.MemberEntity;
    auto& entity_b = c->Second.MemberEntity;

    if (c->First.EdgeAlpha != nullptr && c->Second.EdgeAlpha != nullptr) {
        //Edge-Edge collision
        c->Type = CollisionType::EdgeEdge;
        c->First.IsVertex = false;
        c->Second.IsVertex = false;

        //TODO: set rest, min/max, and friction
        calculate_collision_min_max_ee(c);    //calculation using alpha edge for both members
        c->UnitNormal = c->FetchEdgeMember()->EdgeAlpha->GetUnitNormalVector();

    } else if (c->First.EdgeAlpha != nullptr && c->First.MinEdge != nullptr && c->First.MaxEdge != nullptr) {
        //Edge-Vertex collision
        c->Type = CollisionType::VertexEdge;
        c->First.IsVertex = false;
        c->Second.IsVertex = true;

        //TODO: set rest, min/max, and friction
        calculate_collision_min_max_ve(c);
        c->UnitNormal = c->FetchEdgeMember()->EdgeAlpha->GetUnitNormalVector();
    
    } else if (c->Second.EdgeAlpha != nullptr && c->Second.MinEdge != nullptr && c->Second.MaxEdge != nullptr) {
        //Vertex-Edge collision
        c->Type = CollisionType::VertexEdge;
        c->First.IsVertex = true;
        c->Second.IsVertex = false;

        //TODO: set rest, min/max, and friction
        calculate_collision_min_max_ve(c);
        c->UnitNormal = c->FetchEdgeMember()->EdgeAlpha->GetUnitNormalVector();
    
    } else if (c->First.MinEdge != nullptr && c->First.MaxEdge != nullptr && c->Second.MinEdge != nullptr && c->Second.MaxEdge != nullptr) {
        //Vertex-Vertex collision
        c->Type = CollisionType::VertexVertex;
        calculate_collision_min_max_vv(c);
        c->UnitNormal = c->First.MinEdge->GetUnitNormalVector();    //TODO: temp hack for now
    }

    if (c->First.EdgeAlpha != nullptr && c->Second.EdgeAlpha != nullptr) {
        //Store restitution: product of both edges' restitutions -- this will need extra logic when NOT EdgeEdge
        double first_rest = c->First.MemberEntity->GetShape().GetEdgeRestitution(c->First.EdgeAlpha);
        double second_rest = c->Second.MemberEntity->GetShape().GetEdgeRestitution(c->Second.EdgeAlpha);
        c->Restitution = first_rest * second_rest;
    }
}

//TODO: URGENT! FIX direct rest to keep track of overlap and set edges with the MOST overlap
bool direct_rest_aabb(std::shared_ptr<Entity>& a, std::shared_ptr<Entity>& b, Collision* collision) {
    //touching side edges
    if (a->GetBoundingBox().IsTouchingLeft(b->GetBoundingBox())) {
        /* TODO: TEMP */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; dr A-- h_overlap: " << a->GetBoundingBox().GetHorizontalOverlap(b->GetBoundingBox()) << ", v_overlap: " << a->GetBoundingBox().GetVerticalOverlap(b->GetBoundingBox()) << std::endl;
        collision->Sweep = 0;
        collision->IsDirect = true;
        
        collision->First.EdgeAlpha = &a->GetShape().ShapeDefinition->GetEdges()[1];
        collision->Second.EdgeAlpha = &b->GetShape().ShapeDefinition->GetEdges()[3];

        return true;

    } else if (a->GetBoundingBox().IsTouchingRight(b->GetBoundingBox())) {
        /* TODO: TEMP */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; dr B-- h_overlap: " << a->GetBoundingBox().GetHorizontalOverlap(b->GetBoundingBox()) << ", v_overlap: " << a->GetBoundingBox().GetVerticalOverlap(b->GetBoundingBox()) << std::endl;
        collision->Sweep = 0;
        collision->IsDirect = true;
        
        collision->First.EdgeAlpha = &a->GetShape().ShapeDefinition->GetEdges()[3];
        collision->Second.EdgeAlpha = &b->GetShape().ShapeDefinition->GetEdges()[1];

        return true;
    }

    //same velocity, touching top/bottom edges
    if (a->GetBoundingBox().IsTouchingAbove(b->GetBoundingBox())) {
        /* TODO: TEMP */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; dr C-- h_overlap: " << a->GetBoundingBox().GetHorizontalOverlap(b->GetBoundingBox()) << ", v_overlap: " << a->GetBoundingBox().GetVerticalOverlap(b->GetBoundingBox()) << std::endl;
        collision->Sweep = 0;
        collision->IsDirect = true;
        
        collision->First.EdgeAlpha = &a->GetShape().ShapeDefinition->GetEdges()[2];
        collision->Second.EdgeAlpha = &b->GetShape().ShapeDefinition->GetEdges()[0];

        return true;

    } else if (a->GetBoundingBox().IsTouchingBelow(b->GetBoundingBox())) {
        /* TODO: TEMP */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; dr D-- h_overlap: " << a->GetBoundingBox().GetHorizontalOverlap(b->GetBoundingBox()) << ", v_overlap: " << a->GetBoundingBox().GetVerticalOverlap(b->GetBoundingBox()) << std::endl;
        collision->Sweep = 0;
        collision->IsDirect = true;
        
        collision->First.EdgeAlpha = &a->GetShape().ShapeDefinition->GetEdges()[0];
        collision->Second.EdgeAlpha = &b->GetShape().ShapeDefinition->GetEdges()[2];
        
        return true;
    }
    return false;
}

//TODO: perhaps this alg needs to be made more reusible -- will need to ALWAYS be used instead of collision correction
//TODO: at times these new positions will need to be set at the actual positions; where as at other times, they may need to be set as proposed positions -- perhaps should return instead of setting
// no rotation allowed here -- will always produce an EdgeEdge (EE) collision
bool direct_sweep_aabb(std::shared_ptr<Entity>& a, std::shared_ptr<Entity>& b, Collision* collision) {
    /*      Formula to calculate Sweep timestep
                where edges share the same x or y positions and single-dimension distance = 1/2h1 + 1/2h2 or 1/2w1 + 1/2w2
            
            sdi = ((p2d - p1d) +- 1/2(d1 + d2)) / (v1d - v2d)
                where d is a dimension width (or x) or height (or y)
                    i is 1 or 2, representing the formula containing either the positive or negative single-dimension sum
                    p1d is isolated dimension d (x or y) of the position of entity a
                    p2d is isolated dimension d (x or y) of the position of entity b
                    d1 is the width or height of entity a
                    d2 is the width or height of entity b
                    v1d is the isolated component (i or j) of the velocity of entity a
                    v2d is the isolated component (i or j) of the velocity of entity b
       
       1. Calculate the four possible s-values based on the formula above, keeping as possibilities only those that
            a) are between 0 and 1, inclusive
            b) the new positions of the rects moved by s*v pass the rectangular collision test
       2. Set prospect positions for entities
       3. Determine if a valid MISS has occurred, if no sweep, but velocity segments intersect */
    
    NanoTicker t;

    double s = 999;

    double denominator_x = a->GetPhysics().Velocity.x - b->GetPhysics().Velocity.x;
    double denominator_y = a->GetPhysics().Velocity.y - b->GetPhysics().Velocity.y;

    unsigned int width_a = a->GetWidth();
    unsigned int height_a = a->GetHeight();
    unsigned int width_b = b->GetWidth();
    unsigned int height_b = b->GetHeight();

    //indexes of colliding edges between a and b; will pull from shape edges vector
    short col_edge_index_a = -1;
    short col_edge_index_b = -1;
    sf::Vector2<double>* prospect_a = nullptr;
    sf::Vector2<double>* prospect_b = nullptr;

    //TODO: make the logic for each of these four blocks into a reusible function, called 4 times

    /* x, positive sum - a is right of b */
    double sx1 = ((b->GetPosition().x - a->GetPosition().x) + (0.5 * (a->GetBoundingBox().GetWidth() + b->GetBoundingBox().GetWidth()))) / denominator_x;
    sf::Vector2<double> prospect_a_x1 = vector::Multiply(sx1, a->GetPhysics().Velocity) + a->GetPosition();
    sf::Vector2<double> prospect_b_x1 = vector::Multiply(sx1, b->GetPhysics().Velocity) + b->GetPosition();
    //TODO: perhaps changes to "IsTouching" -- update formulas above to prospect entities to be 1 unit away from each other... HOW DO YOU DO THAT?
    if (sx1 >= 0 && sx1 <= 1 && BoundingBox(prospect_a_x1, width_a, height_a).IsTouchingRight(BoundingBox(prospect_b_x1, width_b, height_b))) {  //check if translated entities are actually overlapping aabb
        /*TODO: temp */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; A" << std::endl;
        s = sx1;
        col_edge_index_a = 3;
        col_edge_index_b = 1;
    } else {
        sx1 = -1;
    }

    /* x, negative sum - a is left of b */
    double sx2 = ((b->GetPosition().x - a->GetPosition().x) - (0.5 * (a->GetBoundingBox().GetWidth() + b->GetBoundingBox().GetWidth()))) / denominator_x;
    sf::Vector2<double> prospect_a_x2 = vector::Multiply(sx2, a->GetPhysics().Velocity) + a->GetPosition();
    sf::Vector2<double> prospect_b_x2 = vector::Multiply(sx2, b->GetPhysics().Velocity) + b->GetPosition();
    if (sx2 >= 0 && sx2 <= 1 && s > sx2 && BoundingBox(prospect_a_x2, width_a, height_a).IsTouchingLeft(BoundingBox(prospect_b_x2, width_b, height_b))) {
        /*TODO: temp */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; B" << std::endl;
        s = sx2;
        col_edge_index_a = 1;
        col_edge_index_b = 3;
    } else {
        sx2 = -1;
    }

    /* y, positive sum - a is below b */
    double sy1 = ((b->GetPosition().y - a->GetPosition().y) + (0.5 * (a->GetBoundingBox().GetHeight() + b->GetBoundingBox().GetHeight()))) / denominator_y;
    sf::Vector2<double> prospect_a_y1 = vector::Multiply(sy1, a->GetPhysics().Velocity) + a->GetPosition();
    sf::Vector2<double> prospect_b_y1 = vector::Multiply(sy1, b->GetPhysics().Velocity) + b->GetPosition();
    if (sy1 >= 0 && sy1 <= 1 && s > sy1 && BoundingBox(prospect_a_y1, width_a, height_a).IsTouchingBelow(BoundingBox(prospect_b_y1, width_b, height_b))) {
        /*TODO: temp */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; C" << std::endl;
        s = sy1;
        col_edge_index_a = 0;
        col_edge_index_b = 2;
    } else {
        sy1 = -1;
    }

    /* y, negative sum - b is above a */
    double sy2 = ((b->GetPosition().y - a->GetPosition().y) - (0.5 * (a->GetBoundingBox().GetHeight() + b->GetBoundingBox().GetHeight()))) / denominator_y;
    sf::Vector2<double> prospect_a_y2 = vector::Multiply(sy2, a->GetPhysics().Velocity) + a->GetPosition();
    sf::Vector2<double> prospect_b_y2 = vector::Multiply(sy2, b->GetPhysics().Velocity) + b->GetPosition();
    if (sy2 >= 0 && sy2 <= 1 && s > sy2 && BoundingBox(prospect_a_y2, width_a, height_a).IsTouchingAbove(BoundingBox(prospect_b_y2, width_b, height_b))) {
        /*TODO: temp */ std::cout << "FIRST: " << a->GetInstanceId() << ", SECOND: " << b->GetInstanceId() << "; D" << std::endl;
        s = sy2;
        col_edge_index_a = 2;
        col_edge_index_b = 0;
    } else {
        sy2 = -1;
    }

    /* TODO: TEMP*/
    if ((a->GetInstanceId() == 0 || b->GetInstanceId() == 0) && (sx1 > -1 || sx2 > -1)) {
        std::cout << "octo horizontal sweep. time to stop..." << std::endl;
    }

    //if s is valid, set prospect positions
    if (s >= 0 && s < 1) {
        collision->IsDirect = true; //mark as a direct collision
        collision->Sweep = s;
        collision->First.EdgeAlpha = &a->GetShape().ShapeDefinition->GetEdges()[col_edge_index_a];
        collision->Second.EdgeAlpha = &b->GetShape().ShapeDefinition->GetEdges()[col_edge_index_b];

        unsigned long time = t();
        // /* */if (a->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "successful direct sweep operation, s = " << collision->Sweep << ", prospect: " << vector::ToString(collision->First.MemberEntity->GetProspectPosition()) << ", otherId: " << collision->Second.MemberEntity->GetInstanceId() << ", other prospect: " << vector::ToString(collision->Second.MemberEntity->GetProspectPosition()) << ", sweep operation took: " << time << std::endl;

        return true;
    } else {    //s is not valid, check for MISS
        //TODO: we also need to calculate intersection between vectors to determine is there is a "miss", assuming none of the s-value produce a valid collision
        sf::Vector2<double> prime_pos_a = a->GetPosition() + a->GetPhysics().Velocity;
        sf::Vector2<double> prime_pos_b = b->GetPosition() + b->GetPhysics().Velocity;
        
        LinearEdge mvmt_a = LinearEdge::CreateLinearEdge(a->GetPosition(), prime_pos_a);
        LinearEdge mvmt_b = LinearEdge::CreateLinearEdge(b->GetPosition(), prime_pos_b);

        sf::Vector2<double> intersection = GetLinearIntersection(mvmt_a, mvmt_b);
        if (intersection != mathconstants::NO_SOLUTION_VEC) { //Mvmt intersects, but no valid sweep --> this is a MISS!
            unsigned long time = t();
            /* */if (a->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "direct sweep operation MISS, s = " << collision->Sweep << ", otherId: " << collision->Second.MemberEntity->GetInstanceId() << ", sweep operation took: " << time << std::endl;
            collision->IsValid = false;
            return true;
        }
    }
    /* */if (a->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "no successful direct sweep with other: " << b->GetInstanceId() << "... sx1: " << sx1 << ", sx2: " << sx2 << ", sy1: " << sy1 << ", sy2: " << sy2 << ", dx: " << denominator_x << ", dy: " << denominator_y << std::endl;
    return false;
}

//TODO: merge code with direct sweep so there is only one function!!!
bool indirect_sweep_aabb(std::shared_ptr<Entity>& primary, std::shared_ptr<Entity>& non_primary, Collision* collision) {
    /*      Formula to calculate Indirect Sweep timestep for Primary entity
                where edges share the same x or y positions and single-dimension distance = 1/2h1 + 1/2h2 or 1/2w1 + 1/2w2
            
            sdi = ((p2d - p1d) +- 1/2(d1 + d2)) / v1d
                where d is a dimension width (or x) or height (or y)
                    i is 1 or 2, representing the formula containing either the positive or negative single-dimension sum
                    p1d is isolated dimension d (x or y) of the position of entity primary
                    p2d is isolated dimension d (x or y) of the **PROSPECT POSITION** of entity non_primary
                    d1 is the width or height of entity primary
                    d2 is the width or height of entity non_primary
                    v1d is the isolated component (i or j) of the velocity of entity primary
       
       1. Calculate the four possible s-values based on the formula above, keeping as possibilities only those that
            a) are between 0 and 1, inclusive
            b) the new positions of the rects moved by s*v pass the rectangular collision test
       2. Set prospect position for entity primary */

    NanoTicker t;

    double s = 999;

    unsigned int width_primary = primary->GetWidth();
    unsigned int height_primary = primary->GetHeight();
    unsigned int width_nonprimary = non_primary->GetWidth();
    unsigned int height_nonprimary = non_primary->GetHeight();

    //indexes of colliding edges between a and b; will pull from shape edges vector
    short col_edge_index_primary = -1;
    short col_edge_index_nonprimary = -1;

    const sf::Vector2<double>& primary_velocity = primary->GetPhysics().Velocity;

    sf::Vector2<double>* prospect_primary = nullptr;
    const sf::Vector2<double>& prospect_nonprimary = non_primary->GetProspectPosition();
    BoundingBox nonprimary_box(prospect_nonprimary, width_nonprimary, height_nonprimary);

    //TODO: make the logic for each of these four blocks into a reusible function, called 4 times
    //TODO: URGENT: HOW CAN WE PROVE THAT THE PRIMARY IN THIS ALG IS ALWAYS THE ENTITY THAT IS NOT INVOLVED IN THE OTHER DIRECT COLLISION?

    /* x, positive sum - primary is right of nonprimary */
    double sx1 = ((prospect_nonprimary.x - primary->GetPosition().x) + (0.5 * (primary->GetBoundingBox().GetWidth() + non_primary->GetBoundingBox().GetWidth()))) / primary_velocity.x;
    sf::Vector2<double> prospect_primary_x1 = vector::Multiply(sx1, primary_velocity) + primary->GetPosition();
    if (sx1 >= 0 && sx1 <= 1 && BoundingBox(prospect_primary_x1, width_primary, height_primary).IsTouchingRight(nonprimary_box)) {  //check if translated entities are actually overlapping aabb
        s = sx1;
        col_edge_index_primary = 3;
        col_edge_index_nonprimary = 1;
    } else {
        sx1 = -1;
    }

    /* x, negative sum - primary is left of nonprimary */
    double sx2 = ((prospect_nonprimary.x - primary->GetPosition().x) - (0.5 * (primary->GetBoundingBox().GetWidth() + non_primary->GetBoundingBox().GetWidth()))) / primary_velocity.x;
    sf::Vector2<double> prospect_primary_x2 = vector::Multiply(sx2, primary_velocity) + primary->GetPosition();
    if (sx2 >= 0 && sx2 <= 1 && s > sx2 && BoundingBox(prospect_primary_x2, width_primary, height_primary).IsTouchingLeft(nonprimary_box)) {
        s = sx2;
        col_edge_index_primary = 1;
        col_edge_index_nonprimary = 3;
    } else {
        sx2 = -1;
    }

    /* y, positive sum - primary is below nonprimary */
    double sy1 = ((prospect_nonprimary.y - primary->GetPosition().y) + (0.5 * (primary->GetBoundingBox().GetHeight() + non_primary->GetBoundingBox().GetHeight()))) / primary_velocity.y;
    sf::Vector2<double> prospect_primary_y1 = vector::Multiply(sy1, primary_velocity) + primary->GetPosition();
    if (sy1 >= 0 && sy1 <= 1 && s > sy1 && BoundingBox(prospect_primary_y1, width_primary, height_primary).IsTouchingBelow(nonprimary_box)) {
        s = sy1;
        col_edge_index_primary = 0;
        col_edge_index_nonprimary = 2;
    } else {
        sy1 = -1;
    }

    /* y, negative sum primary is above nonprimary */
    double sy2 = ((prospect_nonprimary.y - primary->GetPosition().y) - (0.5 * (primary->GetBoundingBox().GetHeight() + non_primary->GetBoundingBox().GetHeight()))) / primary_velocity.y;
    sf::Vector2<double> prospect_primary_y2 = vector::Multiply(sy2, primary_velocity) + primary->GetPosition();
    if (sy2 >= 0 && sy2 <= 1 && s > sy2 && BoundingBox(prospect_primary_y2, width_primary, height_primary).IsTouchingAbove(nonprimary_box)) {
        s = sy2;
        col_edge_index_primary = 2;
        col_edge_index_nonprimary = 0;
    } else {
        sy2 = -1;
    }

    /* TODO: TEMP*/
    if ((primary->GetInstanceId() == 0 || non_primary->GetInstanceId() == 0) && (sx1 > -1 || sx2 > -1)) {
        std::cout << "INDIRECT - octo horizontal sweep. time to stop..." << std::endl;
    }

    //if s is valid, set primary prospect position
    if (s >= 0 && s < 1) {
        collision->Sweep = s;

        if (collision->PrimaryFirst) {  //being sensitive about which member is which in pre-existing collision
            collision->First.EdgeAlpha = &primary->GetShape().ShapeDefinition->GetEdges()[col_edge_index_primary];
            collision->Second.EdgeAlpha = &non_primary->GetShape().ShapeDefinition->GetEdges()[col_edge_index_nonprimary];    
        } else {
            collision->Second.EdgeAlpha = &primary->GetShape().ShapeDefinition->GetEdges()[col_edge_index_primary];
            collision->First.EdgeAlpha = &non_primary->GetShape().ShapeDefinition->GetEdges()[col_edge_index_nonprimary];
        }

        unsigned long time = t();
        /* */if (primary->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "successful indirect sweep operation, s = " << collision->Sweep << ", otherId: " << collision->Second.MemberEntity->GetInstanceId() << ", sweep operation took: " << time << std::endl;

        return true;
    }
    /* */if (primary->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "no successful indirect sweep with nonprimary: " << non_primary->GetInstanceId() << "... sx1: " << sx1 << ", sx2: " << sx2 << ", sy1: " << sy1 << ", sy2: " << sy2 << std::endl;
    return false;
}

void set_min_max_velocities(Collision* collision) {
    // Setting point velocity at extremities
    const sf::Vector2<double>& first_min = collision->First.Min;
    const sf::Vector2<double>& first_max = collision->First.Max;
    const sf::Vector2<double>& second_min = collision->Second.Min;
    const sf::Vector2<double>& second_max = collision->Second.Max;

    const sf::Vector2<double>& first_velocity = collision->First.MemberEntity->GetPhysics().Velocity;
    const sf::Vector2<double>& second_velocity = collision->Second.MemberEntity->GetPhysics().Velocity;
    
    double first_rotation = collision->First.MemberEntity->GetPhysics().RotationalVelocity;
    double second_rotation = collision->Second.MemberEntity->GetPhysics().RotationalVelocity;

    collision->First.MinVelocity = first_velocity + sf::Vector2<double>(first_rotation * first_min.x, -1 * first_rotation * first_min.y);
    collision->First.MaxVelocity = first_velocity + sf::Vector2<double>(first_rotation * first_max.x, -1 * first_rotation * first_max.y);
    collision->Second.MinVelocity = second_velocity + sf::Vector2<double>(second_rotation * second_min.x, -1 * second_rotation * second_min.y);
    collision->Second.MaxVelocity = second_velocity + sf::Vector2<double>(second_rotation * second_max.x, -1 * second_rotation * second_max.y);
}

//This scenario only works for edge-edge collisions
void calculate_collision_min_max_ee(Collision* collision) {
    Edge const* firstEdge = collision->First.EdgeAlpha; 
    Edge const* secondEdge = collision->Second.EdgeAlpha;

    std::shared_ptr<Entity>& firstEntity = collision->First.MemberEntity;
    std::shared_ptr<Entity>& secondEntity = collision->Second.MemberEntity;

    //TODO: URGENT is this note still accurate? -- NOTE: the following calculations will NOT provide any useful info for collisions involving vertical edges

    //Calculate first min based on x values or y values (if x values equal)
    sf::Vector2<double> firstMin;
    if (firstEdge->GetB().x > firstEdge->GetA().x || (firstEdge->GetB().x == firstEdge->GetA().x && firstEdge->GetB().y >= firstEdge->GetA().y)) {
        firstMin = firstEdge->GetA();
    } else {
        firstMin = firstEdge->GetB();
    }

    //Calculate second min based on x values or y values (if x values equal)
    sf::Vector2<double> secondMin;
    if (secondEdge->GetB().x > secondEdge->GetA().x || (secondEdge->GetB().x == secondEdge->GetA().x && secondEdge->GetB().y >= secondEdge->GetA().y)) {
        secondMin = secondEdge->GetA();
    } else {
        secondMin = secondEdge->GetB();
    }

    //Calculate first max based on x values or y values (if x values equal)
    sf::Vector2<double> firstMax;
    if (firstEdge->GetB().x < firstEdge->GetA().x || (firstEdge->GetB().x == firstEdge->GetA().x && firstEdge->GetB().y <= firstEdge->GetA().y)) {
        firstMax = firstEdge->GetA();
    } else {
        firstMax = firstEdge->GetB();
    }

    //Calculate second max based on x values or y values (if x values equal)
    sf::Vector2<double> secondMax;
    if (secondEdge->GetB().x < secondEdge->GetA().x || (secondEdge->GetB().x == secondEdge->GetA().x && secondEdge->GetB().y <= secondEdge->GetA().y)) {
        secondMax = secondEdge->GetA();
    } else {
        secondMax = secondEdge->GetB();
    }

    //Whichever min is less than the other min must be updated to be at the same absolute position -- but be represented in terms of its offset relative to entity position
    if (secondMin.x + secondEntity->GetPosition().x < firstMin.x + firstEntity->GetPosition().x || 
            (secondMin.x + secondEntity->GetPosition().x == firstMin.x + firstEntity->GetPosition().x && secondMin.y + secondEntity->GetPosition().y <= firstMin.y + firstEntity->GetPosition().y)) {
        secondMin = firstMin + firstEntity->GetPosition() - secondEntity->GetPosition();
    } else {
        firstMin = secondMin + secondEntity->GetPosition() - firstEntity->GetPosition();
    }

    //Whichever max is greater than the other max must be updated to be at the same absolute position -- but be represented in terms of its offset relative to entity position
    if (secondMax.x + secondEntity->GetPosition().x > firstMax.x + firstEntity->GetPosition().x ||
            (secondMax.x + secondEntity->GetPosition().x == firstMax.x + firstEntity->GetPosition().x && secondMax.y + secondEntity->GetPosition().y >= firstMax.y + firstEntity->GetPosition().y)) {
        secondMax = firstMax + firstEntity->GetPosition() - secondEntity->GetPosition();
    } else {
        firstMax = secondMax + secondEntity->GetPosition() - firstEntity->GetPosition();
    }

    //Setting member properties
    collision->First.Min = firstMin;
    collision->First.Max = firstMax;
    collision->Second.Min = secondMin;
    collision->Second.Max = secondMax;

    set_min_max_velocities(collision);
}

//Figures out which vertex surrounding edge is the alpha --> if |Aa dot B| > |Ab dot B|; then Aa is the ALPHA edge; where Aa is vertex MIN edge and Ab is vertex MAX edge
void set_edge_alpha_beta_ve(CollisionMember& min_max_member, Edge const* min_edge, Edge const* max_edge, Edge const* alpha_edge) {
    double min_dot = abs(vector::Dot(min_edge->GetUnitSecantVector(), alpha_edge->GetSecantVector()));
    double max_dot = abs(vector::Dot(max_edge->GetUnitSecantVector(), alpha_edge->GetSecantVector()));

    if (min_dot >= max_dot) {
        min_max_member.EdgeAlpha = min_edge;
        min_max_member.EdgeBeta = max_edge;
    } else {
        min_max_member.EdgeAlpha = max_edge;
        min_max_member.EdgeBeta = min_edge;
    }
}

//This version for vertex-edge collisions
void calculate_collision_min_max_ve(Collision* collision) {
    if (collision->First.IsVertex) {
        Edge const* first_min = collision->First.MinEdge;
        Edge const* first_max = collision->First.MaxEdge;
        Edge const* second_alpha = collision->Second.EdgeAlpha;

        set_edge_alpha_beta_ve(collision->First, first_min, first_max, second_alpha);
        collision->First.Min = first_min->GetB();
        collision->First.Max = first_max->GetA();

        collision->Second.Min = first_min->GetB() + collision->First.MemberEntity->GetPosition() - collision->Second.MemberEntity->GetPosition();
        collision->Second.Max = collision->Second.Min;
    
    } else {
        Edge const* second_min = collision->Second.MinEdge;
        Edge const* second_max = collision->Second.MaxEdge;
        Edge const* first_alpha = collision->First.EdgeAlpha;

        set_edge_alpha_beta_ve(collision->Second, second_min, second_max, first_alpha);
        collision->Second.Min = second_min->GetB();
        collision->Second.Max = second_max->GetA();

        collision->First.Min = second_min->GetB() + collision->Second.MemberEntity->GetPosition() - collision->First.MemberEntity->GetPosition();
        collision->First.Max = collision->First.Min;
    }
    set_min_max_velocities(collision);
}

void calculate_collision_min_max_vv(Collision* collision) {
    Edge const* first_min = collision->First.MinEdge;
    Edge const* first_max = collision->First.MaxEdge;
    Edge const* second_min = collision->Second.MinEdge;
    Edge const* second_max = collision->Second.MaxEdge;

    collision->First.Min = first_min->GetB();
    collision->First.Max = first_min->GetA();
    collision->Second.Min = second_min->GetB();
    collision->Second.Max = second_min->GetA();

    double min_dot = abs(vector::Dot(first_min->GetUnitSecantVector(), second_min->GetSecantVector()));
    double max_dot = abs(vector::Dot(first_max->GetUnitSecantVector(), second_max->GetSecantVector()));

    if (min_dot >= max_dot) {
        collision->First.EdgeAlpha = first_min;
        collision->First.EdgeBeta = first_max;
        collision->Second.EdgeAlpha = second_min;
        collision->Second.EdgeBeta = second_max;
    } else {
        collision->First.EdgeAlpha = first_max;
        collision->First.EdgeBeta = first_min;
        collision->Second.EdgeAlpha = second_max;
        collision->Second.EdgeBeta = second_min;
    }
    set_min_max_velocities(collision);
}

void process_collision(std::shared_ptr<Entity>& entity, std::vector<Collision>& collisions, unsigned int index) {
    Collision& subject_collision = collisions[index];
    short num_primary_col = entity->GetPrimaryCollisionCount();
    const int* primary_col_indexes = entity->GetPrimaryCollisionIndexes();

    if (subject_collision.Sweep > -1 && (num_primary_col == 0 || subject_collision.Sweep < collisions[primary_col_indexes[0]].Sweep)) {
        //CASE1: subject_collision is entity's new primary collision because it has a less sweep; replace old list of primaries
        for (unsigned short i = 0; i < num_primary_col; i++) {  //Invalidate former primary collisions on entity
            Collision& c = collisions[primary_col_indexes[i]];
            c.IsDirect = false;
            c.IsValid = false;
            if (c.First.MemberEntity == entity) {
                c.PrimaryFirst = false;
            }
        }
        subject_collision.IsValid = true;
        entity->ClearPrimaryCollisionIndexes();
        entity->PushPrimaryCollisionIndex(index);
        entity->SetProspectPosition(entity->GetPosition() + vector::Multiply(subject_collision.Sweep, entity->GetPhysics().Velocity));
        entity->SetPrimarySweep(subject_collision.Sweep);
        /* */ if (entity->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "CASE1 first: " << subject_collision.First.MemberEntity->GetInstanceId() << ", second: " << subject_collision.Second.MemberEntity->GetInstanceId() << ", collision: " << &subject_collision << ", num_primary: " << entity->GetPrimaryCollisionCount() << ", new s < entity's primary s" << std::endl;

    } else if (subject_collision.Sweep > -1 && num_primary_col > 0 && subject_collision.Sweep == collisions[primary_col_indexes[0]].Sweep) {
        //CASE2: subject_collection is tied  (equal sweep) with other primaries on entity; append this to list of primary collisions
        entity->PushPrimaryCollisionIndexIfNotPresent(index);
        subject_collision.IsValid = true;
        entity->SetProspectPosition(entity->GetPosition() + vector::Multiply(subject_collision.Sweep, entity->GetPhysics().Velocity));
        entity->SetPrimarySweep(subject_collision.Sweep);
        /* */ if (entity->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "CASE2 first: " << subject_collision.First.MemberEntity->GetInstanceId() << ", second: " << subject_collision.Second.MemberEntity->GetInstanceId() << ", collision: " << &subject_collision << ", num_primary: " << entity->GetPrimaryCollisionCount() << ", new s == entity's primary s" << std::endl;

    } else {
        //CASE3: subject_collision is a miss OR it has a sweep greater than the pre-exiting primary collisions; mark as invalid -- may become valid later after indirect sweeps and more processing
        subject_collision.IsDirect = false;
        if (subject_collision.First.MemberEntity == entity) {
            subject_collision.PrimaryFirst = false;
        }
        // auto& other = subject_collision.Second.MemberEntity;
        // other->SetProspectPosition(other->GetPosition() + vector::Multiply(subject_collision.Sweep, other->GetPhysics().Velocity));
        subject_collision.IsValid = false;   //TODO: removing isvalid false here because could be collision from a different side
        subject_collision.Sweep = -1;
        /* */ if (entity->GetInstanceId() == engine::DebugCollisionTrackedInstanceID) std::cout << "CASE3 first: " << subject_collision.First.MemberEntity->GetInstanceId() << ", second: " << subject_collision.Second.MemberEntity->GetInstanceId() << ", collision: " << &subject_collision << ", num_primary: " << entity->GetPrimaryCollisionCount() << ", new s > entity's primary s" << std::endl;
    }
}

void AppendConstraint(Collision& collision, std::vector<Constraint>& constraints, std::unordered_map<unsigned int, Entity_ConstraintWrapper>& entityCache) {    
    //Only create constraint if collision members are moving toward each other 
    if (IsMovingToward(collision)) {
        //TODO: should the OnCollision method for entities be called here? -- maybe not because this will only ever happen for solid-solid
        
        // TODO: check whether this should actually r should be Min, Max, or some calculated value?
        // (1+e)(n dot (va + wa x ra - vb + wb x rb)) -- constraint for collisions
        // n dot (wa x ra) = (wa x ra) dot n = wa dot (ra x n) -- associativity
        // factor out velocity vector -> Jacobian = ( n.x, n.y, (ra x n).z, ..., -n.x, -n.y, -(rb x n).z )
        
        auto& first_entity = collision.First.MemberEntity;
        auto& second_entity = collision.Second.MemberEntity;

        //Use a wrapper so that entities in any constraint know which constraint system they will belong in
        Entity_ConstraintWrapper& first_wrapper = constraint::GetConstraintWrapper(first_entity, entityCache);
        Entity_ConstraintWrapper& second_wrapper = constraint::GetConstraintWrapper(second_entity, entityCache);

        sf::Vector2<double> first_linear_jacobian = (1 + collision.Restitution) * sf::Vector2<double>(collision.UnitNormal);
        sf::Vector2<double> second_linear_jacobian = (1 + collision.Restitution) * sf::Vector2<double>(collision.UnitNormal) * -1.0;

        double first_angle_jacobian = (1 + collision.Restitution) * vector::CrossZ( (sf::Vector2<double>) collision.First.Min, collision.UnitNormal);
        double second_angle_jacobian = (1 + collision.Restitution) * vector::CrossZ( (sf::Vector2<double>) collision.Second.Min, collision.UnitNormal);

        constraints.push_back({
            ConstraintMember(first_wrapper, first_linear_jacobian, first_angle_jacobian),
            ConstraintMember(second_wrapper, second_linear_jacobian, -1.0f * second_angle_jacobian)
        });

        //Add SECOND contact constraint for EdgeEdge collisions -- if necessary
        if (collision.Type == CollisionType::EdgeEdge && (first_entity->HasRotation() || second_entity->HasRotation())) {
            first_angle_jacobian = (1 + collision.Restitution) * vector::CrossZ( (sf::Vector2<double>) collision.First.Max, collision.UnitNormal);
            second_angle_jacobian = (1 + collision.Restitution) * vector::CrossZ( (sf::Vector2<double>) collision.Second.Max, collision.UnitNormal);

            constraints.push_back({
                ConstraintMember(first_wrapper, first_linear_jacobian, first_angle_jacobian),
                ConstraintMember(second_wrapper, second_linear_jacobian, -1.0f * second_angle_jacobian)
            });
        }
    }
}

bool IsOverlappingDomain(const Edge& edge1, const sf::Vector2<double>& position1, const Edge& edge2, const sf::Vector2<double>& position2) {
    int edge1_ax = edge1.GetA().x + position1.x;
    int edge1_bx = edge1.GetB().x + position1.x;
    int edge2_ax = edge2.GetA().x + position2.x;
    int edge2_bx = edge2.GetB().x + position2.x;
    return IsOverlappingDomain(edge1_ax, edge1_bx, edge2_ax, edge2_bx);
}

bool IsOverlappingDomain(double edge1_ax, double edge1_bx, double edge2_ax, double edge2_bx) {
    return (
        (edge1_ax >= edge2_ax && edge1_ax <= edge2_bx) ||
        (edge2_ax >= edge1_ax && edge2_ax <= edge1_bx)
    );
}

bool IsMovingToward(const Collision& c) {
    //To calc whether two entities are moving toward, we need to consider an 'edge' entity and a 'vertex' entity
    //This concept is only naturally related to VE collisions, but we will need to force it for all types
    //A) retrieve edge normal: use VertexFirst to get edge from member; getNormalVector returns vector n
    //B) retrieve vertex: vertex point rel to pos = min - pos
        //Note that for VE and VV, min==max
        //~~~but for EE, extra logic must be run WHEN ROTATION SPEED PRESENT to determine at which edge extreme collision will occur~~~
        //~~~note that a more sophisticated version of sweep alg should know that if rotvel > 0 and member edge slopes not equal, coltype is no longer EE~~~
            //At the point of rotation EE will become VE
    //C) Calc velocity at point vertex entity's vertex (rots applied to distance offset), and edge entity's vertex at the same point
        //Velocity is the sum of entity's current linear velocity and rotational speed (rots) crossed with distance offset
        //Even though points are in same location (absolute), their relation positions will most-likely differ
    //D) Evaluate boolean expression: n dot (vertex point velocity - edge point velocity) < 0 --> moving toward

    const CollisionMember& edge_member = *c.FetchEdgeMember();
    const CollisionMember& vertex_member = *c.FetchVertexMember();

    /*****assuming min is...
     * If VertexFirst, for First min = 0, 1, or -1, based on which of the two possible adjacent edges are saved (determined by least angle --how do we store this?) -- else Second
     * For other member (ON vs OFF), OFF.min = ((ON.min * ON.edge.a) + ON.pos - OFF.pos) / (OFF.edge.b - OFF.edge.a)
     */
    const sf::Vector2<double>& edge_vel = edge_member.MinVelocity;
    const sf::Vector2<double>& vertex_vel = vertex_member.MinVelocity;

    sf::Vector2<double> velocity_difference = vertex_vel - edge_vel;
    auto output = vector::Dot(c.UnitNormal, velocity_difference);
    if (output < 0) {
        return true;
    } else if (output == 0 && 
            (edge_member.MemberEntity->IsFixed() || vector::Dot(edge_vel, edge_member.MemberEntity->GetPhysics().Acceleration) > 0) &&
            (vertex_member.MemberEntity->IsFixed() || vector::Dot(vertex_vel, vertex_member.MemberEntity->GetPhysics().Acceleration) > 0)) {
        return true;
    }
    return false;
}

//Checks for intersections between two LinearEdges, using only the stack, not heap
sf::Vector2<double> GetLinearIntersection(const LinearEdge& edge1, const LinearEdge& edge2) {
    sf::Vector2<double> edge1_a = edge1.GetA();
    sf::Vector2<double> edge1_b = edge1.GetB();
    sf::Vector2<double> edge2_a = edge2.GetA();
    sf::Vector2<double> edge2_b = edge2.GetB();

    if (IsOverlappingDomain(edge1_a.x, edge1_b.x, edge2_a.x, edge2_b.x)) {
        if (edge1_a == edge2_a || edge1_a == edge2_b) {
            return edge1_a;
        } else if (edge1_b == edge2_a || edge1_b == edge2_b) {
            return edge1_b;
        }

        const LinearEdgeFunction& edge1_fn = edge1.GetFn();
        const LinearEdgeFunction& edge2_fn = edge2.GetFn();
        sf::Vector2<double> diff_a = edge2_a - edge1_a;

        int expanded_constant = (edge2_fn.GetSlope() * diff_a.x * -1) + diff_a.y;
        int expanded_coeff = edge1_fn.GetSlope() - edge2_fn.GetSlope();

        if (expanded_coeff != 0) {
            double solution_x = expanded_constant / expanded_coeff;
            return {solution_x, edge1_fn.Evaluate(solution_x)};
        }
    }
    return mathconstants::NO_SOLUTION_VEC;
}

//defined here to avoid duplicate symbols due to forward declaration of this fn in entity.h
bool IsValidByIndex(const std::vector<Collision>& collisions, unsigned int index) {
    return collisions[index].IsValid;
}

}

}