#include "controller/physics/constraint.h"

#include <armadillo>

#include "util/vector_utils.h"
#include "constants.h"

//temp
#include <iostream>

#define ESO_VAR_NUM_DIMENSIONS 3    //2 linear, 1 angular

namespace eso {

ConstraintMember::ConstraintMember(Entity_ConstraintWrapper& _entity, const sf::Vector2<double>& _linear_jacobian, double _angular_jacobian):
        MemberEntity(_entity),    //TODO: check if this works with just MemberEntity(_entity)
        LinearJacobian(_linear_jacobian),
        AngularJacobian(_angular_jacobian) {}

ConstraintMember::ConstraintMember(Entity_ConstraintWrapper& _entity, sf::Vector2<double>&& _linear_jacobian, double _angular_jacobian):
        MemberEntity(_entity),    //TODO: check if this works with just MemberEntity(_entity)
        LinearJacobian(std::move(_linear_jacobian)),
        AngularJacobian(_angular_jacobian) {}

ConstraintMember* Constraint::FetchFixed() {
    if (HasFixed()) {
        return (First.MemberEntity->IsFixed()? &First : &Second);
    }
    return nullptr;
}

short Constraint::GetIdsIndex() const {
    short ids_index = First.MemberEntity.OuterIndex;
    if (ids_index == -1) {
        ids_index = Second.MemberEntity.OuterIndex;
    }
    return ids_index;
}


//TODO: URGENT! NEED to look into removing duplicate constraints caused by OTHER MOBILE ENTITIES
void ConstraintSystem::OpenAppeal(Constraint* constraint) {
    //If fixed present, flag jacobian in duplicate set to keep track of duplicates with fixed members
    if (constraint->HasFixed()) {
        auto* non_fixed = constraint->FetchNonFixed();
        auto* fixed = constraint->FetchFixed();
        
        //store duplicate jacobian coefficients applied to the same non-fixed entity
        auto nonfixed_dup_tuple = std::make_tuple(
                non_fixed->MemberEntity->GetInstanceId(),
                non_fixed->LinearJacobian.x,
                non_fixed->LinearJacobian.y,
                (non_fixed->MemberEntity->HasRotation()? non_fixed->AngularJacobian * non_fixed->MemberEntity->GetPhysics().MomentOfInertia : 0),
                false,   //duplicate not yet found
                false
        );
        //TODO: experimental -- prevents negated version of constraints from being added
        // auto neg_nf_dup_tuple = std::make_tuple(
        //         non_fixed->MemberEntity->GetInstanceId(),
        //         -1 * non_fixed->LinearJacobian.x,
        //         -1 * non_fixed->LinearJacobian.y,
        //         (non_fixed->MemberEntity->HasRotation()? -1 * non_fixed->AngularJacobian * non_fixed->MemberEntity->GetPhysics().MomentOfInertia : 0),
        //         false,   //duplicate not yet found
        //         false
        // );
        //TODO: does this part need to be updated?
        if (non_fixed_tracking_set_.find(nonfixed_dup_tuple) != non_fixed_tracking_set_.end()/* || non_fixed_tracking_set_.find(neg_nf_dup_tuple) != non_fixed_tracking_set_.end()*/) {
            std::get<4>(nonfixed_dup_tuple) = true;  //flag duplicate found
        }
        non_fixed_tracking_set_.insert(std::move(nonfixed_dup_tuple)); //attempt to reinsert this nonfixed entity

        //store count of same fixed entity applications for this system (only count up to 2)
        auto fixed_dup_tuple = std::make_tuple(fixed->MemberEntity->GetInstanceId(), false);
        if (fixed_applicant_set_.find(fixed_dup_tuple) != fixed_applicant_set_.end()) {
            std::get<1>(fixed_dup_tuple) = true;   //flag second appearance of this fixed entity
        }
        fixed_applicant_set_.insert(std::move(fixed_dup_tuple));   //attempt to reinsert this fixed entity
    }
    /* Thank you for your interest in this constraint system. Please check back for our response shortly :)  */
}

bool ConstraintSystem::ConcludeAppeal(Constraint* constraint) {

    //Constraints without fixed entities, ACCEPTED!
    if (constraint->HasFixed()) {
        auto* non_fixed = constraint->FetchNonFixed();
        auto* fixed = constraint->FetchFixed();

        //Fixed duplicate check -- if submitted twice, ACCEPTED!
        auto fixed_dup_tuple = std::make_tuple(fixed->MemberEntity->GetInstanceId(), true);
        if (fixed_applicant_set_.find(fixed_dup_tuple) == fixed_applicant_set_.end()) {
            
            //Non-fixed duplicate check -- if no duplicate jacobians on non-fixed, ACCEPTED!
            auto nonfixed_dup_tuple = std::make_tuple(
                    non_fixed->MemberEntity->GetInstanceId(),
                    non_fixed->LinearJacobian.x,
                    non_fixed->LinearJacobian.y,
                    (non_fixed->MemberEntity->HasRotation()? non_fixed->AngularJacobian * non_fixed->MemberEntity->GetPhysics().MomentOfInertia : 0),
                    true,   //duplicate found
                    false
            );
            //TODO: experimental -- presents negated version of constraints from being added
            // auto neg_nf_dup_tuple = std::make_tuple(
            //         non_fixed->MemberEntity->GetInstanceId(),
            //         -1 * non_fixed->LinearJacobian.x,
            //         -1 * non_fixed->LinearJacobian.y,
            //         (non_fixed->MemberEntity->HasRotation()? -1 * non_fixed->AngularJacobian * non_fixed->MemberEntity->GetPhysics().MomentOfInertia : 0),
            //         true,   //duplicate found
            //         false
            // );
            if (non_fixed_tracking_set_.find(nonfixed_dup_tuple) != non_fixed_tracking_set_.end()/* || non_fixed_tracking_set_.find(neg_nf_dup_tuple) != non_fixed_tracking_set_.end()*/) {
                
                //Fixed is non-duplicated, but non-fixed has not already accepted it, ACCEPTED!
                //TODO: URGENT THINK HARDER ABOUT WHEN TO SET ENTRY AS ALREADY ACCEPTED!!!
                std::get<5>(nonfixed_dup_tuple) = true;    //already accepted into system
                if (non_fixed_tracking_set_.find(nonfixed_dup_tuple) != non_fixed_tracking_set_.end()/* || non_fixed_tracking_set_.find(neg_nf_dup_tuple) != non_fixed_tracking_set_.end()*/) {
                    return false;
                    /* I'm so sorry -- your application has been REJECTED! */
                } else {
                    //track acceptance of this constraint/fixed entity so it is not added a second time!
                    non_fixed_tracking_set_.insert(std::move(nonfixed_dup_tuple));
                }
            }
        }
    }
    AddConstraint(constraint);
    return true;
    /* CONGRATULATIONS! Your application has been accepted! Welcome to our constraint system! */
}

//WARNING: This could potentially be inefficient, as lots of vector expansion and rehashes could be done because we don't know how much to reserve
void ConstraintSystem::AddConstraint(Constraint* constraint) {
    constraints_.push_back(constraint);

    Entity_ConstraintWrapper& first_wrapper = constraint->First.MemberEntity;
    Entity_ConstraintWrapper& second_wrapper = constraint->Second.MemberEntity;

    unsigned int first_entity_key = first_wrapper->GetInstanceId();
    unsigned int second_entity_key = second_wrapper->GetInstanceId();
    
    //Only add entity to map if not present; populate member index so contraint knows entity's index
    if (entity_map_.find(first_entity_key) == entity_map_.end()) {
        first_wrapper.InnerIndex = entity_map_.size();   //set index of entity within map, internal to ConstraintSystem
        entity_map_[first_entity_key] = &first_wrapper;
    }
    if (entity_map_.find(second_entity_key) == entity_map_.end()) {
        second_wrapper.InnerIndex = entity_map_.size();
        entity_map_[second_entity_key] = &second_wrapper;
    }
}

namespace constraint {

Entity_ConstraintWrapper& GetConstraintWrapper(std::shared_ptr<Entity>& entity, std::unordered_map<unsigned int, Entity_ConstraintWrapper>& cache) {
    auto key = entity->GetInstanceId();
    if (cache.find(key) == cache.end()) {
        cache[key] = Entity_ConstraintWrapper(entity.get());
    }
    return cache[key];
}

void Generate(std::vector<std::shared_ptr<Entity>>& entities, std::vector<Constraint>& constraints) {
    for (auto& entity : entities) {
        //TODO: need way for dev to define constraints (by function of velocity) to assign to entity
        //TODO: construct new constraint here and push to world constraints
    }
}

void Solve(std::vector<Constraint>& constraints) {
    unsigned short max_index = 0;
    ConstraintSystem constraint_systems[ESO_VAR_CONSTRAINT_SYSTEM_COUNT_MAX];
    group_constraints(constraints, constraint_systems, max_index);

    //Solves each constraint system independently to lower average size of constraint matrices
    for (unsigned short i = 0; i <= max_index; i++) {
        auto& system = constraint_systems[i];
        if (!system.GetConstraints().empty()) {
            solve_system(system);
        }
    }
}

void group_constraints(std::vector<Constraint>& constraints, ConstraintSystem* constraint_systems, unsigned short& max_index) {
    short next_id = 0;
    short max_id = 0;
    std::vector<short> constraint_system_ids;

    //Populate entity constraint indices and actual constraint system indices -- modifying Entity_ConstraintWrapper::OuterIndex through reference
    for (auto& c : constraints) {
        short& first_member_index = c.First.MemberEntity.OuterIndex;
        short& second_member_index = c.Second.MemberEntity.OuterIndex;
        
        //scenario A (New) - both members have not yet been processed
        if (first_member_index == -1 && second_member_index == -1) {
            if (!c.First.MemberEntity->IsFixed() || !c.Second.MemberEntity->IsFixed()) {
                constraint_system_ids.push_back(next_id);
            
                if (!c.First.MemberEntity->IsFixed()) {
                    first_member_index = next_id;
                }
                if (!c.Second.MemberEntity->IsFixed()) {
                    second_member_index = next_id;
                }
                
                if (next_id > max_id) { //max becomes next; increment next
                    max_id = next_id;
                    next_id++;
                } else {    //next becomes max + 1
                    next_id = max_id + 1;
                }
            }

        //scenario B (Add) - only first member has not yet been processed
        } else if (first_member_index == -1) {
            if (!c.First.MemberEntity->IsFixed()) {
                first_member_index = second_member_index;
            }

        //scenario C (Add) - only the second member has not yet been processed
        } else if (second_member_index == -1) {
            if (!c.Second.MemberEntity->IsFixed()) {
                second_member_index = first_member_index;
            }

        //scenario D (Merge) - both already processed, first's system is merged with second's
        } else if (constraint_system_ids[first_member_index] > constraint_system_ids[second_member_index]) {
            next_id = constraint_system_ids[first_member_index];
            constraint_system_ids[first_member_index] = constraint_system_ids[second_member_index];
        
        //scenario E (Merge) - both already processed, second'd system is merged with first's
        } else if (constraint_system_ids[second_member_index] > constraint_system_ids[first_member_index]) {
            next_id = constraint_system_ids[second_member_index];
            constraint_system_ids[second_member_index] = constraint_system_ids[first_member_index];
        }
    }

    #if ESO_VAR_CONSTRAINT_SOLVER_TYPE == 1
        for (auto& constraint : constraints) {
            auto& system = constraint_systems[constraint_system_ids[constraint.GetIdsIndex()]];

            //submit appeal to add constraint to system
            system.AddConstraint(&constraint);
        }

    #else
        //Submit appeals to add constraint to each appropriate system
        for (auto& constraint : constraints) {
            auto& system = constraint_systems[constraint_system_ids[constraint.GetIdsIndex()]];

            //submit appeal to add constraint to system
            system.OpenAppeal(&constraint);
        }

        //Enact appeals for constraints wrt each system -- appeal my be rejected or accepted
        //If accepted, AddConstraint will be called automatically
        for (auto& constraint : constraints) {
            auto& system = constraint_systems[constraint_system_ids[constraint_system_ids[constraint.GetIdsIndex()]]];

            //close appeal to add constraint to system
            system.ConcludeAppeal(&constraint);
        }
    #endif
    max_index = max_id; //modify calling stack reference
}

void solve_system(ConstraintSystem& constraint_system) {

    //Matrix dimension assignment
    unsigned short d = ESO_VAR_NUM_DIMENSIONS;
    unsigned short n = constraint_system.GetNumEntities();
    unsigned short c = constraint_system.GetNumContraints();

    //Generate inverse mass matrix
    arma::mat inverse_mass_matrix(d * n, d * n, arma::fill::zeros);
    arma::vec mass_diag(d * n);

    //Generate velocity vector
    arma::vec velocity_vector(d * n);

    //Generate external force vector (derived from external forces - i.e. gravity)
    arma::vec ext_force_vector(d * n);

    //Generate Jacobian matrix
    arma::mat jacobian_matrix(c, d * n, arma::fill::zeros);

    //Populate ALL necessary matrices within a single look through all entities in map
    auto& entity_map = constraint_system.RefEntityMap();
    for (auto it = entity_map.begin(); it != entity_map.end(); ++it) {
        //Retrieve entity index WITHIN the collision system
        auto& entity_wrapper = *(it->second);
        unsigned short entity_index = entity_wrapper.InnerIndex;
        
        //Populate inverse mass sparce matrix -- if entity is not mobile, inverse mass=0; if entity has no rotation, inverse moment=0
        const Entity& entity = *(entity_wrapper._Entity);
        double inverse_mass = entity.IsMobile() ? 1 / entity.GetPhysics().Mass : 0; //TODO: was 0.0001, but changed to 0
        double inverse_moment = entity.HasRotation() ? 1 / entity.GetPhysics().MomentOfInertia : 0;

        /* TODO: TEMP */
        // if (entity.GetInstanceId() == 7) {
        //     std::string instance_ids = "INSTANCE IDS: ";
        //     for (auto it = constraint_system.RefEntityMap().begin(); it != constraint_system.RefEntityMap().end(); ++it) {
        //         instance_ids += std::to_string(it->second->get()->GetInstanceId()) + ", ii: " + std::to_string(it->second->InnerIndex) + "; ";
        //     }
        //     std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << instance_ids << std::endl;
        // }

        //Populate ith entity of inverse mass matrix
        mass_diag[entity_index * d] = inverse_mass;
        mass_diag[entity_index * d + 1] = inverse_mass;
        mass_diag[entity_index * d + 2] = inverse_moment;

        //Populate ith entity of velocity vector
        velocity_vector[entity_index * d] = entity.GetPhysics().Velocity.x;
        velocity_vector[entity_index * d + 1] = entity.GetPhysics().Velocity.y;
        velocity_vector[entity_index * d + 2] = entity.GetPhysics().RotationalVelocity;

        //Populate ith entity of ext force vector
        ext_force_vector[entity_index * d] = entity.GetPhysics().NetForce.x;
        ext_force_vector[entity_index * d + 1] = entity.GetPhysics().NetForce.y;
        ext_force_vector[entity_index * d + 2] = entity.GetPhysics().NetTorque;
    }
    inverse_mass_matrix.diag() = mass_diag;

    //Populate Jacobian by looping through constraints
    unsigned short constraint_index = 0;
    for (auto* constraint : constraint_system.RefConstraints()) {

        ConstraintMember& first_member = constraint->First;
        ConstraintMember& second_member = constraint->Second;

        short first_index = first_member.MemberEntity.InnerIndex;
        short second_index = second_member.MemberEntity.InnerIndex;

        //fill in the 3 components of jacobian from first and second entity involved
        arma::rowvec jacobian_row(d * n, arma::fill::zeros);
        
        jacobian_row[first_index * d] = first_member.LinearJacobian.x;
        jacobian_row[first_index * d + 1] = first_member.LinearJacobian.y;
        jacobian_row[first_index * d + 2] = first_member.AngularJacobian;

        jacobian_row[second_index * d] = second_member.LinearJacobian.x;
        jacobian_row[second_index * d + 1] = second_member.LinearJacobian.y;
        jacobian_row[second_index * d + 2] = second_member.AngularJacobian;

        jacobian_matrix.row(constraint_index) = jacobian_row;
        constraint_index++;

        // /* */if (first_member.MemberEntity->GetInstanceId() == 0) std::cout << "making og(1) jacobian with(2): " << second_member.MemberEntity->GetInstanceId() << std::endl;
        // /* */if (second_member.MemberEntity->GetInstanceId() == 0) std::cout << "making og(2) jacobian with(1): " << first_member.MemberEntity->GetInstanceId() << std::endl;
    }

    //solve formula for delta-v vector
    arma::vec lambda_matrix;

    //Calculate A and b and solve linear equation
    #if ESO_VAR_CONSTRAINT_SOLVER_TYPE == 0
        arma::mat A = jacobian_matrix * inverse_mass_matrix * jacobian_matrix.t();
        arma::mat b = -1 * jacobian_matrix * (velocity_vector /*+ inverse_mass_matrix * ext_force_vector*/);
        lambda_matrix = arma::solve(A, b);    //solve for A * lambda = b (linear equation)
    
    //Multiply effective mass by jacobian and velocity (likely to cause crashes due to singular matrix)
    #else
        arma::mat impulse_denominator = jacobian_matrix * inverse_mass_matrix * jacobian_matrix.t();
    
        //TODO: matrix can be singular, meaning there is no inverse -- if det == 0, use the original -- this may be completely wrong
        if (arma::det(impulse_denominator) == 0) {
            std::string mobile_str = "\nTypes: ";
            for (auto it = entity_map.begin(); it != entity_map.end(); ++it) {
                auto& entity_wrapper = *(it->second);
                unsigned short entity_index = entity_wrapper.InnerIndex;
                const Entity& entity = *(entity_wrapper._Entity);
                mobile_str += std::to_string(entity.GetInstanceId()) + ": " + (entity.IsMobile()? "Mobile " : "Fixed ") + "(" + std::to_string(entity_index) + "); ";
            }
            mobile_str += "\n";
            std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>eff mass is singular:\n" << impulse_denominator << ", mobile_str: " << mobile_str << "\njacobian\n" << jacobian_matrix << "imm\n" << inverse_mass_matrix << "jacobian(t)\n" << jacobian_matrix.t() << std::endl;
            lambda_matrix = -1 * impulse_denominator * jacobian_matrix * velocity_vector / 3;
        } else {
            lambda_matrix = -1 * impulse_denominator.i() * jacobian_matrix * velocity_vector;
        }

    #endif

    arma::vec acceleration_vector = inverse_mass_matrix * (jacobian_matrix.t() * lambda_matrix /*+ ext_force_vector*/);

    //add force/torque to each involved entity
    for (auto it = constraint_system.RefEntityMap().begin(); it != constraint_system.RefEntityMap().end(); ++it) {
        auto& entity_wrapper = *(it->second);
        auto& entity = *(entity_wrapper._Entity);

        if (!entity.IsFixed()) {
            unsigned short entity_index = entity_wrapper.InnerIndex;

            sf::Vector2<double> impulse_acc = {
                acceleration_vector[entity_index * d],
                acceleration_vector[entity_index * d + 1]
            };
            
            /* TODO: temp */
            // if (entity.GetInstanceId() == 7) {
                // std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@SOLVING CONSTRAINT - octo prev velocity: " << vector::ToString(entity.GetPhysics().Velocity) << "; vel correction: " << vector::ToString(impulse_acc) << std::endl;
                // std::cout << "prev vel:\n" << velocity_vector << std::endl;
            //     std::cout << "jacobian:\n " << jacobian_matrix << std::endl;
            //     std::cout << "mass:\n" << inverse_mass_matrix << std::endl;
            //     std::cout << "impulse den:\n" << impulse_denominator << std::endl;
                // std::cout << "lambda:\n" << lambda_matrix << std::endl;
                // std::cout << "accel:\n" << acceleration_vector << std::endl;
                
            // }
            
            entity.SetVelocity(entity.GetPhysics().Velocity + impulse_acc);
            entity.RefPhysics().RotationalVelocity = acceleration_vector[entity_index * d + 2];
        }
    }
}

//TODO: deprecate
//void add_colliding_force(Collision& c) {
    /* Apply this formula to determine force caused by this collision:
            Target speed after collision of both entities is calculated
            If EdgeType is EE or EV, this will be used strictly for its magnitude
            If EdgeType is VV, this will be the entire v' vector (before friction)
        
        s1' = (u + m2*e*(v2 - v1)) / msum
        s2' = (u + m1*e*(v1 - v2)) / msum
            --where
            u = m1*v1 + m2*v2   --total initial momentum
            msum = m1 + m2

            Directional unit vector is calculated for EE and VE

        d1' = v1 - 2*dot(n1,v1)*n1
        d2' = v2 - 2*dot(n2,v2)*n2      --if EE OR
            d2' = -1*d1'                --if VE (where second is V)

            Calculate proposed acceleration (new_vel - old_vel)
            --Divide d' by magnitude of itself to make a unit vector

        a1' = ||s1'|| * d1'/||d1'|| - v1
        a2' = ||s2'|| * d2'/||d2'|| - v2

            Determine force caused by collision

        f1 = (a1' - mu1*ap1)*m1
        f2 = -1*f2      --equal and opposite!
            --where
            mu1 = coefficient of friction for colliding edge (alpha1)
            ap1 = (dot(a1',alpha2) / ||alpha2||*||alpha2||) * alpha2     --parallel component of acceleration projected onto second's colliding edge

        TODO: determine torque caused by collision!
        t1 = 
        t2 = 

        v1' = v1 + j * n / m1 ===> final velocity of A
        v2' = v2 - j * n / m2 ===> final velocity of B, where
            
            j = dot(-1 * (1 + e) * (v2 - v1), n) / (1 / m1 + 1 / m2 + rot_dem) -- TODO: what is this?? What is rot_dem?
            n = unit normal vector for B's colliding edge
            e = product of restitution between both colliding edges = e1 * e2
            m1 = mass of A; m2 = mass of B
    */

    /*Entity& a = *c.First.MemberEntity;
    Entity& b = *c.Second.MemberEntity;

    Edge const* edge_a = c.First.EdgeAlpha;
    Edge const* edge_b = c.Second.EdgeAlpha;

    Physics& pa = a.RefPhysics();
    Physics& pb = b.RefPhysics();

    bool has_colliding_edge = c.Type == CollisionType::EdgeEdge || c.Type == CollisionType::VertexEdge;
    double coeff_rest = a.GetShape().GetEdgeRestitution(edge_a) * b.GetShape().GetEdgeRestitution(edge_b); //multiply rest of both colliding sides to get this

    sf::Vector2<double> final_velocity_a;
    sf::Vector2<double> final_velocity_b;

    // calculate expected velocity after collision using momentum formula -- will be used for its magnitude
    if (!a.IsFixed() && !b.IsFixed()) {
        double mass_sum = pa.Mass + pb.Mass;
        sf::Vector2<double> momentum_per_mass = vector::Multiply(pa.Mass / mass_sum, pa.Velocity) +
                vector::Multiply(pb.Mass / mass_sum, pb.Velocity);
        
        final_velocity_a = momentum_per_mass + vector::Multiply(pb.Mass * coeff_rest / mass_sum, pb.Velocity - pa.Velocity);
        final_velocity_b = momentum_per_mass + vector::Multiply(pa.Mass * coeff_rest / mass_sum, pa.Velocity - pb.Velocity);
    
    } else if (!a.IsFixed()) {
        final_velocity_a = vector::Multiply(coeff_rest, pa.Velocity);
    
    } else if (!b.IsFixed()) {
        final_velocity_b = vector::Multiply(coeff_rest, pb.Velocity);
    }

    // only subtract friction when collision has edge(s)
    double friction = 1.0;
    double edge_magnitude = 0;
    sf::Vector2<double> edge_vector;

    if (has_colliding_edge) {
        if (c.Type == CollisionType::VertexEdge) {
            if (c.VertexFirst) {
                edge_vector = {(double) edge_b->GetB().x - edge_b->GetA().x, (double) edge_b->GetB().y - edge_b->GetB().y};
                friction = b.GetShape().GetEdgeFriction(edge_b);
            } else {
                edge_vector = {(double) edge_a->GetB().x - edge_a->GetA().x, (double) edge_a->GetB().y - edge_a->GetB().y};
                friction = a.GetShape().GetEdgeFriction(edge_a);
            }
        } else if (c.Type == CollisionType::EdgeEdge) {
            friction = a.GetShape().GetEdgeFriction(edge_a) * b.GetShape().GetEdgeFriction(edge_b);
            edge_vector = {(double) edge_a->GetB().x - edge_a->GetA().x, (double) edge_a->GetB().y - edge_a->GetB().y}; // picking A as edge vector if EE
        }
        //TODO: don't forget about VertexVertex collisions...

        edge_magnitude = vector::Magnitude(edge_vector);
    }

    // Separated calculations for a and b, depending on whether something is fixed
    sf::Vector2<double> collision_force_a;
    sf::Vector2<double> collision_force_b;
    
    if (!a.IsFixed()) {
        if (has_colliding_edge) {
            // for collisions with edge(s), direction of final velocities are based off of edge normal vectors
            const sf::Vector2<double>& unit_normal_a = c.First.UnitNormal;
            sf::Vector2<double> unit_direction_a = vector::GetUnitVector(pa.Velocity - vector::Multiply(2 * vector::Dot(unit_normal_a, pa.Velocity), unit_normal_a));
            final_velocity_a = vector::Multiply(vector::Magnitude(final_velocity_a), unit_direction_a);
        }
        collision_force_a = (final_velocity_a - pa.Velocity) * pa.Mass;
    }
    if (!b.IsFixed()) {
        if (has_colliding_edge) {
            // for collisions with edge(s), direction of final velocities are based off of edge normal vectors
            const sf::Vector2<double>& unit_normal_b = c.Second.UnitNormal;
            sf::Vector2<double> unit_direction_b = vector::GetUnitVector(pb.Velocity - vector::Multiply(2 * vector::Dot(unit_normal_b, pb.Velocity), unit_normal_b));
            final_velocity_b = vector::Multiply(vector::Magnitude(final_velocity_b), unit_direction_b);
        }
        collision_force_b = (final_velocity_b - pb.Velocity) * pb.Mass;
    }

    // Apply forces to both entities involved in collision
    sf::Vector2<double> force_projection_a;
    sf::Vector2<double> force_projection_b;

    pa.NetForce += collision_force_a;
    pb.NetForce += collision_force_b;
    
    if (has_colliding_edge) {
        force_projection_a = edge_vector * (vector::Dot(pa.NetForce, edge_vector) / (edge_magnitude * edge_magnitude));
        force_projection_b = edge_vector * (vector::Dot(pb.NetForce, edge_vector) / (edge_magnitude * edge_magnitude));

        //subtract friction from forces
        pa.NetForce -= force_projection_a * friction;   //TODO: is this alone enough for walking mechanics?
        pb.NetForce -= force_projection_b * friction;
    }

    // Apply equal and opposite forces for all preexisting forces -- TODO: implement LOCATION of force in order to introduce torque
    // sf::Vector2<double> applied_force_a = pa.NetForce - force_projection_a;    //subtract parallel component of force from force being directly OPPOSITE (subtracted) from a
    // sf::Vector2<double> applied_force_b = pb.NetForce - force_projection_b;

    //pa.NetForce -= applied_force_a;    //Subjecting entire perpendicular force because equal -- TODO: this is not exactly correct...
    //pb.NetForce -= applied_force_b;

    // Apply preexisting external forces to opposite entity
    // pa.NetForce += applied_force_b;
    // pb.NetForce += applied_force_a;
}*/

/*void calculate_impulse_force(Collision& c) {
    Physics& pa = c.First.MemberEntity->RefPhysics();
    Physics& pb = c.Second.MemberEntity->RefPhysics();

    pa.Acceleration = pa.PreviousVelocity - pa.Velocity; //TODO: should we set actual acceleration here?
    pb.Acceleration = pb.PreviousVelocity - pb.Velocity;

    pa.TotalCrushMagnitude += vector::Magnitude(pa.Acceleration * pa.Mass);
}*/

}

}
