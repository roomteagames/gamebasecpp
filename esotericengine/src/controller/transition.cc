#include "controller/transition.h"

#include "controller/loops.h"
#include "model/game/world.h"

//TODO: temp
#include <iostream>

namespace eso {

namespace transition {

//TODO: FIGURE OUT WHY THIS METHOD IS ALLOWING PROGRAM TO IMMEDIATELY STOP
//TODO: merge with loops:: to get body.h??
void EnterNextState(Game& app) {
    short next_state_index = app.ChangeStateIndex;
    if (next_state_index > -1) {
        app.CurrentState = &app.States[next_state_index];
        app.ChangeStateIndex = -1;

        //TODO: write code to initializes entity_grids for each world that uses one
        for (World& world : app.CurrentState->GetCurrentWorlds()) {

        }

        //TODO: some update frames are not being rendered bc update starts before render
        std::thread update(loops::UpdateLoop, std::ref(app));
        std::thread render(loops::RenderLoop, std::ref(app));

        //TODO: figure out why program is stopping here!!!!

        std::cout << "before input" << std::endl;
        loops::InputLoop(app);

        std::cout << "before joins" << std::endl;
        render.join();
        update.join();
        std::cout << "after joins" << std::endl;
    }
    std::cout << "app about to exit" << std::endl;
} 

}

}