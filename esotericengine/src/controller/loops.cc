#include "controller/loops.h"

#include <algorithm>
#include <unordered_map>

#include <SFML/Graphics.hpp>

#include "controller/loading.h"
#include "controller/timing.h"
#include "controller/physics/collision.h"
#include "controller/physics/constraint.h"
#include "model/entity/entity.h"
#include "model/game/game_state.h"
#include "model/game/input_model.h"
#include "view/entity/view_model.h"

//TODO: temp
#include <iostream>
#include "util/vector_utils.h"

namespace eso {

namespace loops {

//helper declarations
void handle_input_events(Game&);

void InputLoop(Game& app) {
    std::cout << "starting input loop" << std::endl;
    bool done = false;

    ActiveLoopTimer timer(app.StartupProps.Game.InputFramerate);
    while (app.ChangeStateIndex == Game::kNoStateChange && !app.Closing) {
        handle_input_events(app);
        timer();
    }
}

void UpdateLoop(Game& app) {
    LazyLoopTimer timer(app.StartupProps.Game.UpdateFramerate);    //TODO: change this BACK to being ActiveLoopTimer

    // std::cout << "starting update loop" << std::endl; -- convert this to official logger
    long frame_count = 0;
    unsigned short framerate = app.UpdateFramerate;

    //use for reuse of entity wrapper
    std::unordered_map<unsigned int, Entity_ConstraintWrapper> entityConstraintCache;

    //THIS is the update loop
    while (app.ChangeStateIndex == Game::kNoStateChange && !app.Closing) {

        // Allow for modification of framerate
        if (app.UpdateFramerate != framerate) {
            framerate = app.UpdateFramerate;
            timer.ResetFramerate(framerate);
        }

        /**/std::cout << "~~~~~~~STARTING FRAME!!~~~~~~~~~~~~~~~~~" << std::endl;
        // std::cout << "next state index: " << app.ChangeStateIndex << ", closing? " << app.Closing << std::endl;

        //Update input model
        app.Inputs.Update();

        //Update loading/auxiliary threads
        loading::UpdateLoadingThreads();

        //TODO: temp hack to allow game to freeze at desired points -- this isn't a bad idea, just come up with a less hacky design
        if (app.FreezeUpdate && !app.Inputs.IsInputBegun(input::kShoulderRight)) continue;

        //World entity loop
        for (World& world : app.CurrentState->GetCurrentWorlds()) {
            
            //Initialize world based on loading progress -- must run on different frames to not introduce slowdown
            auto load_status = world.GetLoadStatus();
            auto load_ack = world.GetLoadAcknowledgement();

            if (load_status >= LoadStatus::WorldLoaded && load_ack < LoadStatus::WorldLoaded) {
                world.OnWorldLoad(app.Assets);
            } else if (load_status >= LoadStatus::AudioLoaded && load_ack < LoadStatus::AudioLoaded) {
                world.OnAudioLoad(app.Assets.Audio);
            }

            //TODO: NEW implementation of update loop

            //update physics for each entity
            for (auto& entity : world.GetEntities()) {
                if (entity->GetStatus() < Entity::Status::INACTIVE) {

                    //Non-fixed entities receive external forces
                    if (!entity->IsFixed() && entity->GetShape().Exists()) {
                        //TODO: how do we handle add ALL external forces in the world (as list?)
                        entity->ApplyForce(world.GetGravity() * entity->GetPhysics().Mass);    //Add force of gravity and TODO: removing for now --> update velocity
                    }
                    entity->Update(app, world);
                    entity->UpdateAcceleration();
                    entity->RefPhysics().PreviousVelocity = entity->GetPhysics().Velocity;
                    entity->UpdateVelocity();
                }
            }

            //clear constraints and collisions, as referenced entities may need to be deleted -- prevents possible dangling refs
            world.CurrentCollisions().clear();
            world.CurrentConstraints().clear();
            entityConstraintCache.clear();

            //clear collisions, clean destroyed or dead entities from vector
            auto remove_end = std::remove_if(world.GetEntities().begin(), world.GetEntities().end(), [&app, &world](auto& entity) {
                entity->ClearPrimaryCollisionIndexes();
                entity->ClearSecondaryCollisionIndexes();

                return (entity->IsDestroyed() || entity->RefStatus() >= Entity::Status::DEAD); //TODO: merge IsDestroyed and Status::DEAD together as one thing!
            });
            //TODO: figure out how to properly erase removed entities without crashing the program -- this appears to be valid code; perhaps there is a dangling reference?
            world.GetEntities().erase(remove_end, world.GetEntities().end());

            //cleanup grid based on changed of entity position
            if (world.UseGrid()) {
                for (EntityGrid& grid : world.GetEntityGrids()) {
                    grid.Update(app);
                }
            }

            //Collision Detection
            if (world.UseGrid()) {
                if (world.UseAdvancedCollisions()) {
                    for (EntityGrid& grid : world.GetEntityGrids()) {
                        unsigned short sector_index = 0;
                        if (grid.IsSectorActive(sector_index)) {
                            for (auto& contents : grid.data) {
                                collision::ExecuteAdvanced(contents, sector_index, grid.grid_dimension, grid.units_per_sector);
                            }
                        }
                        sector_index++;
                    }
                } else {
                    for (EntityGrid& grid : world.GetEntityGrids()) {
                        unsigned short sector_index = 0;
                        if (grid.IsSectorActive(sector_index)) {
                            for (auto& contents : grid.data) {
                                collision::Execute(contents, sector_index, grid.grid_dimension, grid.units_per_sector);
                            }
                        }
                        sector_index++;
                    }
                }
                
            } else {
                if (world.UseAdvancedCollisions()) {
                    collision::ExecuteAdvanced(world.GetEntities());
                } else {
                    collision::Execute(world.GetEntities(), world.CurrentCollisions());    /* <-- most basic collision detection */
                    for (unsigned short i = 0; i < world.GetNumCollisionIterations(); i++) {
                        collision::Refine(world.CurrentCollisions());
                    }
                }
            }

            //Call OnCollision hooks and perform constraint calculation            
            for (Collision& c : world.CurrentCollisions()) {
                if (c.IsValid) {
                    std::shared_ptr<Entity>& first = c.First.MemberEntity;
                    std::shared_ptr<Entity>& second = c.Second.MemberEntity;
                
                    std::cout << "About to create constraint for " << first->GetInstanceId() << " and " << second->GetInstanceId() << std::endl;
                    if (first->OnCollision(second.get(), app) && second->OnCollision(first.get(), app) && c.IsSolid) {
                        first->ReplacePositionWithProspect();
                        second->ReplacePositionWithProspect();

                        collision::SetCollisionProperties(&c);
                        collision::AppendConstraint(c, world.CurrentConstraints(), entityConstraintCache);
                    }
                }
            }

            //Generate and Solve all constraints
            // constraint::Generate(world.GetEntities(), world.CurrentConstraints());
            constraint::Solve(world.CurrentConstraints());

            //Positional update loop
            for (auto& entity : world.GetEntities()) {
                if (entity->GetStatus() < Entity::Status::INACTIVE) {
                    //TODO: TEMP REMOVE SOON
                    entity->UpdateToggleVelocity();

                    //Prevents rounding errors from messing with contact resolution
                    if (std::abs(vector::MagSquaredDiff(entity->GetPhysics().Velocity, entity->GetPhysics().PreviousVelocity)) < 0.01) {
                        entity->SetVelocity(entity->GetPhysics().PreviousVelocity);
                    }

                    //Causes entity to change position based on currently calculated velocity
                    entity->UpdatePosition();

                    //Clear single frame physics properties
                    entity->ClearAcceleration();
                    entity->ClearForces();

                    //TODO: remove SOON
                    entity->RevertToggleVelocity();
                }
            }
        }   //END WORLD LOOP
        
        std::cout << "~~~~~END OF FRAME " << frame_count << "~~~~\n\n" << std::endl;
        timer();
        frame_count++;
    }   //END FRAME
}

//TODO: does this also need a renderer and access to other fields in the GameState?
void RenderLoop(Game& app) {
    LazyLoopTimer timer(app.StartupProps.Game.RenderFramerate);

    //TODO: call init_views function here

    while (app.ChangeStateIndex == Game::kNoStateChange && app.Window->isOpen() && !app.Closing) {
        app.Window->clear(sf::Color::White);
        for (World& world : app.CurrentState->GetCurrentWorlds()) {

            //Initialize views once graphics are loaded
            auto load_status = world.GetLoadStatus();
            auto load_ack = world.GetLoadAcknowledgement();

            if (load_status >= LoadStatus::GraphicsLoaded && load_ack < LoadStatus::GraphicsLoaded) {
                world.OnGraphicsLoad(app.Assets.Visuals);
            }

            for (auto& vp : world.GetViewports()) {
                std::vector<std::unique_ptr<ViewModel>>& views = world.GetViewsByTypeIndex(vp.ViewTypeIndex);   //get list of views to render based on Camera
                app.Window->setView(vp.Viewport); //set Camera's sf::View as the render target
                
                std::remove_if(views.begin(), views.end(), [&app](std::unique_ptr<ViewModel>& view) mutable {
                    view->Render(*app.CurrentState, app.Assets.Visuals, app.Window);
                    return view->IsDestroyed();
                });
            }
        }
        app.Window->display();  //without this, nothing will ever show up
        timer();
    }
}

//helper definitions
void handle_input_events(Game& app) {
    sf::Event e;
    
    InputModel& input_model = app.Inputs;
    sf::RenderWindow* window = app.Window;

    while(window->pollEvent(e)) {
        switch(e.type) {
            case sf::Event::Closed:
                app.Closing = true;
                break;
            case sf::Event::KeyPressed:
                input_model.AddInputPress(e.key.code);
                break;
            case sf::Event::KeyReleased:
                input_model.RemoveInputPress(e.key.code);
                break;
            default:
                break;
        }
    }
}

}

}