#include "controller/construction.h"

#include "asset/generated/world_routing.h"

namespace eso {

namespace construction {

void BuildWorld(World& world, AssetCollection& assets, const AssetProps& props) {
    assets::PopulateWorld(world, assets, props);

    //Assign appropriate shapes to each entity in world
    unsigned short unique_shape_index = 0;
    for (auto& entity : world.GetEntities()) {
        if (entity->IsUniqueShape()) {
            entity->Shape().ShapeDefinition = &world.GetUniqueShapes()[unique_shape_index];
            unique_shape_index++;
        } else {
            entity->Shape().ShapeDefinition = assets.Physics.FetchShape(entity->GetShapeIndex());
        }

        //Log warning if shape assigned to entity is empty
        if (entity->Shape().ShapeDefinition->IsEmpty()) {
            int shape_index = entity->GetOptions().ShapeIndex;
            std::cout
                    << "---WARNING: Shape with index: "
                    << shape_index
                    << ", assigned to entity with ID: "
                    << entity->GetInstanceId()
                    << " is EMPTY! This will cause AABB Collision detection to crash!"
                    << std::endl;
        } else {
            entity->__InitLocation();
        }
    }
}

}

}