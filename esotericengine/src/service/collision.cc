#include "service/collision.h"

namespace eso {

namespace collision {


bool IsCollidingWithInstance(const Entity* self, const std::vector<Collision>& collisions, unsigned int instance_id) {
    for (int i = 0; i < self->GetPrimaryCollisionCount(); i++) {
        int collision_index = self->GetPrimaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        if (c.First.MemberEntity.get() != self) {
            if (c.First.MemberEntity->GetInstanceId() == instance_id) {
                return true;
            }
        } else {
            if (c.Second.MemberEntity->GetInstanceId() == instance_id) {
                return true;
            }
        }
    }
    return false;
}


bool IsCollidingWithType(const Entity* self, const std::vector<Collision>& collisions, unsigned int class_id) {
    for (int i = 0; i < self->GetPrimaryCollisionCount(); i++) {
        int collision_index = self->GetPrimaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        if (c.First.MemberEntity.get() != self) {
            if (c.First.MemberEntity->GetClassId() == class_id) {
                return true;
            }
        } else {
            if (c.Second.MemberEntity->GetClassId() == class_id) {
                return true;
            }
        }
    }
    return false;
}

//Executes fn only once as soon as it finds a collision with ANY of the listed class_ids
void ExecuteForFirstCollisionWithTypes(Entity* self, const std::vector<Collision>& collisions, const std::initializer_list<unsigned int>& class_ids, std::function<void(Entity*, Entity*, const Collision&)> fn) {
    for (int i = 0; i < self->GetPrimaryCollisionCount(); i++) {
        int collision_index = self->GetPrimaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int class_id : class_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                    return;
                }
            } else {
                if (c.Second.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                    return;
                }
            }
        } 
    }

    for (int i = 0; i < self->GetSecondaryCollisionCount(); i++) {
        int collision_index = self->GetPrimaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int class_id : class_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                    return;
                }
            } else {
                if (c.Second.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                    return;
                }
            }
        } 
    }
}

//Executes fn for each collision with ANY of the listed class_ids
void ExecuteForAllCollisionsWithTypes(Entity* self, const std::vector<Collision>& collisions, const std::initializer_list<unsigned int>& class_ids, std::function<void(Entity*, Entity*, const Collision&)> fn) {
    for (int i = 0; i < self->GetPrimaryCollisionCount(); i++) {
        int collision_index = self->GetPrimaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int class_id : class_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                }
            } else {
                if (c.Second.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                }
            }
        } 
    }

    for (int i = 0; i < self->GetSecondaryCollisionCount(); i++) {
        int collision_index = self->GetSecondaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int class_id : class_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                }
            } else {
                if (c.Second.MemberEntity->GetClassId() == class_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                }
            }
        } 
    }
}
  
void ExecuteForFirstCollisionWithInstances(Entity* self, const std::vector<Collision>& collisions, const std::initializer_list<unsigned int>& instance_ids, std::function<void(Entity*, Entity*, const Collision&)> fn) {
    for (int i = 0; i < self->GetPrimaryCollisionCount(); i++) {
        int collision_index = self->GetPrimaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int instance_id : instance_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                    return;
                }
            } else {
                if (c.Second.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                    return;
                }
            }
        } 
    }

    for (int i = 0; i < self->GetSecondaryCollisionCount(); i++) {
        int collision_index = self->GetSecondaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int instance_id : instance_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                    return;
                }
            } else {
                if (c.Second.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                    return;
                }
            }
        } 
    }
}

void ExecuteForAllCollisionsWithInstances(Entity* self, const std::vector<Collision>& collisions, const std::initializer_list<unsigned int>& instance_ids, std::function<void(Entity*, Entity*, const Collision&)> fn) {
    for (int i = 0; i < self->GetPrimaryCollisionCount(); i++) {
        int collision_index = self->GetPrimaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int instance_id : instance_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                }
            } else {
                if (c.Second.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                }
            }
        } 
    }

    for (int i = 0; i < self->GetSecondaryCollisionCount(); i++) {
        int collision_index = self->GetSecondaryCollisionIndexes()[i];
        const Collision& c = collisions[collision_index];

        for (int instance_id : instance_ids) {
            if (c.First.MemberEntity.get() != self) {    //checking that member First does not involve pointer to self. if so, First involves other (as desired)
                if (c.First.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.First.MemberEntity.get(), c);
                }
            } else {
                if (c.Second.MemberEntity->GetInstanceId() == instance_id) {
                    fn(self, c.Second.MemberEntity.get(), c);
                }
            }
        } 
    }
}

const Edge& GetCollidingEdge(Entity* entity, const Collision& collision) {
    if (entity == collision.First.MemberEntity.get()) {
        return *collision.First.EdgeAlpha;
    } else {
        return *collision.Second.EdgeAlpha;
    }
}

const sf::Vector2<double>& GetCollidingEdgeMin(Entity* entity, const Collision& collision) {
    if (entity == collision.First.MemberEntity.get()) {
        return collision.First.Min;
    } else {
        return collision.Second.Min;
    }
}

const sf::Vector2<double>& GetCollidingEdgeMax(Entity* entity, const Collision& collision) {
    if (entity == collision.First.MemberEntity.get()) {
        return collision.First.Max;
    } else {
        return collision.Second.Max;
    }
}

}

}