#include "model/entity/entity.h"

#include "util/vector_utils.h"

namespace eso {

EntityOptions::EntityOptions(CollisionMode mode, bool mobile, unsigned short shape_index, MaterialIndices_Wrapper&& material_indices, bool unique_shape, bool has_rotation):
        Mode(mode),
        Mobile(mobile),
        ShapeIndex(shape_index),
        MaterialIndices(std::move(material_indices)),
        UniqueShape(unique_shape),
        HasRotation(has_rotation) {}
        

void Entity::__InitLocation() {
    ClearPrimaryCollisionIndexes();
    ClearSecondaryCollisionIndexes();

    if (shape_.Exists()) {
        UpdatePosition();
    }
}

//TODO: implement ONLY if necessary
//move ctor
/*Entity::Entity(Entity&& other):
        instance_id_(other.instance_id_),
        class_id_(other.class_id_),
        destroyed_(other.destroyed_),
        position_(std::move(other.position_)),
        initial_position_(std::move(other.initial_position_)),
        prospect_position_(std::move(other.prospect_position_)),
        toggle_velocity_(std::move(other.toggle_velocity_)),
        orientation_(other.orientation_),
        z_min_(other.z_min_),
        z_depth_(other.z_depth_),


{

}*/

BoundingBox Entity::GetBoundingBox() const {
    const BoundingBox& bb = GetShape().ShapeDefinition->GetBoundingBox();
    return BoundingBox(bb.Min + position_, bb.Second + position_, bb.Max + position_, bb.Fourth + position_);
}

bool Entity::IsOverlapBoundingBox(const Entity& other /*TODO: remove --> */, sf::Vector2<double>* override_pos, sf::Vector2<double>* override_pos_other) const {
    const BoundingBox& box = GetShape().ShapeDefinition->GetBoundingBox();
    const BoundingBox& other_box = other.GetShape().ShapeDefinition->GetBoundingBox();
    const sf::Vector2<double>& pos = (override_pos != nullptr) ? *override_pos : GetPosition();
    const sf::Vector2<double>& other_pos = (override_pos_other != nullptr) ? *override_pos_other : other.GetPosition();

    if (((box.Min.x + pos.x >= other_box.Min.x + other_pos.x && box.Min.x + pos.x <= other_box.Max.x + other_pos.x/* + 1*/) || (box.Max.x + pos.x >= other_box.Min.x + other_pos.x/* - 1*/ && box.Max.x + pos.x <= other_box.Max.x + other_pos.x)) &&
            ((box.Min.y + pos.y >= other_box.Min.y + other_pos.y && box.Min.y + pos.y <= other_box.Max.y + other_pos.y/* + 1*/) || (box.Max.y + pos.y >= other_box.Min.y + other_pos.y/* - 1*/ && box.Max.y + pos.y <= other_box.Max.y + other_pos.y))) {
        return true;
    }
    return false;
}

//TODO: deprecate??
bool Entity::IsOverlapBoundingBoxProspect(const Entity& other) const {
    /*const BoundingBox& box = GetShape().GetBoundingBox();
    const sf::Vector2<double>& pos = GetProspectPosition();
    const sf::Vector2<double>& other_pos = other.GetProspectPosition();

    if (instance_id_ == 1) std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MADE IT TO OVERLAP PROSPECT!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;

    //TODO: implement without a for loop
    for (auto& other_vertex : other.GetShape().GetBoundingBox()) {
        if (other_vertex.x + other_pos.x >= box.Min.x && 
                other_vertex.x + other_pos.x <= box.Max.x + pos.x && 
                other_vertex.y + other_pos.y >= box.Min.y + pos.y && 
                other_vertex.y + other_pos.y <= box.Max.y + pos.y) {
            return true;
        }
    }*/
    return false;
}

void Entity::SetVelocity(const sf::Vector2<double> v) {
    physics_.Velocity = v;
}

//TODO: revise this concept -- is breaking physics
void Entity::RevertToggleVelocity() {
    //Only reverts toggle velocity if velocity hasn't already been nullified
    if (vector::MagSquared(physics_.Velocity) >= vector::MagSquared(toggle_velocity_)) {
        physics_.Velocity -= toggle_velocity_;
    }
    toggle_velocity_ = {0,0};
}

void Entity::ApplyForce(const sf::Vector2<double> f) {
    physics_.NetForce += f;
    // physics_.Acceleration += {physics_.NetForce.x / physics_.Mass, physics_.NetForce.y / physics_.Mass};
    // physics_.Velocity += physics_.Acceleration;
}

void Entity::ApplyTorque(double t) {
    physics_.NetTorque += t;
    // physics_.RotationalAcceleration += physics_.NetTorque / physics_.MomentOfInertia;
    // physics_.RotationalVelocity += physics_.RotationalAcceleration;
}

void Entity::UpdateAcceleration() {
    physics_.Acceleration += vector::Multiply(1 / physics_.Mass, physics_.NetForce);
    physics_.RotationalAcceleration += physics_.NetTorque / physics_.MomentOfInertia;
}

void Entity::UpdateVelocity() {
    physics_.Velocity += physics_.Acceleration;
    physics_.RotationalVelocity = physics_.RotationalAcceleration;
}

void Entity::UpdateToggleVelocity() {
    physics_.Velocity += toggle_velocity_; //TODO: remove this awful implementation
}

void Entity::UpdateMovementSystem() {
    const sf::Vector2<double>& mvmt = physics_.Velocity;
    sf::Vector2<double> add_x = sf::Vector2<double>(mvmt.x, 0);
    sf::Vector2<double> add_y = sf::Vector2<double>(0, mvmt.y);

    if (mvmt.x >= 0) {
        if (mvmt.y >= 0) {
            movement_system_ = {
                shape_.ShapeDefinition->GetBoundingBox().Min + position_,
                shape_.ShapeDefinition->GetBoundingBox().Second + add_x + position_,
                shape_.ShapeDefinition->GetBoundingBox().Max + mvmt + position_,
                shape_.ShapeDefinition->GetBoundingBox().Fourth + add_y + position_
            };
        } else {
            movement_system_ = {
                shape_.ShapeDefinition->GetBoundingBox().Min + add_y + position_,
                shape_.ShapeDefinition->GetBoundingBox().Second + mvmt + position_,
                shape_.ShapeDefinition->GetBoundingBox().Max + add_x + position_,
                shape_.ShapeDefinition->GetBoundingBox().Fourth + position_
            };
        }
    } else {
        if (mvmt.y >= 0) {
            movement_system_ = {
                shape_.ShapeDefinition->GetBoundingBox().Min + add_x + position_,
                shape_.ShapeDefinition->GetBoundingBox().Second + position_,
                shape_.ShapeDefinition->GetBoundingBox().Max + add_y + position_,
                shape_.ShapeDefinition->GetBoundingBox().Fourth + mvmt + position_
            };
        } else {
            movement_system_ = {
                shape_.ShapeDefinition->GetBoundingBox().Min + mvmt + position_,
                shape_.ShapeDefinition->GetBoundingBox().Second + add_y + position_,
                shape_.ShapeDefinition->GetBoundingBox().Max + position_,
                shape_.ShapeDefinition->GetBoundingBox().Fourth + add_x + position_
            };
        }
    }
}

void Entity::UpdatePosition() {
    position_ += physics_.Velocity;

    //Update shape based on rotation if mutable
    if (physics_.RotationalVelocity != 0) { //TODO: might need to make this check less sensitive
        orientation_ += physics_.RotationalVelocity;
        shape_.SetRotation(orientation_);
    }

    UpdateMovementSystem();
    prospect_position_ = position_;
    primary_sweep_ = 1;
}

bool Entity::PushPrimaryCollisionIndex(unsigned int index) {
    if (primary_collision_count_ < ESO_VAR_PRIMARY_COLLISION_COUNT_MAX) {
        primary_collision_indexes_[primary_collision_count_] = index;
        primary_collision_count_++;

        return true;
    }
    return false;
}

bool Entity::PushPrimaryCollisionIndexIfNotPresent(unsigned int index) {
    if (primary_collision_count_ < ESO_VAR_PRIMARY_COLLISION_COUNT_MAX) {
        for (unsigned short i = 0; i < primary_collision_count_; i++) {
            unsigned int idx = primary_collision_indexes_[i];
            if (index == idx) {
                return false;
            }
        }
        primary_collision_indexes_[primary_collision_count_] = index;
        primary_collision_count_++;
    }
    return false;
}

bool Entity::PushSecondaryCollisionIndexIfNotPresent(unsigned int index, const std::vector<Collision>& collisions) {
    unsigned short insert_index = secondary_collision_count_;
    for (unsigned short i = 0; i < secondary_collision_count_; i++) {
        unsigned int idx = secondary_collision_indexes_[i];
        if (index == idx) {
            return false;
        }
        if (!collision::IsValidByIndex(collisions, idx)) { //TODO: need to handle the subscript index logic IN that function -- pass in vector and index 
            insert_index = i;   //will overwrite this invalid collision, rather than pushing
            break;
        }
    }
    if (insert_index == secondary_collision_count_) {
        if (secondary_collision_count_ == ESO_VAR_SECONDARY_COLLISION_COUNT_MAX) {
            return false;   //exceeded max num collisions
        }
        secondary_collision_count_++;
    }
    secondary_collision_indexes_[insert_index] = index;
    return true;
}

void Entity::ClearPrimaryCollisionIndexes() {
    for (int i = 0; i < ESO_VAR_PRIMARY_COLLISION_COUNT_MAX; i++) {
        primary_collision_indexes_[i] = -1;
    }
    primary_collision_count_ = 0;
}

void Entity::ClearSecondaryCollisionIndexes() {
    for (int i = 0; i < ESO_VAR_SECONDARY_COLLISION_COUNT_MAX; i++) {
        secondary_collision_indexes_[i] = -1;
    }
    secondary_collision_count_ = 0;
}

void Entity::Reset(const Game& g) {
    position_ = initial_position_;
    // prospect_velocity_ = physics_.Velocity;
    orientation_ = 0;   //TODO: future support for initial orientiation
    physics_ = initial_physics_;
}

bool Entity::OnCollision(Entity* other, Game& app) {
    return true;
}


void Entity::serialize_into(std::ostream& os) const {
    os << "Entity::instance_id_ = " << instance_id_ << ", Entity::destroyed_ = " << destroyed_;
}

std::ostream& operator<<(std::ostream& os, const Entity& entity) {
    entity.serialize_into(os);
    return os;
}

}