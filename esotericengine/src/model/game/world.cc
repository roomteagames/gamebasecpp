#include "model/game/world.h"

namespace eso {

World::World(int id, std::string&& name, sf::Vector2<double>&& gravity, bool use_advanced_collisions, bool use_grid, LoadStatus load_status, unsigned short num_collision_iterations):
		id_(id),
		name_(std::move(name)),
		gravity_(std::move(gravity)),
		use_advanced_collisions_(use_advanced_collisions),
		use_grid_(use_grid),
		load_status_(load_status),
		num_collision_iterations_(num_collision_iterations) {}

//TODO: maybe do something in the future
void World::OnWorldLoad(AssetCollection& assets) {
	load_acknowledgement_ = LoadStatus::WorldLoaded;
}

//TODO: figure out where/when this should be called!!!
void World::OnGraphicsLoad(VisualCollection& assets) {
	for (auto& view_type_list : views_) {
		for (auto& view : view_type_list) {
			// INFO: for SpriteViews, this will set loaded Spritesprites to view
			view->InitDependencies(assets);
		}
	}
	load_acknowledgement_ = LoadStatus::GraphicsLoaded;
}

//TODO: maybe do something in the future
void World::OnAudioLoad(AudioCollection& assets) {
	load_acknowledgement_ = LoadStatus::Done;
}

void World::InitEntityGrids(const WorldProps& props) {
	std::cout << name_ << "1" << std::endl;
    entity_grids_.reserve(focuses_.size());
	std::cout << name_ << "2" << std::endl;
	for (auto focus : focuses_) {
		entity_grids_.push_back(EntityGrid(entities_, focus, props));
	}
	std::cout << name_ << "3" << std::endl;
}

unsigned short World::CreateViewType(unsigned int num_reserved_views) {
	std::vector<std::unique_ptr<ViewModel>> v;
	v.reserve(num_reserved_views);
	views_.push_back(std::move(v));
	return views_.size() - 1;	//returns index of newly created views list
}

std::vector<std::unique_ptr<ViewModel>>& World::GetViewsByTypeIndex(unsigned short type_index) {
	if (views_.size() <= type_index) {
		std::cout << "FATAL ERROR in World::GetViewsByTypeIndex: specified view type has not been initialized. Please call CreateViewType() to create a new view_type -- its return value is the new index." << std::endl;
	}
	return views_[type_index];
}

void World::ResetEntities(const Game& g) {
	for (auto& entity : entities_) {
		entity->Reset(g);
	}
}

}