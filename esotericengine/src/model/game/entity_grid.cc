#include "model/game/entity_grid.h"

#include <algorithm>

namespace eso {

inline bool EntityGrid::assign_entity(std::shared_ptr<Entity>& entity, short current_grid_index) {
    const Shape& shape = *entity->GetShape().ShapeDefinition;   //TODO: URGENT! What if shape is null?
    if (std::shared_ptr<Entity> f = focus.lock()) {
        //Must determine entity cell index for EVERY sector this entity falls in based on vertices of entity's AABB!
        for (auto& entity_vertex : shape.GetBoundingBox()) {
            //Calc offset from focus sector -- focus is in CENTER of sector, so add 1 sector every time modulo is past the halfway point of the calculated sector
            sf::Vector2<double> units_from_focus = entity_vertex - f->Position();  //TODO: need a way to configure and store focuses of world on creation
            sf::Vector2i sectors_from_focus = (sf::Vector2i) (units_from_focus / (double) units_per_sector);
            
            sf::Vector2i remainders(
                ((int) units_from_focus.x) % units_per_sector,
                ((int) units_from_focus.y) % units_per_sector
            );
            remainders /= (units_per_sector / 2);
            sectors_from_focus += remainders; 

            unsigned short index_offset = sectors_from_focus.x + (sectors_from_focus.y * grid_dimension);
            unsigned short new_grid_index = focus_sector_index + index_offset;

            //Only modify grid status/position if entity's target grid index has changed
            if (new_grid_index != current_grid_index) {

                //Determine status of entity based on offset -- only set if status is less than already status
                //Entity status begins at UNDEFINED which is the largest status
                if (sectors_from_focus.x <= inner_tier_count
                        && sectors_from_focus.y <= inner_tier_count
                        && Entity::Status::ACTIVE_VISIBLE < entity->GetStatus()) {
                    entity->RefStatus() = Entity::Status::ACTIVE_VISIBLE;

                } else if (sectors_from_focus.x <= middle_tier_count
                        && sectors_from_focus.y <= middle_tier_count
                        && Entity::Status::ACTIVE_INVISIBLE < entity->GetStatus()) {
                    entity->RefStatus() = Entity::Status::ACTIVE_INVISIBLE;

                } else if (sectors_from_focus.x <= outer_tier_count 
                        && sectors_from_focus.y <= outer_tier_count
                        && Entity::Status::INACTIVE < entity->GetStatus()) {
                    entity->RefStatus() = Entity::Status::INACTIVE;

                } else {
                    entity->RefStatus() = Entity::Status::DEAD; //TODO: what happens with a DEAD entity? Does it just get removed?
                }

                if (entity->GetStatus() != Entity::Status::DEAD) {
                    data[new_grid_index].push_back(entity);
                    return true;
                }
            }
        }
    }
    return false;
}

EntityGrid::EntityGrid(std::vector<std::shared_ptr<Entity>>& _entities, const std::weak_ptr<Entity> _focus, const WorldProps& _props):
        focus(_focus),
        inner_tier_count(_props.VisibleSectorRadius),
        middle_tier_count(_props.VisibleSectorRadius + _props.InvisibleSectorRadius),
        outer_tier_count(_props.VisibleSectorRadius + _props.InvisibleSectorRadius + _props.InactiveSectorRadius),
        units_per_sector(_props.UnitsPerBlock * _props.BlocksPerSector)
{
    //calculate and set numeric values
    grid_dimension = outer_tier_count * 2 + 1;
    grid_size = grid_dimension * grid_dimension;
    focus_sector_index = grid_size / 2;

    auto f = focus.lock();
    for (auto& entity : _entities) {
        assign_entity(entity);
	}
}

bool EntityGrid::IsSectorActive(short index) const {
    short focus_position = grid_dimension / 2;
    short diff_x = focus_position - (index % grid_dimension);
    short diff_y = focus_position - (index / grid_dimension);
    return (
        diff_x < middle_tier_count && diff_x > -1 * middle_tier_count &&
        diff_y < middle_tier_count && diff_y > -1 * middle_tier_count
    );
}

void EntityGrid::Update(Game& app) {
    unsigned short i = 0;
    for (auto& contents : data) {

        //Grid index is recalculated for each entity. If moved, pushed to new sector and removed from previous
        auto remove_end = std::remove_if(contents.begin(), contents.end(), [this, i, &app](auto& entity) {
            bool is_moved = assign_entity(entity, i);
            return (is_moved || entity->IsDestroyed() || entity->GetStatus() > Entity::Status::INACTIVE);
        });        
        contents.erase(remove_end, contents.end());
        i++;
    }
}

}