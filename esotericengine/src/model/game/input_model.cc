#include "model/game/input_model.h"

namespace eso {

InputModel::InputModel(const InputConfig& config) : config_(config), index_seed_(config.UniqueSeed), state_(0), previous_state_(0), state_buffer_(0) {
    init_keycode_map(config);
}

InputModel::InputModel(InputConfig&& config) : config_(std::move(config)), index_seed_(config.UniqueSeed), state_(0), previous_state_(0), state_buffer_(0) {
    init_keycode_map(config);
}

void InputModel::AddInputPress(unsigned int keycode) {
    unsigned int virtual_keycode = calc_virtual_keycode(keycode);

    //add virtual key bit to state
    state_buffer_ |= virtual_keycode;
}

void InputModel::RemoveInputPress(unsigned int keycode) {
    unsigned int virtual_keycode = calc_virtual_keycode(keycode);

    //use & to zero-out removed key bit, but maintain all other bits (bit inverse)
    state_buffer_ &= ~virtual_keycode;
}

void InputModel::Update() {
    previous_state_ = state_;
    state_ = state_buffer_;
}

void InputModel::Reset(InputConfig& config) {
    config_ = config;
    index_seed_ = config_.UniqueSeed;
    init_keycode_map(config_);
}

bool InputModel::IsInputPressed(unsigned int keycode) const {
    return (state_ & keycode) == keycode;
}

bool InputModel::IsInputBegun(unsigned int keycode) const {
    return (previous_state_ & keycode) != keycode && (state_ & keycode) == keycode;
}

bool InputModel::IsInputReleased(unsigned int keycode) const {
    return (previous_state_ & keycode) == keycode && (state_ & keycode) != keycode;
}



//helper functions
void InputModel::init_keycode_map(const InputConfig& config) {
    
    //initialize keycode map to 0
    for (int i = 0; i < INPUT_MAP_SIZE; i++) {
        keycode_map_[i] = 0;
    }

    //set particular indexes to virtual keycode values
    keycode_map_[config.FaceUp % index_seed_] =         input::kFaceUp;
    keycode_map_[config.FaceRight % index_seed_] =      input::kFaceRight;
    keycode_map_[config.FaceDown % index_seed_] =       input::kFaceDown;
    keycode_map_[config.FaceLeft % index_seed_] =       input::kFaceLeft;
    keycode_map_[config.DirectionUp % index_seed_] =    input::kDirectionUp;
    keycode_map_[config.DirectionRight % index_seed_] = input::kDirectionRight;
    keycode_map_[config.DirectionDown % index_seed_] =  input::kDirectionDown;
    keycode_map_[config.DirectionLeft % index_seed_] =  input::kDirectionLeft;
    keycode_map_[config.ShoulderRight % index_seed_] =  input::kShoulderRight;
    keycode_map_[config.ShoulderLeft % index_seed_] =   input::kShoulderLeft;
    keycode_map_[config.TriggerRight % index_seed_] =   input::kTriggerRight;
    keycode_map_[config.TriggerLeft % index_seed_] =    input::kTriggerLeft;
    keycode_map_[config.ThrustRight % index_seed_] =    input::kThrustRight;
    keycode_map_[config.ThrustLeft % index_seed_] =     input::kThrustLeft;
    keycode_map_[config.FrontRight % index_seed_] =     input::kFrontRight;
    keycode_map_[config.FrontLeft % index_seed_] =      input::kFrontLeft;
    keycode_map_[config.Aux1 % index_seed_] =           input::kAux1;
    keycode_map_[config.Aux2 % index_seed_] =           input::kAux2;
    keycode_map_[config.Aux3 % index_seed_] =           input::kAux3;
    keycode_map_[config.Aux4 % index_seed_] =           input::kAux4;
    keycode_map_[config.Aux5 % index_seed_] =           input::kAux5;
    keycode_map_[config.Aux6 % index_seed_] =           input::kAux6;
    keycode_map_[config.Aux7 % index_seed_] =           input::kAux7;
    keycode_map_[config.Aux8 % index_seed_] =           input::kAux8;
}

}