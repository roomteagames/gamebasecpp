#include "model/game/physics.h"

#include "util/vector_utils.h"

namespace eso {

Physics::Physics(int _mass, sf::Vector2<double>&& _velocity, double _rotational_velocity, sf::Vector2<double>&& _center):
        Mass(_mass),
        Velocity(std::move(_velocity)),
        RotationalVelocity(_rotational_velocity),
        CenterOfRotation(std::move(_center)) {}

Physics::Physics(int _mass, const sf::Vector2<double>& _acceleration, double _rotational_acc, const sf::Vector2<double>& _center):
        Mass(_mass),
        Acceleration(_acceleration),
        RotationalAcceleration(_rotational_acc),
        CenterOfRotation(_center) {}

std::ostream& operator<<(std::ostream& os, const Physics& physics) {
    os  << "Physics: { "
        << "Mass: " << physics.Mass << ", "
        << "Velocity: " << vector::ToString(physics.Velocity) << ", "
        << "PreviousVelocity: " << vector::ToString(physics.PreviousVelocity) << ", "
        << "Acceleration: " << vector::ToString(physics.Acceleration) << ", "
        << "NetForce: " << vector::ToString(physics.NetForce) << ", "
        << "CenterOfRotation: " << vector::ToString(physics.CenterOfRotation) << ", "
        << "RotationalVelocity: " << physics.RotationalVelocity << ", "
        << "TotalCrushMagnitude: " << physics.TotalCrushMagnitude
        << " }";
    
    return os;
}

}