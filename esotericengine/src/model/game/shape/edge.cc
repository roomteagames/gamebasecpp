#include "model/game/shape/edge.h"

#include <cmath>

//temp for logging
#include <iostream>

namespace eso {

//private
Edge::Edge(unsigned short index, sf::Vector2<double> a, sf::Vector2<double> b, EdgeFunction* fn):
        index_(index),
        a_(a),
        b_(b),
        fn_(fn)
{
    update_incline_angle();
}

//TODO: other physics is represented in RADIANS -- perhaps this should be in RADIANS as well; or it may mess things up!!!
void Edge::update_incline_angle() {
    int diffx = b_.x - a_.x;
    int diffy = b_.y - a_.y;
    if (diffy == 0) { //horizontal
        if (diffx >= 0) {
            incline_angle_ = 0;
        } else {
            incline_angle_ = 180;
        }
    } else if (diffx == 0) { //vertical
        if (diffy >= 0) {
            incline_angle_ = 90;
        } else {
            incline_angle_ = 270;
        }
    } else {    //inclined
        incline_angle_ = std::atan(1.0f * diffy / diffx) * 180 / M_PI;
    }
}

Edge::~Edge() {
    delete fn_;
}


Edge::Edge(const Edge& other):
        index_(other.index_),
        a_(other.a_),
        b_(other.b_),
        incline_angle_(other.incline_angle_)
{
    fn_ = other.fn_->Clone();
}

Edge::Edge(Edge&& other):
        index_(other.index_),
        a_(std::move(other.a_)),
        b_(std::move(other.b_)),
        fn_(other.fn_),
        incline_angle_(other.incline_angle_)
{
    other.fn_ = nullptr;
}

Edge& Edge::operator=(const Edge& other) {
    index_ = other.index_;
    a_ = other.a_;
    b_ = other.b_;
    fn_ = other.fn_->Clone();
    incline_angle_ = other.incline_angle_;

    return *this;
}

Edge& Edge::operator=(Edge&& other) {
    index_ = other.index_;
    a_ = std::move(other.a_);
    b_ = std::move(other.b_);
    fn_ = other.fn_;
    incline_angle_ = other.incline_angle_;

    other.fn_ = nullptr;

    return *this;
}

sf::Vector2<double> Edge::GetNormalVector(double x) const {
    if (a_.x < b_.x || (IsVerticalLine() && a_.y < b_.y)) {
        return fn_->GetNormalVector(x);
    } else {
        return vector::Multiply(-1, fn_->GetNormalVector(x));
    }
}

sf::Vector2<double> Edge::GetUnitNormalVector(double x) const {
    if (a_.x < b_.x || (IsVerticalLine() && a_.y < b_.y)) {
        return fn_->GetUnitNormalVector(x);
    } else {
        return vector::Multiply(-1, fn_->GetUnitNormalVector(x));
    }
}

sf::Vector2<double> Edge::GetSecantVector() const {
    return b_ - a_;
}

sf::Vector2<double> Edge::GetUnitSecantVector() const {
    sf::Vector2<double> secant_vector = GetSecantVector();
    double magnitude = vector::Magnitude(secant_vector);
    return vector::Divide(secant_vector, magnitude);
}

//static
Edge Edge::CreateLinearEdge(unsigned short index, sf::Vector2<double> a, sf::Vector2<double> b) {
    sf::Vector2<double> diff = b - a;
    double slope = 0;
    if (diff.x == 0 && diff.y != 0) {
        slope = mathconstants::NO_SLOPE;
    } else {
        slope = diff.y / diff.x;
    }
    std::cout << "slope for edge = " << slope << std::endl;
    return Edge(index, a, b, new LinearEdgeFunction(slope));
}

//TODO: update once we create a subclass for each separate function degree
Edge Edge::CreateParabolicEdge(unsigned short index, sf::Vector2<double> a, sf::Vector2<double> b) {
    sf::Vector2<double> diff = b - a;
    if (diff.y == 0) {
        return Edge(index, a, b, new PolynomialEdgeFunction(0, 0));
    }
    double slope = diff.y / diff.x;
    return Edge(index, a, b, new PolynomialEdgeFunction(slope / diff.x, 2));
}

Edge Edge::CreateCubicEdge(unsigned short index, sf::Vector2<double> a, sf::Vector2<double> b) {
    sf::Vector2<double> diff = b - a;
    if (diff.y == 0) {
        return Edge(index, a, b, new PolynomialEdgeFunction(0, 0));
    }
    double slope = diff.y / diff.x;
    return Edge(index, a, b, new PolynomialEdgeFunction(slope / (diff.x * diff.x), 3));
}

Edge Edge::Create4thDegreeEdge(unsigned short index, sf::Vector2<double> a, sf::Vector2<double> b) {
    sf::Vector2<double> diff = b - a;
    if (diff.y == 0) {
        return Edge(index, a, b, new PolynomialEdgeFunction(0, 0));
    }
    double slope = diff.y / diff.x;
    return Edge(index, a, b, new PolynomialEdgeFunction(slope / (diff.x * diff.x * diff.x), 4));
}

Edge Edge::Create5thDegreeEdge(unsigned short index, sf::Vector2<double> a, sf::Vector2<double> b) {
    sf::Vector2<double> diff = b - a;
    if (diff.y == 0) {
        return Edge(index, a, b, new PolynomialEdgeFunction(0, 0));
    }
    double slope = diff.y / diff.x;
    return Edge(index, a, b, new PolynomialEdgeFunction(slope / (diff.x * diff.x * diff.x * diff.x), 5));
}


//private
LinearEdge::LinearEdge(sf::Vector2<double> a, sf::Vector2<double> b, LinearEdgeFunction&& fn):
        a_(a),
        b_(b),
        fn_(std::move(fn)) {}

//static
LinearEdge LinearEdge::CreateLinearEdge(sf::Vector2<double> a, sf::Vector2<double> b) {
    sf::Vector2<double> diff = b - a;
    if (diff.y == 0 && diff.x != 0) {
        return LinearEdge(a, b, LinearEdgeFunction(0));
    } else if (diff.x == 0) {
        return LinearEdge(a, b, LinearEdgeFunction(mathconstants::NO_SLOPE));
    }
    double slope = diff.y / diff.x;
    return LinearEdge(a, b, LinearEdgeFunction(slope));
}

};