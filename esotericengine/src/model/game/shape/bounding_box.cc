#include "model/game/shape/bounding_box.h"

namespace eso {

BoundingBox::BoundingBox():
        Min(0, 0),
        Second(0, 0),
        Max(0, 0),
        Fourth(0, 0) {}

BoundingBox::BoundingBox(sf::Vector2<double> _min, sf::Vector2<double> _second, sf::Vector2<double> _max, sf::Vector2<double> _fourth):
        Min(_min),
        Second(_second),
        Max(_max),
        Fourth(_fourth),
        Width((Second - Min).x),
        Height((Fourth - Min).y) {}

BoundingBox::BoundingBox(sf::Vector2<double> _center, double _width, double _height):
        Min(_center + sf::Vector2<double>(-1 * _width / 2, -1 * _height / 2)),
        Second(_center + sf::Vector2<double>(_width / 2, -1 * _height / 2)),
        Max(_center + sf::Vector2<double>(_width / 2, _height / 2)),
        Fourth(_center + sf::Vector2<double>(-1 * _width / 2, _height / 2)),
        Width(_width),
        Height(_height) {}

//TODO: implement without a for loop
bool BoundingBox::IsOverlap(const BoundingBox& other) const {
    for (auto& other_vertex : other) {
        if (other_vertex.x >= Min.x && 
                other_vertex.x <= Max.x && 
                other_vertex.y >= Min.y && 
                other_vertex.y <= Max.y) {
            return true;
        }
    }
    return false;
}
 
double BoundingBox::GetHorizontalOverlap(const BoundingBox& other) const {
    if (this->Max.x >= other.Min.x && other.Max.x >= this->Max.x) {
        return this->Max.x - other.Min.x;
    } else if (other.Max.x >= this->Min.x && this->Min.x >= other.Min.x) {
        return other.Max.x - this->Min.x;
    }
    return 0.0;
}

double BoundingBox::GetVerticalOverlap(const BoundingBox& other) const {
    if (this->Max.y >= other.Min.y && other.Max.y >= this->Max.y) {
        return this->Max.y - other.Min.y;
    } else if (other.Max.y >= this->Min.y && this->Min.y >= other.Min.y) {
        return other.Max.y - this->Min.y;
    }
    return 0.0;
}

BoundingBox BoundingBox::GetTranslation(const sf::Vector2<double>& t) const {
    return {
        Min + t,
        Second + t,
        Max + t,
        Fourth + t
    };
}

std::string BoundingBox::ToString() const {
    return "(" + std::to_string(Min.x) + "," + std::to_string(Min.y) + ")--(" + std::to_string(Max.x) + "," + std::to_string(Max.y) + ")";
}

namespace boundingbox {

bool IsOverlap(const sf::Vector2<double>& center1, unsigned int width1, unsigned int height1, const sf::Vector2<double>& center2, unsigned int width2, unsigned int height2) {
    sf::Vector2<double> half_dimensions1 = sf::Vector2<double>(width1 / 2, height1 / 2);
    sf::Vector2<double> half_dimensions2 = sf::Vector2<double>(width2 / 2, height2 / 2);
    
    sf::Vector2<double> min1 = center1 - half_dimensions1;
    sf::Vector2<double> min2 = center2 - half_dimensions2;
    sf::Vector2<double> max1 = center1 + half_dimensions1;
    sf::Vector2<double> max2 = center2 + half_dimensions2;

    bool overlap_x = (min1.x >= min2.x && min1.x <= max2.x) || (min2.x >= min1.x && min2.x <= max1.x);
    bool overlap_y = (min1.y >= min2.y && min1.y <= max2.y) || (min2.y >= min1.y && min2.y <= max1.y);

    return overlap_x && overlap_y;
}

}

}