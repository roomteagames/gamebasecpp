#include "model/game/shape/shape.h"

#include <cmath>

namespace eso {

//TODO: NOTE: edges not being populated in these ctors -- probably want edges created so that coldet will work properly
Shape::Shape(const std::string& name, const sf::Vector2<double>& min, const sf::Vector2<double>& max):
        name_(name),
        bounding_box_(
            min,
            min + sf::Vector2<double>((max - min).x, 0),
            max,
            min + sf::Vector2<double>(0, (max - min).y)
        ),
        center_((max - min) / 2.0),
        empty_(false) {}

Shape::Shape(std::string&& name, sf::Vector2<double>&& min, sf::Vector2<double>&& max):
        name_(std::move(name)),
        bounding_box_(
            min,
            min + sf::Vector2<double>((max - min).x, 0),
            max,
            min + sf::Vector2<double>(0, (max - min).y)
        ),
        center_((max - min) / 2.0),
        empty_(false) {}

Shape::Shape(const Shape& s):
        name_(s.name_),
        bounding_box_(s.bounding_box_),
        center_(s.center_),
        edges_(s.edges_),
        mutable_(true),  //Shape becomes mutable when copied!
        source_(&s),     //Retain const ptr to original shape
        empty_(s.empty_)
{
    std::cout << "copying shape \"" << name_ << "\" -- becomes mutable" << std::endl;
}

Shape::Shape(Shape&& s):
        name_(std::move(s.name_)),
        bounding_box_(std::move(s.bounding_box_)),
        center_(std::move(s.center_)),
        edges_(std::move(s.edges_)),
        mutable_(s.mutable_),
        empty_(s.empty_)
{
    std::cout << "moving shape" << std::endl;
}

Shape& Shape::operator=(const Shape& s) {
    name_ = s.name_;
    bounding_box_ = s.bounding_box_;
    center_ = s.center_;
    edges_ = s.edges_;
    mutable_ = true;  //Shape becomes mutable when copied!
    source_ = &s;     //Retain const ptr to original shape
    empty_ = s.empty_;

    return *this;
}

Shape& Shape::operator=(Shape&& s) {
    name_ = std::move(s.name_);
    bounding_box_ = std::move(s.bounding_box_);
    center_ = std::move(s.center_);
    edges_ = std::move(s.edges_);
    mutable_ = s.mutable_;
    empty_ = s.empty_;

    return *this;
}

Shape Shape::CreateRectangle(std::string&& name, unsigned int width, unsigned int height) {
    sf::Vector2<double> max(width / 2, height / 2);
    sf::Vector2<double> min = max * -1.0;
    Shape shape(std::move(name), min, max);
    std::cout << "creating rectangle " << shape.GetName() << ", min={" << min.x << "," << min.y << "}, max={" << max.x << "," << max.y << "}" << std::endl;
    
    //IMPORTANT -- Must create edge and THEN move edge into vector; otherwise contained EdgeFunction (pointer to heap obj) is overwritten by latest Edge's fn
    Edge e1 = Edge::CreateLinearEdge(0, min, shape.bounding_box_.Second);
    Edge e2 = Edge::CreateLinearEdge(1, shape.bounding_box_.Second, max);
    Edge e3 = Edge::CreateLinearEdge(2, max, shape.bounding_box_.Fourth);
    Edge e4 = Edge::CreateLinearEdge(3, shape.bounding_box_.Fourth, min);

    shape.Edges().push_back(std::move(e1));
    shape.Edges().push_back(std::move(e2));
    shape.Edges().push_back(std::move(e3));
    shape.Edges().push_back(std::move(e4));


    /**/const Edge& test_edge = shape.Edges()[0];
    return shape;
}

Shape Shape::CreateRectangle(std::string&& name, sf::Vector2<double> min, sf::Vector2<double> max) {
    int width = max.x - min.x;
    int height = max.y - min.y;
    Shape shape(std::move(name), min, max);

    //IMPORTANT -- Must create edge and THEN move edge into vector; otherwise contained EdgeFunction (pointer to heap obj) is overwritten by latest Edge's fn
    Edge e1 = Edge::CreateLinearEdge(0, min, shape.bounding_box_.Second);
    Edge e2 = Edge::CreateLinearEdge(1, shape.bounding_box_.Second, max);
    Edge e3 = Edge::CreateLinearEdge(2, max, shape.bounding_box_.Fourth);
    Edge e4 = Edge::CreateLinearEdge(3, shape.bounding_box_.Fourth, min);

    shape.Edges().reserve(4);
    shape.Edges().push_back(std::move(e1));
    shape.Edges().push_back(std::move(e2));
    shape.Edges().push_back(std::move(e3));
    shape.Edges().push_back(std::move(e4));

    return shape;
}

bool Shape::Rotate(double radians) {
    if (mutable_) {
        double sine_angle = sin(radians);
        double cosine_angle = cos(radians);

        int calculated_x;
        int calculated_y;
        double calculated_m;

        unsigned short i = 0;
        for (Edge& edge : edges_) {
            if (i == 0) {
                calculated_x = (int) round(edge.GetA().x * cosine_angle - edge.GetA().y * sine_angle);
                calculated_y = (int) round(edge.GetA().x * sine_angle + edge.GetA().y * cosine_angle);
            }
            edge.SetA(calculated_x, calculated_y);

            calculated_x = (int) round(edge.GetB().x * cosine_angle - edge.GetB().y * sine_angle);
            calculated_y = (int) round(edge.GetB().x * sine_angle + edge.GetB().y * cosine_angle);
            edge.SetB(calculated_x, calculated_y);

            double run = 1.0 * (edge.GetB().x - edge.GetA().x);
            if (run != 0) {
                calculated_m = (edge.GetB().y - edge.GetA().y) / run;
            } else {
                calculated_m = mathconstants::NO_SLOPE;
            }
            
            edge.Fn()->SetSlope(calculated_m);
            
            i++;
        }

        //TODO: figure out a clean way to move this into the for loop above
        update_bounding_box();
    }
    return mutable_;
}

bool Shape::SetRotation(double orientation) {
    if (mutable_) {
        double sine_angle = sin(orientation);
        double cosine_angle = cos(orientation);

        int calculated_x;
        int calculated_y;
        double calculated_m;

        unsigned short i = 0;
        //Input edges come from source shape; calculate and set actual shape edges
        for (const Edge& edge : source_->GetEdges()) {
            if (i == 0) {
                calculated_x = (int) round(edge.GetA().x * cosine_angle - edge.GetA().y * sine_angle);
                calculated_y = (int) round(edge.GetA().x * sine_angle + edge.GetA().y * cosine_angle);
            }
            edges_[i].SetA(calculated_x, calculated_y);

            calculated_x = (int) round(edge.GetB().x * cosine_angle - edge.GetB().y * sine_angle);
            calculated_y = (int) round(edge.GetB().x * sine_angle + edge.GetB().y * cosine_angle);
            edges_[i].SetB(calculated_x, calculated_y);

            double run = 1.0 * (edge.GetB().x - edge.GetA().x);
            if (run != 0) {
                calculated_m = (edge.GetB().y - edge.GetA().y) / run;
            } else {
                calculated_m = mathconstants::NO_SLOPE;
            }
            
            edges_[i].Fn()->SetSlope(calculated_m);
            
            i++;
        }

        //TODO: figure out a clean way to move this into the for loop above
        update_bounding_box();
    }
    return mutable_;
}

bool Shape::ScaleFromCenter(double scalar) {
    if (mutable_) {
        /* TODO: write scale code */
        update_bounding_box();
    }
    return mutable_;
}

void Shape::update_bounding_box() {
    double min_x = mathconstants::__max_int;
    double max_x = mathconstants::__min_int;
    double min_y = mathconstants::__max_int;
    double max_y = mathconstants::__min_int;

    for (const auto& edge : edges_) {
        if (edge.GetA().x < min_x) {
            min_x = edge.GetA().x;
        }
        if (edge.GetA().x > max_x) {
            max_x = edge.GetA().x;
        }

        if (edge.GetA().y < min_y) {
            min_y = edge.GetA().y;
        }
        if (edge.GetA().y > max_y) {
            max_y = edge.GetA().y;
        }
    }
    bounding_box_ = BoundingBox(
        {min_x, min_y},
        {max_x, min_y},
        {max_x, max_y},
        {min_x, max_y}
    );
    int test = 0;
}

EdgeMaterial& EntityShape::RefEdgeMaterial(Edge const* edge) {
    unsigned short index = edge->GetIndex();
    return EdgeMaterials[index];    //this is making an assumption that EdgeMaterials will ALWAYS be populated!
}

const EdgeMaterial& EntityShape::GetEdgeMaterial(Edge const* edge) const {
    unsigned short index = edge->GetIndex();
    return EdgeMaterials[index];    //this is making an assumption that EdgeMaterials will ALWAYS be populated!
}

double EntityShape::GetEdgeFriction(Edge const* edge) const {
    Material const* material = GetEdgeMaterial(edge).MaterialDefinition;
    if (material != nullptr) {
        return material->GetFriction();
    }
    return 0;
}

double EntityShape::GetEdgeRestitution(Edge const* edge) const {
    Material const* material = GetEdgeMaterial(edge).MaterialDefinition;
    if (material != nullptr) {
        return material->GetRestitution();
    }
    return 1;
}

}