#include "model/game/shape/edge_function.h"

#include <cmath>

#include "util/vector_utils.h"

namespace eso {

EdgeFunction::~EdgeFunction() {}

/*
EdgeFunction::EdgeFunction(const EdgeFunction& other):
        type_(other.type_),
        ID(other.ID) {}

EdgeFunction::EdgeFunction(EdgeFunction&& other):
        type_(other.type_),
        ID(other.ID)
{
    other.ID = -1;
}

EdgeFunction& EdgeFunction::operator=(const EdgeFunction& other) {
    type_ = other.type_;
    ID = other.ID;

    return *this;
}

EdgeFunction& EdgeFunction::operator=(EdgeFunction&& other) {
    type_ = other.type_;
    ID = other.ID;

    other.ID = -1;

    return *this;
}
*/

sf::Vector2<double> EdgeFunction::GetNormalVector(double x) const {
    double slope = GetSlope(x);
    if (slope == mathconstants::NO_SLOPE) {
        return sf::Vector2<double>(1, 0);
    }
    return sf::Vector2<double>(slope, -1);
}

sf::Vector2<double> EdgeFunction::GetUnitNormalVector(double x) const {
    sf::Vector2<double> normal_vector = GetNormalVector(x);
    double magnitude = vector::Magnitude(normal_vector);
    return vector::Divide(normal_vector, magnitude);
}

std::string EdgeFunction::ToString(double x) const {
    std::cout << "calling to string -- id: " << ID << "," << GetType() << "," << Evaluate(x) << "," << GetSlope(x) << std::endl;
    return "EdgeFunction with type=" + std::to_string(GetType()) + ", y(" + std::to_string(x) + ")=" + std::to_string(Evaluate(x)) + ", y'(" + std::to_string(x) + ")=" + std::to_string(GetSlope(x));
}

PolynomialEdgeFunction::PolynomialEdgeFunction(double coefficient, int degree):
        EdgeFunction(FunctionType::POLYNOMIAL),
        coefficient_(coefficient),
        degree_(degree) {}

/*
PolynomialEdgeFunction::PolynomialEdgeFunction(const PolynomialEdgeFunction& other):
        type_(other.type_),
        ID(other.ID),
        coefficient_(other.coefficient_),
        degree_(other.degree_) {}

PolynomialEdgeFunction::PolynomialEdgeFunction(PolynomialEdgeFunction&& other):
        type_(other.type),
        ID(other.ID),
        coefficient_(other.coefficient_),
        degree_(other.degree_)
{
    other.ID = -1;
    other.degree = 0;
}

PolynomialEdgeFunction& PolynomialEdgeFunction::operator=(const PolynomialEdgeFunction& other) {
    type_ = other.type_;
    ID = other.ID;
    coefficient_ = other.coefficient_;
    degree_ = other.degree_;

    return *this;
}

PolynomialEdgeFunction& PolynomialEdgeFunction::operator=(PolynomialEdgeFunction&& other) {
    type_ = other.type_;
    ID = other.ID;
    coefficient_ = other.coefficient_;
    degree_ = other.degree_;

    other.ID = -1;
    other.degree_ = 0;

    return *this;
}
*/

double PolynomialEdgeFunction::Evaluate(double x) const {
    double output;
    switch (degree_) {
        case 0:
            output = 0;
            break;
        case 1:
            output = coefficient_ * x;
            break;
        case 2:
            output = coefficient_ * x * x;
            break;
        case 3:
            output = coefficient_ * x * x * x;
            break;
        case 4:
            output = coefficient_ * x * x * x * x;
            break;
        case 5:
            output = coefficient_ * x * x * x * x * x;
            break;
        default:
            output = (double) coefficient_ * std::pow(x, degree_);
            break;
    }
    return output;
}

double PolynomialEdgeFunction::GetSlope(double x) const {
    double output;
    switch (degree_) {
        case 0:
            output = 0;
            break;
        case 1:
            output = coefficient_;
            break;
        case 2:
            output = 2 * coefficient_ * x;
            break;
        case 3:
            output = 3 * coefficient_ * x * x;
            break;
        case 4:
            output = 4 * coefficient_ * x * x * x;
            break;
        case 5:
            output = 5 * coefficient_ * x * x * x * x;
            break;
        default:
            output = (double) degree_ * coefficient_ * std::pow(x, degree_ - 1);
            break;
    }
    return output;
}

LinearEdgeFunction::LinearEdgeFunction(double slope):
        EdgeFunction(FunctionType::LINEAR),
        slope_(slope) {}

/*
LinearEdgeFunction::LinearEdgeFunction(const LinearEdgeFunction& other):
        type_(other.type_),
        ID(other.ID),
        slope_(other.slope_) {}

LinearEdgeFunction::LinearEdgeFunction(LinearEdgeFunction&& other):
        type_(other.type_),
        ID(other.ID),
        slope_(other.slope_)
{
    other.type_ = -1;
    other.ID = -1;
}

LinearEdgeFunction& LinearEdgeFunction::operator=(const LinearEdgeFunction& other) {
    type_ = other.type_;
    ID = other.ID;
    slope_ = other.slope_;

    return *this;
}

LinearEdgeFunction& LinearEdgeFunction::operator=(LinearEdgeFunction&& other) {
    type_ = other.type_;
    ID = other.ID;
    slope_ = other.slope_;

    other.type_ = -1;
    other.ID = -1;

    return *this;
}
*/

double LinearEdgeFunction::Evaluate(double x) const {
    std::cout << "evaluating -> " << std::to_string(x) << " * " << std::to_string(slope_) << " = " << std::to_string(x*slope_) << ", no slope? " << std::to_string(slope_ == mathconstants::NO_SLOPE) << std::endl;
    if (slope_ == mathconstants::NO_SLOPE) {
        return mathconstants::INFINITE_SOLUTIONS;
    }
    return (slope_ * x);
}

}