#include "model/game/shape/material.h"

namespace eso {

Material::Material(double friction, double restitution):
    friction_(friction),
    restitution_(restitution) {}

namespace material {

/*const */Material* CreateBasic() {
    return new Material(0, 1);
}

}

}