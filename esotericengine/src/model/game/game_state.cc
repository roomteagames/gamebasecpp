#include "model/game/game_state.h"

#include "controller/loading.h"
#include "controller/construction.h"

namespace eso {

GameState::GameState(): volatile_worlds_(kDefaultVolatileWorldsSize, [](const std::unique_ptr<World>& current, const std::unique_ptr<World>& other) {
    return *(current.get()) == *(other.get());
}) {}

GameState::GameState(unsigned short num_volatile_worlds): volatile_worlds_(num_volatile_worlds, [](const std::unique_ptr<World>& current, const std::unique_ptr<World>& other) {
    return *(current.get()) == *(other.get());
}) {} 

// World& GameState::LoadVolatileWorld(unsigned int world_id, std::function<void(unsigned int, World&)> load_fn) {
//     config_.VolatileWorlds.Emplace(world_id);
//     World& world = config_.VolatileWorlds.GetTop();
//     if (world.entities.size() < 1) {
//         load_fn(world_id, world);
//     }
//     return world;
// }

void GameState::Init(const AssetProps& asset_props, const WorldProps& world_props, AssetCollection& assets) {
    //TODO: add for loop through all permanent worlds
    
    for (unsigned int world_id : preload_init_volatile_world_ids_) {
        volatile_worlds_.Push(std::make_unique<World>());
        World& world = *volatile_worlds_.GetBottom();
        construction::BuildWorld(world, assets, asset_props);
        current_worlds_.push_back(world);
    }
    for (unsigned int world_id : preload_deferred_volatile_world_ids_) {
        volatile_worlds_.Push(std::make_unique<World>());
        World& world = *volatile_worlds_.GetBottom();
        construction::BuildWorld(world, assets, asset_props);
    }

    for (auto& world : permanent_worlds_) {
        world->InitEntityGrids(world_props);
    }

    for (auto& world : volatile_worlds_) {
        //showcase iterator returns Wrapper<World>, so we need to call Value to get the actual element
        world.Value->InitEntityGrids(world_props);
    }
}

World* GameState::ActivatePermanentWorldById(unsigned int world_id) {
    for (std::unique_ptr<World>& perm_world_ptr : permanent_worlds_) {
        World& perm_world = *perm_world_ptr;
        if (perm_world.GetId() == world_id) {
            for (World& current_world : current_worlds_) {
                if (current_world.GetId() == world_id) {
                    return &perm_world;
                }
            }
            current_worlds_.push_back(perm_world);
            return &perm_world;
        }
    }
    return nullptr;
}

World* GameState::ActivatePermanentWorldByIndex(unsigned short world_index) {
    World& target_world = *permanent_worlds_[world_index];
    int world_id = target_world.GetId();
    
    for (World& current_world : current_worlds_) {
        if (current_world.GetId() == world_id) {
            return &target_world;
        }
    }
    
    current_worlds_.push_back(target_world);
    return &target_world;
}

bool GameState::DeactivateWorld(unsigned int world_id) {
    int index = 0;
    for (World& current_world : current_worlds_) {
        if (current_world.GetId() == world_id) {
            current_worlds_.erase(current_worlds_.begin() + index);
            return true;
        }
        index++;
    }
    return false;
}

bool GameState::DeleteVolatileWorldById(unsigned int world_id) {
    int index = 0;
    for (auto& world_wrapper : volatile_worlds_) {
        World& world = *world_wrapper.Value;
        if (world.GetId() == world_id) {
            volatile_worlds_.DeleteAt(index);
            return true;
        }
        index++;
    }
    return false;
}

bool GameState::DeleteVolatileWorldByIndex(unsigned short world_index) {
    if (volatile_worlds_.GetSize() > world_index) {
        volatile_worlds_.DeleteAt(world_index);
        return true;
    }
    return false;
}




}