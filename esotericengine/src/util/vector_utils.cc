#include "util/vector_utils.h"

#include <cmath>

namespace eso {

namespace vector {

int Dot(const sf::Vector2i& v1, const sf::Vector2i& v2) {
    return (v1.x * v2.x) + (v1.y * v2.y);
}

double Dot(const sf::Vector2<double>& v1, const sf::Vector2<double>& v2) {
    return (v1.x * v2.x) + (v1.y * v2.y);
}

int CrossZ(const sf::Vector2i& v1, const sf::Vector2i& v2) {
    return (v1.x * v2.y) - (v2.x - v1.y);
}

double CrossZ(const sf::Vector2<double>& v1, const sf::Vector2<double>& v2) {
    return (v1.x * v2.y) - (v2.x - v1.y);
}

sf::Vector2i MultiplyVectors(const sf::Vector2i& v1, const sf::Vector2i& v2) {
    return {v1.x * v2.x, v1.y * v2.y};
}

sf::Vector2<double> MultiplyVectors(const sf::Vector2<double>& v1, const sf::Vector2<double>& v2) {
    return {v1.x * v2.x, v1.y * v2.y};
}

int MagSquared(const sf::Vector2i& v) {
    return (v.x * v.x) + (v.y * v.y);
}

double MagSquared(const sf::Vector2<double>& v) {
    return (v.x * v.x) + (v.y * v.y);
}

int MagSquaredDiff(const sf::Vector2i& v1, const sf::Vector2i& v2) {
    return MagSquared(v1 - v2);
}

double MagSquaredDiff(const sf::Vector2<double>& v1, const sf::Vector2<double>& v2) {
    return MagSquared(v1 - v2);
}

double Magnitude(const sf::Vector2i& v) {
    return std::sqrt(MagSquared(v));
}

double Magnitude(const sf::Vector2<double>& v) {
    return std::sqrt(MagSquared(v));
}

double MagnitudeDiff(const sf::Vector2i& v1, const sf::Vector2i& v2) {
    return std::sqrt(MagSquaredDiff(v1, v2));
}

double MagnitudeDiff(const sf::Vector2<double>& v1, const sf::Vector2<double>& v2) {
    return std::sqrt(MagSquaredDiff(v1, v2));
}

sf::Vector2<double> Round(const sf::Vector2<double>& v, unsigned short num_places) {
    double rounding_factor = std::pow(10, num_places);
    return {
        std::roundf(v.x * rounding_factor) / rounding_factor,
        std::roundf(v.y * rounding_factor) / rounding_factor
    };
}

sf::Vector2i GetUnitVector(const sf::Vector2i& v) {
    double mag = Magnitude(v);
    return {static_cast<int>(v.x / mag), static_cast<int>(v.y / mag)};
}

sf::Vector2<double> GetUnitVector(const sf::Vector2<double>& v) {
    double mag = Magnitude(v);
    return {v.x / mag, v.y / mag};
}

sf::Vector2<double> AddIntFloat(const sf::Vector2i& v1, const sf::Vector2<double>& v2) {
    return sf::Vector2<double>(v1.x + v2.x, v1.y + v2.y);
}

}

}