#include <string>

#include "os.h"

namespace eso {

namespace file {

std::string GetExecutableDirectory() {
	//TODO: also maybe save the full path somewhere so this doesn't have to be called multiple times
	//on Mac OS X the working directory defaults to your home folder
	//instead of the path of the executable, so we need to fix that
	auto MAX_PATH = 1024;
	std::string project_dir = "";
	#if defined(OS_IS_MAC)
		uint32_t i = 1024;
		char* s;
		s = new char[i];
		_NSGetExecutablePath(s, &i);
		std::string str = std::string(s);
		project_dir += str.substr(0, str.find_last_of("/"));
		project_dir += "/";
		delete[] s;
	
	#elif defined(OS_IS_WINDOWS)
		wchar_t self[MAX_PATH] = { 0 };
		DWORD nchar;

		SetLastError(0);
		nchar = GetModuleFileNameW(NULL, self, MAX_PATH);

		if (nchar == 0 ||
			(nchar == MAX_PATH &&
			((GetLastError() == ERROR_INSUFFICIENT_BUFFER) ||
				(self[MAX_PATH - 1] != 0))))
			return nil;

		project_dir = std::string(self);
	
	#elif defined(OS_IS_LINUX)
		std::string path = "";
		pid_t pid = getpid();
		char buf[20] = {0};
		sprintf(buf,"%d",pid);
		std::string _link = "/proc/";
		_link.append( buf );
		_link.append( "/exe");
		char proc[512];
		int ch = readlink(_link.c_str(),proc,512);
		if (ch != -1) {
			proc[ch] = 0;
			path = proc;
			std::string::size_type t = path.find_last_of("/");
			path = path.substr(0,t);
		}

		project_dir = path + "/";

	#endif

    return project_dir;
}
    
}

}