#include "util/binary_reader.h"

#include <fstream>

//temp
#include <iostream>

namespace eso {

BinaryReader::BinaryReader(const std::string& file_name):
        file_name_(file_name) {}

void BinaryReader::Init() {
    // Open the stream as binary, move to end of stream
    std::ifstream stream(file_name_, std::ios_base::in | std::ios::binary | std::ios::ate);

    if (stream.is_open()) {
        std::cout << "FILE IS OPEN: " << file_name_ << std::endl;

        // Determine the file length
        std::size_t size = stream.tellg();
        stream.seekg(0, std::ios_base::beg);
        
        // Reserve vector space and read
        data_.reserve(size);
        data_.insert(data_.begin(),
               std::istream_iterator<unsigned char>(stream),
               std::istream_iterator<unsigned char>());
        stream.close();

        std::cout << "size: " << size << ", data size: " << data_.size() << std::endl;

        Reset();
    } else {
        is_error_ = true;
    }
}

void BinaryReader::Reset() {
    read_index_ = 0;
    is_done_ = is_error_ = false;
}

//TODO: handle two types of string: pascal and fixed
std::string BinaryReader::NextVarString() {
    unsigned int index = read_index_;
    unsigned short size = data_[index++];
    unsigned int length = GetLength();

    //Ensure there are enough bytes remaining to read value
    if (read_index_ + size <= length) {
        std::string value(data_.begin() + index, data_.begin() + (index + size));
        read_index_ = index + size;

        //Check whether this was the last datum to read
        if (read_index_ + size == length) {
            is_done_ = true;
        }
        return value;

    } else {
        is_done_ = is_error_ = true;
        //NOTE: read_index_ is NOT incremented in the error case
    }
    return "";
}



}