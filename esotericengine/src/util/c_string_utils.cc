#include "util/c_string_utils.h"

#include <iostream>

namespace eso {

namespace utils {
;
const unsigned short kCharNumBase = 48;

unsigned long partial_strtoul(const char* str, int start_index, int length, unsigned short base) {
    unsigned long output = 0;
    const unsigned short kOutOfBounds = start_index + length;

    for (int i = start_index; i < kOutOfBounds; i++) {
        if (str[i] != '\0') {
            unsigned short numeral;
            switch (str[i]) {
                case 'a':
                case 'A':
                    numeral = 10;
                    break;
                case 'b':
                case 'B':
                    numeral = 11;
                    break;
                case 'c':
                case 'C':
                    numeral = 12;
                    break;
                case 'd':
                case 'D':
                    numeral = 13;
                    break;
                case 'e':
                case 'E':
                    numeral = 14;
                    break;
                case 'f':
                case 'F':
                    numeral = 15;
                    break;
                default:
                    numeral = str[i] - kCharNumBase;
                    break;
            }
            unsigned short degree = kOutOfBounds - i - 1;
            
            unsigned factor = 1;
            for (int j = 0; j < degree; j++) {
                factor *= base;
            }
            output += numeral * factor;
        }
    }
    return output;
}

}

}