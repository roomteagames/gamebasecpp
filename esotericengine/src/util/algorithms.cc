#include "util/algorithms.h"

namespace eso {

namespace util {

std::vector<double> CalcRandomSummands(double sum, unsigned short num_summands, double lower_threshold, double upper_threshold) {
    const double kEvenFactor = sum / num_summands;
    const double kMaxLowerGap = even_factor * lower_threshold;
    const double kMaxUpperGap = even_factor * upper_threshold;
    const double kSmallestValue = kEvenFactor - kMaxLowerGap;
    const double kLargestValue = kEvenFactor + kMaxUpperGap;

    std::vector<double> summands;
    summands.reserve(num_summands);
 
    double remaining_sum = sum;
    for (unsigned short i = 0; i < num_summands; i++) {
        double required_remainder_min = kSmallestValue * (num_summands - i - 1);
        double required_remainder_max = kLargestValue * (num_summands - i - 1);


    }


}


std::vector<unsigned int> CalcRandomSummands(unsigned int sum, unsigned short num_summands, unsigned int smallest_threshold, unsigned int largest_threshold) {
    unsigned int even_factor = sum / num_summands;
    unsigned int smallest_value = even_factor - smallest_threshold;
    unsigned int largest_value = even_factor + largest_threshold;
    
    assert smallest_value >= 0;
    assert largest_value <= sum;

}

}

}