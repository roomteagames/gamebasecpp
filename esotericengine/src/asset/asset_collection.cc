#include "asset/asset_collection.h"

#include "constants.h"

//TODO: temp
#include <iostream>

namespace eso {

VisualCollection::VisualCollection(const AssetProps& props):
        props_(props),
        texture_store_(0x0, props.TextureCapacity) {}

Texture_Spritesheets* VisualCollection::FetchOrCreateTextureSpritesheet(const std::string& texture_name) {
    std::cout << "beginning to load texture" << std::endl;
    if (!texture_store_.ContainsKey(texture_name)) {
        texture_store_.Add(texture_name, Texture_Spritesheets(texture_name));
    }
    return texture_store_[texture_name];
}

const Texture_Spritesheets* VisualCollection::FetchTextureSpritesheet(const std::string& texture_name) {
    auto* texture_spritesheet = texture_store_[texture_name];
    if (texture_spritesheet != nullptr) {
        return texture_spritesheet;
    }
    return nullptr;
}

const sf::Texture* VisualCollection::FetchTexture(const std::string& texture_name) {
    auto* texture_spritesheet = FetchTextureSpritesheet(texture_name);
    if (texture_spritesheet != nullptr && texture_spritesheet->IsTextureLoaded()) {
        return &texture_spritesheet->GetTexture();
    }
    return nullptr;
}

const Spritesheet* VisualCollection::FetchSpritesheet(const std::string& texture_name, unsigned short spritesheet_index) {
    const auto* texture_spritesheet = FetchTextureSpritesheet(texture_name);
    if (texture_spritesheet != nullptr && texture_spritesheet->GetNumSpritesheets() > spritesheet_index) {
        return &texture_spritesheet->GetSpritesheet(spritesheet_index);
    }
    return nullptr; 
}

sf::Font* VisualCollection::FetchOrCreateFont(const std::string& font_name) {
    if (fonts_.find(font_name) == fonts_.end()) {
        sf::Font font;
        std::string font_path = system::FontsDirectory + "/" + font_name + system::FontsExtension;
        if (font.loadFromFile(font_path)) {
            fonts_[font_name] = font;
        } else {
            return nullptr;
        }
        
    }
    return &fonts_[font_name];
}

const sf::Font* VisualCollection::FetchFont(const std::string& font_name) {
    if (fonts_.find(font_name) != fonts_.end()) {
        return &fonts_[font_name];
    }
    return nullptr;
}


AudioCollection::AudioCollection(const AssetProps& props):
        props_(props)/*,
        sound_store_(0x1, props.SoundCapacity),
        music_store_(0x2, props.MusicCapacity)*/ {}


//TODO: passing props into ctor may not be necessary at all...
PhysicsCollection::PhysicsCollection(const AssetProps& props):
        props_(props) {}

unsigned short PhysicsCollection::AddShape(Shape&& shape) {
    unsigned short index = num_shapes_;

    shape.mutable_ = false; //shapes in collection are shared and cannot be modified/rotated
    shapes_[index] = std::move(shape);
    
    num_shapes_++;
    
    return index;
}

unsigned short PhysicsCollection::AddMaterial(Material&& material) {
    unsigned short index = num_materials_;

    materials_[index] = std::move(material);

    num_materials_++;

    return index;
}


AssetCollection::AssetCollection(const AssetProps& props):
        Visuals(props),
        Audio(props),
        Physics(props) {}

}