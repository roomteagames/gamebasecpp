#include "view/entity/stateful_view.h"

namespace eso {

StatefulView::StatefulView(const std::shared_ptr<Entity>& model, unsigned short starting_state, unsigned short starting_frame, unsigned long locked_states,
                bool auto_advance_frames, unsigned int frame_length, unsigned long looping_states):
        ViewModel(model),
        state_(starting_state),
        frame_num_(starting_frame),
        locked_states_(locked_states),
        auto_advance_frames_(auto_advance_frames),
        frame_length_(frame_length),
        looping_states_(looping_states) {}

void StatefulView::update_view(const GameState& game_state, const std::shared_ptr<Entity>& model) {
    if (auto_advance_frames_) {
        if (frame_progress_ < frame_length_) {
            frame_progress_++;
        } else {
            frame_progress_ = 0;
            if (frame_num_ + 1 < GetNumStateFrames()) {
                frame_num_++;
            } else if (IsLoopingState(state_)) {
                frame_num_ = 0;
            }
        }
        previous_frame_ = frame_num_;
    }
}

bool StatefulView::IsStateUpdated() const {
    return (state_ != previous_state_);
}

void StatefulView::UpdateFrame(unsigned short frame) {
    previous_frame_ = frame_num_;
    frame_num_ = frame;
    frame_progress_ = 0;
}


void StatefulView::UpdateState(unsigned short state) {
    previous_state_ = state_;
    state_ = state;
}
    
}