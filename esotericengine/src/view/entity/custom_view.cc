#include "view/entity/custom_view.h"

namespace eso {

CustomView::CustomView(const std::shared_ptr<Entity>& model, const std::function<void(VisualCollection&, sf::RenderWindow*)>& render_fn):
        ViewModel(model),
        render_fn_(render_fn) {}

void CustomView::draw_view(const GameState& state, const std::shared_ptr<Entity>& model, VisualCollection& assets, sf::RenderWindow* window) {
    render_fn_(assets, window);
}

}