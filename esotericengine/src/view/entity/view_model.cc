#include "view/entity/view_model.h"

#include "util/vector_utils.h"

namespace eso {

ViewFunctions::ViewFunctions(bool (*eval_model)(const GameState&, const std::shared_ptr<Entity>&),
                void (*pre_draw)(const GameState&, const std::shared_ptr<Entity>&, ViewModel*, VisualCollection&, sf::RenderWindow*),
                void (*post_draw)(const GameState&, const std::shared_ptr<Entity>&, ViewModel*, VisualCollection&, sf::RenderWindow*)):
        EvaluateModel(eval_model),
        PreDraw(pre_draw),
        PostDraw(post_draw) {}

//default protected implementations, can be overridden in subclass
void ViewModel::set_view_position(const std::shared_ptr<Entity>& m) {
    SetPosition((sf::Vector2<double>)
        vector::MultiplyVectors(
            (sf::Vector2<double>) (m->GetPosition()/* + m->GetShape().ShapeDefinition->GetBoundingBox().Min*/),
            scale_
        )
    );
    SetDimensions(
        m->GetWidth() * scale_.x,
        m->GetHeight() * scale_.y
    );
    SetOrientation(
        m->GetOrientationDegrees()  // Graphical rotation must be defined in degrees, per sfml
    );
}


//ctors / public methods
ViewModel::ViewModel(const std::shared_ptr<Entity>& model, ViewFunctions&& view_functions):
        model_(model),
        view_functions_(std::move(view_functions)) {}

ViewModel::ViewModel(const std::shared_ptr<Entity>& model, sf::Vector2<double> scale, ViewFunctions&& view_functions):
        model_(model),
        scale_(scale),
        view_functions_(std::move(view_functions)) {}

void ViewModel::Render(const GameState& state, VisualCollection& assets, sf::RenderWindow* window) {
    if (!IsDestroyed()) {
        const auto m = GetModel();
        if (m != nullptr) {

            //std::cout << "Model status: " << m->GetStatus() << ", visible is " << Entity::Status::ACTIVE_VISIBLE << ", will draw? " << will_draw_view(m) << std::endl;

            //Model must be in a renderable state
            if (m->GetStatus() == Entity::Status::ACTIVE_VISIBLE && will_draw_view(m)) {
                
                //Custom evaluation model function must be undefined or return true to render
                if (view_functions_.EvaluateModel == nullptr || view_functions_.EvaluateModel(state, m)) {
                    set_view_position(m);
                    
                    if (view_functions_.PreDraw != nullptr) {
                        view_functions_.PreDraw(state, m, this, assets, window);
                    }
        
                    draw_view(state, m, assets, window);
                    
                    if (view_functions_.PostDraw != nullptr) {
                        view_functions_.PostDraw(state, m, this, assets, window);
                    }
                }
            }
            update_view(state, m);
        }
    }
}

bool ViewModel::IsDestroyed() const {
    using wt = std::weak_ptr<Entity>;
    return !model_.owner_before(wt{}) && !wt{}.owner_before(model_);  //checks if model_ == wt{}
}

}