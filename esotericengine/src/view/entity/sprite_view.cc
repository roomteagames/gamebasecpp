#include "view/entity/sprite_view.h"

#include "controller/loading.h"
#include "util/rect_utils.tpp"

//TODO: temp remove
#include <iostream>

namespace eso {

SpriteView::SpriteView(const std::shared_ptr<Entity>& model, const std::string& texture_name, unsigned short spritesheet_index, unsigned short starting_state, unsigned short starting_frame,
                        unsigned long locked_states, bool auto_advance_frames, unsigned int frame_length, unsigned long looping_states):
        StatefulView(
            model,
            starting_state,
            starting_frame,
            locked_states,
            auto_advance_frames,
            frame_length,
            looping_states
        ),
        spritesheet_config_(
            texture_name,
            spritesheet_index
        )
{
    loading::RequestTexture(texture_name);
}
        

SpriteView::SpriteView(const std::shared_ptr<Entity>& model, const sf::Texture& texture, const Spritesheet* spritesheet, unsigned short starting_state, unsigned short starting_frame, unsigned long locked_states, bool auto_advance_frames, unsigned int frame_length, unsigned long looping_states):
        StatefulView(
            model,
            starting_state,
            starting_frame,
            locked_states,
            auto_advance_frames,
            frame_length,
            looping_states
        ),
        spritesheet_(spritesheet)
{
    sprite_.setTexture(texture);
}

void SpriteView::InitDependencies(VisualCollection& assets) {
    // Sets spritesheet if not already set
    // WARNING: Will probably behave badly if both spritesheet_config_ and spritesheet_ are not intialized
    if (spritesheet_ == nullptr) {
        //TODO: URGENT figure out what they're talking about here...
        auto* spritesheet = assets.FetchSpritesheet(
            spritesheet_config_.Name,
            spritesheet_config_.SheetIndex
        );
        if (spritesheet != nullptr) {
            spritesheet_ = spritesheet;
        }
    }
    update_sprite_structure();
}

//WARNING: Will crash engine if spritesheet_ is NOT initialized!
sf::IntRect SpriteView::build_sprite_rect() {
    const sf::Vector2u& frame_size = spritesheet_->GetFrameSize();
    const sf::Vector2u& origin = spritesheet_->GetOrigin();
    
    return sf::IntRect(
        origin.x + (frame_num_ * frame_size.x),
        origin.y + (state_ * frame_size.y),
        frame_size.x,
        frame_size.y
    );
}

void SpriteView::update_sprite_structure() {
    sf::IntRect rect = build_sprite_rect();
    sprite_.setTextureRect(rect);

    //rotation/scaling occurs around this point
    //TODO: do we need this if origin is defined at the spritesheet level?
    //sprite_.setOrigin(rect.width / 2, rect.height / 2); //TODO: add support for center of rotation offset (as can be set in Physics struct)
}

void SpriteView::prepare_sprite(const std::shared_ptr<Entity>& model) {
    sprite_.setPosition(position_.x, position_.y);    //this location logic may be the most common reason for overriding this method
    sprite_.setRotation(orientation_);
}

void SpriteView::draw_view(const GameState& state, const std::shared_ptr<Entity>& model, VisualCollection& assets, sf::RenderWindow* window) {
    if (IsFrameUpdated() || IsStateUpdated()) {
        update_sprite_structure();
    }
    prepare_sprite(model);
    draw_sprite(assets, window);
}

void SpriteView::draw_sprite(VisualCollection& assets, sf::RenderWindow* window) {
    /* TODO: add support for transformations and filters */
    /* This can be overriden for custom logic with sprite */
    
    window->draw(sprite_);
}

}