#include "view/camera.h"

#include "util/vector_utils.h"

namespace eso {

// Camera::Camera(sf::View&& view, unsigned short view_type_index):
//         view_(view),
//         view_type_index_(view_type_index) {}

//unsigned int, unsigned short, const std::shared_ptr<Entity>&, sf::Vector2<double>&&

Camera::Camera(unsigned int instance_id, unsigned short view_type_index, const std::shared_ptr<Entity>& focus, sf::Vector2<double>&& dimensions, double zoom, unsigned int class_id):
        Entity(instance_id, class_id, focus->GetPosition(), EntityOptions(CollisionMode::Ghost, true, 0, false)),
        view_type_index_(view_type_index),
        focus_(focus),
        viewport_((sf::Vector2f) focus->GetPosition(), (sf::Vector2f) dimensions) {

    viewport_.zoom(zoom);
}


void Camera::Update(Game& g, World& w) {
    //TODO: allow updating zoom if zoom updates during gameplay. Will need to adjust entity bounding box dimensions, and potentially the center itself if too close to a "view edge"
    std::shared_ptr<Entity> f = GetFocus();
    viewport_.setCenter((sf::Vector2f) f->GetPosition());
//     SetPosition(f->GetPosition()); -- TODO: once collision detection is fixed, we can begin moving the entity subclass with the view
}

}

