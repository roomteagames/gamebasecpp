#include "view/spritesheet.h"

#include "controller/loading.h"

//TEMP
#include <fstream>

namespace eso {

Spritesheet::Spritesheet(const sf::Vector2u& origin, const sf::Vector2u& frame_size, const std::array<unsigned short, kMaxSpritesheetStates>& frames_per_state):
        origin_(origin),
        frame_size_(frame_size),
        frames_per_state_(frames_per_state) {}

Spritesheet::Spritesheet(sf::Vector2u&& origin, sf::Vector2u&& frame_size, std::array<unsigned short, kMaxSpritesheetStates>&& frames_per_state):
        origin_(std::move(origin)),
        frame_size_(std::move(frame_size)),
        frames_per_state_(std::move(frames_per_state)) {}

Texture_Spritesheets::Texture_Spritesheets(const std::string& name, unsigned short num_spritesheets):
        name_(name)
{
    spritesheets_.reserve(num_spritesheets);
}

//TODO: TEMP
// std::string getcwd_string( void ) {
//     auto PATH_MAX = 200;
//    char buff[PATH_MAX];
//    getcwd( buff, PATH_MAX );
//    std::string cwd( buff );
//    return cwd;
// }

bool Texture_Spritesheets::LoadTexture(const std::string& file_name) {
    //TODO: temp - remove
    if (FILE* file = fopen(file_name.c_str(), "r")) {
        std::cout << "---FILE EXISTS!" << std::endl;
    } else {
        std::cout << "---FILE DOES NOT EXIST!" << std::endl;
    }

    //std::cout << "+++Current path is: " << getcwd_string() << std::endl;

    bool is_loaded = texture_.loadFromFile(file_name);
    if (is_loaded) {
        texture_loaded_ = true;
    }
    return is_loaded;
}

//Manually overloaded -- lvalue params, rvalues params, and rvalue ss
unsigned short Texture_Spritesheets::AddSpritesheet(const sf::Vector2u& origin, const sf::Vector2u& frame_size, const std::array<unsigned short, kMaxSpritesheetStates>& frames_per_state) {
    spritesheets_.emplace_back(origin, frame_size, frames_per_state);
    return (unsigned short) spritesheets_.size() - 1;
}

unsigned short Texture_Spritesheets::AddSpritesheet(sf::Vector2u&& origin, sf::Vector2u&& frame_size, std::array<unsigned short, kMaxSpritesheetStates>&& frames_per_state) {
    spritesheets_.emplace_back(std::move(origin), std::move(frame_size), std::move(frames_per_state));
    return (unsigned short) spritesheets_.size() - 1;
}

unsigned short Texture_Spritesheets::AddSpritesheet(Spritesheet&& sheet) {
    spritesheets_.push_back(std::move(sheet));
    return (unsigned short) spritesheets_.size() - 1;
}

}