#include "sample/sample_config.h"

#include <thread>

#include "controller/loops.h"

namespace eso {

namespace sample {

PropsCollection GenerateSampleProps() {
    GameProps gp;
    GraphicsProps rp;
    WorldProps sip;
    AssetProps ap;
    WorldProps dp;
    LifecycleHooks lh;

    PropsCollection props = {
        std::move(gp),
        std::move(sip),
        std::move(rp),
        std::move(ap),
        std::move(dp),
        std::move(lh)
    };

    return props;
}

/* TODO: deprecate once startup::Execute works */
// void RunLoops(GameState& state) {
//     std::thread update(eso::loops::UpdateLoop, std::ref(state));
//     std::thread render(eso::loops::RenderLoop, std::ref(state));
//     eso::loops::InputLoop(state);

//     update.join();
//     render.join();
// }

}

}