#include "sample/sample_inputs.h"

#include <SFML/Window.hpp>

namespace eso {

namespace sample {

void PopulateKeyboardInputs1(InputConfig& config) {
    config.FaceUp = sf::Keyboard::O;
    config.FaceRight = sf::Keyboard::Semicolon;
    config.FaceDown = sf::Keyboard::L;
    config.FaceLeft = sf::Keyboard::K;
    config.DirectionUp = sf::Keyboard::W;
    config.DirectionRight = sf::Keyboard::D;
    config.DirectionDown = sf::Keyboard::S;
    config.DirectionLeft = sf::Keyboard::A;
    config.ShoulderRight = sf::Keyboard::Slash;
    config.ShoulderLeft = sf::Keyboard::Z;
    config.TriggerRight = sf::Keyboard::Period;
    config.TriggerLeft = sf::Keyboard::X;
    config.ThrustRight = sf::Keyboard::Comma; 
    config.ThrustLeft  = sf::Keyboard::C; 
    config.FrontRight = sf::Keyboard::Return;
    config.FrontLeft = sf::Keyboard::Tab;
    config.Aux1 = sf::Keyboard::F1;
    config.Aux2 = sf::Keyboard::F2;
    config.Aux3 = sf::Keyboard::F3;
    config.Aux4 = sf::Keyboard::F4;
    config.Aux5 = sf::Keyboard::F5;
    config.Aux6 = sf::Keyboard::F6;
    config.Aux7 = sf::Keyboard::F7;
    config.Aux8 = sf::Keyboard::F8;
}

}

}