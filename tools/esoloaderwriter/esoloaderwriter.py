#!/usr/bin/env python3
#
# Copyright (c) 2020 Roomtea Games All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#    * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#    * Neither the name of Roomtea Games nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''
--PREREQS
A. Define memory buffer struct (stateful), allowing n-bytes to be read at a time to be converted to variable of type
B. Implement util methods to read a portion of the binary at position known by buffer struct and return variable of type (this could be part of mem buffer struct)

--PROCESS
Generate routing -> esoloader.cc (in some subfolder of app)
Create function def and switch statement (by entity type) -- param is the stateful buffer (A)
For each header file in all subdirectories of app project (should this include entity.h in esotericengine?)
    For each class in the header file
        If class extends Entity (or Entity subclass)
            Append case for entity type id
            Use CppHeaderParser to collect all ctors of class
            If multiple, select first ctor only...?
            Extract list of arguments by type
            For each argument (type)
                append appropriate util method (B) to create variable for instance based on ith param of ctor
            generate make_shared line using name of class and params based on defined variables above
generate line to return the shared pointer
'''

import os
import sys
import json
import argparse
from CppHeaderParser import CppHeaderParser

ENTITY_CLASS = 'eso::Entity'

'''
Generate object contain ctor param names and types
TODO: enhance to handle entity subclasses
'''
def get_header_ctor_dict(header):
    class_dict = {}
    for class_name in header.classes:
        clazz = header.classes[class_name]

        '''Retrieve class_id from defines'''
        class_id = 0
        define_var = f'{class_name}_ID'
        for define in header.defines:
            if define.startswith(define_var):
                define_segs = define.split()
                if len(define_segs) > 1:
                    try:
                        class_id = int(define_segs[1])
                    except:
                        print(f'ERROR: class_id expected int, but {class_id} provided')

        '''Collect class ctor param names and types'''
        if ENTITY_CLASS in [inherit['class'] for inherit in clazz['inherits']]:
            for method in clazz['methods']['public']:
                if method['name'] == class_name:
                    current_class_dict = {}
                    params_list = [{'name': param['name'], 'type': param['type']} for param in method['parameters']]

                    current_class_dict['params'] = params_list
                    current_class_dict['id'] = class_id
                    current_class_dict['namespace'] = clazz['namespace']
                    class_dict[class_name] = current_class_dict

    return class_dict

'''
Parse ALL Cpp Headers in subdirectories of specified project directory
'''
def get_project_ctor_dict(project_root):
    payload_dict = {}
    project_class_dict = {}
    dependency_file_set = set()

    for dir_name, dir_names, file_names in os.walk(project_root):
        for file_name in file_names:
            if file_name.lower().endswith(('.h', '.hpp', '.c', '.cc', '.cpp')):
                try:
                    header = CppHeaderParser.CppHeader(f'{dir_name}/{file_name}')
                    header_dict = get_header_ctor_dict(header)

                    '''Only append file if there is at least one defined class'''
                    if header_dict:
                        project_class_dict.update(header_dict)
                        dependency_file_set.add(f'{file_name}')

                except CppHeaderParser.CppParseError as e:
                    print(f'WARNING: {e}')
    
    payload_dict['ctor_dict'] = project_class_dict
    payload_dict['dependencies'] = dependency_file_set
    return payload_dict


'''
Steps:
1. Read entity_routing_template
2. For each line, append line to array
3. For dependencies line, load from dependency_template and format with payload dependencies
4. For entity_cases line, load from entity_case_template and format with class_id, namespace, class_name
    *** Need to figure out how to obtain class_id -- this may not be in header file
5. For each parameter type for class_name in payload, get type and generate binary reader function class list
    Join with commas and format reader_actions line with this
6. Save to {src_dir}/{asset}/{generated}/{file_name}.cc
'''
def write_loader(payload, src_root, asset_dir, generated_dir, output_file):
    '''Set template file names'''
    config_dir = f'{os.path.dirname(os.path.realpath(__file__))}/config'
    main_template = f'{config_dir}/entity_routing_template.txt'
    dependency_template = f'{config_dir}/dependency_template.txt'
    case_template = f'{config_dir}/entity_case_template.txt'
    reader_template = f'{config_dir}/reader_action_template.txt'
    action_filename = f'{config_dir}/action_mapping.json'

    '''Create output path if not exist; switch to it'''
    os.chdir(src_root)
    
    if not os.path.exists(asset_dir):
        os.makedirs(asset_dir)
    os.chdir(asset_dir)

    if not os.path.exists(generated_dir):
        os.makedirs(generated_dir)
    os.chdir(generated_dir)
    
    '''Generate dependencies'''
    dependencies_str = ''
    with open(dependency_template, 'r') as template:
        template_content = template.read()
        dependencies = []
        
        for dep_file in payload['dependencies']:
            dep_str = template_content.format(dependency=dep_file)
            dependencies.append(dep_str)
        
        dependencies_str = '\n'.join(dependencies)

    '''Case generation'''
    cases_str = ''
    with open(case_template, 'r') as template:
        template_content = template.read()
        
        with open(reader_template, 'r') as r_template:
            reader_content = r_template.read()

            with open(action_filename, 'r') as action_file:
                action_dict = json.load(action_file)
                entity_cases = []
            
                for class_name in payload['ctor_dict']:
                    class_dict = payload['ctor_dict'][class_name]
                    class_id = class_dict['id']
                    namespace = class_dict['namespace']
                    params = class_dict['params']

                    param_actions = []
                    for param in params:
                        type_key = param['type']
                        if type_key in action_dict:
                            action = action_dict[type_key]
                            reader_str = reader_content.format(action=action)
                            param_actions.append(reader_str)
                        else:
                            print(f'WARNING: Key {type_key} not found in action mapping. Skipping...')

                    actions_str = ', '.join(param_actions)
                    case_str = template_content.format(class_id=class_id, namespace=namespace, class_name=class_name, reader_actions=actions_str)
                    entity_cases.append(case_str)

                cases_str = '\n'.join(entity_cases)            


    '''Main template'''
    output_str = ''
    with open(main_template) as template:
        template_content = template.read()
        output_str = template_content.format(dependencies=dependencies_str, entity_cases=cases_str)

    '''Write output'''
    with open(output_file, 'w') as out_file:
        out_file.write(output_str)

    
        
        




    

def main():
    print('-------------------------------------------\n~~~WELCOME TO THE ESOTERIC LOADER WRITER~~~\n-------------------------------------------')

    '''Argument Parsing'''
    parser = argparse.ArgumentParser(description='Eso Loader Writer')
    parser.add_argument('-i', '--input', type=str, help='Include/header Directory to recursively read entity subclasses', required=True)
    parser.add_argument('-o', '--output', type=str, help='Source directory root in which to write output', required=True)
    parser.add_argument('-a', '--assetDir', type=str, help='Source relative subdirectory for assets', default='asset', required=False)
    parser.add_argument('-g', '--generatedDir', type=str, help='Source relative subdirectory under assets for generated files', default='generated', required=False)
    parser.add_argument('-n', '--name', type=str, help='Name of generated file', default='entity_routing.cc', required=False)
    args = parser.parse_args()

    include_root = args.input
    src_root = args.output
    asset_dir = args.assetDir
    generated_dir = args.generatedDir
    generated_file = args.name

    '''Main logic'''
    payload = get_project_ctor_dict(include_root)
    print(payload)
    write_loader(payload, src_root, asset_dir, generated_dir, generated_file)


if __name__ == '__main__':
    main()