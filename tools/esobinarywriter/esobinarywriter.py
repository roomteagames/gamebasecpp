#!/usr/bin/env python3
#
# Copyright (c) 2020 Roomtea Games All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#    * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#    * Neither the name of Roomtea Games nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''
Assists in writing to a binary file, to be used for Esoteric engine assets
Can be run using an input YAML file, or as an interactable program
Generates a binary file in specified directory with specified name
'''

import argparse
import yaml
import sys
import os
from struct import pack

'''Token Managment'''
VALID_FIELD_TYPES = ['f', 'F', 'n', 'N', 'L', 'un', 'uN', 'uL', '?', 'c', 's', 'v', 'V']

'''Type Limits'''
SHORT_MAX = 2**15 - 1
SHORT_MIN = -(2**15)
USHORT_MAX = 2**16 - 1
USHORT_MIN = 0

INT_MAX = 2**31 - 1
INT_MIN = -(2**31)
UINT_MAX = 2**32 - 1
UINT_MIN = 0

LONG_MAX = 2**63 - 1
LONG_MIN = -(2**63)
ULONG_MAX = 2**64 - 1
ULONG_MIN = 0

BOOL_TRUE_VALUES = [1, 't', 'T', True]
BOOL_FALSE_VALUES = [0, 'f', 'F', False, None]

'''Phrase Management'''
TOO_LARGE_MESSAGE = 'Value exceeds max range for type '
TOO_SMALL_MESSSAGE = 'Value is below min range for type '
BAD_VALUE_MESSAGE = 'Invalid value given for type. Must be '




#TODO: define process: each component of the asset has a number of properties (defined somewhere), and certain properties may be repeating n-times (flag this)
def run_config_mode(args) -> None:
    print('INFO: Entering config mode...')

    config = {'fields': []}
    last_field = False
    i = 0

    '''Asset name'''
    config['name'] = input('---Enter asset name: ')

    '''Handle field config'''
    while not last_field:
        print('   ...')
        field_name = input(f'   Name for field {i}: ')

        valid_type = False
        field_type = None

        while not valid_type:
            field_type = input(f'   Field type: ')

            if field_type not in VALID_FIELD_TYPES:
                print(f"      ERROR: field type '{field_type}' is not in '{','.join(VALID_FIELD_TYPES)}'")
            else:
                valid_type = True

        field_def = input("   Default value: ")

        field_config = {
            'name': field_name,
            'type': field_type,
            'default': field_def
        }

        #Special case: handle aux fields for certain types
        if field_type == 's':
            field_config['length'] = int(input(f"   Length of string: "))

        config['fields'].append(field_config)

        last_field = input("   Another field? Enter 'y': ") != 'y'
        i += 1

    '''Save configuration'''
    output_name = f"{config['name']}.yaml"
    with open(f"{args.configDir}/{output_name}", 'w') as output:
        yaml.dump(config, output)
    
    print(f"   \nINFO: Successfully wrote file '{output_name}' to directory '{args.configDir}'")


def run_load_mode(args):
    print('INFO: Entering load mode...')
    binary_output = b''
    class_id_config = {'name': 'class_id', 'type': 'uN', 'default': 0}

    with open(f"{args.input}", 'r') as input_file:
        input_data = yaml.full_load(input_file)

        for element in input_data:
            asset = [*element][0]

            #TODO: cache this so file only needs to be opened once
            with open(f"{args.configDir}/{asset}.yaml") as config_file:
                assets_config = yaml.full_load(config_file)
                fields_config = assets_config['fields']

                '''Always begin by appending class_id to binary'''
                class_id = assets_config['id']
                class_id_binary = encode_field_value(class_id, class_id_config)
                binary_output += class_id_binary

                '''Append value for each field in config'''
                i = 0
                ctor_values = element[asset]

                for field_config in fields_config:
                    raw_value = ctor_values[i]

                    binary_value = encode_field_value(raw_value, field_config)
                    binary_output += binary_value
                    i += 1

    '''Write binary output to output file'''
    with open(args.output, 'wb') as output_file:
        output_file.write(binary_output)

    print(f'INFO: Load mode complete. Saved output to {args.output}')



'''Reusable function used to format value based on given type
   Raises exception if value is not appropriate to type'''
def encode_field_value(value, config):

    field_type = config['type']
    formatted_value = None
    pack_format = None
    field_aux = None

    #Handle integer types
    if field_type in ['n', 'N', 'L', 'un', 'uN', 'uL']:
        formatted_value = int(value)

        if field_type == 'n': #2 byte integer
            pack_format = 'h'
            if formatted_value > SHORT_MAX:
                raise Exception(TOO_LARGE_MESSAGE + 'n')
            if formatted_value < SHORT_MIN:
                raise Exception(TOO_SMALL_MESSAGE + 'n')
        
        elif field_type == 'N': #4 byte integer
            pack_format = 'i'
            if formatted_value > INT_MAX:
                raise Exception(TOO_LARGE_MESSAGE + 'N')
            if formatted_value < INT_MIN:
                raise Exception(TOO_SMALL_MESSAGE + 'N')

        elif field_type == 'L': #8 byte integer
            pack_format = 'q'
            if formatted_value > LONG_MAX:
                raise Exception(TOO_LARGE_MESSAGE + 'L')
            if formatted_value < LONG_MIN:
                raise Exception(TOO_SMALL_MESSAGE + 'L')

        if field_type == 'un': #2 byte unsigned integer
            pack_format = 'H'
            if formatted_value > USHORT_MAX:
                raise Exception(TOO_LARGE_MESSAGE + 'un')
            if formatted_value < USHORT_MIN:
                raise Exception(TOO_SMALL_MESSAGE + 'un')
        
        elif field_type == 'uN': #4 byte unsigned integer
            pack_format = 'I'
            if formatted_value > UINT_MAX:
                raise Exception(TOO_LARGE_MESSAGE + 'uN')
            if formatted_value < UINT_MIN:
                raise Exception(TOO_SMALL_MESSAGE + 'uN')

        elif field_type == 'uL': #8 byte unsigned integer
            pack_format = 'Q'
            if formatted_value > ULONG_MAX:
                raise Exception(TOO_LARGE_MESSAGE + 'uL')
            if formatted_value < ULONG_MIN:
                raise Exception(TOO_SMALL_MESSAGE + 'uL')

    #Handle floating point types
    elif field_type in ['f', 'F']:
        formatted_value = float(value)

        if field_type == 'f':
            pack_format = 'f'
        elif field_type == 'F':
            pack_format = 'd'

    #Handle boolean type
    elif field_type == '?':
        pack_format = '?'
        formatted_value = 1 if value in BOOL_TRUE_VALUES else 0

    #Handle fixed string type
    elif field_type == 's':
        field_aux = config['length']
        pack_format = f'{field_aux}s'
        formatted_value = str.encode(value)
        len(value) > field_aux and print(f'WARNING: Length of value "{value}" is {len(value)}, but field \'{config["name"]}\' defines max length as {field_aux}. String is truncated...')

    #Handle pascal string (varchar) types
    elif field_type == 'v':
        pack_format = f'{len(value) + 1}p'
        formatted_value = str.encode(value)
        len(value) > 255 and print(f'WARNING: Length of value for type v string exceeds 255 characters. String is truncated...')
        
    #Handle long string (varchar) types
    elif field_type == 'V':
        print('not yet supported...')
    
    return pack(pack_format, formatted_value)


def main():
    print('-------------------------------------------\n~~~WELCOME TO THE ESOTERIC BINARY WRITER~~~\n-------------------------------------------')

    '''Runtime modes'''
    CONFIG_MODE = 0
    INTERACTIVE_MODE = 1
    LOAD_MODE = 2

    '''Argument parsing'''
    parser = argparse.ArgumentParser(description='Generate binary asset file')
    parser.add_argument('-c', '--configDir', type=str, help='Directory containing asset config files.', default='./config')
    parser.add_argument('-i', '--input', type=str, help='Full path to input file', required=False)
    parser.add_argument('-o', '--output', type=str, help='Full path to output file.', default='./output/out.asset')
    parser.add_argument('-s', '--saveInputAs', type=str, help='Save input file name based on interactive run')
    args = parser.parse_args()

    '''Determine runtime mode'''
    mode = CONFIG_MODE
    if args.input:
        mode = LOAD_MODE

    '''Call appropriate mode'''
    if mode == CONFIG_MODE:
        run_config_mode(args)
    elif mode == LOAD_MODE:
        run_load_mode(args)


if __name__ == '__main__':
    main()