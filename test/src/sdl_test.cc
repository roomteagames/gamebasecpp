#include "tests/sdl_test.h"

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include "controller/loops.h"
#include "model/game/game_state.h"
#include "model/entity/test/octo_girl.h"
#include "sample/sample_config.h"

namespace esotest {

int TestSdl() {
//     if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
//         std::cout << "There was an error" << std::endl;
//     } else {
//         std::cout << "Success!" << std::endl;
//     }

//     SDL_Window* window = SDL_CreateWindow(
//         "Hello there!",
//         SDL_WINDOWPOS_UNDEFINED,
//         SDL_WINDOWPOS_UNDEFINED,
//         500,
//         500,
//         SDL_WINDOW_RESIZABLE
//     );

//     SDL_SetWindowMinimumSize(window, 500, 500);

//     SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);

//     SDL_RenderSetLogicalSize(renderer, 1920, 1080);

//     SDL_Rect rect = {0, 0, 1920, 1080};

//     if (window == NULL) {
//         std::cout << "window could not be created: " << SDL_GetError() << std::endl;
//         return 1;
//     }
    
//     SDL_Event e;
//     bool quit = false;

//     while (!quit){
//         SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
//         SDL_RenderClear(renderer);

//         SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
//         SDL_RenderFillRect(renderer, &rect);
        
//         // SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
//         // SDL_RenderDrawRect(renderer, &rect);

//         SDL_RenderPresent(renderer);


//         while (SDL_PollEvent(&e)){
//             if (e.type == SDL_QUIT){
//                 quit = true;
//             }
//             if (e.type == SDL_MOUSEBUTTONDOWN) {
                
//             }
//         }
//         SDL_Delay(16);
//     }

//     SDL_DestroyRenderer(renderer);
//     SDL_DestroyWindow(window);
//     SDL_Quit();

    return 0;
}

int TestSdlGpu() {
//     GPU_Target* screen = GPU_Init(
//         500,                            //width
//         500,                            //height
//         GPU_DEFAULT_INIT_FLAGS            //flags
//     );

//     //Figure out how to set logic size!
//     // SDL_SetWindowResizable(screen, SDL_TRUE);



//     if(screen == NULL) {
// 		return -1;
//     }

//     SDL_Color red = {255, 0, 0, 255};

//     SDL_Event event;
//     bool done = false;

//     while(!done)
//         {
//             // Check events

//             while(SDL_PollEvent(&event))
//             {
//                 if(event.type == SDL_QUIT)
//                     done = 1;
//                 else if(event.type == SDL_KEYDOWN)
//                 {
//                     if(event.key.keysym.sym == SDLK_ESCAPE)
//                         done = 1;
//                 }
//             }

//             GPU_Clear(screen);
//             GPU_RectangleFilled(screen, 30, 0, 100, 100, red);
//             GPU_Flip(screen);

//             SDL_Delay(20);
//         }

//     GPU_FreeTarget(screen);

//     GPU_Quit();

    return 0;
}

int TestSfml() {

    //Create OctoGirl
    // OctoGirl girl(0x01, 100, 200);
    

    // create the window
    // sf::Window window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Default, sf::ContextSettings(32));
    // window.setVerticalSyncEnabled(true);

    // activate the window
    // window.setActive(true);

    // load resources, initialize the OpenGL states, ...
    // eso::PropsCollection props = eso::sample::GenerateSampleProps();
    // eso::InputModel input_model(props.InputConfig);
    // eso::GameState state(props.GameProps, props.GameStateConfig, &window, input_model);

    //TODO: need to write code to intialize sfml window before loops start running
    // eso::sample::RunLoops(state);
    

    // run the main loop
    // bool running = true;
    // while (running)
    // {
    //     // handle events
    //     sf::Event event;
    //     while (window.pollEvent(event))
    //     {
    //         if (event.type == sf::Event::Closed)
    //         {
    //             // end the program
    //             running = false;
    //         }
    //         else if (event.type == sf::Event::Resized)
    //         {
    //             // adjust the Camera when the window is resized
    //             glViewport(0, 0, event.size.width, event.size.height);
    //         }
    //     }

    //     // clear the buffers
    //     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //     // draw...

    //     // end the current frame (internally swaps the front and back buffers)
    //     window.display();
    // }
    return 0;
}

}