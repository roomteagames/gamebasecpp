#include "tests/loop_test.h"

#include <functional>
#include <iostream>
#include <thread>

#include "config.h"
#include "controller/loops.h"
#include "controller/startup.h"
#include "model/game/game_state.h"
#include "model/game/input_model.h"
#include "sample/sample_inputs.h"
#include "sample/sample_config.h"

namespace esotest {

// EntityChildA::EntityChildA(unsigned int instance_id, int x, int y):
//         eso::Entity(instance_id, 0, x, y) {}

// void EntityChildA::Update(eso::GameState& state) {
//     if (state.input_model.IsInputPressed(eso::input::kFaceLeft)) {
//         std::cout << "Pressing button" << std::endl;
//         state.next_state = 0;
//     }
// }


// EntityChildB::EntityChildB(unsigned int instance_id, int x, int y, bool is_angry):
//         eso::Entity(instance_id, 1, x, y), is_angry_(is_angry) {}

// void EntityChildB::Update(eso::GameState& state) {
//     // std::cout << "Updating -- EntityChildB -- next state is " << state.next_state << std::endl;
// }


// ViewChild1::ViewChild1(const eso::Entity* model, const std::string& color):
//         eso::EntityView(model), color_(color) {}

// void ViewChild1::Render() {
//     std::cout << "Rendering -- ViewChild1" << std::endl;
// }


// ViewChild2::ViewChild2(const eso::Entity* model, unsigned int width, unsigned int height, unsigned char red, unsigned char green, unsigned char blue):
//         eso::EntityView(model), width_(width), height_(height), color_(red, green, blue) {}

// void ViewChild2::Render() {
//     std::cout << "Rendering -- ViewChild2" << std::endl;
//     // GPU_Rectangle(screen, model_->GetX(), model_->GetY(), GetWidth(), GetHeight(), color_);
// }


// int TestLoops() {
//     //Create SFML window
//     sf::Window window(sf::VideoMode(500, 500), "Test Loops", sf::Style::Default);

//     //Create game config
//     eso::GameProps g;
//     g.UpdateFramerate = 1;
//     g.RenderFramerate = 1;

//     eso::InputConfig c;
    // eso::sample::PopulateKeyboardInputs1(c);

//     //Create game models
//     eso::InputModel m(c);

//     eso::Entity* e1 = new EntityChildA(100, 0, 0);
//     eso::Entity* e2 = new EntityChildB(101, 0, 0, true);
//     eso::Entity* e3 = new EntityChildA(102, 10, 5);

//     eso::EntityView* v1 = new ViewChild1(e1, "RED");
//     eso::EntityView* v2 = new ViewChild2(e2, 10, 5, 255, 0, 0);
//     eso::EntityView* v3 = new ViewChild2(e3, 100, 200, 0, 0, 255);

//     //TODO: this may already be handled in SFML
//     // eso::Camera* vp1 = new eso::Camera(
//     //         222,    //instance_id
//     //         0,      //x (worldly)
//     //         0,      //y (worldly)
//     //         18,     //width (worldly)
//     //         12,     //height (worldly)
//     //         50,     //screen_x
//     //         0,      //screen_y
//     //         600,    //screen_width (pixels)
//     //         400     //screen_height (pixels)
//     // );

//     sf::View view1(sf::FloatRect(200, 200, 300, 200));


//     std::vector<eso::Entity*> entities;
//     entities.push_back(e1);
//     entities.push_back(e2);
//     entities.push_back(e3);
//     // entities.push_back(vp1);

//     std::vector<eso::EntityView*> views;
//     views.push_back(v1);
//     views.push_back(v2);
//     views.push_back(v3);

//     std::vector<eso::Camera*> viewports;
//     viewports.push_back(vp1);

//     std::vector<eso::World> worlds;
//     worlds.emplace_back(entities, views, viewports);

//     eso::GameState state = {g, m, worlds};

//     //Native C++ Thread test
//     std::thread update(eso::loops::UpdateLoop, std::ref(state));
//     std::thread render(eso::loops::RenderLoop, std::ref(state));

//     eso::loops::InputLoop(state);
//     update.join();
//     render.join();

//     delete v3;
//     delete v2;
//     delete v1;
//     delete e3;
//     delete e2;
//     delete e1;
//     delete vp1;

//     return 0;
// }


TimelineA::TimelineA(unsigned int instance_id):
        eso::Timeline<6>(instance_id, 0x01)
{
    //ESO_MACRO_POPULATE_ARRAY(delays_, 100, 300, 1000, 20, 56);
}

void TimelineA::Update(eso::Game& app) {
    eso::Timeline<6>::Update(app);
    if (ChangedState() && !IsFinished()) {
        std::cout << "updated " << GetState() << std::endl;
    }
}

int TestTimeline() {
//     //config
//     eso::GameProps p;
//     eso::InputConfig c;
//     eso::sample::PopulateKeyboardInputs1(c);

//     //models
//     eso::InputModel input_model(c);

//     std::shared_ptr<eso::Entity> timeline = std::make_shared<TimelineA>(0x01);

//     state_config.PermanentWorlds.emplace_back(0x01);
//     state_config.DefaultWorlds.push_back(
//         state_config.PermanentWorlds[0]
//     );

//     //add entities to world(s)
//     state_config.PermanentWorlds[0]
//             .entities
//             .push_back(timeline);

//     sf::Window w(sf::VideoMode(800,600), "Test Timeline");
    

//     eso::GameState state(p, state_config, &w, input_model);

//     std::cout << "beginning test timeline" << std::endl;

//     //loops
//     std::thread update(eso::loops::UpdateLoop, std::ref(state));
//     eso::loops::InputLoop(state);

//     update.join();

//     std::cout << "after update join" << std::endl;

//     w.close();

    return 0;
}

int TestStartup() {
    eso::InputConfig input_config;
    eso::sample::PopulateKeyboardInputs1(input_config);
    std::vector<eso::GameState> states;
    
    eso::GameState default_state;
    default_state.GetPermanentWorlds().push_back({});
    
    // eso::startup::Execute(eso::sample::GenerateSampleProps(), input_config, std::move(states));

    return 0;
}

}