/*

Warning:
-This file is generated and maintained by the Esoteric Engine esoloaderwriter tool
-Any direct changes to this file will be lost once the esoloaderwriter.py script re-runs

*/

#include "asset/generated/entity_routing.h"

// -- always include construction
#include "controller/construction.h"

// -- match header files with entity subclasses found. include all of these here
#include "octogirl_test_game.h"

namespace eso {

namespace assets {

void LoadEntity(BinaryReader& in, unsigned int class_id, World& world, AssetCollection& assets) {
    switch (class_id) {
    case 1:
        construction::MakeEntity<octogirl::OctoGirl>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt(), in.NextUShort(), in.NextUShort());
        break;
    case 2:
        construction::MakeEntity<octogirl::Block>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 3:
        construction::MakeEntity<octogirl::FallingBlock>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 7:
        construction::MakeEntity<octogirl::LongFallingBlock>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 4:
        construction::MakeEntity<octogirl::Gem>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 5:
        construction::MakeEntity<octogirl::Spring>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 6:
        construction::MakeEntity<octogirl::Flipper>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    }
}

} /* END assets */

} /* END eso */