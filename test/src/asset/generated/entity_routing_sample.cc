/*

Warning:
-This file is generated and maintained by the Esoteric Engine esoloaderwriter tool
-Any direct changes to this file will be lost once the esoloaderwriter.py script re-runs

*/

#include "asset/generated/entity_routing.h"

// -- always include construction
#include "controller/construction.h"

// -- match header files with entity subclasses found. include all of these here
#include "octogirl_test_game.h"

namespace eso {

void LoadEntity(BinaryReader& in, unsigned int class_id, World& world, AssetCollection& assets) {
    switch (class_id) {
    case 1: // OctoGirl::OctoGirl(unsigned int instance_id, int x, int y, unsigned short anxiety, unsigned short happiness)
        construction::MakeEntity<octogirl::OctoGirl>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt(), in.NextUShort(), in.NextUShort());
        break;
    case 2: // Block::Block(unsigned int instance_id, int x, int y)
        construction::MakeEntity<octogirl::Block>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 3: // FallingBlock::FallingBlock(unsigned int instance_id, int x, int y, eso::Physics&& physics)
        construction::MakeEntity<octogirl::FallingBlock>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 4: // Gem::Gem(unsigned int instance_id, int x, int y)
        construction::MakeEntity<octogirl::Gem>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 5: // Spring::Spring(unsigned int instance_id, int x, int y)
        construction::MakeEntity<octogirl::Spring>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 6: // Flipper::Flipper(unsigned int instance_id, int x, int y)
        construction::MakeEntity<octogirl::Flipper>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    case 7: // LongFallingBlock::LongFallingBlock(unsigned int instance_id, int x, int y, eso::Physics&& physics)
        construction::MakeEntity<octogirl::LongFallingBlock>(world, assets, in.NextUInt(), in.NextInt(), in.NextInt());
        break;
    }
}


}