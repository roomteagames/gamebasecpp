#include "tests/input_test.h"

#include <array>
#include <bitset>
#include <functional>
#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>

#include "config.h"
#include "model/game/input_model.h"
#include "controller/timing.h"

#define INDEX_TYPE unsigned char
#define ARRAY_SIZE 219

namespace esotest {

struct KeyPair {
    INDEX_TYPE key_index; 
    unsigned int original_keycode;
};

//helper declarations
void EventLoop(sf::Window&, std::function<void(unsigned int)>, std::function<void(unsigned int)> = NULL, std::function<bool(void)> = NULL);

int TestInputModel() {
    sf::RenderWindow window(sf::VideoMode(800, 600), "Test Input Model", sf::Style::Default);
    window.display();

    eso::InputConfig c;
    c.FaceUp = sf::Keyboard::O;
    c.FaceRight = sf::Keyboard::Semicolon;
    c.FaceDown = sf::Keyboard::L;
    c.FaceLeft = sf::Keyboard::K;
    c.DirectionUp = sf::Keyboard::W;
    c.DirectionRight = sf::Keyboard::D;
    c.DirectionDown = sf::Keyboard::S;
    c.DirectionLeft = sf::Keyboard::A;
    c.ShoulderRight = sf::Keyboard::Slash;
    c.ShoulderLeft = sf::Keyboard::Z;
    c.TriggerRight = sf::Keyboard::Period;
    c.TriggerLeft = sf::Keyboard::X;
    c.ThrustRight = sf::Keyboard::Comma; 
    c.ThrustLeft  = sf::Keyboard::C; 
    c.FrontRight = sf::Keyboard::Return;
    c.FrontLeft = sf::Keyboard::Tab;
    c.Aux1 = sf::Keyboard::F1;
    c.Aux2 = sf::Keyboard::F2;
    c.Aux3 = sf::Keyboard::F3;
    c.Aux4 = sf::Keyboard::F4;
    c.Aux5 = sf::Keyboard::F5;
    c.Aux6 = sf::Keyboard::F6;
    c.Aux7 = sf::Keyboard::F7;
    c.Aux8 = sf::Keyboard::F8;

    eso::InputModel m(c);

    auto on_keypress = [&](unsigned int key) {
        m.AddInputPress(key);
    };

    auto on_keyrelease = [&](unsigned int key) {
        m.RemoveInputPress(key);
    };

    auto finish_iteration = [&]() {
        m.Update();

        if (m.IsInputBegun(eso::input::kDirectionUp)) {
            std::cout << "Found it!!" << std::endl;
        }
        if (m.IsInputReleased(eso::input::kShoulderRight)) {
            std::cout << "Shot gun!!" << std::endl;
        }
        if (m.IsInputBegun(eso::input::kFaceDown|eso::input::kShoulderRight)) {
            std::cout << "Up and down pressed at the same time" << std::endl;
        }
        if (m.IsInputPressed(eso::input::kFrontLeft)) {
            std::cout << "quitting..." << std::endl;
            return true;
        }
        // std::bitset<32> b(m.GetState());
        // std::cout << "current state: " << b << std::endl;
        return false;
    };

    EventLoop(window, on_keypress, on_keyrelease, finish_iteration);

    return 0;
}

int TestUniqueInputs() {
    sf::RenderWindow window(sf::VideoMode(800, 600), "Test Unique Inputs", sf::Style::Default);
    window.display();

    std::array<unsigned char, ARRAY_SIZE> keycodes;
    for (int i = 0; i < ARRAY_SIZE; i++) {
        keycodes[i] = (INDEX_TYPE) i;
    }
    int hello = 1;

    std::vector<KeyPair> key_history;
    key_history.reserve(100);

    auto on_keypress = [&](unsigned int key) {
        INDEX_TYPE key_index = key % ARRAY_SIZE;
        hello = 4;

        for (int i = 0; i < key_history.size(); i++) {
            if (key_history[i].key_index == key_index) {
                std::cout << "Found match: index " << key_index << " matched with key " << key_history[i].original_keycode << ", for pressed key " << key << std::endl;
            }
        }
        key_history.push_back({key_index, key});

        return 0;
    };

    EventLoop(window, on_keypress);

    return 0;
}

void EventLoop(sf::RenderWindow& window, std::function<void(unsigned int)> on_keypress, std::function<void(unsigned int)> on_keyrelease, std::function<bool(void)> finish_iteration) {
    eso::ActiveLoopTimer timer(30);
    sf::Event e;
    while (window.isOpen()) {
        while(window.pollEvent(e)) {
            switch (e.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::KeyPressed:
                    // std::cout << "pressed key: " << e.key.code << std::endl;
                    on_keypress(e.key.code);
                    break;
                case sf::Event::KeyReleased:
                    if (on_keyrelease != NULL) {
                        on_keyrelease(e.key.code);
                    }
                    break;
                default:
                    break;
            }
        }

        if (finish_iteration != NULL) {
            if (finish_iteration()) {
                window.close();
            }
        }
        timer();
    }
}

}