#include "tests/boost_test.h"

namespace esotest {

int TestMacros() {

    int arr[5];
    POPULATE_ARRAY(arr, 1, 2, 3, 4, 5);

    for (int i; i < 5; i++) {
        std::cout << arr[i] << std::endl;
    }

    return 0;
}

}