#include "octogirl_test_game.h"

#include <iostream>

#include <SFML/Graphics.hpp>

//temp
#include <tuple>

#include "asset/generated/view_model_routing.h"
#include "sample/sample_inputs.h"
#include "view/entity/sprite_view.h"
#include "view/camera.h"
#include "controller/loading.h"
#include "controller/construction.h"
#include "util/vector_utils.h"
#include "service/collision.h"
#include "constants.h"

namespace octogirl {

OctoGirl::OctoGirl(unsigned int instance_id, int x, int y, unsigned short anxiety, unsigned short happiness):
        eso::Entity(
            instance_id,        // instance_id (unique)
            OctoGirl_ID,        // class_id (for ALL OctoGirl instances)
            sf::Vector2<double>(x, y), // position (starting shape midpoint of entity)
            eso::EntityOptions(
                eso::CollisionMode::Solid,  // solid (YES -- will be involved in collision detection)
                true,                              // mobile (YES -- movement is affected by external forces)
                0,                                  // shape_index (shape will come from this index in shape asset collection)
                {no_friction_idx, no_friction_idx, no_friction_idx, no_friction_idx},    // (list of materials to reference from material asset collection for each edge of shape) -- 0010 is basic player friction config
                false                               // unique_shape (NO -- shape can be referenced and remain untouched -- Rotation MUST remain off)
            )
        ),
        anxiety_(anxiety),
        happiness_(happiness) {}

void OctoGirl::MakeAnxious(unsigned short amount) {
    anxiety_ += amount;
}

void OctoGirl::MakeCalm(unsigned short amount) {
    anxiety_ -= amount;
}

void OctoGirl::MakeHappy(unsigned short amount) {
    happiness_ += amount;
}

void OctoGirl::MakeSad(unsigned short amount) {
    happiness_ -= amount;
}

//core gameplay logic
void OctoGirl::Update(eso::Game& g, eso::World& w) {
    if (g.Inputs.IsInputPressed(eso::input::kFrontRight)) {
        g.Closing = true;
    }
    
    //movement -- TODO: reimplement to use move::Walk instead of setting velocity
    sf::Vector2<double> mvmt;
    if (g.Inputs.IsInputBegun(eso::input::kDirectionRight)) {
        mvmt += {MOVE_SPEED, 0};
    }
    if (g.Inputs.IsInputBegun(eso::input::kDirectionLeft)) {
        mvmt += {-1 * MOVE_SPEED, 0};
    }

    // if (mvmt.x != 0 || mvmt.y != 0) std::cout << "trying to move to (" << mvmt.x << ", " << mvmt.y << ")" << std::endl;
    ApplyForce(mvmt);

    //jumping action! TODO: reimplement this to use force instead of raw force!!
    if (g.Inputs.IsInputBegun(eso::input::kFaceDown)) {
        sf::Vector2<double> jump = {0, -1 * JUMP_SPEED};
        ApplyForce(jump);
    }

    //apply force to the right!
    // if (g.Inputs.IsInputBegun(eso::input::kTriggerRight)) {
    //     ApplyForce({JUMP_SPEED, 0});
    // }

    if (g.Inputs.IsInputBegun(eso::input::kFrontLeft)) {
        std::cout << "what am I? (" << position_.x << ", " << position_.y << ")" << std::endl;
    }

    // framerate control
    if (g.Inputs.IsInputBegun(eso::input::kAux1)) {
        g.UpdateFramerate = SLOW_UPDATE_FRAMERATE;
    } else if (g.Inputs.IsInputBegun(eso::input::kAux2)) {
        g.UpdateFramerate = UPDATE_FRAMERATE;
    }

    //temp debug
    /*
    if (position_.x <= -44) {
        std::cout << "grid: " << g.CurrentState->GetCurrentWorlds()[0].get().UseGrid() << std::endl;
        g.other_flag = true;
    }*/

    //reset action!
    if (g.Inputs.IsInputBegun(eso::input::kFaceUp)) {
        eso::World& w = g.CurrentState->GetCurrentWorlds().front();
        w.ResetEntities(g);
    }

    //Permanently freeze world updates!
    if (g.Inputs.IsInputBegun(eso::input::kFrontLeft)) {
        g.FreezeUpdate = !g.FreezeUpdate;
    }
    std::cout << "octo position: " << eso::vector::ToString(position_) << "; velocity: " << eso::vector::ToString(physics_.Velocity) << "; accel: " << eso::vector::ToString(physics_.Acceleration) << "; force: " << eso::vector::ToString(physics_.NetForce) << std::endl;
}

bool OctoGirl::OnCollision(eso::Entity* other, eso::Game& app) {
    // return other->GetClassId() != 0x03; //dont collide with falling blocks!
    return true;
}

Block::Block(unsigned int instance_id, int x, int y):
        eso::Entity(
            instance_id,
            Block_ID,
            sf::Vector2<double>(x, y),
            eso::EntityOptions(
                eso::CollisionMode::Solid,
                false,
                0,
                {full_friction_idx, no_friction_idx, no_friction_idx, no_friction_idx},  // 1000 -- is basic block friction config
                false
            )
        ) {}

FallingBlock::FallingBlock(unsigned int instance_id, int x, int y, eso::Physics&& physics):
        eso::Entity(
            instance_id,
            FallingBlock_ID,
            sf::Vector2<double>(x, y),
            std::move(physics),
            eso::EntityOptions(
                eso::CollisionMode::Solid,
                true,
                0,
                {full_friction_idx, no_friction_idx, no_friction_idx, no_friction_idx}
            )
        )
{
    initial_physics_ = physics_;
    previous_position_ = position_;
}

void FallingBlock::Update(eso::Game& app, eso::World& world) {
    //Health/damage logic
    if (health_ > 0) {
        if (invincibility_frames_ > 0) {
            is_invincible_ = true;
            invincibility_frames_--;
        } else {
            is_invincible_ = false;
        }
    } else {
        //TODO: destroying object crashing game
        status_ = eso::Entity::Status::DEAD;
    }
}

bool FallingBlock::OnCollision(eso::Entity* other, eso::Game&) {
    if (!is_invincible_ && other->GetInstanceId() == 0) {
        invincibility_frames_ = 50;
        health_--;
    }
    return true;
}

LongFallingBlock::LongFallingBlock(unsigned int instance_id, int x, int y, eso::Physics&& physics):
        eso::Entity(
            instance_id,
            LongFallingBlock_ID,
            sf::Vector2<double>(x, y),
            std::move(physics),
            eso::EntityOptions(
                eso::CollisionMode::Solid,
                true,
                3,
                {full_friction_idx, no_friction_idx, no_friction_idx, no_friction_idx}
            )
        )
{}

Gem::Gem(unsigned int instance_id, int x, int y):
    eso::Entity(
        instance_id,
        Gem_ID,
        sf::Vector2<double>(x, y),
        eso::EntityOptions(
            eso::CollisionMode::Ghost,
            false
        )
    ) {}

Spring::Spring(unsigned int instance_id, int x, int y):
    eso::Entity(
        instance_id,
        Spring_ID,
        sf::Vector2<double>(x, y),
        eso::EntityOptions(
            eso::CollisionMode::Solid,
            false,
            0,
            {mega_bouncy_idx, slightly_bouncy_idx, slightly_bouncy_idx, slightly_bouncy_idx}
        )
    ) {}

Flipper::Flipper(unsigned int instance_id, int x, int y):
    eso::Entity(
        instance_id,
        Flipper_ID,
        sf::Vector2<double>(x, y),
        eso::EntityOptions(
            eso::CollisionMode::None,
            false,
            0,
            {full_friction_idx, no_friction_idx, no_friction_idx, no_friction_idx}
        )
    ) {}

void Flipper::Update(eso::Game& g, eso::World& w) {
    short num_orientations = 4;
    bool is_update = false;
    if (g.Inputs.IsInputBegun(eso::input::kShoulderLeft)) {
        orientation_ = (orientation_ + 1) % num_orientations;
        is_update = true;
    } else if (g.Inputs.IsInputBegun(eso::input::kShoulderRight)) {
        orientation_ = (orientation_ - 1) % num_orientations;
        is_update = true;
    } else if (g.Inputs.IsInputBegun(eso::input::kThrustRight)) {
        orientation_ = (orientation_ + 3) % num_orientations;
        is_update = true;
    }

    //Update world gravity based on orientation change
    if (is_update) {
        auto grav = w.GetGravity();
        double x_coeff = orientation_ % 2 == 1 ? 0 : (orientation_ == 0 ? 1 : -1);
        double y_coeff = orientation_ % 2 == 0 ? 0 : (orientation_ == 1 ? -1 : 1);
        w.Gravity() = sf::Vector2<double>(x_coeff * grav.y, y_coeff * grav.x);
    }
}

BoundingBoxView::BoundingBoxView(const std::shared_ptr<eso::Entity>& model, const std::string& texture_name, unsigned short spritesheet_index, unsigned short starting_state, bool auto_advance_frames, unsigned int frame_length):
        eso::SpriteView(
            model,
            texture_name,
            spritesheet_index,
            starting_state,
            0,
            eso::bin::bNone,
            auto_advance_frames,
            frame_length,
            eso::bin::bAll64
        ) { std::cout << "Created BBView with texture name" << std::endl; }

BoundingBoxView::BoundingBoxView(const std::shared_ptr<eso::Entity>& model, const sf::Texture& texture, const eso::Spritesheet* spritesheet, unsigned short starting_state, bool auto_advance_frames, unsigned int frame_length):
        eso::SpriteView(
            model,
            texture,
            spritesheet,
            starting_state,
            0,
            eso::bin::bNone,
            auto_advance_frames,
            frame_length,
            eso::bin::bAll64
        ) { std::cout << "Created BBView with existing texture" << std::endl; }

void BoundingBoxView::SetBBPoints(const sf::Vector2<double> p1, const sf::Vector2<double> p2, const sf::Vector2<double> p3, const sf::Vector2<double> p4) {
    point1 = p1;
    point2 = p2;
    point3 = p3;
    point4 = p4;
}

void BoundingBoxView::SetMvmtPoints(const sf::Vector2<double> p1, const sf::Vector2<double> p2, const sf::Vector2<double> p3, const sf::Vector2<double> p4) {
    pointMvmt1 = p1;
    pointMvmt2 = p2;
    pointMvmt3 = p3;
    pointMvmt4 = p4;
}

void BoundingBoxView::SetVertices(const sf::Vector2<double> p1, const sf::Vector2<double> p2, const sf::Vector2<double> p3, const sf::Vector2<double> p4) {
    vertex1 = p1;
    vertex2 = p2;
    vertex3 = p3;
    vertex4 = p4;
}

void BoundingBoxView::prepare_sprite(const std::shared_ptr<eso::Entity>& model) {
    const eso::BoundingBox& box = model->GetBoundingBox();
    const eso::BoundingBox& mvmt = model->GetMovementSystem();
    
    sf::Vector2<double> render_offset(box.GetWidth() / 2, box.GetHeight() / 2);
    SetMvmtPoints(mvmt.Min - render_offset, mvmt.Second - render_offset, mvmt.Max - render_offset, mvmt.Fourth - render_offset);
    SetBBPoints(box.Min - render_offset, box.Second - render_offset, box.Max - render_offset, box.Fourth - render_offset);
    SetText(std::to_string(model->GetInstanceId()));

    sprite_.setPosition(point1.x, point1.y);    //this location logic may be the most common reason for overriding this method
    sprite_.setRotation(orientation_);

    //TODO: maybe move this somewhere else in the future
    if (const OctoGirl* girl = dynamic_cast<OctoGirl*>(model.get())) {
        if (girl->GetAnxiety() < 5) {
            UpdateState(0);
        } else if (girl->GetAnxiety() < 10) {
            UpdateState(1);
        } else {
            UpdateState(2);
        }
    } else if (const FallingBlock* block = dynamic_cast<FallingBlock*>(model.get())) {
        if (block->IsInvincible()) {
            UpdateState(1);
        } else {
            UpdateState(0);
        }
    }
}

void BoundingBoxView::draw_sprite(eso::VisualCollection& assets, sf::RenderWindow* window) {

    std::cout << "About to draw view at " << eso::vector::ToString(position_) << std::endl;

    //Draw lines that describe bounding box - TODO: this is offset because of the axis of rotation offset -- fix!
    sf::RectangleShape rect1;
    rect1.setSize((sf::Vector2f) (pointMvmt3 - pointMvmt1));
    rect1.setFillColor(sf::Color::Red);
    rect1.setPosition((sf::Vector2f) pointMvmt1);
    window->draw(rect1);

    sf::RectangleShape rect2;
    rect2.setSize((sf::Vector2f) (point3 - point1));
    rect2.setFillColor(sf::Color::Green);
    rect2.setPosition((sf::Vector2f) point1);
    window->draw(rect2);

    //Draw shape edges
    sf::VertexArray lines(sf::LineStrip, 4);
    lines[0].color = sf::Color::Blue;
    lines[1].color = sf::Color::Blue;
    lines[2].color = sf::Color::Blue;
    lines[3].color = sf::Color::Blue;
    lines[0].position = (sf::Vector2f) vertex1;
    lines[1].position = (sf::Vector2f) vertex2;
    lines[2].position = (sf::Vector2f) vertex3;
    lines[3].position = (sf::Vector2f) vertex4;
    window->draw(lines);

    window->draw(sprite_);

    sf::Text t;

    //Draw text over image
    auto* font = assets.FetchOrCreateFont();
    if (font != nullptr) {
        t.setFont(*font); // font is a sf::Font
    }
    t.setString(text);
    t.setCharacterSize(20); // in pixels, not points!
    t.setFillColor(sf::Color::White);  
    // text.setStyle(sf::Text::Bold | sf::Text::Underlined);
    t.setPosition((sf::Vector2f) point1);
    window->draw(t);
}


/*void BlockView::render_model(eso::GameState& state, const std::shared_ptr<eso::Entity>& model, sf::RenderWindow* window) {
    SpriteView::render_model(state, model, window);

    sf::Text id_text;
    // id_text.setString(model->GetInstanceId());
    id_text.setFont(octogirl::font);  //TODO: update to use assets.FetchOrCreateFont()
    id_text.setString(std::to_string(model->GetInstanceId()));
    id_text.setCharacterSize(20);
    id_text.setFillColor(sf::Color::Green);
    id_text.setStyle(sf::Text::Bold);
    id_text.setPosition((sf::Vector2<double>) (model->GetPosition() - sf::Vector2<double>(12, 12)));
    // id_text.setPosition(sf::Vector2<double>(0,0));
    window->draw(id_text);

    // sf::Vertex vertices[] = {
    //     sf::Vertex((sf::Vector2<double>) model->GetBoundingMin(), sf::Color::Blue),
    //     sf::Vertex((sf::Vector2<double>) model->GetBoundingSecond(), sf::Color::Blue),
    //     sf::Vertex((sf::Vector2<double>) model->GetBoundingMax(), sf::Color::Blue),
    //     sf::Vertex((sf::Vector2<double>) model->GetBoundingFourth(), sf::Color::Blue)
    // };
    // // draw it
    // window->draw(vertices, 4, sf::LineStrip);
}*/


std::unique_ptr<eso::ViewModel> create_view(const std::shared_ptr<eso::Entity>& entity, const std::string& key, const std::string& resource, eso::AssetCollection& assets, const eso::AssetProps& props, eso::Spritesheet&& ss, bool auto_advance) {
    auto* ts = assets.Visuals.FetchOrCreateTextureSpritesheet(key);
    std::string image_path = eso::system::ExecutableDirectory + props.ImageDirectory + resource + props.ImageExtension;

    if (!ts->IsSpritesheetsCreated()) {    
        ts->LoadTexture(image_path);
        ts->AddSpritesheet(std::move(ss));
    }
    return std::make_unique<octogirl::BoundingBoxView>(entity, ts->GetTexture(), &ts->GetSpritesheet(ts->GetNumSpritesheets() - 1), 0, auto_advance, 5);
}

unsigned short create_views(eso::World& world, eso::AssetCollection& assets, const eso::AssetProps& props) {
    int num_entities = world.GetEntities().size();
    unsigned short view_index = world.CreateViewType(num_entities);
    world.GetViewsByTypeIndex(view_index).reserve(num_entities);

    auto& views = world.GetViewsByTypeIndex(view_index);
    for (const auto& entity : world.GetEntities()) {
        if (entity->GetClassId() == OctoGirl_ID) {
            eso::Spritesheet s({0,0}, {32,32}, {1});
            views.push_back(create_view(entity, "octogirl", "spider", assets, props, std::move(s), false));
        } else if (entity->GetClassId() == Block_ID) {
            eso::Spritesheet s({0,0}, {32,32}, {1});
            views.push_back(create_view(entity, "block", "Generic_block_dk_blue", assets, props, std::move(s), false));
        } else if (entity->GetClassId() == FallingBlock_ID) {
            eso::Spritesheet s({0,0}, {32,32}, {1, 1});
            views.push_back(create_view(entity, "falling_block", "falling_block", assets, props, std::move(s), false));
        } else if (entity->GetClassId() == Gem_ID) {
            eso::Spritesheet s({0,0}, {32,32}, {1});
            views.push_back(create_view(entity, "gem", "green_gem1.png", assets, props, std::move(s), false));
        }
        
    }
    world.RefLoadStatus() = eso::LoadStatus::Done;
    return view_index;
}

void create_camera(eso::World& world, eso::AssetCollection& assets, unsigned short view_index, unsigned short upb) {
    world.GetFocuses().reserve(1);
    world.GetViewports().reserve(1);

    //TODO: obviously need to somehow build this into the engine -- building and setting camera
    eso::construction::MakeEntity<eso::Camera>(world, assets, 0x10, view_index, world.GetEntities().front(), eso::vector::Multiply(upb, sf::Vector2<double>(CAMERA_WIDTH_BLOCKS, CAMERA_HEIGHT_BLOCKS)));
    eso::Camera& camera = (eso::Camera&) *world.GetEntities().back();

    //TODO: need to build focus selection INTO THE ENGINE
    world.GetFocuses().push_back(world.GetEntities().front());

    //TODO: this method is confusing and should be internal to THE ENGINE
    world.GetViewports().push_back(camera.ConstructViewportIndex());

    std::cout << "created Camera" << std::endl;
}


}   /* END octogirl */



namespace eso {

namespace startup {

//TODO: redesign this to make it MORE DECLARATIVE -- custom code should be optional, not mandatory!
//This is a test init method definition -- the game developer should be writing their own
void Init(PropsCollection& props, InputConfig& input, AssetCollection& assets, std::vector<GameState>& states, short& starting_state) {
    std::cout << "running the init method with collisions tracking: " << engine::DebugCollisionTrackedInstanceID << std::endl;

    //TODO: why are the pixels so small? Is it my computer???
    // props.GraphicsProps.WidthPixels = 3840;
    // props.GraphicsProps.HeightPixels = 2160;
    
    //Global property for entire game window -- can be further customized with Camera zoom
    props.Graphics.WidthPixels = DEFAULT_RESO_WIDTH;
    props.Graphics.HeightPixels = DEFAULT_RESO_HEIGHT;

    props.Game.UpdateFramerate = UPDATE_FRAMERATE;
    props.Game.RenderFramerate = RENDER_FRAMERATE;

    //Set demo type based on argv[0]
    octogirl::demo_type = (char*) "--default";
    if (props.Game.argc >= 2) {
        octogirl::demo_type = props.Game.argv[1];
    }
    
    //TODO: URGEMT do we need this hook, or can you put this somewhere else?
    props.Hooks.BeforeOpen = [](eso::Game& g) {
        std::cout << "before open hook" << std::endl;
    };

    props.Hooks.AfterClose = [](auto& g) { std::cout << "after close hook" << std::endl; };


    //Initialize shapes! -- TODO: move this to an external resource file or something
    assets.Physics.AddShape(Shape::CreateRectangle("32x32 Rect", 32, 32));
    assets.Physics.AddShape(Shape::CreateRectangle("64x32 Rect", 64, 32));
    assets.Physics.AddShape(Shape::CreateRectangle("32x64 Rect", 32, 64));
    assets.Physics.AddShape(Shape::CreateRectangle("64x64 Rect", 64, 64));

    //Initialize materials! -- TODO: move this to an external resource file or something
    octogirl::no_friction_idx = assets.Physics.AddMaterial(Material(0, REST_NONE));
    octogirl::full_friction_idx = assets.Physics.AddMaterial(Material(1, REST_NONE));
    octogirl::slightly_bouncy_idx = assets.Physics.AddMaterial(Material(1, REST_MID));
    octogirl::bouncy_idx = assets.Physics.AddMaterial(Material(1, REST_HIGH));
    octogirl::mega_bouncy_idx = assets.Physics.AddMaterial(Material(1, REST_MEGA));


    GameState gs;
    gs.Init(props.Asset, props.World, assets);

    std::cout << "before create worlds" << std::endl;

    //TODO: add all this logic to the GameState class itself -- add to function called by Init, but this function can be used outside of Init
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(0, "TestBed", engine::GetGravity(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(1, "HighStackDemo", engine::GetGravity(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(2, "MultiStackDemo", engine::GetGravity(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(3, "AerialDemo", engine::GetGravity(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(4, "KickingDemo", engine::GetGravity(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(5, "EmptyDemo", engine::GetGravity(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(6, "NoGravity", sf::Vector2<double>(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(7, "Flipper", engine::GetGravity(), false, false));
    gs.GetPermanentWorlds().push_back(std::make_unique<World>(8, "TestLoad", engine::GetGravity(), false, false));

    //Determine world to build and set as active based on demo type (program arg)
    int world_id = 0;    //testbed is default

    if (TEST_HIGHSTACK) {
        world_id = 1;
    } else if (TEST_MULTISTACK) {
        world_id = 2;
    } else if (TEST_AERIAL) {
        world_id = 3;
    } else if (TEST_KICK) {
        world_id = 4;
    } else if (TEST_EMPTY) {
        world_id = 5;
    } else if (TEST_NOGRAVITY) {
        world_id = 6;
    } else if (TEST_FLIPPER) {
        world_id = 7;
    } else if (TEST_LOAD) {
        world_id = 8;
    }

    std::cout << "about to activate world" << std::endl;

    //Activate and build selected world
    World& w = *gs.GetPermanentWorlds()[world_id];  //TODO: when refactored into the engine, world_id should NOT be treated as index!!
    gs.ActivatePermanentWorldById(world_id);
    std::cout << "activated world " << w.GetId() << ": " << w.GetName() << std::endl;

    construction::BuildWorld(w, assets, props.Asset); //TODO: fix later - you shouldn't have to manually call BuildWorld, even if it's permanent
    std::cout << "size of entities: " << w.GetEntities().size() << std::endl;

    states.reserve(1);
    states.push_back(std::move(gs));    //this line of code is NOT user friendly -- you CAN'T just copy gs into vector because the std::unique_ptr<World> can't be copied

    sample::PopulateKeyboardInputs1(input);
}

} /* END startup */

namespace assets {

//Testbed
void World0_00(World& world, AssetCollection& assets, const AssetProps& props) {
    world.GetEntities().reserve(100);

    /*TODO: fix -- order of entities affects coldet! */
    
    std::cout << "creating octogirl" << std::endl;
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 0x0, 0, -300);
    std::cout << "done with octogirl" << std::endl;
    construction::MakeEntity<octogirl::Block>(world, assets, 0x1, 0, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x2, 32, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x3, 64, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x4, 96, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x5, 128, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x6, 192, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x7, 350, 200);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x8, 382, 200);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x9, 414, 200);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x40, 446, 200);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x41, 478, 200);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x42, 510, 200);
    // eso::construction::MakeEntity<octogirl::Block>(world, assets, 0x43, 542, 200);
    for (int i = 0; i < 10; i++) {
        construction::MakeEntity<octogirl::Block>(world, assets, 0x44 + i, 318 - (i * 32), 200);
    }
    construction::MakeEntity<octogirl::Block>(world, assets, 0x4a, 542, 200);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x4b, 574, 200);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x4c, 608, 200);

    //falling grid of blocks on lower right section
    // construction::MakeEntity<octogirl::LongFallingBlock>(world, assets, 0x51, 350, 100);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x50, 350, 0);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x52, 510, -350);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x54, 510, 50);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x57, 510, -260);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x58, 510, 100);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x59, 478, -100);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x60, 478, -400);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x61, 478, -500);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x62, 505, -650);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x63, 350, 0);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x63, 330, -100);
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x64, 330, 130, Physics(1, sf::Vector2<double>(5, 0)));

    // Two colliding blocks --> exposes collisions always inelastic bug
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x55, 100, -300, Physics(1, sf::Vector2<double>(5, 0)));
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x56, 400, -320, Physics(1, sf::Vector2<double>(-10, 0)));

    // octogirl::create_entity(std::make_shared<octogirl::FallingBlock>(0xa, 192, 36, sf::Vector2<double>()), entities, views);
    // construction::MakeEntity<octogirl::Gem>(world, assets, 0xb, 32, -50);

    //extended platform
    construction::MakeEntity<octogirl::Block>(world, assets, 0x11, -32, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x12, -64, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x13, -96, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x14, -128, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x17, -160, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x16, -192, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x17, -224, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x18, -256, 4);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x19, -288, 4);

    //tall wall
    construction::MakeEntity<octogirl::Block>(world, assets, 0x20, -128, -28);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x21, -128, -60);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x22, -128, -92);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x23, -128, -124);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x20, -128, -28);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x20, -128, -28);
    
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x1, 300, -100, sf::Vector2<double>(-5, -1)); // issue a 
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x8, 192, -300, sf::Vector2<double>(-5, -1)); // issue b
    //TODO: figure out why the initial velocity I set below is being ignored the first time!
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0xc, 0, -100, Physics(1, {0,0}, 0.1)); // issue c -- momentum not working correctly?

    //demonstrating 1 to 1 momentum -- outdated code; will not compile
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0xd, 350, -100, sf::Vector2<double>(-10, -7));
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0xe, 100, -100, sf::Vector2<double>(5, -7));
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0xf, 400, -400, sf::Vector2<double>(-3, 0));

    //fast shooting falling block --> Singular
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x1a, 350, -300, Physics(1, {-10, -1.5}));

    //block flying upward toward character --> exposes restitution bug
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x1d, 100, -200, Physics(5, {0, -20}));
    // construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0x1d, 100, -200, Physics(1));

    //springs
    construction::MakeEntity<octogirl::Spring>(world, assets, 0x1b, 100, -28);
    construction::MakeEntity<octogirl::Spring>(world, assets, 0x1c, 132, -28);
    construction::MakeEntity<octogirl::Spring>(world, assets, 0x1e, 68, -28);

    //ceiling above springs
    construction::MakeEntity<octogirl::Block>(world, assets, 0x30, 34, -1000);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x31, 68, -1000);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x32, 100, -1000);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x33, 132, -1000);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x34, 164, -1000);
    construction::MakeEntity<octogirl::Block>(world, assets, 0x35, 196, -1000);

    //flipper for gravity manipulation
    construction::MakeEntity<octogirl::Flipper>(world, assets, 0x36, 100, 100);

    //TODO: figure out a way to not have to add Camera to entities; also figure out how to specify Camera size

    #if IS_STRESS_TEST == 1
    //Memory stress test -- add lots of blocks
    for (int i = 0 ; i < NUM_STRESS_TEST_ENTITIES - NUM_STANDARD_ENTITIES; i++) {
        construction::MakeEntity<octogirl::FallingBlock>(world, assets, i + NUM_STANDARD_ENTITIES, 192 + ((i%10)*33), -100 + ((i/10)*33));
    }
    #endif

    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}

//DEMO: HighStack
void World0_01(World& world, AssetCollection& assets, const AssetProps& props) {
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 0, 0, 0);

    //Standing on fixed wall
    construction::MakeEntity<octogirl::Block>(world, assets, 1, 0, 32);
    construction::MakeEntity<octogirl::Block>(world, assets, 2, 0, 64);
    construction::MakeEntity<octogirl::Block>(world, assets, 3, 0, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 4, 0, 128);
    construction::MakeEntity<octogirl::Block>(world, assets, 5, 0, 160);
    construction::MakeEntity<octogirl::Block>(world, assets, 6, 0, 192);

    //Fixed floor
    construction::MakeEntity<octogirl::Block>(world, assets, 7, 32, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 8, 64, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 9, 96, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 10, 128, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 11, 160, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 12, 192, 192);

    //Falling Blocks Stack
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 13, 128, 0);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 14, 128, -100);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 15, 128, -150);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 16, 140, -300);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 17, 128, -250);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 18, 128, -350);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 19, 115, -500);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 20, 100, -600);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 21, 85, -700);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 22, 65, -800);

    //Stress test blocks
    #if IS_STRESS_TEST == 1
        for (int i = 0; i < NUM_STRESS_TEST_ENTITIES; i++) {
            construction::MakeEntity<octogirl::Block>(world, assets, 23 + i, 90, -900 - (100 * i));
        }
    #endif

    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}

//DEMO: MultiStack -- NOTE: this world currently breaks the engine due to singular matrix issue
void World0_02(World& world, AssetCollection& assets, const AssetProps& props) {
    construction::MakeEntity<octogirl::Block>(world, assets, 0, 0, 0);  // <-- this is the focus
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 1, 210, -150); // <-- BUG! If octogirl is created AFTER series of blocks, cause singular matrix!

    //Fixed floor
    construction::MakeEntity<octogirl::Block>(world, assets, 7, 32, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 8, 64, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 9, 96, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 10, 128, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 11, 160, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 12, 192, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 13, 224, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 14, 256, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 15, 288, 192);

    //Grid of stacks <-- BUG! Singular matrix if block falls directly onto the center of three blocks in a row
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 20, 206, 50);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 21, 206, 0);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 22, 206, 100);

    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 23, 174, -50); // <-- CURRENTLY CHEATING! Adding extra 1 unit space between grid
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 24, 174, 25);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 25, 174, -100);

    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 26, 142, -200);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 27, 142, 0);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 28, 142, 50);

    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}


//DEMO: Aerial
void World0_03(World& world, AssetCollection& assets, const AssetProps& props) {

    //Protagonist falling block
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 0, 0, 0, Physics(1, sf::Vector2<double>(5, -20)));

    //Antagonist fallings blocks!
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 1, 50, -300);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 2, 150, 500, Physics(1, sf::Vector2<double>(0, -30)));
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 3, 1100, -350, Physics(1, sf::Vector2<double>(-15, -10)));

    //Floor at the bottom
    construction::MakeEntity<octogirl::Block>(world, assets, 10, 128, 1000);
    // construction::MakeEntity<octogirl::Block>(world, assets, 11, 96, 1000); <-- BUG! Singular matrix when this is present
    construction::MakeEntity<octogirl::Block>(world, assets, 12, 64, 1000);

    //Octogirl sitting on block (allow for inputs)
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 50, -200, -200);
    construction::MakeEntity<octogirl::Block>(world, assets, 51, -200, -168);


    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}

//DEMO: Kicking
void World0_04(World& world, AssetCollection& assets, const AssetProps& props) {

    //One block ABOVE octogirl
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 0, 0, 0);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 1, -10, -100);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 2, -170, 0);

    //Upper floor
    construction::MakeEntity<octogirl::Block>(world, assets, 10, 0, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 11, 32, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 12, 64, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 13, 96, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 14, 128, 96);

    //Lower floor
    construction::MakeEntity<octogirl::Block>(world, assets, 20, -128, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 21, -160, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 22, -192, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 23, -224, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 24, -256, 192);

    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}

void World0_05(World& world, AssetCollection& assets, const AssetProps& props) {
    //Only have octogirl without any other objects
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 0, 0, 0);

    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}

void World0_06(World& world, AssetCollection& assets, const AssetProps& props) {
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 0, 0, 0);
    construction::MakeEntity<octogirl::Block>(world, assets, 1, 100, -100);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 2, 100, 0);

    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}

//FLIPPER WORLD -- changing gravity
void World0_07(World& world, AssetCollection& assets, const AssetProps& props) {
    //One block ABOVE octogirl
    construction::MakeEntity<octogirl::OctoGirl>(world, assets, 0, 0, 0);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 1, -10, -100);
    construction::MakeEntity<octogirl::FallingBlock>(world, assets, 2, -170, 0);

    //Upper floor
    construction::MakeEntity<octogirl::Block>(world, assets, 10, 0, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 11, 32, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 12, 64, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 13, 96, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 14, 128, 96);

    //Lower floor
    construction::MakeEntity<octogirl::Block>(world, assets, 20, -128, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 21, -160, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 22, -192, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 23, -224, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 24, -256, 192);

    construction::MakeEntity<octogirl::Flipper>(world, assets, 25, 100, 100);

    //Rectangular structure
    construction::MakeEntity<octogirl::Block>(world, assets, 26, -288, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 27, -288, 160);
    construction::MakeEntity<octogirl::Block>(world, assets, 28, -288, 128);
    construction::MakeEntity<octogirl::Block>(world, assets, 29, -288, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 30, -288, 64);
    construction::MakeEntity<octogirl::Block>(world, assets, 31, -288, 32);
    construction::MakeEntity<octogirl::Block>(world, assets, 32, -288, 0);
    construction::MakeEntity<octogirl::Block>(world, assets, 33, -288, -32);
    construction::MakeEntity<octogirl::Block>(world, assets, 34, -288, -64);
    construction::MakeEntity<octogirl::Block>(world, assets, 35, -288, -96);
    construction::MakeEntity<octogirl::Block>(world, assets, 36, -288, -128);
    construction::MakeEntity<octogirl::Block>(world, assets, 37, -288, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 38, -256, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 39, -224, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 40, -192, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 41, -160, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 42, -128, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 43, -96, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 44, -64, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 45, -32, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 46, 0, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 47, 32, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 48, 64, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 49, 96, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 50, 128, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 51, 160, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 52, 192, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 53, 224, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 54, 256, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 55, 288, -160);
    construction::MakeEntity<octogirl::Block>(world, assets, 56, 288, -128);
    construction::MakeEntity<octogirl::Block>(world, assets, 57, 288, -96);
    construction::MakeEntity<octogirl::Block>(world, assets, 58, 288, -64);
    construction::MakeEntity<octogirl::Block>(world, assets, 59, 288, -32);
    construction::MakeEntity<octogirl::Block>(world, assets, 60, 288, 0);
    construction::MakeEntity<octogirl::Block>(world, assets, 61, 288, 32);
    construction::MakeEntity<octogirl::Block>(world, assets, 62, 288, 64);
    construction::MakeEntity<octogirl::Block>(world, assets, 63, 288, 96);
    construction::MakeEntity<octogirl::Block>(world, assets, 64, 288, 128);
    construction::MakeEntity<octogirl::Block>(world, assets, 65, 288, 160);
    construction::MakeEntity<octogirl::Block>(world, assets, 66, 288, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 67, 256, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 68, 224, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 69, 192, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 70, 160, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 71, 128, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 72, 96, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 73, 64, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 74, 32, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 75, 0, 192);
    construction::MakeEntity<octogirl::Block>(world, assets, 76, -32, 192);

    unsigned short view_index = octogirl::create_views(world, assets, props);
    octogirl::create_camera(world, assets, view_index, 32);
}

void PopulateWorld(World& world, AssetCollection& assets, const AssetProps& props) {
    switch (world.GetId()) {
    case 0:
        World0_00(world, assets, props);
        break;
    case 1:
        World0_01(world, assets, props);
        break;
    case 2:
        World0_02(world, assets, props);
        break;
    case 3:
        World0_03(world, assets, props);
        break;
    case 4:
        World0_04(world, assets, props);
        break;
    case 5:
        World0_05(world, assets, props);
        break;
    case 6:
        World0_06(world, assets, props);
        break;
    case 7:
        World0_07(world, assets, props);
        break;
    case 8:
        std::cout << "~~About to load the world..." << std::endl;
        loading::LoadWorldAssets("testbed1", world, assets, props); // Pass in custom loader function here if necessary
        loading::LoadSpritesheets(assets.Visuals, props);             // Pass in custom loader function here if necessary
        break;
    }
}

void LoadViewModel(BinaryReader& in, unsigned short view_index, unsigned int vm_class_id, World& world, AssetCollection& assets) {

}

} /* END assets */


} /* END eso */