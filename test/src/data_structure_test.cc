#include "tests/data_structure_test.h"

#include <iostream>
#include <string>

#include "model/entity/entity.h"
#include "controller/timing.h"
#include "util/showcase.tpp"
#include "asset/store.tpp"

namespace esotest {

int TestShowcase() {
    //Testing a showcase of references
    eso::ShowcaseRef<int> s(5);
    
    int a = 1;
    int b = 2;
    int c = 3;
    int d = 4;
    int e = 5;
    int f = 6;
    
    s.Push(a);
    s.Push(b);
    s.Push(c);
    s.Push(d);
    s.Push(e);
    s.Push(b);
    s.Push(b);
    s.Push(d);
    s.Push(a);
    s.Push(f);
    std::cout << "showcase: " << s << std::endl;

    return 0;
}

int TestStore() {
    eso::Store<eso::Entity> store(0x0, 100000);
    
    eso::MilliTicker ticker;
    std::string units = " ms";
    for (unsigned int i = 0; i < 1000000; i++) {
        store.Add(std::to_string(i), {i,i,sf::Vector2<double>(i,i), eso::Shape()});
    }
    long duration = ticker();
    std::string key = "999536";
    eso::Entity* entity = store[key];
    
    std::cout << "duration: " << duration << units << std::endl;
    if (entity == nullptr) {
        std::cout << "entity " << key << " is null" << std::endl;
    } else {
        std::cout << "entity " << key << " = " << entity->GetInstanceId() << std::endl;
    }

    return 0;
}

}