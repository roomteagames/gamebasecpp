#ifndef ESO_ENGINE_TESTS_DATASTRUCTURETEST_H_
#define ESO_ENGINE_TESTS_DATASTRUCTURETEST_H_

namespace esotest {

int TestShowcase();
int TestStore();

}

#endif /* ESO_ENGINE_TESTS_DATASTRUCTURETEST_H_ */