#ifndef ESO_ENGINE_TESTS_OCTOGIRL_GAME_H_
#define ESO_ENGINE_TESTS_OCTOGIRL_GAME_H_

//global test variables

#define MOVE_SPEED 5
#define JUMP_SPEED 10

//OPTIMAL: 1920,1080
#define DEFAULT_RESO_WIDTH 1920
#define DEFAULT_RESO_HEIGHT 1080

//OPTIMAL: 24,12
#define CAMERA_WIDTH_BLOCKS 36
#define CAMERA_HEIGHT_BLOCKS 18

//OPTIMAL: 100,60 (although 100 render looks beautiful)
#define UPDATE_FRAMERATE 60
#define SLOW_UPDATE_FRAMERATE 5
#define RENDER_FRAMERATE 60

#define NUM_STANDARD_ENTITIES 11

/** STRESS TEST RESULTS (CPU Usage)
 * 100 Falling Blocks/100 FPS: CPU: 11% (framerate OK), Memory: 38.5 MB
 * 1000 Falling Blocks/100 FPS: CPU: 94% (~20 FPS), Memory: 
 * 10000 Falling Blocks/100 FPS: CPU: 110% (~0 FPS -- hangs), Memory: 
 */
#define IS_STRESS_TEST 0
#define NUM_STRESS_TEST_ENTITIES 500

//RESTITUTION
#define REST_NONE 0
#define REST_LOW 0.1
#define REST_MID 0.3
#define REST_HIGH 0.5
#define REST_MEGA 5

//SHORTCUTS FOR TESTING DEMO TYPE
#define TEST_HIGHSTACK strcmp(octogirl::demo_type, "--highstack") == 0 || strcmp(octogirl::demo_type, "-h") == 0
#define TEST_MULTISTACK strcmp(octogirl::demo_type, "--multistack") == 0 || strcmp(octogirl::demo_type, "-m") == 0
#define TEST_AERIAL strcmp(octogirl::demo_type, "--aerial") == 0 || strcmp(octogirl::demo_type, "-a") == 0
#define TEST_KICK strcmp(octogirl::demo_type, "--kick") == 0 || strcmp(octogirl::demo_type, "-k") == 0
#define TEST_EMPTY strcmp(octogirl::demo_type, "--empty") == 0 || strcmp(octogirl::demo_type, "-e") == 0
#define TEST_NOGRAVITY strcmp(octogirl::demo_type, "--nograv") == 0 || strcmp(octogirl::demo_type, "-n") == 0
#define TEST_FLIPPER strcmp(octogirl::demo_type, "--flipper") == 0 || strcmp(octogirl::demo_type, "-f") == 0
#define TEST_LOAD strcmp(octogirl::demo_type, "--load") == 0 || strcmp(octogirl::demo_type, "-l") == 0

//class_id definitions
#define OctoGirl_ID 1
#define Block_ID 2
#define FallingBlock_ID 3
#define Gem_ID 4
#define Spring_ID 5
#define Flipper_ID 6
#define LongFallingBlock_ID 7

#include <SFML/System.hpp>

#include "test_data_read.h"
#include "view/entity/sprite_view.h"
#include "model/game/game.h"
#include "util/macros.h"


namespace octogirl {

static char* demo_type;

//Material indices
static unsigned short no_friction_idx;
static unsigned short full_friction_idx;
static unsigned short slightly_bouncy_idx;
static unsigned short bouncy_idx;
static unsigned short mega_bouncy_idx;

//---MODELS---
class OctoGirl : public eso::Entity {
private:
    unsigned short anxiety_;
    unsigned short happiness_;
    bool colliding_;

public:
    OctoGirl(unsigned int instance_id, int x, int y, unsigned short anxiety = 0, unsigned short happiness = 0);

    unsigned short GetAnxiety() const { return anxiety_; }
    unsigned short GetHappiness() const { return happiness_; }

    void MakeAnxious(unsigned short amount = 1);
    void MakeHappy(unsigned short amount = 1);
    void MakeCalm(unsigned short amount = 1);
    void MakeSad(unsigned short amount = 1);

    virtual void Update(eso::Game&, eso::World&) override;

    virtual bool OnCollision(eso::Entity* other, eso::Game&) override;
};

class Block : public eso::Entity {
public:
    Block(unsigned int instance_id, int x, int y);
};

class FallingBlock : public eso::Entity {
private:
    sf::Vector2<double> previous_position_;
    unsigned short health_ = 20;

    bool is_invincible_;
    unsigned short invincibility_frames_;

public:
    FallingBlock(unsigned int instance_id, int x, int y, eso::Physics&& physics=eso::Physics());

    const sf::Vector2<double>& GetPreviousPosition() const { return previous_position_; }
    unsigned short GetHealth() const { return health_; }
    bool IsInvincible() const { return is_invincible_; }

    virtual void Update(eso::Game&, eso::World&) override;
    virtual bool OnCollision(eso::Entity* other, eso::Game&) override;
};

class LongFallingBlock : public eso::Entity {
public:
    LongFallingBlock(unsigned int instance_id, int x, int y, eso::Physics&& physics=eso::Physics());
};

class Gem : public eso::Entity {
public:
    Gem(unsigned int instance_id, int x, int y);
};

class Spring : public eso::Entity {
public:
    Spring(unsigned int instance_id, int x, int y);
};

class Flipper : public eso::Entity {
private:
    short orientation_ = 1;
public:
    Flipper(unsigned int instance_id, int x, int y);

    virtual void Update(eso::Game&, eso::World&) override;
};


//---CUSTOM VIEWS---
class BoundingBoxView : public eso::SpriteView {
private:
    sf::Vector2<double> point1;
    sf::Vector2<double> point2;
    sf::Vector2<double> point3;
    sf::Vector2<double> point4;

    sf::Vector2<double> pointMvmt1;
    sf::Vector2<double> pointMvmt2;
    sf::Vector2<double> pointMvmt3;
    sf::Vector2<double> pointMvmt4;

    sf::Vector2<double> vertex1;
    sf::Vector2<double> vertex2;
    sf::Vector2<double> vertex3;
    sf::Vector2<double> vertex4;

    std::string text = "Test";

protected:
    virtual void prepare_sprite(const std::shared_ptr<eso::Entity>&) override;
    virtual void draw_sprite(eso::VisualCollection&, sf::RenderWindow*) override;
public:
    BoundingBoxView(const std::shared_ptr<eso::Entity>& model, const std::string& texture_name, unsigned short spritesheet_index, unsigned short starting_state = 0, bool auto_advance_frames = true, unsigned int frame_length = 5);
    BoundingBoxView(const std::shared_ptr<eso::Entity>& model, const sf::Texture& texture, const eso::Spritesheet* spritesheet, unsigned short starting_state = 0, bool auto_advance_frames = true, unsigned int frame_length = 5);

    void SetBBPoints(const sf::Vector2<double> p1, const sf::Vector2<double> p2, const sf::Vector2<double> p3, const sf::Vector2<double> p4);
    void SetMvmtPoints(const sf::Vector2<double> p1, const sf::Vector2<double> p2, const sf::Vector2<double> p3, const sf::Vector2<double> p4);
    void SetVertices(const sf::Vector2<double> p1, const sf::Vector2<double> p2, const sf::Vector2<double> p3, const sf::Vector2<double> p4);
    void SetText(const std::string& str) { text = str; }
};

//Demo control functions
unsigned short create_views(eso::World& world, unsigned short view_index);
std::unique_ptr<eso::ViewModel> create_view(const std::shared_ptr<eso::Entity>&, const std::string&, const std::string&, eso::AssetCollection&, const eso::AssetProps&, eso::Spritesheet&&, bool);
void create_camera(eso::World& world, eso::AssetCollection& assets, unsigned short view_index, unsigned short upb);

}

#endif /* ESO_ENGINE_TESTS_OCTOGIRL_GAME_H_ */