#ifndef TEST_READ_DATA_H
#define TEST_READ_DATA_H

#include <iostream>

namespace test_data_read {
    int run();
}

#endif /* TEST_READ_DATA_H */