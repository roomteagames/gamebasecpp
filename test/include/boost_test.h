#ifndef ESO_ENGINE_TESTS_BOOSTTEST_H_
#define ESO_ENGINE_TESTS_BOOSTTEST_H_

#include <iostream>
#include <boost/preprocessor.hpp>

namespace esotest {

#define SET_ARRAY_ELEMENT(r, arr, i, element) arr[i] = element;
#define POPULATE_ARRAY(arr, ...) BOOST_PP_SEQ_FOR_EACH_I(SET_ARRAY_ELEMENT, arr, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

int TestMacros();

}

#endif /* ESO_ENGINE_TESTS_BOOSTTEST_H_ */