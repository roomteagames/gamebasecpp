#ifndef ESO_ENGINE_TESTS_INPUTTEST_H_
#define ESO_ENGINE_TESTS_INPUTTEST_H_

namespace esotest {

int TestInputModel();
int TestUniqueInputs();

}

#endif /* ESO_ENGINE_TESTS_INPUTTEST_H_ */