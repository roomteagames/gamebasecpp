#ifndef ESO_ENGINE_TESTS_LOOPTEST_H_
#define ESO_ENGINE_TESTS_LOOPTEST_H_

#include <string>
#include <vector>

#include <SFML/Graphics.hpp>

#include "model/entity/timeline.tpp"
#include "view/entity/entity_view.h"

namespace esotest {

// class EntityChildA : public eso::Entity {
// private:
//     bool will_destroy_ = false;

// public:
//     EntityChildA(unsigned int instance_id, int x, int y);

//     void Update(eso::GameState&) override;
//     void Destroy() { will_destroy_ = true; }
// };

// class EntityChildB : public eso::Entity {
// private:
//     bool is_angry_;
// public:
//     EntityChildB(unsigned int instance_id, int x, int y, bool is_angry);

//     bool IsAngry() const { return is_angry_; }

//     void Update(eso::GameState&) override;
// };

// class ViewChild1 : public eso::EntityView {
// private:
//     const std::string& color_;
// public:
//     ViewChild1(const eso::Entity* model, const std::string& color);

//     const std::string& GetColor() const { return color_; }

//     void Render() override;
// };

// class ViewChild2 : public eso::EntityView {
// private:
//     unsigned int width_, height_;
//     sf::Color color_;
// public:
//     ViewChild2(const eso::Entity* model, unsigned int width, unsigned int height, unsigned char red, unsigned char green, unsigned char blue);

//     unsigned int GetWidth() const { return width_; }
//     unsigned int GetHeight() const { return height_; }
//     const sf::Color& GetColor() const { return color_; }

//     void Render() override;
// };

class TimelineA : public eso::Timeline<6> {
public:
    TimelineA(unsigned int instance_id);

    void Update(eso::Game&) override;
};


int TestLoops();

int TestTimeline();

int TestStartup();

}

#endif /* ESO_ENGINE_TESTS_LOOPTEST_H_ */