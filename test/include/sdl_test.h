#ifndef ESO_ENGINE_TESTS_SDLTEST_H_
#define ESO_ENGINE_TESTS_SDLTEST_H_

#include <iostream>

#include "model/entity/entity.h"

namespace esotest {

int TestSdl();
int TestSdlGpu();
int TestSfml();

}

#endif /* ESO_ENGINE_TESTS_SDLTEST_H_ */