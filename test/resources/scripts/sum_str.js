function sumStr(str) {
    var sum = 0;
    for (var i = 0; i < str.length; i++) {
        sum += parseInt(str[i]);
    }
    return sum;
}

var str = process.argv[2];
var sum = sumStr(str);
console.log(`Sum: ${sum}`);