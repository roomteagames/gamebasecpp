find_package(SFML 2.5 COMPONENTS graphics audio system REQUIRED)

set(TEST_HEADERS
    include/octogirl_test_game.h
)

set(TEST_SOURCES
    src/octogirl_test_game.cc
    src/asset/generated/entity_routing.cc
)

add_executable(test ${TEST_HEADERS} ${TEST_SOURCES})
target_include_directories(
    test
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(
    test
    PRIVATE
        eso
        sfml-graphics
)

#Create link for resources
set (RES_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/resources)
set (RES_DEST ${CMAKE_CURRENT_BINARY_DIR}/resources)

add_custom_command(
 TARGET test POST_BUILD
 COMMAND ${CMAKE_COMMAND} -E create_symlink ${RES_SOURCE} ${RES_DEST} 
 DEPENDS ${RES_DEST}
 COMMENT "symbolic link resources folder from ${RES_SOURCE} => ${RES_DEST}"
)