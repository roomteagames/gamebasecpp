#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#Performs Mac/Linux Installation for Esoteric Engine

#FAIL if g++ not installed
g++ --version 2>&1 /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: g++ is NOT installed. Cannot build Esoteric Engine. Please install g++ first."
  exit 1
fi

#FAIL if cmake not installed
cmake --version 2>&1 /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: cmake is NOT installed. Cannot build Esoteric Engine. Please install cmake first."
  exit 1
fi

#Install armadillo if not already present
ARMADILLO_NAME=armadillo-9.880.1
ARMADILLO_LIB_NAME=armadillo-9.88.1  #for some reason make install removes 0 from version number
ARMADILLO_ZIP=${ARMADILLO_NAME}.tar.xz

#TODO: dylib may only be a mac extension!
if [ -z /usr/local/lib/lib${ARMADILLO_LIB_NAME}.dylib ] || [ -z /usr/local/include/armadillo ]; then
  [ -z /usr/local/lib/lib${ARMADILLO_NAME}.dylib ] && echo 'INFO: lib${ARMADILLO_LIB_NAME}.dylib does not exist in /usr/local/lib. Installing...'
  [ -z /usr/local/include/armadillo ] && echo 'INFO: Include directory `armadillo` does not exist in /usr/local/include. Installing...'

  cd DIR
  [ -d temp ] && rm -rf temp
  mkdir temp
  curl -L https://sourceforge.net/projects/arma/files/${ARMADILLO_ZIP}/download > temp/${ARMADILLO_ZIP}
  tar -xvf temp/${ARMADILLO_ZIP} -C temp

  cd temp/${ARMADILLO_NAME}
  cmake .
  make
  make install  #copies binaries to /usr/local/lib; copies header files to /usr/local/include

  cd -
  rm -rf temp

  echo "INFO: ${ARMADILLO_NAME} installation successful!"
else
  echo "INFO: ${ARMADILLO_NAME} already exists on this system."
fi

echo "INFO: Installation COMPLETE!"