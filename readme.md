This is the base for basic SFML I/O, file reading, and memory management in a C++ game.
Also includes built-in constraint-based physics.

--INSTALLATION NOTES--

-Required dependencies:
C++ (17)
Cmake (3.5)
SFML (2.5)
Armadillo (9.88)

-Mac Installation TODO: automate this
check whether C++ 17 compilation is possible (G++, CLANG, etc)
    if not, download and install Xcode tools
check whether CMake is installed
    if not, download mac binary and following install instructions
check whether SFML library exists on machine
    if not, download to /Library/Frameworks
check whether Armadillo library exists on machine
    if not, download and extract tar.xz
    $(
        cd $ARMADILLO_DIR
        cmake .
        make
        sudo make install
    )
    make install command -- includes saved in /usr/local/include; libraries saved to /usr/local/lib

-Linux Installation
...
CMake can be acquired through dnf, yum, apt, aptitude
Install OpenBLAS for better performance
...

-Windows Installation


--SYSTEM VARIABLES--



STATIC MEMORY SPECIFICATION
This library contains several compile-time preprocessor variables that assign sizes to static arrays

    VARIABLE                                    DEFAULT_VALUE   DESCRIPTION
    ESO_VAR_SHAPE_COUNT_MAX.....................50              Max number of shape presets that can be stored in memory
    ESO_VAR_FRICTION_SERIES_COUNT_MAX...........10              Max number of friction series presets that can be stored in memory
    ESO_VAR_RESITUTION_SERIES_COUNT_MAX.........10              Max number of restitution series presets that can be stored in memory
    ESO_VAR_EDGE_FUNCTION_COUNT_MAX.............5               Max number of edge functions presets that can be stored in memory
    ESO_VAR_TEXTURE_QUEUE_SIZE..................1000            Max number of textures that can be stored in memory before being purged
    ESO_VAR_WORLD_LOADING_THREAD_MAX............5               Max number of loading threads that can exist simultaneously
    ESO_VAR_PRIMARY_COLLISION_COUNT_MAX.........5               Max number of primary collisions that can be stored for an entity
    ESO_VAR_SECONDARY_COLLISION_COUNT_MAX.......5               Max number of secondary collisions that can be stored for an entity

